// OBJ main

#include "my_stm32.h"

void main(void)
{
  PushAppAddress(BASE_APPADDR);
  AssignAppAddress();
  AssignVTORLength();
  uint32_t lStartAddr = GetStartAddr();
  PushAppAddress(lStartAddr);
  JampToApp(lStartAddr);
}
