// ����������� � �������� ������
// Runtime Control
// � Olgvel, 2014-2016
//
#ifndef __MY_RTCTL_H
#define __MY_RTCTL_H

#include "my_stm32.h"
#include "r_vap3.h"

#define INSRCCMDBUFSIZE     768
#define INBINCMDBUFSIZE     1024+64 //576

typedef struct {
  const pvoid             *sRCAppCtxTable;
  const char              *sRCTextCmdNames;
  const TCHANDLER_TypeDef *sRCTextCmdHandlers;
  const char              *sRCBinCmdNames;
  const BCHANDLER_TypeDef *sRCBinCmdHandlers;
  ADDRMESSAGE_CALLBACK sRCOnBCTextAnswer;
  //
} RTCTL_DESCR_TypeDef;

typedef struct {
  uint32_t sREDID;
  uint16_t sRELID;
  uint16_t sREPrio;
  uint32_t sREGateAddr;
  uint16_t sREGatePort;
  uint16_t sREGateLID;
  //
} Route_Elem_TypeDef;

#ifdef __cplusplus
extern "C" {
#endif

RTCTL_DESCR_TypeDef* RTCtl_Init(RTCTL_DESCR_TypeDef *aDescr);
void RTCtl_Process(RTCTL_DESCR_TypeDef *aDescr);
void RTCtl_ExecCommands(RTCTL_DESCR_TypeDef *aDescr,char *aMessage,int aLength,void *aSource);
void RTCtl_ExecTextCommands(RTCTL_DESCR_TypeDef *aDescr,char *aMessage,int aLength);
void RTCtl_OutTextCommands(RTCTL_DESCR_TypeDef *aDescr,uint8_t *aCmdIdx,int aCount);
//
bool RTCtl_PostBCBlockEx(RTCTL_DESCR_TypeDef *aDescr,
  uint32_t aSender,uint32_t aDest,char *aBCBlock,int aLength);
/*
bool BCGetParPtr(BINCMD_PAR_TypeDef *aBCP,char **aParPtr,int *aParLen);
bool BCGetStrPar(BINCMD_PAR_TypeDef *aBCP,char *aStr,int *aParLen);
bool BCGetUIntPar(BINCMD_PAR_TypeDef *aBCP,uint32_t *aValue);
bool BCGetIntPar(BINCMD_PAR_TypeDef *aBCP,int *aValue);
bool BCPutParPtr(BINCMD_PAR_TypeDef *aBCP,char **aParPtr,int aParLen);
char* BCPutStrPar(BINCMD_PAR_TypeDef *aBCP,char *aStr,int aParLen);
bool BCPutUIntPar(BINCMD_PAR_TypeDef *aBCP,uint32_t aValue);
*/
void TCOutMessage(TXTCMD_PAR_TypeDef *aPar,const char *aMessage,int aLength);
void TCOutText(TXTCMD_PAR_TypeDef *aPar,const char *aText);
void TCOutUInt(TXTCMD_PAR_TypeDef *aPar,const char *aBeforeText,uint32_t aValue,const char *aAfterText);
void TCOutTextIP(TXTCMD_PAR_TypeDef *aPar,void *aBeforeText,uint32_t aValue,void *aAfterText);

void LogOkError(bool aState);

#ifdef __cplusplus
}
#endif

#endif /* __MY_RTCTL_H */

