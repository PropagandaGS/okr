// ����������� � �������� ��������� VAP2
// � Olgvel, 2011-2013
//
#ifndef __MY_VAP2_H
#define __MY_VAP2_H

#include "my_stm32.h"
//

#define VAPBUFSIZE        256
#define VAPTUNNELBUFSIZE  208

#define BROADCASTDEVICE   0x0F0F0F0F
#define CPROCDATA         0x8000
#define PINGDATA          0x4000

#define VAPFRAMESIZE      sizeof(VAP_Frame_TypeDef)

/* Exported types ------------------------------------------------------------*/

typedef struct {
  uint32_t sVFPreamb;
  uint16_t sVFLenFlags;
  uint16_t sVFNumFlags;
  uint32_t sVFPreaMask;
  char     sVFData[];
  //
} VAP_Frame_TypeDef;

typedef struct {
  uint16_t sVBLength;
  uint16_t sVBCommand;
  uint32_t sVBDeviceID;
  uint32_t sVBTimeStamp;
  //
} VAP_Block_TypeDef;

typedef enum {
  VSK_UNKNOWN, VSK_MASTER, VSK_SLAVE, VSK_AUTO
  //
} VAP_SENDER_TypeDef;


/*
typedef struct {
  MY_USART_TypeDef* sVDUSART;
  uint32_t sVDSelfID;
  uint16_t sVDCurNumber;
  uint16_t sVDRcvCRC;
  UINT32_U sVDRcvPreamb;
  UINT32_U sVDSndPreamb;
  union {
    VAP_Frame_TypeDef sVDRcvFrame;
    uint8_t sVDRcvFBytes[sizeof(VAP_Frame_TypeDef)];
  };
  char *sVDTempBuffer;
  uint16_t sVDTempBufSize;
  uint16_t sVDTempBufCount;
  //
  char *sVDTunnelBuffer;
  uint16_t sVDTunnelBufSize;
  uint16_t sVDTunnelBufCount;
  //
  void *sVDExtData;
  uint32_t sVDLastReadTicks;
  //
  MESSAGE_CALLBACK sVDOnTunnelRead;
  UINT32_U sVDViewState;
  uint16_t sVDExtDataSize;
  uint16_t sVDBegPos;
  uint16_t sVDResult;
  int16_t  sVDFIndex;
  bool     sVDIsMaster;
  bool     sVDNewState;
  //
} VAP_Descr_TypeDef;
*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
//==============================================================================

void VAP2_BuildFrame(VAP_SENDER_TypeDef aSender,uint32_t aDevID,uint16_t aFrameNum,
  uint16_t aFlags,void *aData,uint16_t aDataLen,void *aFrame,uint16_t *aFrameLen);

bool VAP2_ParseFrame(void *aFrame,uint16_t aFrameLen,VAP_SENDER_TypeDef aSenderFlt,
  uint32_t aDevID, VAP_SENDER_TypeDef *aSender,uint16_t *aFrameNum,uint16_t *aFlags,
  void *aData,uint16_t *aDataLen,int *aRetPos);

char* GetNextCmdPtr(int aParLen,char *aParams,int *aLength);
void AddNextCmdParam(void *aPar,int aParLen,char *aParams,int *aLength);
bool AddAnswerState(bool aState,char *aParams,int *aLength);
bool FindNextCmdParam(char *aParams,int aLength,int *aParPos,int *aParLen);
bool FindNextCmdIntPar(char *aParams,int aLength,int *aParPos,int *aParLen,uint32_t *aPar);
bool FindNextCmdIntParDef(char *aParams,int aLength,int *aParPos,int *aParLen,uint32_t *aPar,uint32_t aDef);

/*
VAP_Descr_TypeDef* Vap_Init(VAP_Descr_TypeDef *aVapDescr,MY_USART_TypeDef *aMyUSART,bool aUseTunnel);
void Vap_Process(VAP_Descr_TypeDef *aVapDescr);
bool Vap_GetTunnelBuffer(VAP_Descr_TypeDef *aVapDescr,pvoid *aPBuffer,int *aBufSize);
void Vap_SetTunnelBufferCount(VAP_Descr_TypeDef *aVapDescr,int aCount);
void Vap_PostTunnelData(VAP_Descr_TypeDef *aVapDescr,void *aBuffer,int aSize);
*/
#endif /* __MY_VAP2_H */

