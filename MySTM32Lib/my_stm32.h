// �������� ���������� ������� ��� STM32
// � Olgvel, 2011-2018
//

#ifndef __MY_STM32_H
#define __MY_STM32_H

#include "r_vap3.h"

#ifdef __STM32F0XX_CONF_H
#include "my_stm32f0.h"
#endif /* __STM32F0XX_CONF_H */

#ifdef __STM32F10x_CONF_H
#include "my_stm32f1.h"
#endif /* __STM32F10x_CONF_H */

#ifdef __STM32F2xx_CONF_H
#include "my_stm32f2.h"
#endif /* __STM32F2xx_CONF_H */

#ifdef __STM32F4xx_CONF_H
#include "my_stm32f4.h"
#endif /* __STM32F2xx_CONF_H */

#define SYSTICK_HANDLER void SysTick_Handler(void){ TICKS++;}
#define IRQ_HANDLER(SYSNAME, HNDNAME, PARNAME) void SYSNAME(void) { HNDNAME(PARNAME);}
#define CLASS_IRQ_HANDLER(SYSNAME, HNDNAME) extern "C" void SYSNAME(void) { HNDNAME();}
//
#define IRQ_DBLHANDLER(SYSNAME, HNDNAME, PARNAMEA, PARNAMEB) void SYSNAME(void) { HNDNAME(PARNAMEA); HNDNAME(PARNAMEB);}
#define USART_IRQ_HANDLER(SYSNAME, PARNAME) IRQ_HANDLER(SYSNAME, MyUSART_IRQHandler, PARNAME)
#define USART_DMA_TX_IRQ_HANDLER(SYSNAME, PARNAME) IRQ_HANDLER(SYSNAME, MyUSART_DMA_TX_IRQHandler, PARNAME)
#define SPI_IRQ_HANDLER(SYSNAME, PARNAME) IRQ_HANDLER(SYSNAME, MySPI_IRQHandler, PARNAME)
#define SPI_DMA_TX_IRQ_HANDLER(SYSNAME, PARNAME) IRQ_HANDLER(SYSNAME, MySPI_DMA_TX_IRQHandler, PARNAME)
#define SPI_DMA_RX_IRQ_HANDLER(SYSNAME, PARNAME) IRQ_HANDLER(SYSNAME, MySPI_DMA_RX_IRQHandler, PARNAME)

#define UART_IRQ_HANDLER(SYSNAME, PARNAME) IRQ_HANDLER(SYSNAME, MyUART_IRQHandler, PARNAME)
#define UART_DMA_TX_IRQ_HANDLER(SYSNAME, PARNAME) IRQ_HANDLER(SYSNAME, MyUART_DMA_TX_IRQHandler, PARNAME)
#define UART_DMA_TX_IRQ_DBLHANDLER(SYSNAME, PARNAMEA, PARNAMEB) IRQ_DBLHANDLER(SYSNAME, MyUART_DMA_TX_IRQHandler, PARNAMEA, PARNAMEB)

#define  USE_DMA_TX      ((uint8_t)0x01)
#define  IN_DMA_TX       ((uint8_t)0x02)
#define  IN_IRQ_TX       ((uint8_t)0x04)
#define  TX_PIN_SET      ((uint8_t)0x08)

#define  IN_ANY_TX       (IN_IRQ_TX | IN_DMA_TX)

#define  BASE_RAMADDR    0x20000000
#define  APPADDRSIGN     0x51372604

#define  BASE_APPADDR    0x08000000
#define  SYSBLOCKSIGN    0x30574648
#define  CFGBLOCKSIGN    0xB2A7658C
#define  APPENBSIGN      0x3A75
#define  FLASH32K        0x0020
#define  FLASH64K        0x0040
#define  FLASH128K       0x0080
#define  FLASH256K       0x0100
#define  FLASH512K       0x0200
#define  FLASH1024K      0x0400

#define  SYSBLOCK_BASE   (BASE_APPADDR+VTORLENGTH)
//#define  SYSBLOCKLENGTH  sizeof(SYSBLOCK_TypeDef)
#define  SYSBLOCK        ((SYSBLOCK_TypeDef *) SYSBLOCK_BASE)

#define WRITE_FLAG       0x8000
#define EXTBUF_FLAG      0x4000
#define REV_FLAG         0x2000
#define SF_FLAG          0x1000
#define BUFSIZE_MASK     0x0FFF

#define NOIDX            -1

/* Exported types ------------------------------------------------------------*/

typedef struct {
  __IO uint16_t b15_0;
  __IO uint16_t b16_31;
  __IO uint32_t b32_63;
  __IO uint32_t b64_95;
} UNIQUE_TypeDef;


typedef struct {
  uint16_t mBaseSegmCode;
  uint8_t mBegPage;
  uint8_t mPageCount;
} MEM_TypeDef;

typedef uint8_t HWNAME_ARRAY[12];
                  // 0x0184
typedef struct {  // 48465730 00000108 4400 2800 0000 A800 C87B 387E 687739303163730000000000 00012601 FE9E4D2D
                  // 2CEF300F FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF 00400008
                  // 48465730 00400008 4400 2800 0000 A800 687D D87F 687739303163730000000000 20012601 7296522D62F92BFEFFFFFFFF
    uint32_t hsbSignature;
    uint32_t hsbBaseAddr;
    uint16_t hsbSysBlockSize;
    uint16_t hsbSysBlockCRCSz;
    uint16_t hsbVerInfoSize;
    uint16_t hsbLinkTableSize;
    uint16_t hsbProgSize;
    uint16_t hsbFullSize;
    HWNAME_ARRAY hsbConfig;
    uint32_t hsbVersion;
    uint32_t hsbVersionDT;
    // ==================
    uint32_t hsbFullCRC;
    uint32_t hsbDevID;
    uint32_t hsbLoadFlags;
    uint32_t hsbMirrorSign;
    uint32_t hsbStartLoadDT;
    uint32_t hsbLoadedDT;
    uint32_t hsbNextAddr;
//#ifdef __STM32F10x_CONF_H
    uint16_t hsbAppEnbSign;
    uint16_t hsbAppIndex;
    //
    MEM_TypeDef hsbCfgA, hsbCfgB;

//#endif /* __STM32F10x_CONF_H */
    //hsbVerInfo: record end;
} SYSBLOCK_TypeDef;

typedef struct {
  MY_PESTATE_TypeDef sOUPS;
  const PIN_HWCFG_TypeDef *sHWCFG;
} MY_OUT_TypeDef, *MY_OUT_PtrDef;

typedef void(* PINCHANGESTATE_CALLBACK)(void *aContext,bool aNewState);

typedef struct {
  const USART_HWCFG_TypeDef *sHWCFG;
  __IO int16_t  sSendingPos, sReceivePos;
  int16_t  sSendPos, sWritePos, sReadPos, sCheckPos;
  __IO uint8_t sFlags, sRsv1, sRsv2, sRsv3;
  uint32_t sReceiveTicks, sSendTicks;
  //
  char *sInBuffer, *sOutBuffer;
  uint16_t  sInBufSize, sOutBufSize;
} MY_USART_TypeDef, *MY_USART_PtrDef;

typedef void(* SUBFRAME_CALLBACK) (char *aSrc,char *aDest,uint32_t *aSFData);
typedef void(* FRAMECOPY_CALLBACK)(void *aOwner,char *aSrc,char *aDest,uint16_t aCount);
typedef void(* FRAMEFIX_CALLBACK)(void *aOwner,char *aSrc,uint16_t aCount);

typedef struct {
  const SPI_HWCFG_TypeDef *sHWCFG;
  void *sOwnerDescr;
  __IO uint8_t sInTrans, sInRx;
  __IO uint16_t  sBufCur, sBufSubCur;
  uint16_t sBufPos;
  uint32_t sStartTicks, sStopTicks;
  char *sBuffer;
  uint16_t  sBufSize, sBufOffs;
  SUBFRAME_CALLBACK sOnPrepSubFrame;
  SUBFRAME_CALLBACK sOnReadSubFrame;
  FRAMECOPY_CALLBACK sOnCopyAfterRead;
  FRAMEFIX_CALLBACK sOnAfterExtTrans;
} MY_SPI_TypeDef;

typedef struct {
  LINK_HDR
  //
  const USART_HWCFG_TypeDef *sHWCFG;
  __IO uint8_t sUFlags, sRsv1, sRsv2, sRsv3;
  uint32_t sBaudRate;
  //
} MY_UART_TypeDef;

/* Exported functions ------------------------------------------------------- */
#ifdef __cplusplus
extern "C" {
#endif

void IncStartCounter(bool aIsLowPart);
int GetStartCounter(bool aIsLowPart);

void GetCfgParams(uint32_t *aAddress, uint32_t *aLength);

void MySys_Init(void);

void PIN_Config(const PIN_HWCFG_TypeDef *aConfig);
bool PIN_Get(const PIN_HWCFG_TypeDef *aConfig);
void PIN_Set(const PIN_HWCFG_TypeDef *aConfig, unsigned int aState);
void PIN_CondSet(const PIN_HWCFG_TypeDef *aConfig,bool aNewState,bool *aLastState);
void IRQ_Init(uint8_t aIRQChan);
void IRQ_InitEx(uint8_t aIRQChan, uint8_t aPrePri, uint8_t aSubPri);

MY_OUT_TypeDef* MyOUT_Init(MY_OUT_TypeDef *aMyOUT,const PIN_HWCFG_TypeDef *aConfig);
//
//------------------------------------------------------------------------------
MY_USART_TypeDef* MyUSART_Init(MY_USART_TypeDef *aMyUSART,const USART_HWCFG_TypeDef *aConfig);
MY_USART_TypeDef* MyUSART_InitEx(MY_USART_TypeDef *aMyUSART,const USART_HWCFG_TypeDef *aConfig,uint32_t aBaudRate);
int MyUSART_Write(MY_USART_TypeDef *aMyUSART,void *aBuffer,int aCount);
int MyUSART_WriteW(MY_USART_TypeDef *aMyUSART,void *aBuffer,int aCount);
int MyUSART_WriteEx(MY_USART_TypeDef *aMyUSART,void *aBuffer,int aCount,bool aDataSend);
int MyUSART_WriteText(MY_USART_TypeDef *aMyUSART,void *aBuffer);
int MyUSART_WriteTextW(MY_USART_TypeDef *aMyUSART,void *aBuffer);
//
bool MyUSART_StartSend(MY_USART_TypeDef *aMyUSART);
void MyUSART_IRQHandler(MY_USART_TypeDef *aMyUSART);
void MyUSART_DMA_TX_IRQHandler(MY_USART_TypeDef *aMyUSART);
void MyUSART_ReturnDirByTC(MY_USART_TypeDef *aMyUSART);
void MyUSART_UpdateReceivePos(MY_USART_TypeDef *aMyUSART);
//
//------------------------------------------------------------------------------
MY_UART_TypeDef* MyUART_Init(MY_UART_TypeDef *aDescr,const USART_HWCFG_TypeDef *aConfig);
MY_UART_TypeDef* MyUART_InitEx(MY_UART_TypeDef *aDescr,const USART_HWCFG_TypeDef *aConfig,uint32_t aBaudRate);
int MyUART_WriteEx(MY_UART_TypeDef *aDescr,char *aBuffer,int aCount,bool aDataSend);
bool MyUART_Read(MY_UART_TypeDef *aDescr,char *aBuffer,int aBufSize,
  unsigned int aWaitTime, int *aCount);
//
void MyUART_Initialize(MY_UART_TypeDef *aDescr);
bool MyUART_StartSend(MY_UART_TypeDef *aDescr);
void MyUART_IRQHandler(MY_UART_TypeDef *aDescr);
void MyUART_DMA_TX_IRQHandler(MY_UART_TypeDef *aDescr);
void MyUART_ReturnDirByTC(MY_UART_TypeDef *aDescr);
void MyUART_UpdateReceivePos(MY_UART_TypeDef *aDescr);
//
//--------------------------------------------------------------------------------
MY_SPI_TypeDef* MySPI_Init(MY_SPI_TypeDef *aSPI,const SPI_HWCFG_TypeDef *aConfig);
uint16_t MySPI_DataOI(MY_SPI_TypeDef *aSPI,uint16_t aData);
uint16_t MySPI_DataIn(MY_SPI_TypeDef *aSPI);

//
bool MySPI_Prepare(MY_SPI_TypeDef *aSPI,uint16_t aBuffOffs);
bool MySPI_AddFrame(MY_SPI_TypeDef *aSPI,void *aBufPtr,uint16_t aCount,
  uint16_t aFlags,uint32_t aSFData,char **aFramePtr);
bool MySPI_Start(MY_SPI_TypeDef *aSPI);
void MySPI_DMA_RX_IRQHandler(MY_SPI_TypeDef *aSPI);

bool FLASH_Clear(uint32_t aIndex,uint32_t aCount);
bool FLASH_Write(uint32_t aAddr,void *aBuffer,int aCount);
bool FLASH_Write2(uint32_t aAddr,void *aBuffer,int aCount);
bool FLASH_Verify(uint32_t aAddr,void *aBuffer,int aCount);

bool InitNamedData(uint32_t aStartAddr,uint32_t aMaxSize);
bool DeleteNamedData(char *aName,int aNameLen);
bool DeleteNamedIdxData(char *aName,int aNameLen,int aNameIdx);
bool WriteNamedData(char *aName,int aNameLen,void *aSource,int aCount);
bool WriteNamedIdxData(char *aName,int aNameLen,int aNameIdx,void *aData,int aDataLen);
bool FindNamedData(char *aName,int aNameLen,char **aData,int *aCount);
bool FindNamedIdxData(char *aName,int aNameLen,int aNameIdx,char **aData,int *aCount);
bool FindNextNamedDataAndIdx(uint32_t *aAddr,char *aName,int aNameLen,char **aData,int *aCount,int *aNameIdx);
//
uint32_t ReadUIntVar(char *aName,uint32_t aDefValue);
bool WriteUIntVar(char *aName,uint32_t aValue,uint32_t aDefValue);
char* ReadNamedStr(char *aName,char *aStr,char *aDefStr);

bool IsValidAppAddr(uint32_t aAddr);
uint32_t DefaultAppAddr(bool aIsHiAddr);
uint32_t AppAddress(void);
uint32_t GetVTORLength(uint32_t aAddr);
uint32_t GetMaxCodeDataSize(uint32_t aAddr);
uint32_t VTORLength(void);
void PushAppAddress(uint32_t aAddr);
uint32_t PopAppAddress(void);
void AssignAppVTOR(uint32_t aAddr);
void AssignAppAddress(void);
void AssignVTORLength(uint32_t aDefVTORLength);
bool AssignCfgBlock(uint32_t aAddr);
bool ReadCfgBlock(uint32_t aAddr);
bool GetSysBlock(uint32_t aAddr,SYSBLOCK_TypeDef **aSysBlock);
bool FLASH_SetReadProtection(void);
//
bool VTOR_Setup(uint32_t aAddr);
void JampToApp(uint32_t aAddr);
bool FLASH_Test(uint32_t aAddr,char *aBuf,int aCount);
//
void RTC_Initialize(void);
uint32_t DeviceID(void);
uint32_t CRC32(void *aBuffer,int aCount);
void BkpMemClear(void);
bool App_Change(uint32_t aAddr);
uint32_t GetStartAddr(void);
void Delay(uint32_t aDelay);
void PostLogTextDly(void *aText,uint32_t aDelay);
void GetTimeStamp(UINT64_U *aTimeStamp);
//
uint32_t random32(void);

#ifdef __cplusplus
}
#endif
//
#ifdef __STM32F2xx_DAC_H
void DAC_Config(const DAC_HWCFG_TypeDef *aConfig);
void DAC_TriangleStart(const DAC_HWCFG_TypeDef *aConfig);
void DAC_TriangleStop(const DAC_HWCFG_TypeDef *aConfig);
void DAC_Start(const DAC_HWCFG_TypeDef *aConfig,void *aBuffer,uint32_t aSize);
void DAC_Stop(const DAC_HWCFG_TypeDef *aConfig);
#endif /* __STM32F2xx_DAC_H */

#endif /* __MY_STM32_H */

