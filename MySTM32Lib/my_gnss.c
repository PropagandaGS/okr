// ���������� ���������� � gps/glonass �������
// � Olgvel, 2018
//
#include "my_gnss.h"

#define GNSS_READ_WAIT_TIME  20
#define GNSS_BUFFER_SIZE     256

typedef enum {
  GNF_LOGENB
  //
} GNSS_FLAG_TypeDef;

//==============================================================================

GNSS_DESCR_TypeDef* GNSS_Init(GNSS_DESCR_TypeDef *aDescr,MY_UART_TypeDef *aMyUART,
  const PIN_HWCFG_TypeDef *aGNRST)
{
  if (aDescr == NULL)
    aDescr = FixMemAlloc( sizeof(GNSS_DESCR_TypeDef) );
  //
  MemClear(aDescr, sizeof(GNSS_DESCR_TypeDef));
  aDescr->sGNUART = aMyUART;
  //
  PIN_Config(aGNRST);
  PIN_Set(aGNRST, TRUE);
  //
  CSET(aDescr->sGNFlags, GNF_LOGENB, FALSE); //ReadUIntVar(WMODE, DEFWMODE));
  //CSET(aDescr->sESFlags, ESF_PWRCTL, aPowerCtl);
  //    
  return aDescr;
}

void GNSS_Process(GNSS_DESCR_TypeDef *aDescr)
{
  char lRxStr[GNSS_BUFFER_SIZE+4];
  int lRxCount;
  //
  if (MyCBuf_Read(aDescr->sGNUART->sLRx, lRxStr, GNSS_BUFFER_SIZE,
        GNSS_READ_WAIT_TIME, &lRxCount)) {
    //HandleAnswer(aDescr, lRxStr, lReadCount);
    if TEST_OK(aDescr->sGNFlags, GNF_LOGENB)      
      PostLogMsg("GNSS >> ", lRxStr, lRxCount, S_CRLF);      
  }
}

void AddNmeaCSum(char *aStr)
{
  uint8_t lSC=0;
  while (*aStr != 0) 
    lSC ^= *aStr++;
  *aStr++ = '*';
  Int2HexZ(lSC, aStr, 2);
}  

void _TCH_GNConfig(GNSS_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  char lStr[64];
  //
  aPar->sTCPResult = (aPar->sTCPCount == 0) || (aPar->sTCPCount > 0) && (aPar->sTCPVals[0] <= 48);
  if (aPar->sTCPResult) {
    switch (aPar->sTCPVals[0]) {
      case  0: CSET(aDescr->sGNFlags, GNF_LOGENB, FALSE); break;
      case  1: CSET(aDescr->sGNFlags, GNF_LOGENB, TRUE); break;
      case 10: AssignStr("$PSIMIPR,T", lStr); break;
      case 11: AssignStr("$PSIMIPR,W,115200", lStr); break;
    }
    if (aPar->sTCPVals[0] >= 10) {
      AddNmeaCSum(&lStr[1]);
      MyCBuf_WriteTextW(aDescr->sGNUART->sLTx, lStr);
      MyCBuf_WriteText(aDescr->sGNUART->sLTx, S_CRLF);
      PostLogText(lStr);
    }  
  }  
}
