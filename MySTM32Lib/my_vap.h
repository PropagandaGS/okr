// ����������� � �������� ��������� VAP
// � Olgvel, 2011-2013
//
#include "my_stm32.h"
//

#define VAPBUFSIZE        256
#define VAPTUNNELBUFSIZE  208

/* Exported types ------------------------------------------------------------*/

typedef struct {
  uint16_t sCBAddPos;
  uint16_t sCBCheckPos;
  uint16_t sCBDelPos;
  uint16_t sCBSize;
  CHAR_ARRAY* sCBBuffer;
  //
} MY_CBUF_TypeDef;

typedef struct {
  uint32_t sVFPreamb;
  uint16_t sVFCRC;
  uint16_t sVFLength;
  uint16_t sVFNumber;
  uint16_t sVFLengthInv;
  uint32_t sVFDeviceID;
  //
} VAP_Frame_TypeDef;

typedef struct {
  uint16_t sVBLength;
  uint16_t sVBCommand;
  uint32_t sVBDeviceID;
  uint32_t sVBTimeStamp;
  //
} VAP_Block_TypeDef;

typedef struct {
  MY_USART_TypeDef* sVDUSART;
  uint32_t sVDSelfID;
  uint16_t sVDCurNumber;
  uint16_t sVDRcvCRC;
  UINT32_U sVDRcvPreamb;
  UINT32_U sVDSndPreamb;
  union {
    VAP_Frame_TypeDef sVDRcvFrame;
    uint8_t sVDRcvFBytes[sizeof(VAP_Frame_TypeDef)];
  };
  char *sVDTempBuffer;
  uint16_t sVDTempBufSize;
  uint16_t sVDTempBufCount;
  //
  char *sVDTunnelBuffer;
  uint16_t sVDTunnelBufSize;
  uint16_t sVDTunnelBufCount;
  //
  void *sVDExtData;
  uint32_t sVDLastReadTicks;
  //
  MESSAGE_CALLBACK sVDOnTunnelRead;
  UINT32_U sVDViewState;
  uint16_t sVDExtDataSize;
  uint16_t sVDBegPos;
  uint16_t sVDResult;
  int16_t  sVDFIndex;
  bool     sVDIsMaster;
  bool     sVDNewState;
  //
} VAP_Descr_TypeDef;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
/*
void MyInitCBuf(MY_CBUF_TypeDef *aMyCBuf,void *aBuffer,int aSize);
void MyPutToCBuf(MY_CBUF_TypeDef *aMyCBuf,void *aBuffer,int aSize);
*/
//==============================================================================

VAP_Descr_TypeDef* Vap_Init(VAP_Descr_TypeDef *aVapDescr,MY_USART_TypeDef *aMyUSART,bool aUseTunnel);
void Vap_Process(VAP_Descr_TypeDef *aVapDescr);
bool Vap_GetTunnelBuffer(VAP_Descr_TypeDef *aVapDescr,pvoid *aPBuffer,int *aBufSize);
void Vap_SetTunnelBufferCount(VAP_Descr_TypeDef *aVapDescr,int aCount);
void Vap_PostTunnelData(VAP_Descr_TypeDef *aVapDescr,void *aBuffer,int aSize);

