// ���������� Runtime Control
// � Olgvel, 2014-2016
//
#include "my_rtctl.h"

const char* BINCMD_STATES = "#*:+~";

void LogOkError(bool aState)
{
  if (aState) PostLogText(":OK"S_CRLF);
  else PostLogText(":ERROR"S_CRLF);
}

void DispatchTextCmd(RTCTL_DESCR_TypeDef *aDescr,uint8_t aCmdIdx,char *aParams,int aParLen)
{
  int lIndex = 0, lParPos, lParLen;
  char *lCmd;
  void **lCtxPtr;
  TXTCMD_PAR_TypeDef lPar;
  uint16_t lFlags;
  int16_t  lCtxIdx;
  uint8_t lCount=0;
  //
  TrimRight(aParams, &aParLen);
  lCmd = GetShortStrZ(aCmdIdx, aDescr->sRCTextCmdNames);
  PostLogText(lCmd);
  if (aParLen > 0) {
    PostLogText("=");
    PostLogMessage(aParams, aParLen);
    PostLogText(S_CRLF);
  }
  else PostLogText(":");
  //
  lFlags = aDescr->sRCTextCmdHandlers[aCmdIdx].sTCFlags;
  //
  while ((lCount < MAX_TXTCMDPARAMS) && ((lFlags & TCFANY) != 0 ) &&
         GetInParam(aParams, aParLen, &lIndex, &lParPos, &lParLen)) {
    if ((lFlags & TCFANY) == TCFINT ) {
      lPar.sTCPPars[lCount] = NULL;
      lPar.sTCPVals[lCount] = Str2IntDef(&aParams[lParPos], lParLen, 0);
    }
    else {
      lPar.sTCPPars[lCount] = &aParams[lParPos];
      lPar.sTCPVals[lCount] = lParLen;
    }
    lCount++; lFlags >>= TCFBITS;
  }
  lPar.sTCPCmdIdx = aCmdIdx;
  lPar.sTCPCount = lCount;

  //
  lPar.sTCPCommonRes = lCount != 0; lPar.sTCPResult = FALSE;
  if assigned(aDescr->sRCTextCmdHandlers[aCmdIdx].sTCHandler) {
    lCtxIdx = aDescr->sRCTextCmdHandlers[aCmdIdx].sTCCtxIdx;
    if (lCtxIdx > 0) lCtxPtr = aDescr->sRCAppCtxTable[lCtxIdx]; else 
    if (lCtxIdx < 0) lCtxPtr = (void*)&aDescr->sRCAppCtxTable[-lCtxIdx]; 
    else lCtxPtr = NULL;
    aDescr->sRCTextCmdHandlers[aCmdIdx].sTCHandler(*lCtxPtr, &lPar);
    if (lCount == 0) PostLogText(S_CRLF);
    if (lPar.sTCPCommonRes) LogOkError(lPar.sTCPResult);
  }
}

void RTCtl_ExecTextCommands(RTCTL_DESCR_TypeDef *aDescr,char *aMessage,int aLength)
{
  int lIndex, lCEPos, lPos = 0;
  //
  //PostLogMessage(aMessage, aLength); PostLogText(S_CRLF);
  //PostLogInt("Length=", aLength, S_CRLF);
  //PostLogInt("StrLengthEx=", StrLengthEx(&aMessage[lPos], "=;"S_CR), S_CRLF);
  while ((lPos < aLength) && FindShortStr(&aMessage[lPos], StrLengthEx(&aMessage[lPos], "=;"S_CR, aLength-lPos),
            aDescr->sRCTextCmdNames, &lIndex, &lCEPos)) {
    lCEPos += lPos;
    if ((lCEPos < aLength) && (aMessage[lCEPos] == '=')) lCEPos++;
    //
    if (GetCharPos(';', &aMessage[lCEPos], aLength-lCEPos, &lPos)) lPos += lCEPos;
    else lPos = aLength;
    DispatchTextCmd(aDescr, lIndex, &aMessage[lCEPos], lPos-lCEPos);
    lPos++;
  }
}

void RTCtl_OutTextCommands(RTCTL_DESCR_TypeDef *aDescr,uint8_t *aCmdIdx,int aCount)
{
  for (int I=0; I < aCount; I++)
    DispatchTextCmd(aDescr, aCmdIdx[I], NULL, 0);
}

void TCOutMessage(TXTCMD_PAR_TypeDef *aPar,const char *aMessage,int aLength)
{
  PostLogMessage(aMessage, aLength);
}

void TCOutText(TXTCMD_PAR_TypeDef *aPar,const char *aText)
{
  TCOutMessage(aPar, aText, TOEND);
}

void TCOutUInt(TXTCMD_PAR_TypeDef *aPar,const char *aBeforeText,uint32_t aValue,const char *aAfterText)
{
  char lStr[12];
  if (aBeforeText != NULL) TCOutText(aPar, aBeforeText);
  TCOutText(aPar, Int2StrZ(aValue, lStr, sizeof(lStr)-1));
  if (aAfterText != NULL) TCOutText(aPar, aAfterText);
}

void TCOutTextIP(TXTCMD_PAR_TypeDef *aPar,void *aBeforeText,uint32_t aValue,void *aAfterText)
{
  char lStr[16];
  if (aBeforeText != NULL) TCOutText(aPar, aBeforeText);
  TCOutText(aPar, Int2TextIPZ(aValue, lStr));
  if (aAfterText != NULL) TCOutText(aPar, aAfterText);
}

//==============================================================================
bool RTCtl_ExecBinCommands(RTCTL_DESCR_TypeDef *aDescr,char *aCProc,int aCProcLen,
  char *aAnsCProc,int *aAnsCProcLen)
{
  char *lName, *lAnsName;
  int  lNameLen, lIndex, lCtxIdx, lRemParPos, lRemAnsPos, lCmdLen;
  uint32_t lLenCode;
  uint16_t *lLenPrefCode;
  void *lContext;
  BINCMD_PAR_TypeDef lBCP;
  BCS_TypeDef lParState;
  uint8_t  lOffs;
  //
  lBCP.sBCPGet = aCProc; lBCP.sBCPGetPos = 0; lBCP.sBCPGetEnd = aCProcLen;
  lBCP.sBCPPut = aAnsCProc; lBCP.sBCPPutPos = 0; *aAnsCProcLen = 0;
  lLenPrefCode = (uint16_t*)((*aCProc & 0x80) != 0);
  //
  //PostLogInt("ExecBinCmd [", aCProcLen, "] "S_CRLF);
  //PostLogHexStr("", aCProc, aCProcLen, S_CRLF);
  //
  while (lBCP.sBCPGetPos < (aCProcLen-1)) {
    lRemParPos = lBCP.sBCPGetPos;
    lRemAnsPos = lBCP.sBCPPutPos;
    //
    if assigned(lLenPrefCode) {
      lBCP.sBCPGetEnd = lRemParPos + 1;
      lLenPrefCode = (uint16_t*)&lBCP.sBCPPut[lRemAnsPos];
      BCGetParPtr(&lBCP, &lAnsName, &lCmdLen);
      lBCP.sBCPGetEnd = lRemParPos + lCmdLen;
      lBCP.sBCPGetPos -= lCmdLen;
    }
    else lCmdLen = aCProcLen;
    //
    if (BCGetParPtr(&lBCP, &lName, &lNameLen)) {
      if (GetCharPos(lName[0], (char*)BINCMD_STATES, 0, &lIndex)) {
        //PostLogInt("lIndex=", lIndex, S_CRLF);
        lParState = (BCS_TypeDef)lIndex;
        if (lParState == BCS_Query) {
          if assigned(lLenPrefCode) lBCP.sBCPPutPos += sizeof(uint16_t);
          lAnsName = BCPutStrPar(&lBCP, lName, lNameLen);
        }
        lName++; lNameLen--;
      }
      else lParState = BCS_Notify;
      lBCP.sBCPState = lParState;
      lBCP.sBCPResult = lParState;
      if (FindShortStr(lName, lNameLen, aDescr->sRCBinCmdNames, &lIndex, NULL)) {
        if assigned(aDescr->sRCBinCmdHandlers[lIndex].sBCHandler) {
          lCtxIdx = aDescr->sRCBinCmdHandlers[lIndex].sBCCtxIdx;
          if (lCtxIdx > 0) lContext = *(pvoid*)aDescr->sRCAppCtxTable[lCtxIdx]; else 
          if (lCtxIdx < 0) lContext = (pvoid*)aDescr->sRCAppCtxTable[-lCtxIdx]; 
          else lContext = NULL;
          aDescr->sRCBinCmdHandlers[lIndex].sBCHandler(lContext, &lBCP);
        }
        else lBCP.sBCPResult = BCS_NotFound;
      }
      else lBCP.sBCPResult = BCS_NotFound;
      if (lParState == BCS_Query) {
        if (lBCP.sBCPResult == BCS_Query) lBCP.sBCPResult = BCS_Neutral;
        *lAnsName = BINCMD_STATES[lBCP.sBCPResult];
        //PostLogText("AnsName: "); PostLogMessage(lAnsName, lNameLen); PostLogText(S_CRLF);
      }
      if assigned(lLenPrefCode) {
        SetFieldLengthEx(lBCP.sBCPPutPos - lRemAnsPos - sizeof(uint16_t),
          &lLenCode, &lOffs, TRUE);
        *lLenPrefCode = lLenCode;
      }
      else break;
    }
    else break;
    lBCP.sBCPGetPos = lRemParPos + lCmdLen + sizeof(uint16_t);
  }
  *aAnsCProcLen = lBCP.sBCPPutPos;
  return lBCP.sBCPPutPos > 0;
}

//==============================================================================

bool DecodeBase64Commands(char *aMessage,int *aLength,uint32_t *aDest,uint32_t *aSender,uint16_t *aQNum)
{
  char lFrameBuf[3*INBINCMDBUFSIZE];
  int  lFrameLen = INBINCMDBUFSIZE, lLen, lRetPos;
  uint16_t lNumber, lFlags, lDataLen;
  VAP_SENDER_TypeDef lSender;
  //
  lLen = *aLength;
  if (lLen < 24) return FALSE;
  if (aMessage[lLen-1] == CR) lLen--;
  if (DecodeBase64(aMessage, lLen, &lFrameBuf, &lFrameLen)) {
    //PostLogText("After DecodeBase64"S_CRLF);
    if (VAPParseFrame(&lFrameBuf, lFrameLen, VSK_MASTER, BROADCASTDEVICE, aDest,aSender,
       &lSender, &lNumber, &lFlags, aMessage, &lDataLen, &lRetPos) && ((lFlags & CPROCDATA) != 0)) {
      *aLength = lDataLen;
      *aQNum = lNumber;
      return TRUE;
    }
  }
  return FALSE;
}
/*
bool FrameHdrSize(uint32_t aDestID,uint32_t aSenderID,uint8_t *aSize)
{
  *aSize = VAPFRAMESIZE;
  if ((aDestID != 0)||(aSenderID != 0)) (*aSize) += 2*sizeof(uint32_t);
  return TRUE;
}
*/
void RTCtl_ExecCommands(RTCTL_DESCR_TypeDef *aDescr,char *aMessage,int aLength,void *aSource)
{
  char lAnsFrame[3*INBINCMDBUFSIZE];
  int lAnsLen;
  uint32_t lDestID, lSenderID;
  uint16_t lQNumber, lAnsFrameLen;
  uint8_t lHdrSize;
  //
  //PostLogMessage(aMessage, aLength); PostLogText(S_CR);
  //PostLogInt("ExecCmdLen: ", aLength, S_CRLF);
  //
  lAnsLen = aLength;
  if (DecodeBase64Commands(aMessage, &lAnsLen, &lDestID, &lSenderID, &lQNumber) &&
      VAPFrameHdrSize(lDestID, lSenderID, &lHdrSize) &&
      RTCtl_ExecBinCommands(aDescr, aMessage, lAnsLen, &lAnsFrame[lHdrSize], &lAnsLen)) { //BROADCASTDEVICE, lQNumber, TRUE);
    //
    //PostLogInt("BCText cmd/ans [", aLength, "]/");
    //PostLogInt("ExecBinAns [", lAnsLen, "] "S_CRLF);
    //PostLogHexStr("", &lAnsFrame[VAPFRAMESIZE], MaxInt(lAnsLen, 32), S_CRLF);
    //
    VAPBuildFrame(VSK_SLAVE, BROADCASTDEVICE, lSenderID,lDestID, lQNumber, CPROCDATA,
      &lAnsFrame[lHdrSize], lAnsLen, &lAnsFrame, &lAnsFrameLen);
    //PostLogInt("", lAnsFrameLen, "/");
    EncodeBase64(&lAnsFrame, lAnsFrameLen, aMessage, &aLength);
    //PostLogInt("", aLength, "]"S_CRLF);
    aMessage[aLength++] = CR;
    //
    //PostLogInt("EncFrameLen [", aLength, "]"S_CRLF);
    //
    //PostLogMessage(aMessage, aLength); PostLogText(S_CR);
    if assigned(aDescr->sRCOnBCTextAnswer)
      aDescr->sRCOnBCTextAnswer(aSource, aMessage, aLength);
  }
  else {
    RTCtl_ExecTextCommands(aDescr, aMessage, aLength);
  }
}

//==============================================================================
RTCTL_DESCR_TypeDef* RTCtl_Init(RTCTL_DESCR_TypeDef *aDescr)
{
  if (aDescr == NULL)
    aDescr = FixMemAlloc( sizeof(RTCTL_DESCR_TypeDef) );
  MemClear(aDescr, sizeof(RTCTL_DESCR_TypeDef));
  //
  return aDescr;
}

void RTCtl_Process(RTCTL_DESCR_TypeDef *aDescr)
{
}

