// ����������� � �������� ���������
// ����� WiFi ESP8266, ESP32
// � olgvel, 2016-19
//
#include "my_stm32.h"
//
#define ESPLINKMAX         5


/* Exported types ------------------------------------------------------------*/

typedef enum { ESC_NONE, ESC_IPD, ESC_SEND, ESC_SENDBUF, ESC_UART,
  ESC_EOFF, ESC_MODE, ESC_MUX, ESC_SRV, ESC_START, ESC_CLOSE,
  ESC_JAP, ESC_LAP, ESC_SAP, ESC_LIF, ESC_DHCP, ESC_AUTOCONN,
  ESC_STAMAC, ESC_STA, ESC_AP, ESC_STATUS, ESC_FSR, ESC_PING,
  //
} ESP_CMD_TypeDef;

typedef enum {
  ESM_NORM, ESM_TRANS, ESM_PROG,
  //
} ESP_MODE_TypeDef;

typedef enum {
  ESS_START, ESS_INIT, ESS_WORK, ESS_TRANS,
  //
} ESP_STATE_TypeDef;

typedef enum {
  ESF_TRANSMODE, ESF_PWRRST, ESF_PWRCTL, ESF_ANYCONNQRY, ESF_ANYDCONNQRY, ESF_ANYACTIVE, ESF_MSGRDY, ESF_ENABLED, ESF_DEBUG,
  //
} ESP_FLAG_TypeDef;

typedef enum {
  ELF_ACTIVE, ELF_SERVER, ELF_CONNQRY, ELF_DCONNQRY, ELF_WRIBSY,
  //
} EL_FLAG_TypeDef;

typedef enum {
  ELM_NONE, ELM_UDP, ELM_TCPC, ELM_TCPS,
  //
} ESP_LM_TypeDef;

typedef struct ESP_DESCR ESP_DESCR_TypeDef;

typedef struct ESP_LINK {
  LINK_HDR
  //
  ESP_DESCR_TypeDef* sELESP;
  uint32_t sELAddress, sELInAddress;
  uint16_t sELPort, sELLocalPort, sELR16_1;
  uint8_t sELID;//, sLFlags, sELRes1;
  ESP_LM_TypeDef sELMode;
  uint32_t sELReconnDelay;
  uint32_t sELNoReadTime;
  uint32_t sELDisconnTicks;
  MHANDLER_TypeDef sELOnConnected;
  MHANDLER_TypeDef sELOnDisconnected;
  //
} ESP_LINK_TypeDef, *ESP_LINK_PtrDef;

struct ESP_DESCR {
  MY_UART_TypeDef* sESUART;
  const PIN_HWCFG_TypeDef* sESEnbPIN;
  const PIN_HWCFG_TypeDef* sESProgPIN;
  MY_LINK_TypeDef *sESTransLink;
  ESP_LINK_PtrDef sESLink[ESPLINKMAX];
  ESP_LINK_TypeDef *sESSelLink;
  ESP_LINK_TypeDef *sESSendLink;
  MY_CBUF_TypeDef *sESDbgCBuf;
  //
  uint32_t sESCommandTicks;
  uint32_t sESDisconnTicks;
  uint32_t sESStateDelay;
  uint32_t sESModeRetDelay;
  int sESRdLink, sESRdLen;
  uint32_t sESRdAddr,sESRdPort;
  uint32_t sESSTAIP,sESAPIP;
  uint32_t sESSubMac;
  //
  ESP_CMD_TypeDef sESCommand;
  ESP_CMD_TypeDef sESCmdAnswer;
  ESP_STATE_TypeDef sESState;
  uint8_t  sESSubState;
  //
  uint16_t sESFlags;
  uint8_t  sESRdFlags;
  uint8_t  sESSendIdx, sESTest, sESAPChan;
  int16_t  sESWrLen;
  uint16_t sESSrvPort;
  MHANDLER_TypeDef sESOnAfterInit;
  MHANDLER_TypeDef sESOnTransLink;
  //
};

#ifdef __cplusplus
extern "C" {
#endif
  
#define TCN_ESPC         "\x05+ESPC\0"
#define TCH_ESPC         {(TCHANDLER)TCHEspCommand,TCFSTR,ACX_ESP}
void TCHEspCommand(ESP_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_ESPRST       "\x09+ESPRESET\0"
#define TCH_ESPRST       {(TCHANDLER)TCHEspReset,(TCFINT<<TCFBITS)|TCFINT,ACX_ESP}
void TCHEspReset(ESP_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_WMODE        "\x06+WMODE\0"
#define TCH_WMODE        {(TCHANDLER)TCHWiFiMode,TCFINT,ACX_ESP}
void TCHWiFiMode(ESP_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_WNETACC      "\x08+WNETACC\0"
#define TCH_WNETACC      {(TCHANDLER)TCHWiFiNetAcc,(TCFSTR<<TCFBITS)|TCFSTR,ACX_ESP}
void TCHWiFiNetAcc(ESP_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#ifdef __cplusplus
}
#endif

#define WMODE            "WMODE"
#define DEFWMODE         1

/* Exported functions ------------------------------------------------------- */

ESP_DESCR_TypeDef* ESP_Init(ESP_DESCR_TypeDef *aDescr,MY_UART_TypeDef *aMyUART,
  const PIN_HWCFG_TypeDef *aEnbPIN,bool aPowerCtl,const PIN_HWCFG_TypeDef *aProgPIN);
void ESP_Process(ESP_DESCR_TypeDef *aDescr);
//
//ESP_LINK_TypeDef* ESP_AddLink(ESP_DESCR_TypeDef *aDescr,uint16_t aBufSize,ESP_LM_TypeDef aLinkMode);
ESP_LINK_TypeDef* ESP_AddLink(ESP_DESCR_TypeDef *aDescr,uint16_t aOutBufSize, uint16_t aInBufSize,
  ESP_LM_TypeDef aLinkMode);
bool ESP_LinkConnect(ESP_LINK_TypeDef* aDescr,uint32_t aReconnDelay,uint32_t aNoReadTime);
bool ESP_LinkParams(ESP_LINK_TypeDef* aDescr,uint32_t aRmtAddr,uint16_t aRmtPort,uint16_t aLocalPort);
bool ESP_LinkDisconnect(ESP_LINK_TypeDef* aDescr,bool aIgnoreReconn);

void ESP_AssignTransLink(ESP_DESCR_TypeDef *aDescr,MY_LINK_TypeDef *aLink);

//void ESP_UpdateSensors(ESP_DESCR_TypeDef *aDescr,uint32_t aIsBegin);
//void ESP_NewSensor(ESP_DESCR_TypeDef *aDescr,uint32_t aSensorID);

