// ���������� ���������� � �������� SIMCOM
// � Olgvel, 2012-15
//
#include "my_simcom.h"
#include "my_rtctl.h"

#define USEIPMODE              FALSE
#define TEXTSEND               FALSE

#define SCREAD_WAIT_TIME       10
#define NORMAL_POWER_DOWN      "NORMAL POWER DOWN"
#define NO_JAMMING             "NO JAMMING"
#define JAMMING_DETECTED       "JAMMING DETECTED"
#define INTERFERENCE_DETECTED  "INTERFERENCE DETECTED"

#define GMODE                  "GMODE"
#define DEFGMODE               1
#define SIMCARD                "SIMCARD"
#define DEFSIMCARD             0

#define RECEIVEWAIT_TIMEOUT    180000
#define NOANSWER_TIMEOUT       13000
#define NOREG_TIMEOUT          90000   //45000
#define NOANSWER_TIMESHF       10000
#define LONGWAITCMD_TIMEOUT    45000
//#define PRIOQREG_TIMEOUT       20000
#define RECONN_TIMEOUT         5000
#define LDRMODECODE            0xA55A
#define LDRMODECODE_BKP_INDEX  0x12

#define SMSERROR_THRESH   3
#define IPERROR_THRESH    3

#define SIGLEVEL_PARPOS   6
#define VOLTAGE_PARPOS    6
#define SMSOK_PARPOS      7
#define SMSCONFIRM_PARPOS 6
#define INSMS_PARPOS      6
#define SMSLIST_PARPOS    7
#define INCALL_PARPOS     7
#define INIP_PARPOS       9
#define CREG_PARPOS       7
#define SIMOPER_PARPOS    7
#define IPENB_PARPOS      8
#define USSD_PARPOS       7
#define JAMINFO_PARPOS    7

#define UNKNOWNCPROV      -1

#define STDCTLIPADDR     0xC2F6755A        // "194.246.117.90"
#define STDCTLIPPORT     9090
#define STDTNLIPADDR     0xC2F6755A        // "194.246.117.90"
#define STDTNLIPPORT     5505

#define DEFCPROVITEMS    8


const CPROV_ITEM_TypeDef STD_CPROV_ARRAY[DEFCPROVITEMS] = {
  {"MTS UKR", "www.umc.ua"}, {"KYIVSTAR", "www.kyivstar.net"},
  {"LIFE", "internet"},
  {"MTS RUS", "internet.mts.ru"}, {"MegaFon", "internet.nw"},
  {"Beeline", "internet.beeline.ru"}, {"TELE2", "internet.tele2.ru"},
  {"WIN", "internet"}
};

// +CSPN:  "KYIVSTAR" "MTS UKR" "Beeline" "MTS RUS" "MegaFon" "WIN"

int32_t GetCProvIndex(char *aSouStr,int32_t aLength)
{
  for (int I=0;I<DEFCPROVITEMS;I++)
    if (SubtextComp((void*)&STD_CPROV_ARRAY[I].sCPISimName, aSouStr, aLength))
      return I;
  return ERROR_RESULT;
}

bool CondLogText(bool aCond,char *aTextTRUE)
{
  if (aCond && (aTextTRUE != NULL)) { PostLogText(aTextTRUE); PostLogText(S_CRLF); }
  return aCond;
}

void DoSMSNotify(SIMCOM_DESCR_TypeDef *aDescr,int32_t aState)
{
  if ((aDescr->sSCOutSMSCount != 0) && (aDescr->sSCOnSMSNotify != NULL))
    aDescr->sSCOnSMSNotify(aState);
  aDescr->sSCOutSMSCount = 0;
}

void DoSMSConfirm(SIMCOM_DESCR_TypeDef *aDescr,int32_t aState)
{
  if (aDescr->sSCOnSMSConfirm != NULL)
    aDescr->sSCOnSMSConfirm(aState);
}

bool DoChangeCReg(SIMCOM_DESCR_TypeDef *aDescr,bool aState)
{
  if (aState) aDescr->sSCLastCRegTicks = TICKS;
  if (aDescr->sSCRegister != aState) {
    aDescr->sSCRegister = aState;
    if (aDescr->sSCOnChangeRegister != NULL)
      aDescr->sSCOnChangeRegister(aState);
    return TRUE;
  }
  return FALSE;
}

void DoChangeOper(SIMCOM_DESCR_TypeDef *aDescr)
{
  if (aDescr->sSCOnChangeOperator != NULL)
    aDescr->sSCOnChangeOperator(TRUE);
}

void DoIPConnChangeState(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelConn,bool aState)
{
  aDescr->sSCIPCONN[aSelConn].sCDConnDisconn = FALSE;
  if (aDescr->sSCIPCONN[aSelConn].sCDConnected != aState) {
    aDescr->sSCIPCONN[aSelConn].sCDConnected = aState;
    if (aDescr->sSCIPCONNFX[aSelConn].sSDOnChangeState != NULL)
      aDescr->sSCIPCONNFX[aSelConn].sSDOnChangeState(aState);
    if (aState) aDescr->sSCIPCONN[aSelConn].sCDStartTime = TICKS;
    else aDescr->sSCIPCONN[aSelConn].sCDDisconnTime = TICKS;
    aDescr->sSCConnDisconnTicks = TICKS;
  }
}

void DoIPConnSended(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelConn,bool aState)
{
  if (aDescr->sSCIPCONNFX[aSelConn].sSDOnSended != NULL)
    aDescr->sSCIPCONNFX[aSelConn].sSDOnSended(aState);
}

void DoChangeJammingState(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aJamState)
{
  //char lStr[4];
  //
  //PostLogText("ChangeJammState="); PostLogText(Int2StrDigZ(aJamState, lStr, 1)); PostLogText(S_CRLF);
  if (aJamState != aDescr->sSCJamState) {
    aDescr->sSCJamState = aJamState;
    if (aDescr->sSCOnJamming != NULL) aDescr->sSCOnJamming(aJamState);
  }
}

void DoReset(SIMCOM_DESCR_TypeDef *aDescr)
{
  MyPS_Setup(&aDescr->sSCReset, TRUE, 1000, 500, 1);
}

void EndOfPowerSwitch(void *aSender,SIMCOM_DESCR_TypeDef *aDescr) // void *aContext)
{
  aDescr->sSCPowerSwitching = FALSE;
  //
  PostLogText("EndOfPowerSwitch!"S_CRLF);
  if (!aDescr->sCSWasAnswer) {
    DoReset(aDescr);
    PostLogText("SimReset!"S_CRLF);
  }  
}

void HandleInCall(SIMCOM_DESCR_TypeDef *aDescr)
{
  PostLogText(aDescr->sSCInNumber);
  if (aDescr->sSCInCallRcv) PostLogText(" *");else PostLogText(" #");
  PostLogText(S_CRLF);
  //
  if (aDescr->sSCOnInCall != NULL)
    aDescr->sSCOnInCall(aDescr->sSCInNumber, aDescr->sSCInCallRcv);
}

void PowerSwitch(SIMCOM_DESCR_TypeDef *aDescr)
{
  if (aDescr->sSCReady)
    if ((!aDescr->sSCRegister) || (aDescr->sSCNeedSIMChange)) {
      aDescr->sSCSecondSIM = !aDescr->sSCSecondSIM;
      //PostLogText("--- SecondSIM = ");
      //if (aDescr->sSCSecondSIM) PostLogText("TRUE"S_CRLF);else PostLogText("FALSE"S_CRLF);
    }
    else;
  else {
    PIN_Set(aDescr->sSCCardPIN, aDescr->sSCSecondSIM);
    aDescr->sSCNeedSIMChange = FALSE;
  }
  //
  aDescr->sSCPowerSwitching = TRUE;
  MyPS_Setup(&aDescr->sSCPower, TRUE, 2000, 1500, 1);
  aDescr->sSCProvIndex = UNKNOWNCPROV;
  //
  DoSMSNotify(aDescr, ERROR_RESULT);
  DoIPConnChangeState(aDescr, CTLCONNID, FALSE);
  DoIPConnChangeState(aDescr, TNLCONNID, FALSE);
  DoChangeCReg(aDescr, FALSE);
  //
  MemClear(&aDescr->sSCIPCONN, sizeof(IPCONN_DESCR_TypeDef)*IPCONNCOUNT);
  //
  aDescr->sSCAnswerTicks = TICKS;
  aDescr->sSCQTicks = TICKS;
  aDescr->sSCLastCRegTicks = TICKS;
  aDescr->sSCWaitCommand = SCCMD_NONE;
  aDescr->sSCSMSSending = FALSE;
  //aDescr->sSCIPWaitState = EMPTYCONNID;
  aDescr->sSCIPSending = EMPTYCONNID;
  aDescr->sSCLastIPSending = 0;
  aDescr->sSCLastConnDisconn = 0;
  aDescr->sSCLongWaitCTicks = 0;
  aDescr->sSCReady = FALSE;
  //aDescr->sSCRegister = FALSE;
  aDescr->sSCCurLevel = 0;
  aDescr->sSCInSMSRcv = FALSE;
  aDescr->sSCInCallRcv = FALSE;
  aDescr->sSCInIPLen = 0;
  aDescr->sSCIPAvailable = FALSE;
  aDescr->sSCIPOpenClose = FALSE;
  aDescr->sSCSelfIPAddr = 0;
  aDescr->sSCOutSMSErrors = 0;
  aDescr->sSCOutIPErrors = 0;
  //aDescr->sSCNeedSIMChange = FALSE;
  aDescr->sSCIPOpenCloseTicks = 0;
  aDescr->sCSWasAnswer = FALSE;
  //
  PostLogText("PowerSwitch"S_CRLF);
}

void PortWriteText(SIMCOM_DESCR_TypeDef *aDescr,char *aBuffer)
{
  int lLen;
  //
  MyCBuf_WriteText(aDescr->sSCUART->sLTx, aBuffer);
  //PostLogText("SIMCOM: ");
  PostLogText(aBuffer);
  lLen = StrLength(aBuffer);
  if ((lLen > 0) && (aBuffer[lLen-1] == CR))
    PostLogText(S_LF);
}

void PortWriteSMSHeader(SIMCOM_DESCR_TypeDef *aDescr)
{
  int lCount, lVal;
  char lIStr[4], lStr[32];
  //
  aDescr->sSCSMSSending = TRUE;
  aDescr->sSCOutSMSID = ERROR_RESULT;
  lCount = AddStr("AT+CSCS=\x22", lStr, 0);
  if (aDescr->sSCUCS2Mode) lCount = AddStr("UCS2", lStr, lCount);
  else lCount = AddStr("GSM", lStr, lCount);
  lCount = AddStr("\x22;+CSMP=", lStr, lCount);
  if ((aDescr->sSCOutSMSFlags & 2) == 0) lCount = AddStr("17", lStr, lCount);
  else lCount = AddStr("49", lStr, lCount);
  lCount = AddStr(",167,0,", lStr, lCount);
  if (aDescr->sSCUCS2Mode) lVal = 25; else lVal = 241;
  if ((aDescr->sSCOutSMSFlags & 1) != 0) lVal--;
  lCount = AddStr(Int2StrZ(lVal, lIStr, 3), lStr, lCount);
  lCount = AddStr(";+CMGS=\x22", lStr, lCount);
  PortWriteText(aDescr, lStr);
  PortWriteText(aDescr, aDescr->sSCOutSMSNumber);
  PortWriteText(aDescr, "\x22,145"S_CR);
}

void ReadJamParams(SIMCOM_DESCR_TypeDef *aDescr)
{
  aDescr->sSCJamMode = ReadUIntVar(JAMMODE, 0);
  aDescr->sSCJamThresh = ReadUIntVar(JAMTHR, 90);
}

void PostCommand(SIMCOM_DESCR_TypeDef *aDescr,SIMCOM_CMD_TypeDef aCommand)
{
  //char lHexStr[768];
  char lTmp[16];
  uint8_t lSelConn;
  //
  aDescr->sSCQTicks = TICKS;
  aDescr->sSCWaitCommand = aCommand;
  //
  switch (aCommand) {
    case SCCMD_TEST:
      PortWriteText(aDescr, "AT"S_CR); break;
    case SCCMD_INIT:
      ReadJamParams(aDescr);
      PortWriteText(aDescr,  // AT+CMGF=1;+CSCS="GSM";+CNMI=2,2,0,1,1;+CSMP=17,167,0,0;+CLIP=1;+CIPMUX=1;+CSPN?;E0
        "AT+CMGF=1;+CSCS=\x22GSM\x22;+CNMI=2,0,0,0,0;+CSMP=17,167,0,0;+CLIP=1;+CIPMUX=1;+GMR;+GSN;"); //+CSCB=0;"); //+CNMI=2,2,0,1,1; +CSCB=0
      if (aDescr->sSCJamMode > 0) {
        Int2StrZ(aDescr->sSCJamThresh, lTmp, 3);
        PortWriteText(aDescr, "+SJDR=1,1,");
        PortWriteText(aDescr, lTmp);
        PortWriteText(aDescr, ",1;");
      }
      PortWriteText(aDescr, "+CSPN?;E0"S_CR);
      break;
    case SCCMD_MODEL:
      PortWriteText(aDescr, "AT+CGMM"S_CR); break;
    case SCCMD_SN:
      PortWriteText(aDescr, "AT+CGSN"S_CR); break;
    case SCCMD_REGLEVEL:
      PortWriteText(aDescr, "AT+CBC;+CREG?;+CGATT?;+CSQ;+CMGD=0,3;+CMGL=\x22REC UNREAD\x22"S_CR);
      //if (aDescr->sSCJamMode > 0) PortWriteText(aDescr->sSCUART, ";+SJDR?");
      //PortWriteText(aDescr->sSCUART, S_CR);
      break;
    case SCCMD_SIMOPER:
      PortWriteText(aDescr, "AT+CSPN?"S_CR); break;
    case SCCMD_SMSHDR:
      PortWriteSMSHeader(aDescr);
      break;
    case SCCMD_SMSBODY:
      MyCBuf_Write(aDescr->sSCUART->sLTx, aDescr->sSCOutSMS, aDescr->sSCOutSMSCount);
      MyCBuf_WriteText(aDescr->sSCUART->sLTx, S_EOF);
      aDescr->sSCLongWaitCTicks = TICKS;
      PostLogMessage(aDescr->sSCOutSMS, aDescr->sSCOutSMSCount);
      PostLogText(S_CRLF);
      break;
    case SCCMD_IPOPEN:
      if (aDescr->sSCProvIndex >= 0) {
        aDescr->sSCIPOpenClose = TRUE;
        PortWriteText(aDescr, "AT+CSTT=\x22");
        PortWriteText(aDescr, (char*)&STD_CPROV_ARRAY[aDescr->sSCProvIndex].sCPIAPN);
        PortWriteText(aDescr, "\x22;+CIICR;+CIFSR"S_CR);
        aDescr->sSCIPOpenCloseTicks = TICKS;
        aDescr->sSCLongWaitCTicks = TICKS;
      }
      break;
    case SCCMD_IPCLOSE:
      if (aDescr->sSCProvIndex >= 0) {
        aDescr->sSCIPOpenClose = TRUE;
        aDescr->sSCIPOpenCloseTicks = TICKS;
        PortWriteText(aDescr, "AT+CIPSHUT"S_CR);
      }
      break;
    case SCCMD_IPSTART:   // AT+CIPSTART=0,"TCP","194.246.117.90","5500"
      if (aDescr->sSCProvIndex >= 0) {
        lSelConn = aDescr->sSCIPSelConn;
        aDescr->sSCIPCONN[lSelConn].sCDConnDisconn = TRUE;
        aDescr->sSCConnDisconnTicks = TICKS;
        Int2StrDigZ(lSelConn, lTmp, 1);
        PortWriteText(aDescr, "AT+CIPSTART=");
        PortWriteText(aDescr, lTmp);
          //PostLogText( (char*)&lTmp );
        PortWriteText(aDescr, ",\x22TCP\x22,\x22");
        //
        PortWriteText(aDescr, Int2TextIPZ(aDescr->sSCIPCONNFX[lSelConn].sCDAddr, lTmp));
          //PostLogText( (char*)&lTmp );
        //
        PortWriteText(aDescr, "\x22,\x22");
        PortWriteText(aDescr,
          Int2StrZ(aDescr->sSCIPCONNFX[lSelConn].sCDPort, lTmp, 5) );
        PortWriteText(aDescr, "\x22"S_CR);
        aDescr->sSCLongWaitCTicks = TICKS;
      }
      break;
    case SCCMD_IPSTOP:
      if (aDescr->sSCProvIndex >= 0) {
        lSelConn = aDescr->sSCIPSelConn;
        aDescr->sSCIPCONN[lSelConn].sCDConnDisconn = TRUE;
        aDescr->sSCConnDisconnTicks = TICKS;
        Int2StrDigZ(lSelConn, lTmp, 1);
        PortWriteText(aDescr, "AT+CIPCLOSE=");
        PortWriteText(aDescr, lTmp);
        PortWriteText(aDescr, S_CR);
      }
      break;
    case SCCMD_IPHDR:
      lSelConn = aDescr->sSCIPSelConn;
      aDescr->sSCIPSending = lSelConn;
      PortWriteText(aDescr, "AT+CIPSEND=");
      PortWriteText(aDescr, Int2StrDigZ(lSelConn, lTmp, 1));
      if (!TEXTSEND) {
        PortWriteText(aDescr, ",");
        PortWriteText(aDescr,
          Int2StrDigZ(aDescr->sSCIPCONN[lSelConn].sCDOutBufCount, lTmp, 3));
      }
      PortWriteText(aDescr, S_CR);
      //PostLogText(&lTmp);
      //PostLogText(S_CRLF);
      break;
    case SCCMD_IPBODY:
      lSelConn = aDescr->sSCIPSelConn;
      MyCBuf_Write(aDescr->sSCUART->sLTx, aDescr->sSCIPCONNFX[lSelConn].sCDOutBuf,
        aDescr->sSCIPCONN[lSelConn].sCDOutBufCount);
      if (TEXTSEND) PortWriteText(aDescr, S_EOF);
      aDescr->sSCLongWaitCTicks = TICKS;

      //PostLogText(Bin2HexStrZ(&aDescr->sSCOutIP, &lHexStr, aDescr->sSCOutIPLen, TRUE));
      //PostLogText(S_CRLF);
      break;
    case SCCMD_PWRDOWN:
      PortWriteText(aDescr, "AT+CPOWD=1"S_CR); break;
    case SCCMD_HANGUP:
      PortWriteText(aDescr, "AT+CHUP"S_CR);
      if (aDescr->sSCInCallRcv) {
        aDescr->sSCInCallRcv = FALSE;
        HandleInCall(aDescr);
      }
      break;
    case SCCMD_BALANCE:
      PortWriteText(aDescr, "AT+CUSD=1,\x22*100#\x22"S_CR); break;
  }
}

//  SCANS_UNKNOWN, SCANS_OK, SCANS_ERROR, SCANS_BUSY, SCANS_POWEROFF,
//  SCANS_PROMPT, SCANS_SMSOK, SCANS_SMSRCV,
//RING
//+CLIP: "+79817257694",145,"",,"",0
//NO CARRIER
//
//+CMT: "+79817257694","","14/08/30,13:23:02+16"
//+79817257694 > 0422043504410442002004100411042104140415      //���� �����
//+79817257694 > 0054006500730074002004100411042104140415      //Test �����
//+79817257694 > 0422043504410442002004100411042104140415002004220435044104420020041004110421041404150020042204350441044200200410041104210414041500200422043504410442002004100411042104140415002004220435044104420020041004110421041404150020042204350441044200200410041104210414041500200030003900380037
//+CMGS: 43

SIMCOM_ANS_TypeDef DetectAnswer(char *aMsg,int32_t aLength,uint32_t *aParam)
{
   int32_t lIndex;
   if (aLength == 0) return SCANS_UNKNOWN; else
   if (SubstrComp("OK", aMsg, aLength)) return SCANS_OK; else
   if (SubstrComp("ERROR", aMsg, aLength)) return SCANS_ERROR; else
   if (SubstrComp("BUSY", aMsg, aLength)) return SCANS_BUSY; else
   if (SubstrComp("NORMAL POWER DOWN", aMsg, aLength)) return SCANS_POWERDOWN; else
   if (SubstrComp("+CPIN: NOT READY", aMsg, aLength)) return SCANS_NOTREADY; else
   if (SubstrComp("+PDP: DEACT", aMsg, aLength)) return SCANS_PDPDEACT; else
   if (SubstrComp("+CSQ:", aMsg, aLength)) return SCANS_SIGLEVEL; else
   if (SubstrComp("+CBC:", aMsg, aLength)) return SCANS_VOLTAGE; else
   if (SubstrComp("+CREG:", aMsg, aLength)) return SCANS_CREG; else
   if (SubstrComp("+CGATT:", aMsg, aLength)) return SCANS_IPENB; else
   if (SubstrComp("+CMGS:", aMsg, aLength)) return SCANS_SMSOK; else
   if (SubstrComp("+CDS:", aMsg, aLength)) return SCANS_SMSCONFIRM; else
   if (SubstrComp(">", aMsg, aLength)) return SCANS_PROMPT; else
   if (SubstrComp("+CMT:", aMsg, aLength)) return SCANS_SMSRCV; else
   if (SubstrComp("+CMGL:", aMsg, aLength)) return SCANS_SMSLIST; else
   if (SubstrComp("+CUSD:", aMsg, aLength)) return SCANS_USSD; else
   if (SubstrComp("+SJDR:", aMsg, aLength)) return SCANS_JAMINFO; else
   if (SubstrComp("RING", aMsg, aLength)) return SCANS_RING; else
   if (SubstrComp("+CLIP:", aMsg, aLength)) return SCANS_INCALL; else
   if (SubstrComp("NO CARRIER", aMsg, aLength)) return SCANS_NOCARRIER; else
   if (SubstrComp("+RECEIVE,", aMsg, aLength)) return SCANS_IPRCV; else
   if (SubstrComp("+CSPN:", aMsg, aLength)) return SCANS_SIMOPER; else
   if (SubstrComp("SHUT OK", aMsg, aLength)) return SCANS_IPSHUT; else
   if ((aLength > 3) && (aMsg[1] == ',') && (aMsg[0] >= '0') && (aMsg[0] <= '7')) {
     if (aMsg[2] == ' ') lIndex = 3; else lIndex = 2;
     *aParam = (aMsg[0] - '0');
     if (SubstrComp("SEND OK", &aMsg[lIndex], aLength-lIndex)) return SCANS_IPSENDOK; else
     if (SubstrComp("SEND FAIL", &aMsg[lIndex], aLength-lIndex)) return SCANS_IPSENDFAIL; else
     if (SubstrComp("CONNECT OK", &aMsg[lIndex], aLength-lIndex)) return SCANS_IPCONNOK; else
     if (SubstrComp("CONNECT FAIL", &aMsg[lIndex], aLength-lIndex)) return SCANS_IPCONNFAIL; else
     if (SubstrComp("ALREADY CONNECT", &aMsg[lIndex], aLength-lIndex)) return SCANS_IPALREADYCONN; else
     if (SubstrComp("CLOSED", &aMsg[lIndex], aLength-lIndex)) return SCANS_IPCLOSED; else
     if (SubstrComp("CLOSE OK", &aMsg[lIndex], aLength-lIndex)) return SCANS_IPCLOSEOK; else
     return SCANS_UNKNOWN;
   } else
   if (TextIP2Int(aMsg, aLength, aParam)) return SCANS_IPADDR; else
   //
   return SCANS_UNKNOWN;
}

void HandleMSG(SIMCOM_DESCR_TypeDef *aDescr,char *aMessage,int32_t aLength)
{
  PostLogText(aDescr->sSCInNumber);
  PostLogText(" > ");
  if (UCS2StrToWin1251(aMessage, aLength, aMessage)) aLength >>= 2;
  PostLogMessage(aMessage, aLength);
  PostLogText(S_CRLF);
  if (aDescr->sSCOnMsgReceive != NULL)
    aDescr->sSCOnMsgReceive(aDescr->sSCInNumber, aMessage, aLength);
}

void HandleIP(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelIPConn,char *aMessage,int32_t aLength)
{
  if (aSelIPConn < IPCONNCOUNT)
    if (aDescr->sSCIPCONNFX[aSelIPConn].sSDOnRead != NULL)
      aDescr->sSCIPCONNFX[aSelIPConn].sSDOnRead(aMessage, aLength);
}

void NeedRestart(SIMCOM_DESCR_TypeDef *aDescr,bool aSIMChange)
{
  aDescr->sSCNeedSIMChange = aSIMChange;
  aDescr->sSCAnswerTicks = TICKS - NOANSWER_TIMEOUT;
  if (aSIMChange)
    PostLogText(" SIM Change!"S_CRLF);
}

void CommandHandled(SIMCOM_DESCR_TypeDef *aDescr,SIMCOM_ANS_TypeDef aAnswer)
{
  if (!aDescr->sSCReady)
    aDescr->sSCLastCRegTicks = TICKS;
  aDescr->sSCReady = TRUE;
  aDescr->sSCLastResult = aAnswer;
  aDescr->sSCAnswerTicks = TICKS;
  aDescr->sSCWaitCommand = SCCMD_NONE;
}

void HandleAnswer(SIMCOM_DESCR_TypeDef *aDescr,char *aMessage,int32_t aLength)
{
  int32_t lAnsLen, lIndex, lParPos, lParLen, lIntPar;
  //char lTmp[16];
  uint32_t lParam;
  SIMCOM_ANS_TypeDef lAnswer;
  //bool lState;
  //
  // PostLogMessage(aMessage, aLength);
  //
  while (aLength > 0) {
    if (aDescr->sSCInIPLen > 0) {
      lAnsLen = aDescr->sSCInIPLen;
      if (lAnsLen > aLength) lAnsLen = aLength;
      aDescr->sSCInIPLen = 0;
      HandleIP(aDescr, aDescr->sSCInIPConn, aMessage, lAnsLen);
    }
    else {
      lAnsLen = CharPos(CR, aMessage, aLength);
      if (lAnsLen == ERROR_RESULT) lAnsLen = aLength;
      //
      if (aDescr->sSCInSMSRcv) {
        aDescr->sSCInSMSRcv = FALSE;
        HandleMSG(aDescr, aMessage, lAnsLen);
      }
      else
        if (lAnsLen > 0) {
          lAnswer = DetectAnswer(aMessage, lAnsLen, &lParam);
          PostLogMessage(aMessage, lAnsLen); PostLogText(S_CRLF);
          aDescr->sSCLongWaitCTicks = 0;
          //
          switch (lAnswer) {
            case SCANS_PDPDEACT:
              PostLogText(" Answer > PDP DEACT!"S_CRLF);
              if (aDescr->sSCIPOpenClose) {
                PostLogText(" Before restart ..."S_CRLF);
                NeedRestart(aDescr, FALSE);
              }
              break;
              //
            case SCANS_NOTREADY:
              NeedRestart(aDescr, FALSE);
              aDescr->sSCShdlCommand = aDescr->sSCWaitCommand;
              break;
              //
            case SCANS_BUSY: ;
            case SCANS_IPSENDFAIL: ;
              if ((lAnswer == SCANS_IPSENDFAIL) && (lParam < IPCONNCOUNT))
                DoIPConnSended(aDescr, lParam, FALSE);
            case SCANS_ERROR:
              if (aDescr->sSCWaitCommand == SCCMD_INIT) {
                NeedRestart(aDescr, TRUE);
                break;
              } else
              if (aDescr->sSCWaitCommand == SCCMD_IPOPEN) {
                aDescr->sSCSelfIPAddr = 0;
                aDescr->sSCIPOpenClose = FALSE;
                DoIPConnChangeState(aDescr, CTLCONNID, FALSE);
                DoIPConnChangeState(aDescr, TNLCONNID, FALSE);
              }
            case SCANS_IPSENDOK:
              if ((lAnswer == SCANS_IPSENDOK) && (lParam < IPCONNCOUNT))
                DoIPConnSended(aDescr, lParam, TRUE);
            case SCANS_OK:
              if ((aDescr->sSCWaitCommand == SCCMD_SMSHDR) ||
                  (aDescr->sSCWaitCommand == SCCMD_SMSBODY)) {
                aDescr->sSCSMSSending = FALSE;
                aDescr->sSCSMSSendTicks = TICKS;
                //if (lAnswer != SCANS_OK)
                DoSMSNotify(aDescr, aDescr->sSCOutSMSID);
                //DoSMSSended(aDescr, lAnswer == SCANS_OK);
                if (lAnswer == SCANS_OK)
                  if (aDescr->sSCOutSMSErrors > 0) aDescr->sSCOutSMSErrors--; else;
                else if (aDescr->sSCOutSMSErrors < 255) aDescr->sSCOutSMSErrors++;
                if (aDescr->sSCOutSMSErrors >= SMSERROR_THRESH) {
                  NeedRestart(aDescr, TRUE);
                  break;
                }
              }
              if ((aDescr->sSCWaitCommand == SCCMD_IPOPEN) ||
                  (aDescr->sSCWaitCommand == SCCMD_IPHDR) ||
                  (aDescr->sSCWaitCommand == SCCMD_IPBODY)) {
                //aDescr->sSCIPWaitState = EMPTYCONNID;
                if (lParam < IPCONNCOUNT)
                  aDescr->sSCIPCONN[lParam].sCDOutBufCount = 0;
                aDescr->sSCIPSendTicks = TICKS;
                aDescr->sSCLastIPSending = aDescr->sSCIPSending;
                aDescr->sSCIPSending = EMPTYCONNID;
                if (lAnswer == SCANS_IPSENDOK)
                  if (aDescr->sSCOutIPErrors > 0) aDescr->sSCOutIPErrors--; else;
                else if (aDescr->sSCOutIPErrors < 255) aDescr->sSCOutIPErrors++;
                if (aDescr->sSCOutIPErrors >= IPERROR_THRESH) {
                  PostLogText("IP Error thresh overflow!!!"S_CRLF);
                  NeedRestart(aDescr, FALSE);
                  break;
                }
              }
              //
              CommandHandled(aDescr, lAnswer);
              break;
              //
            case SCANS_PROMPT:
              if (aDescr->sSCWaitCommand == SCCMD_SMSHDR)
                PostCommand(aDescr, SCCMD_SMSBODY);
              else
                if (aDescr->sSCWaitCommand == SCCMD_IPHDR)
                  PostCommand(aDescr, SCCMD_IPBODY);
              break;
            case SCANS_CREG:
              lIndex = CREG_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                  lIndex = Str2IntDef(&aMessage[lParPos], lParLen, 0);
                  //
                  if (DoChangeCReg(aDescr, (lIndex == 1)||(lIndex == 5) ) &&
                      aDescr->sSCRegister) {
                    IncStartCounter(TRUE);
                    if ((aDescr->sSCLastProvIndex >= 0) &&
                        (aDescr->sSCLastProvIndex != aDescr->sSCProvIndex)) {
                      DoChangeOper(aDescr);
                      aDescr->sSCLastProvIndex = aDescr->sSCProvIndex;
                    }
                  }
                }
                else DoChangeCReg(aDescr, FALSE);
              }
              else DoChangeCReg(aDescr, FALSE);
              //
              break;
            case SCANS_IPENB:
              lIndex = IPENB_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                lIndex = Str2IntDef(&aMessage[lParPos], lParLen, 0);
                aDescr->sSCIPAvailable = (lIndex == 1);
              }
              else aDescr->sSCIPAvailable = FALSE;
              //
              break;
            case SCANS_IPADDR:
              aDescr->sSCSelfIPAddr = lParam;
              aDescr->sSCIPOpenClose = FALSE;
              //PostLogText("IP address detected: "S_CRLF);
              //Int2Hex(aDescr->sSCSelfIPAddr, &lTmp, 8);
              //PostLogMessage(&lTmp, 8); PostLogText(S_CRLF);
              //
              //Int2TextIP(aDescr->sSCSelfIPAddr, &lTmp);
              //PostLogText(&lTmp); PostLogText(S_CRLF);
              //
              break;
            case SCANS_IPCONNOK:                                           // 5912
              if (lParam < IPCONNCOUNT) {
                //aDescr->sSCIPCONN[lParam].sCDConnected = TRUE;
                DoIPConnChangeState(aDescr, lParam, TRUE);
                //PostLogText("Connected !!!"S_CRLF);
              }
              //
              break;
            case SCANS_IPSHUT:
              DoIPConnChangeState(aDescr, CTLCONNID, FALSE);
              DoIPConnChangeState(aDescr, TNLCONNID, FALSE);
              aDescr->sSCIPOpenClose = FALSE;
              aDescr->sSCSelfIPAddr = 0;
              CommandHandled(aDescr, lAnswer);
              break;
            case SCANS_IPCLOSED: ;
            case SCANS_IPCLOSEOK: ;
            case SCANS_IPCONNFAIL:
              if (lParam < IPCONNCOUNT) {
                //aDescr->sSCIPCONN[lParam].sCDConnecting = FALSE;
                DoIPConnChangeState(aDescr, lParam, FALSE);
                //aDescr->sSCIPCONN[lParam].sCDConnected = FALSE;
                //aDescr->sSCIPCONN[lParam].sCDDisconnTime = TICKS;
                //PostLogText("Disconnected !!!"S_CRLF);
              }
              //
              break;
            case SCANS_SIGLEVEL:
              lIndex = SIGLEVEL_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen))
                aDescr->sSCCurLevel = Str2IntDef(&aMessage[lParPos], lParLen, 0);
              else aDescr->sSCCurLevel = 0;
              //
              break;
            case SCANS_SMSOK:
              lIndex = SMSOK_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                aDescr->sSCOutSMSID = Str2IntDef(&aMessage[lParPos], lParLen, ERROR_RESULT);
              }
              break;
            case SCANS_SMSCONFIRM:
              lIndex = SMSCONFIRM_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen) &&
                  GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen))
                lIntPar = Str2IntDef(&aMessage[lParPos], lParLen, ERROR_RESULT);
                DoSMSConfirm(aDescr, lIntPar);
              break;
            case SCANS_VOLTAGE:
              lIndex = VOLTAGE_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen) &&
                  GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen) &&
                  GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                aDescr->sSCCurVoltage = (Str2IntDef(&aMessage[lParPos], lParLen, 0)+5)/10;
              }
              else aDescr->sSCCurVoltage = 0;
              //PostLogText( Int2StrZ(aDescr->sSCCurVoltage, &lTmp, 3)); PostLogText(S_CRLF);
              //
              break;
            case SCANS_SIMOPER:
              lIndex = SIMOPER_PARPOS;
              //PostLogText("SCANS_SIMOPER"S_CRLF);
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen))
                aDescr->sSCProvIndex = GetCProvIndex(&aMessage[lParPos], lParLen);
              else aDescr->sSCProvIndex = ERROR_RESULT;
              //
              break;
            case SCANS_SMSRCV:
              aDescr->sSCInSMSRcv = TRUE;
              lIndex = INSMS_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                MemCopy(&aMessage[lParPos], &aDescr->sSCInNumber, lParLen);
                aDescr->sSCInNumber[lParLen] = 0;
              }
              break;
            case SCANS_SMSLIST:
              lIndex = SMSLIST_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen) &&
                  GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen) &&
                  SubstrComp("REC UNREAD", &aMessage[lParPos], lParLen) &&
                  GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                MemCopy(&aMessage[lParPos], &aDescr->sSCInNumber, lParLen);
                aDescr->sSCInNumber[lParLen] = 0;
                aDescr->sSCInSMSRcv = TRUE;
              }
              break;

            case SCANS_INCALL:
              lIndex = INCALL_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                MemCopy(&aMessage[lParPos], &aDescr->sSCInNumber, lParLen);
                aDescr->sSCInNumber[lParLen] = 0;
              }
              if (!aDescr->sSCInCallRcv) {
                aDescr->sSCInCallRcv = TRUE;
                HandleInCall(aDescr);
              }
              break;

            case SCANS_NOCARRIER:
              if (aDescr->sSCInCallRcv) {
                aDescr->sSCInCallRcv = FALSE;
                HandleInCall(aDescr);
              }
              break;

            case SCANS_IPRCV:
              lIndex = INIP_PARPOS;
              aDescr->sSCLastCRegTicks = TICKS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                aDescr->sSCInIPConn = Str2IntDef(&aMessage[lParPos], lParLen, 0);
                if (GetInParam(aMessage, lAnsLen-1, &lIndex, &lParPos, &lParLen))
                  aDescr->sSCInIPLen = Str2IntDef(&aMessage[lParPos], lParLen, 0);
              }
              break;

            case SCANS_POWERDOWN:
              aDescr->sSCAnswerTicks = TICKS - NOANSWER_TIMESHF;
              break;

            case SCANS_USSD:
              lIndex = USSD_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen) &&
                  GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                AssignStr("USSD", aDescr->sSCInNumber);
                HandleMSG(aDescr, &aMessage[lParPos], lParLen);
              }
              break;

            case SCANS_JAMINFO:
              lIndex = JAMINFO_PARPOS;
              if (GetInParam(aMessage, lAnsLen, &lIndex, &lParPos, &lParLen)) {
                if (SubstrComp(NO_JAMMING, &aMessage[lParPos], lParLen)) lParam = 0; else
                if (SubstrComp(JAMMING_DETECTED, &aMessage[lParPos], lParLen)) lParam = 1; else
                if (SubstrComp(INTERFERENCE_DETECTED, &aMessage[lParPos], lParLen)) lParam = 2; else lParam = 3;
                if (lParam <= 2)
                  DoChangeJammingState(aDescr, lParam);
              }
              break;
          }
        }
    }
    lAnsLen++;
    if (aMessage[lAnsLen] == 10) lAnsLen++;
    aMessage = &aMessage[lAnsLen];
    aLength -= lAnsLen;
    aDescr->sCSWasAnswer = TRUE;
    //aDescr->sSCRcvWaitTicks = TICKS;
  }
}

bool CheckNextIPStart(SIMCOM_DESCR_TypeDef *aDescr)
{
  int lCount;
  uint8_t lSelConn;
  //
  if ((aDescr->sSCIPConnMask != 0) && (aDescr->sSCSelfIPAddr > 0) &&
      IsTimeOut(TICKS, aDescr->sSCConnDisconnTicks, RECONN_TIMEOUT)) {
    lSelConn = aDescr->sSCLastConnDisconn + 1;
    for (lCount=0;lCount<IPCONNCOUNT;lCount++) {
      if (lSelConn >= IPCONNCOUNT) lSelConn = 0;
      if (( (aDescr->sSCIPConnMask & (1 << lSelConn)) != 0) &&
          (!aDescr->sSCIPCONN[lSelConn].sCDConnected) &&
          (!aDescr->sSCIPCONN[lSelConn].sCDConnDisconn)) {
        aDescr->sSCIPSelConn = lSelConn;
        aDescr->sSCLastConnDisconn = lSelConn;
        return TRUE;
      }
      lSelConn++;
    }
  }
  return FALSE;
}

bool CheckNextIPStop(SIMCOM_DESCR_TypeDef *aDescr)
{
  int lCount;
  uint8_t lSelConn;
  //
  if ((aDescr->sSCSelfIPAddr > 0) &&
      IsTimeOut(TICKS, aDescr->sSCConnDisconnTicks, RECONN_TIMEOUT)) {
    lSelConn = aDescr->sSCLastConnDisconn + 1;
    for (lCount=0;lCount<IPCONNCOUNT;lCount++) {
      if (lSelConn >= IPCONNCOUNT) lSelConn = 0;
      if (( (aDescr->sSCIPConnMask & (1 << lSelConn)) == 0) &&
          (aDescr->sSCIPCONN[lSelConn].sCDConnected) &&
          (!aDescr->sSCIPCONN[lSelConn].sCDConnDisconn)) {
        aDescr->sSCIPSelConn = lSelConn;
        aDescr->sSCLastConnDisconn = lSelConn;
        return TRUE;
      }
      lSelConn++;
    }
  }
  return FALSE;
}

bool CheckNextIPSend(SIMCOM_DESCR_TypeDef *aDescr)
{
  int lCount;
  uint8_t lSelConn;
  //
  if ((aDescr->sSCIPAvailable) && (aDescr->sSCIPSending == EMPTYCONNID) &&
      (!aDescr->sSCSMSSending)) {
    lSelConn = aDescr->sSCLastIPSending + 1;
    for (lCount=0;lCount<IPCONNCOUNT;lCount++) {
      if (lSelConn >= IPCONNCOUNT) lSelConn = 0;
      if ((aDescr->sSCIPCONN[lSelConn].sCDConnected) &&
          (aDescr->sSCIPCONN[lSelConn].sCDOutBufCount > 0)) {
        aDescr->sSCIPSelConn = lSelConn;
        return TRUE;
      }
      lSelConn++;
    }
  }
  return FALSE;
}

//=====================================================================================

SIMCOM_DESCR_TypeDef* SimCom_Init(SIMCOM_DESCR_TypeDef *aDescr,MY_UART_TypeDef *aUART,
  const PIN_HWCFG_TypeDef *aPowerPIN,const PIN_HWCFG_TypeDef *aCardPIN,
  const PIN_HWCFG_TypeDef *aResetPIN)
{
  if (aDescr == NULL)
    aDescr = FixMemAlloc( sizeof(SIMCOM_DESCR_TypeDef) );
  //
  MemClear(aDescr, sizeof(SIMCOM_DESCR_TypeDef));
  aDescr->sSCUART = aUART;
  aDescr->sSCLdrMode = (ReadBackupParam(LDRMODECODE_BKP_INDEX) == LDRMODECODE);
  WriteBackupParam(LDRMODECODE_BKP_INDEX, 0);
  //
  MyOUT_Init(&aDescr->sSCPower, aPowerPIN);
  //aDescr->sSCPower.sOUPS.sPSInverseSet = TRUE;
  ((MY_PESTATE_TypeDef*)&aDescr->sSCPower)->sPSOnStop = (MYHANDLER_CALLBACK)EndOfPowerSwitch;
  ((MY_PESTATE_TypeDef*)&aDescr->sSCPower)->sPSData = aDescr;
  PIN_Set(aDescr->sSCPower.sHWCFG, FALSE);      
  //
  MyOUT_Init(&aDescr->sSCReset, aResetPIN);
  aDescr->sSCReset.sOUPS.sPSInverseSet = TRUE;
  PIN_Set(aDescr->sSCReset.sHWCFG, TRUE);
  //
  aDescr->sSCCardPIN = aCardPIN;
  PIN_Config(aCardPIN);
  //
  //AssignStr(STDALARMSMSNUM, &aDescr->sSCOutSMSNumber);
  //aDescr->sSCUseIPMode = USEIPMODE;
  //
  aDescr->sSCLastProvIndex = UNKNOWNCPROV;
  //
  aDescr->sSCIPCONNFX[CTLCONNID].sCDAddr = ReadUIntVar(GIP, STDCTLIPADDR);
  aDescr->sSCIPCONNFX[CTLCONNID].sCDPort = ReadUIntVar(GPORT, STDCTLIPPORT);

  aDescr->sSCIPCONNFX[CTLCONNID].sCDOutBuf = FixMemAlloc( CTLBUFSIZE );
  aDescr->sSCIPCONNFX[CTLCONNID].sCDOutBufSize = CTLBUFSIZE;
  //
  aDescr->sSCIPCONNFX[TNLCONNID].sCDAddr = ReadUIntVar(TNLIP, STDTNLIPADDR);
  aDescr->sSCIPCONNFX[TNLCONNID].sCDPort = ReadUIntVar(TNLPORT, STDTNLIPPORT);
  aDescr->sSCIPCONNFX[TNLCONNID].sCDOutBuf = FixMemAlloc( TNLBUFSIZE );
  aDescr->sSCIPCONNFX[TNLCONNID].sCDOutBufSize = TNLBUFSIZE;
  //
  aDescr->sSCMode = ReadUIntVar(GMODE, DEFGMODE);
  if (aDescr->sSCMode == 1) aDescr->sSCIPConnMask = 1; else
  if (aDescr->sSCMode == 2) aDescr->sSCIPConnMask = 0;
  aDescr->sSCSecondSIM  = ReadUIntVar(SIMCARD, DEFSIMCARD);
  ReadJamParams(aDescr);
  //
  aDescr->sSCAnswerTicks = TICKS - NOANSWER_TIMESHF;
  aDescr->sSCQTicks = TICKS;
  /*
  if (!WasWDGStart())
    aDescr->sSCShdlCommand = SCCMD_PWRDOWN;
  else aDescr->sSCShdlCommand = SCCMD_PWRDOWN;//SCCMD_INIT;
  */
  if (aDescr->sSCLdrMode) PIN_Set(aPowerPIN, TRUE);
  else PostCommand(aDescr, SCCMD_PWRDOWN);
  //
  return aDescr;
}

void SimCom_PostSimLdrMode(SIMCOM_DESCR_TypeDef *aDescr)
{
  WriteBackupParam(LDRMODECODE_BKP_INDEX, LDRMODECODE);
  PostLogText("  !!! SIM Loader mode will be activated after reboot !!!"S_CRLF);
}

void SimCom_Process(SIMCOM_DESCR_TypeDef *aDescr)
{
  char lRxStr[SIMBUFFERSIZE+4];
  int lReadCount;
  //
  if (aDescr->sSCMode == 0) return;
  //
  if (aDescr->sSCLdrMode) { /*
    if (MyUSART_Read(aDescr->sSCLDRUSART, lRxStr, SIMBUFFERSIZE,
          SCREAD_WAIT_TIME, &lReadCount)) {
      MyUSART_Write(aDescr->sSCUSART, lRxStr, lReadCount);
    }
    if (MyUSART_Read(aDescr->sSCUSART, lRxStr, SIMBUFFERSIZE,
          SCREAD_WAIT_TIME, &lReadCount)) {
      MyUSART_Write(aDescr->sSCLDRUSART, lRxStr, lReadCount);
    }
    return; */
  }
  MyPS_Process(&aDescr->sSCPower);
  MyPS_Process(&aDescr->sSCReset);
  //
  if (MyCBuf_Read(aDescr->sSCUART->sLRx, lRxStr, SIMBUFFERSIZE,
        SCREAD_WAIT_TIME, &lReadCount)) {
    HandleAnswer(aDescr, lRxStr, lReadCount);
  }
  /*
  if (CondLogText( IsTimeOutEx(TICKS, &aDescr->sSCRcvWaitTicks, RECEIVEWAIT_TIMEOUT), "Reset by RcvWait timeout!")) {
    DoReset(aDescr);
    return;
  }*/
  if (aDescr->sSCPowerSwitching) return;
  //
  if ((!aDescr->sSCReady) && (!aDescr->sSCNeedSIMChange) && IsTimeOut(TICKS, aDescr->sSCQTicks, 2500))
    PostCommand(aDescr, SCCMD_TEST);
  //
  if ((aDescr->sSCReady) && (!aDescr->sSCNeedSIMChange) && (aDescr->sSCWaitCommand == SCCMD_NONE)) {
    //
    if ((aDescr->sSCIPConnMask != 0) && (aDescr->sSCIPAvailable) && (aDescr->sSCSelfIPAddr == 0) &&
        (!aDescr->sSCIPOpenClose) && IsTimeOut(TICKS, aDescr->sSCIPOpenCloseTicks, 3000))
      PostCommand(aDescr, SCCMD_IPOPEN); else
    //
    if ((aDescr->sSCIPConnMask == 0) && (aDescr->sSCSelfIPAddr != 0) &&
        (!aDescr->sSCIPOpenClose) && IsTimeOut(TICKS, aDescr->sSCIPOpenCloseTicks, 3000))
      PostCommand(aDescr, SCCMD_IPCLOSE); else
    //
    if (CheckNextIPStart(aDescr)) PostCommand(aDescr, SCCMD_IPSTART); else
    //
    if (CheckNextIPStop(aDescr)) PostCommand(aDescr, SCCMD_IPSTOP); else
//    if ((!aDescr->sSCSMSSending) && (!aDescr->sSCIPSending) &&
//        IsTimeOut(TICKS, aDescr->sSCLastCRegTicks, PRIOQREG_TIMEOUT)) {
//      PostCommand(aDescr, SCCMD_REGLEVEL); PostLogText("<<<REGLEVEL>>>"S_CRLF);} else
    //
    if ((aDescr->sSCRegister) &&
        (aDescr->sSCOutSMSCount != 0) && (!aDescr->sSCSMSSending) &&
        (aDescr->sSCIPSending == EMPTYCONNID) && IsTimeOut(TICKS, aDescr->sSCSMSSendTicks, 1500))
      PostCommand(aDescr, SCCMD_SMSHDR); else
    //
    if (CheckNextIPSend(aDescr)) PostCommand(aDescr, SCCMD_IPHDR); else
//    if ((aDescr->sSCIPAvailable) && (aDescr->sSCIPWaitState) &&
//        (aDescr->sSCIPSending == EMPTYCONNID) && (!aDescr->sSCSMSSending))
//      { aDescr->sSCIPSelConn = TNLCONNID; PostCommand(aDescr, SCCMD_IPHDR);} else
    //
    if ((!aDescr->sSCSMSSending) && (aDescr->sSCIPSending == EMPTYCONNID))
      if (IsTimeOut(TICKS, aDescr->sSCQTicks, 3500))
        if (aDescr->sSCShdlCommand != SCCMD_NONE) {
          PostCommand(aDescr, aDescr->sSCShdlCommand);
          aDescr->sSCShdlCommand = SCCMD_NONE;
        }
        else
          PostCommand(aDescr, SCCMD_REGLEVEL);
  }
  if ((aDescr->sSCLongWaitCTicks != 0) &&
       CondLogText(   IsTimeOut(TICKS, aDescr->sSCLongWaitCTicks, LONGWAITCMD_TIMEOUT), "LongWait timeout!") ||
      (aDescr->sSCLongWaitCTicks == 0) &&
        (CondLogText( IsTimeOut(TICKS, aDescr->sSCAnswerTicks, NOANSWER_TIMEOUT), "NoAnswer timeout!") ||
       CondLogText( IsTimeOut(TICKS, aDescr->sSCLastCRegTicks, NOREG_TIMEOUT), "NoRegs timeout!"))) {
    PowerSwitch(aDescr);
    aDescr->sSCShdlCommand = SCCMD_INIT;
  }
}

void SimCom_SWSIMCard(SIMCOM_DESCR_TypeDef *aDescr,void *aParams,int aParLen)
{
  NeedRestart(aDescr, TRUE);
}

void SimCom_PostCommand(SIMCOM_DESCR_TypeDef *aDescr,SIMCOM_CMD_TypeDef aCommand)
{
  PostCommand(aDescr, aCommand);
}

bool SimCom_GetSMSBuffer(SIMCOM_DESCR_TypeDef *aDescr,char **aPBuffer,int *aBufSize)
{
  if ((aDescr->sSCRegister) && (aDescr->sSCOutSMSCount == 0)) {
    *aPBuffer = &aDescr->sSCOutSMS[0];
    *aBufSize = SIMSMSBUFSIZE;
    return TRUE;
  }
  else return FALSE;
}

bool SimCom_PostSMSBuffer(SIMCOM_DESCR_TypeDef *aDescr,char *aNumber,int aBufCount,uint16_t aFlags)
{
  if (aDescr->sSCOutSMSCount == 0) {
    if (aNumber != NULL) AssignStr(aNumber, aDescr->sSCOutSMSNumber);
    aDescr->sSCUCS2Mode = IsExtAscii(aDescr->sSCOutSMS, aBufCount);
    if (aDescr->sSCUCS2Mode) {
      Win1251ToUCS2Str(aDescr->sSCOutSMSNumber, TOEND, aDescr->sSCOutSMSNumber);
      Win1251ToUCS2Str(aDescr->sSCOutSMS, aBufCount, aDescr->sSCOutSMS);
      aBufCount <<= 2;
    }
    aDescr->sSCOutSMSCount = aBufCount;
    aDescr->sSCOutSMSFlags = aFlags;
    return TRUE;
  }
  else return FALSE;
}

bool SimCom_SendSMS(SIMCOM_DESCR_TypeDef *aDescr,char *aNumber,char *aMessage,int aCount,uint16_t aFlags)
{
  char *lBuffer;
  int lSize;
  //
  if (SimCom_GetSMSBuffer(aDescr, &lBuffer, &lSize)) {
    if (aCount > lSize) aCount = lSize;
    MemCopy(aMessage, lBuffer, aCount);
    return SimCom_PostSMSBuffer(aDescr, aNumber, aCount, aFlags);
  }
  return FALSE;
}

bool SimCom_GetIPConnBuffer(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelIPConn,
  char **aPBuffer,int *aBufSize,bool aEnbAdd)
{
  uint16_t lBufCount;
  //
  if ((aSelIPConn < IPCONNCOUNT) && aDescr->sSCIPCONN[aSelIPConn].sCDConnected) {
    lBufCount = aDescr->sSCIPCONN[aSelIPConn].sCDOutBufCount;
    if ( !aEnbAdd && (lBufCount == 0) ||
       aEnbAdd && (aDescr->sSCIPSending != aSelIPConn) ) {
      //
      *aPBuffer = &aDescr->sSCIPCONNFX[aSelIPConn].sCDOutBuf[lBufCount];
      *aBufSize = aDescr->sSCIPCONNFX[aSelIPConn].sCDOutBufSize - lBufCount;
      return TRUE;
    }
  }
  return FALSE;
}

bool SimCom_PostIPConnBuffer(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelIPConn,
  int aBufCount)
{
  if (aSelIPConn < IPCONNCOUNT) {
     //(aDescr->sSCIPCONN[aSelIPConn].sCDOutBufCount == 0)) {
    aDescr->sSCIPCONN[aSelIPConn].sCDOutBufCount += aBufCount;
    return TRUE;
  }
  else return FALSE;
}

bool SimCom_IPConnWrite(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelIPConn,
  void *aBuf,int aBufCount)
{
  if ((aSelIPConn < IPCONNCOUNT) &&
     (aDescr->sSCIPCONN[aSelIPConn].sCDOutBufCount == 0)) {
    //
    MemCopy(aBuf, aDescr->sSCIPCONNFX[aSelIPConn].sCDOutBuf, aBufCount);
    aDescr->sSCIPCONN[aSelIPConn].sCDOutBufCount = aBufCount;
    return TRUE;
  }
  else return FALSE;
}

void SimCom_Reset(SIMCOM_DESCR_TypeDef *aDescr)
{
  DoReset(aDescr);
}

//=====================================================================================

void TCHGsmMode(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  int lValue;
  aPar->sTCPResult = (aPar->sTCPCount == 0) || (aPar->sTCPCount > 0) && (aPar->sTCPVals[0] <= 2);
  if (aPar->sTCPCount > 0) {
    lValue = aPar->sTCPVals[0];
    if (lValue == 1) aDescr->sSCIPConnMask = 1; else
    if (lValue == 2) aDescr->sSCIPConnMask = 0;
    WriteUIntVar(GMODE, lValue, DEFGMODE);
  }
  else TCOutUInt(aPar, "", aDescr->sSCMode, "");
}

void TCHSwitchSIMCard(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount == 0) {
    NeedRestart(aDescr, TRUE);
    aPar->sTCPResult = TRUE;
  }
}

void TCHSIMCard(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  aPar->sTCPResult = (aPar->sTCPCount == 0) || (aPar->sTCPCount > 0) && (aPar->sTCPVals[0] <= 1);
  if (aPar->sTCPCount > 0) {
    WriteUIntVar(SIMCARD, aPar->sTCPVals[0], DEFSIMCARD);
    if (aDescr->sSCSecondSIM != aPar->sTCPVals[0]) NeedRestart(aDescr, TRUE);
  }
  else TCOutUInt(aPar, "", aDescr->sSCSecondSIM, "");
}

void TCHIPModeControl(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  aPar->sTCPResult = (aPar->sTCPCount > 0) && (aPar->sTCPVals[0] <= 1);
  if (aPar->sTCPResult) {
    aDescr->sSCIPConnMask = aPar->sTCPVals[0];
    //WriteUIntVar(GMODE, aDescr->sSCIPConnMask, DEFGMODE);
  }
}

void TCHATCommand(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount > 0) {
    MyCBuf_Write(aDescr->sSCUART->sLTx, aPar->sTCPPars[0], aPar->sTCPVals[0]);
    MyCBuf_WriteText(aDescr->sSCUART->sLTx, S_CR);
    aPar->sTCPResult = TRUE;
  }
}

void TCHSIMOff(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount == 0) {
    Delay(500);
    SimCom_PostCommand(aDescr, SCCMD_PWRDOWN);
    aPar->sTCPResult = TRUE;
  }
}

void TCHSIMReset(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount == 0) {
    Delay(500);
    DoReset(aDescr);
    aPar->sTCPResult = TRUE;
  }
}

void TCHSendSMS(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  char lBuf[16];
  int lMsgLen=0, lFlags=0;
  if (aPar->sTCPCount > 0) {
    MakeStr(aPar->sTCPPars[0], aPar->sTCPVals[0], lBuf);
    if (aPar->sTCPCount > 1) {
      lMsgLen = aPar->sTCPVals[1];
      if (aPar->sTCPCount > 2) lFlags = aPar->sTCPVals[2];
    }
    aPar->sTCPResult = SimCom_SendSMS(aDescr, lBuf, aPar->sTCPPars[1], lMsgLen, lFlags);
  }
}

// +CMT: "002B003300380030003500300033003800340030003000310030","","15/05/16,16:23:31+12"
// 002B003300380030003500300033003800340030003000310030 > Test1
// +CMGL: 1,"REC READ","+380503840010","","15/05/16,17:23:33+12"
// Test4
// +CMGL: 2,"REC READ","+380503840010","","15/05/16,17:26:01+12"
// Test6
// +CMGL: 3,"REC UNREAD","+380503840010","","15/05/16,17:35:26+12"
// 04220435043A04410442043E0432044B043500200035


// AT+CIPMUX=1  AT+CGATT?
// AT+CSTT="www.kyivstar.net";+CIICR;+CIFSR
// AT+CSTT="www.umc.ua";+CIICR;+CIFSR
// AT+CIPSTART=0,"TCP","194.246.117.90","5500"
// AT+CIPSEND=0,12  AT+CIPCLOSE  AT+CIPSHUT AT+CIPSTATUS
// "KYIVSTAR","","12/10/14,09:47:59+12" AT+CPOWD=1
// +79789468401	5400
