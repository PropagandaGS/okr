// ���������� ���������� � �������� WiFi ESP8266, ESP32
// � Olgvel, 2016-19
//
#include "my_esp.h"

#define ESP_WAIT_TIME       500
#define ESP_RCVBUF_SIZE     1024

#define ESP_START_RST_TIME  1700
#define ESP_RESET_WAIT_TIME 700

#define ESP_START_PRG_TIME  20000
#define ESP_STOP_PRG_TIME   10000

#define CQUERY              0x80

#define APPASSW             "housewell18"
#define WSIID               "WSIID"
#define WSPWD               "WSPWD"

typedef enum {
  ESA_ERROR, ESA_OK, ESA_READY, ESA_CONNECT, ESA_CLOSED, ESA_SENDOK, ESA_FAIL, ESA_NOAP, ESA_FAEXC,
  //
} ESPANS_TypeDef;

typedef enum {
  ERD_CANCEL, ERD_CRLF, ERD_DP, ERD_BKTSP,
  //
} ESPRD_TypeDef;

const char* ESP_ANSWERS =
  "\x05""ERROR\0\x02OK\0\x05ready\0\x07""CONNECT\0\x06""CLOSED\0"\
  "\x07SEND OK\0\x04""FAIL\0\x05No AP\0\x13""Fatal exception (0)\0";

/*typedef enum { ESC_NONE, ESC_IPD, ESC_SEND, ESC_SENDBUF, ESC_UART,
  ESC_EOFF, ESC_MODE, ESC_MUX, ESC_SRV, ESC_START, ESC_CLOSE,
  ESC_JAP, ESC_LAP, ESC_SAP, ESC_LIF, ESC_DHCP, ESC_AUTOCONN,
  ESC_STAMAC, ESC_STA, ESC_AP, ESC_STATUS, ESC_FSR, ESC_PING,
  //
} ESP_CMD_TypeDef;
*/

const char* ESP_COMMANDS = "\x01_\0\x04+IPD\0\x08+CIPSEND\0\x0B+CIPSENDBUF\0"\
  "\x05+UART\0\x02""E0\0\x07+CWMODE\0\x07+CIPMUX\0\x0A+CIPSERVER\0\x09+CIPSTART\0"\
  "\x09+CIPCLOSE\0\x06+CWJAP\0\x06+CWLAP\0\x06+CWSAP\0\x06+CWLIF\0\x07+CWDHCP\0"\
  "\x0B+CWAUTOCONN\0\x0A+CIPSTAMAC\0\x07+CIPSTA\0\x06+CIPAP\0"\
  "\x0A+CIPSTATUS\0\x06+CIFSR\0\x05+PING\0";

const char ESP_INIT_CMDS[9] = { ESC_EOFF, ESC_MUX, ESC_AUTOCONN, ESC_MODE,
    ESC_JAP, ESC_SAP|CQUERY, ESC_SAP, ESC_FSR, ESC_SRV };

// APIP,"192.168.4.1"  APMAC,"5e:cf:7f:f8:f2:c1"  STAIP,"192.168.227.21"  STAMAC,"5c:cf:7f:f8:f2:c1"

void EspReinit(ESP_DESCR_TypeDef *aDescr,ESP_MODE_TypeDef aMode,uint32_t aRetTime);
void EspHandleAnswer(ESP_DESCR_TypeDef *aDescr,char *aMsg,int aLength);
void EspHandleMain(ESP_DESCR_TypeDef *aDescr);
void EspHandle_IPD(ESP_DESCR_TypeDef *aDescr,char *aMsg,int aLength,int *aIndex);
void EspPostCommand(ESP_DESCR_TypeDef *aDescr,char aCmd);
bool EspLinkByIndex(ESP_DESCR_TypeDef *aDescr,int aIndex,ESP_LINK_TypeDef **aLink);
bool EspCheckConnections(ESP_DESCR_TypeDef *aDescr);
void EspCheckSendData(ESP_DESCR_TypeDef *aDescr);
void EspSetState(ESP_DESCR_TypeDef *aDescr,ESP_STATE_TypeDef aNewState);
void DoDisconnLink(ESP_LINK_TypeDef* aLink,bool aCanReconn);

//==============================================================================

ESP_DESCR_TypeDef* ESP_Init(ESP_DESCR_TypeDef *aDescr,MY_UART_TypeDef *aMyUART,
  const PIN_HWCFG_TypeDef *aEnbPIN,bool aPowerCtl,const PIN_HWCFG_TypeDef *aProgPIN)
{
  if (aDescr == NULL)
    aDescr = FixMemAlloc( sizeof(ESP_DESCR_TypeDef) );
  //
  MemClear(aDescr, sizeof(ESP_DESCR_TypeDef));
  aDescr->sESUART = aMyUART;
  aDescr->sESEnbPIN = aEnbPIN;
  aDescr->sESProgPIN = aProgPIN;
  CSET(aDescr->sESFlags, ESF_ENABLED, ReadUIntVar(WMODE, DEFWMODE));
  CSET(aDescr->sESFlags, ESF_PWRCTL, aPowerCtl);
  //
  EspReinit(aDescr, ESM_NORM,0);
  //
  return aDescr;
}

void EspReinit(ESP_DESCR_TypeDef *aDescr,ESP_MODE_TypeDef aMode,uint32_t aRetTime)
{
  ESP_LINK_TypeDef *lLink;
  PIN_Config(aDescr->sESProgPIN);
  PIN_Set(aDescr->sESProgPIN, aMode != ESM_PROG);
  PIN_Config(aDescr->sESEnbPIN);
  PIN_Set(aDescr->sESEnbPIN, !TEST_OK(aDescr->sESFlags, ESF_PWRCTL));
  //
  for (int I=0;I<ESPLINKMAX;I++)
    if (EspLinkByIndex(aDescr, I, &lLink)) DoDisconnLink(lLink, aMode==ESM_NORM);
  //
  aDescr->sESSrvPort = 5005;
  aDescr->sESStateDelay = ESP_START_RST_TIME;
  aDescr->sESCommandTicks = TICKS;
  aDescr->sESFlags &= F(ESF_ANYCONNQRY)|F(ESF_ENABLED);
  aDescr->sESRdFlags = 0;
  if (aMode!=ESM_NORM) {
    INCLUDE(aDescr->sESFlags, ESF_TRANSMODE);
    if (aMode==ESM_TRANS) PostLogText("ESP transparent mode ..."S_CRLF""S_CRLF);else
    if (aMode==ESM_PROG) PostLogText("ESP programming mode ..."S_CRLF""S_CRLF);
  }
  //
  INCLUDE(aDescr->sESFlags, ESF_PWRRST);
  INCLUDE(aDescr->sESRdFlags, ERD_CRLF);
  INCLUDE(aDescr->sESRdFlags, ERD_DP);
  EspSetState(aDescr, ESS_START);
  aDescr->sESCommand = aDescr->sESCmdAnswer = ESC_NONE;
}

const char ESP_ENDS[8] = {2, LF, CR, 1, DP, 1, '>'}; //2, ' ', '>'};

void SetupDisconnTicks(ESP_DESCR_TypeDef *aDescr)
{
  ESP_LINK_TypeDef *lLink;
  for (int I=0;I<ESPLINKMAX;I++)
    if (EspLinkByIndex(aDescr, I, &lLink))
      lLink->sELDisconnTicks = TICKS;
}

bool TestNoLinkFlag(ESP_DESCR_TypeDef *aDescr,EL_FLAG_TypeDef aFlag)
{
  ESP_LINK_TypeDef *lLink;
  for (int I=0;I<ESPLINKMAX;I++)
    if (EspLinkByIndex(aDescr, I, &lLink) && TEST_OK(lLink->sLFlags, aFlag))
      return FALSE;
  return TRUE;
}

bool SetAnyGrpFlags(ESP_LINK_TypeDef* aLink,
  EL_FLAG_TypeDef aELFlag,ESP_FLAG_TypeDef aFSFlag,bool aIsSet)
{
  if (TEST_OK(aLink->sLFlags, aELFlag) != aIsSet) {
    if (aIsSet) {
      INCLUDE(aLink->sLFlags, aELFlag);
      INCLUDE(aLink->sELESP->sESFlags, aFSFlag);
    }
    else {
      EXCLUDE(aLink->sLFlags, aELFlag);
      if (TestNoLinkFlag(aLink->sELESP, aELFlag)) EXCLUDE(aLink->sELESP->sESFlags, aFSFlag);
    }
    return TRUE;
  }
  return FALSE;
}

bool SetConnFlags(ESP_LINK_TypeDef* aLink,bool aIsSet)
{
  return SetAnyGrpFlags(aLink, ELF_CONNQRY, ESF_ANYCONNQRY, aIsSet);
}

bool SetDisconnFlags(ESP_LINK_TypeDef* aLink,bool aIsSet)
{
  return SetAnyGrpFlags(aLink, ELF_DCONNQRY, ESF_ANYDCONNQRY, aIsSet);
}

bool SetActiveFlags(ESP_LINK_TypeDef* aLink,bool aIsSet)
{
  return SetAnyGrpFlags(aLink, ELF_ACTIVE, ESF_ANYACTIVE, aIsSet);
}

void DoDisconnLink(ESP_LINK_TypeDef* aLink,bool aCanReconn)
{
  if (SetActiveFlags(aLink, FALSE)) {
    EXCLUDE(aLink->sLFlags, ELF_WRIBSY);  
    MHandler_Call(&aLink->sELOnDisconnected, aLink);
  }  
  aLink->sELDisconnTicks = TICKS;
  if (aCanReconn && ((aLink->sELMode == ELM_UDP)||(aLink->sELMode == ELM_TCPC)) &&
      (aLink->sELReconnDelay > 0))
    SetConnFlags(aLink, TRUE);
  MyCBuf_Clear(aLink->sLTx); MyCBuf_Clear(aLink->sLRx);
}

void ViewRdBuf(ESP_DESCR_TypeDef *aDescr)
{
  MY_CBUF_TypeDef *lSrc;
  lSrc = aDescr->sESUART->sLRx;
  PostLogInt("SAdd=", lSrc->sCAddPos, ", ");
  PostLogInt("Del=", lSrc->sCDelPos, ", ");
  PostLogInt("Chk=", lSrc->sCChkPos, ", ");
  PostLogInt("Bsz=", lSrc->sCBufSize, S_CRLF);
}
/*
void ESP_Process(ESP_DESCR_TypeDef *aDescr)
{
  char lRxStr[ESP_RCVBUF_SIZE];
  int lRxCount;
  //
  if (IsTimeOutEx(TICKS, &aDescr->sESCommandTicks, 1500)) 
    MyCBuf_WriteText(aDescr->sESUART->sLTx, "UART5 !!!"S_CRLF);  
  //
  if (MyCBuf_Read(aDescr->sESUART->sLRx, lRxStr, ESP_RCVBUF_SIZE, 100, &lRxCount))
    MyCBuf_WriteEx(aDescr->sESUART->sLTx, lRxStr, lRxCount, TRUE);
}
*/
void ESP_Process(ESP_DESCR_TypeDef *aDescr)
{
  ESP_LINK_TypeDef *lLink;
  char lRxStr[ESP_RCVBUF_SIZE];
  int lRxCount, lRes;
  //
  if TEST_NO(aDescr->sESFlags, ESF_ENABLED) return;
  //
  if (aDescr->sESState == ESS_TRANS)
    if (aDescr->sESSubState != 0) {
      MyCBuf_MoveTo(aDescr->sESTransLink->sLRx, aDescr->sESUART->sLTx,  TOEND, FALSE, NULL);
      MyCBuf_MoveTo(aDescr->sESUART->sLRx, aDescr->sESTransLink->sLTx, TOEND, FALSE, NULL);
    }else;
  else
    if (aDescr->sESCmdAnswer == ESC_IPD) {
      if (EspLinkByIndex(aDescr, aDescr->sESRdLink, &lLink)) {
        if (!MyCBuf_MoveTo(aDescr->sESUART->sLRx, lLink->sLRx, aDescr->sESRdLen,
            FALSE, &lRxCount)) aDescr->sESRdLen -= lRxCount;
        else aDescr->sESCmdAnswer = ESC_NONE;
        //if (lRxCount > 0) { //(aDescr->sESTest < 4) {
        //  PostLogInt("after_moveto [", aDescr->sESCmdAnswer, "] ");
        //  PostLogInt("[", aDescr->sESRdLen, "]"S_CRLF);
        // }
      }
      else {
        if (MyCBuf_Read(aDescr->sESUART->sLRx, NULL, aDescr->sESRdLen, 0, &lRxCount))
          aDescr->sESRdLen -= lRxCount;
        if (aDescr->sESRdLen == 0) aDescr->sESCmdAnswer = ESC_NONE;
        //PostLogInt("after_read [", aDescr->sESCmdAnswer, "]"S_CRLF);
      }
    }
    else {
      lRes = MyCBuf_ReadFmt(aDescr->sESUART->sLRx, lRxStr, ESP_RCVBUF_SIZE, ESP_ENDS, aDescr->sESRdFlags, ESP_WAIT_TIME, &lRxCount);
      switch (lRes) {
        case RRF_TIMER:
          PostLogInt("ESP TIMER [", lRxCount, "]  ");
          PostLogMessage(lRxStr, lRxCount); PostLogText(S_CRLF);
          break;
        case RRF_INFULL:
          PostLogInt("ESP INFULL [", lRxCount, "]"S_CRLF);
          break;
        case ERD_DP:
        case ERD_CRLF:
//          EXCLUDE(aDescr->sESRdFlags, ERD_BKTSP);
          if TEST_NO(aDescr->sESFlags, ESF_ANYACTIVE) {
            PostLogInt("ESP [", lRxCount, "]  ");
            PostLogMessage(lRxStr, lRxCount); PostLogText(S_CRLF);
          }
          else if TEST_OK(aDescr->sESFlags, ESF_DEBUG) {
            MyCBuf_WriteIntEx(aDescr->sESDbgCBuf, "ESP [", lRxCount, "]  ", FALSE);
            MyCBuf_WriteEx(aDescr->sESDbgCBuf, lRxStr, lRxCount, FALSE);
            MyCBuf_WriteText(aDescr->sESDbgCBuf, S_CRLF);
          }
          // ViewRdBuf(aDescr);
          //
          if (lRxCount > 0) EspHandleAnswer(aDescr, lRxStr, lRxCount);
          if (lRes == ERD_CRLF) {
            aDescr->sESCmdAnswer = ESC_NONE;
            INCLUDE(aDescr->sESRdFlags, ERD_DP);
          }
          break;
          //
        case ERD_BKTSP:
          EXCLUDE(aDescr->sESRdFlags, ERD_BKTSP);
          //PostLogInt("ESP SEND[", lRxCount, "]"S_CRLF);
          MyCBuf_MoveTo(aDescr->sESSelLink->sLTx, aDescr->sESUART->sLTx, aDescr->sESWrLen, FALSE, NULL);
          //ViewRdBuf(aDescr);
          break;
          //
      }
    }
  EspHandleMain(aDescr);
}

ESP_LINK_TypeDef* ESP_AddLink(ESP_DESCR_TypeDef *aDescr,uint16_t aOutBufSize, uint16_t aInBufSize,
  ESP_LM_TypeDef aLinkMode)
{
  ESP_LINK_TypeDef* lLink;
  int I;
  bool lIsFind=FALSE;
  //
  if (aLinkMode == ELM_TCPS)
    for (I=0;I<ESPLINKMAX;I++)
      if (aDescr->sESLink[I] == NULL) { lIsFind = TRUE; break; } else;
  else
    for (I=ESPLINKMAX-1;I>=0;I--)
      if (aDescr->sESLink[I] == NULL) { lIsFind = TRUE; break; }
  if (lIsFind) {
    lLink = aDescr->sESLink[I] = FixMemAlloc( sizeof(ESP_LINK_TypeDef) );
    MemClear(lLink, sizeof(ESP_LINK_TypeDef));
    //
    lLink->sLRx = MyCBuf_Init(aDescr, aInBufSize);
    lLink->sLTx = MyCBuf_Init(aDescr, aOutBufSize);
    CSET(lLink->sLFlags, ELF_SERVER, aLinkMode == ELM_TCPS);
    lLink->sELID = I;
    //
    lLink->sELMode = aLinkMode;
    lLink->sELESP = aDescr;
    return lLink;
  }
  else return NULL;
}

void ESP_AssignTransLink(ESP_DESCR_TypeDef *aDescr,MY_LINK_TypeDef *aLink)
{
  aDescr->sESTransLink = aLink;
}

//==============================================================================

bool EspLinkByIndex(ESP_DESCR_TypeDef *aDescr,int aIndex,ESP_LINK_TypeDef **aLink)
{
  if ((aIndex >= 0)&&(aIndex < ESPLINKMAX)&& assigned(aDescr->sESLink[aIndex])) {
    *aLink = aDescr->sESLink[aIndex];
    return TRUE;
  }
  else return FALSE;
}

bool CheckSAPParams(ESP_DESCR_TypeDef *aDescr,char *aMsg,int aLength)
{
  return FALSE;
}

void EspHandleAnswer(ESP_DESCR_TypeDef *aDescr,char *aMsg,int aLength)
{
  ESP_LINK_TypeDef *lLink;
  int lIndex=0, lParPos, lParLen, lAnsIndex, lLength, lParA, lParB, lParC, lParD, lSIIDLen;
  char *lSIID;
  //
  if (aDescr->sESCmdAnswer == ESC_NONE) {
    if ((aLength > 0) && (aMsg[0] == '+')) {
      if ((aLength > 4) && (aMsg[4] == COMMA)) lLength = 4;else lLength = aLength;
      if (FindShortStr(aMsg, lLength, ESP_COMMANDS, &lAnsIndex, NULL)) {
        // PostLogInt("!cmd[", lAnsIndex, "] ");
        aDescr->sESCmdAnswer = (ESP_CMD_TypeDef)lAnsIndex;
        if (lAnsIndex == ESC_IPD) {
          lIndex = 5; aDescr->sESRdLink = aDescr->sESRdLen = -2;
          aDescr->sESRdAddr = aDescr->sESRdPort = 0;
          if (GetInIntParamNoDef(aMsg, aLength, &lIndex, &aDescr->sESRdLink, -1) &&
              GetInIntParamNoDef(aMsg, aLength, &lIndex, &aDescr->sESRdLen, -1) &&
              GetInIPParamNoDef(aMsg, aLength, &lIndex, &aDescr->sESRdAddr, 0) &&
              GetInIntParamNoDef(aMsg, aLength, &lIndex, &lParA, 0)) aDescr->sESRdPort = lParA;
          // PostLogInt("ipd_find[", aDescr->sESRdLink, "] "); PostLogInt("[", aDescr->sESRdLen, "] ");
        }
        else EXCLUDE(aDescr->sESRdFlags, ERD_DP);
        //
        //PostLogText(S_CRLF);
      }
    }
    else {
      //lParA = lParB = -2;
      if ((aLength > 1) && (aMsg[1] == COMMA))
        if (GetInIntParamNoDef(aMsg, aLength, &lIndex, &lParA, -1))
          GetInIntParamNoDef(aMsg, aLength, &lIndex, &lParB, -1);
      //PostLogInt("ans_find [", lIndex, "] "); PostLogInt("[", lParA, "] "); PostLogInt("[", lParB, "] ");
      //PostLogMessage(&aMsg[lIndex], aLength-lIndex); PostLogText(S_CRLF);
      if (FindShortStr(&aMsg[lIndex], aLength-lIndex, ESP_ANSWERS, &lAnsIndex, NULL))
        switch (lAnsIndex) {
          case ESA_ERROR:
          case ESA_FAIL:
            aDescr->sESCommand = ESC_NONE;
            break;
          case ESA_OK:
            //PostLogInt("Ans OK - ", aDescr->sESCommand, "  ");
            if ((aDescr->sESCommand != ESC_SEND)&&(aDescr->sESCommand != ESC_SENDBUF))
              aDescr->sESCommand = ESC_NONE;
            else INCLUDE(aDescr->sESRdFlags, ERD_BKTSP);
            //PostLogInt("", aDescr->sESCommand, S_CRLF);
            break;
          case ESA_SENDOK:
            if assigned(aDescr->sESSendLink) { 
              EXCLUDE(aDescr->sESSendLink->sLFlags, ELF_WRIBSY);  
              aDescr->sESSendLink = NULL;
            }  
            //PostLogText("SendOk detected!"S_CRLF);
            aDescr->sESCommand = ESC_NONE;
            break;
          case ESA_READY:
            if (aDescr->sESState == ESS_START)
              INCLUDE(aDescr->sESFlags, ESF_MSGRDY);
            else EspReinit(aDescr, ESM_NORM,0);
            break;
          case ESA_FAEXC:
            if (aDescr->sESState != ESS_START)
              EspReinit(aDescr, ESM_NORM,0);
            break;
          case ESA_CONNECT:
            if (EspLinkByIndex(aDescr, lParA,  &lLink)) {
              lLink->sLRx->sCAccessTicks = TICKS;
              if (SetActiveFlags(lLink, TRUE)) {
                EXCLUDE(lLink->sLFlags, ELF_WRIBSY);  
                MHandler_Call(&lLink->sELOnConnected, lLink);
              }  
            }
            break;
          case ESA_CLOSED:
            if (EspLinkByIndex(aDescr, lParA,  &lLink)) DoDisconnLink(lLink, TRUE);
            break;
        }
    }
  }
  else
    switch (aDescr->sESCmdAnswer) {
      case ESC_UART:
        break;
      case ESC_SAP:
        // PostLogMsg("", aMsg, aLength, S_CRLF);
        if (GetInParam(aMsg, aLength, &lIndex, &lParPos, &lSIIDLen)) {
          lSIID = &aMsg[lParPos];
          if (GetInParam(aMsg, aLength, &lIndex, &lParPos, &lParLen) &&
              GetInIntParamNoDef(aMsg, aLength, &lIndex, &lParA, -1) &&
              GetInIntParamNoDef(aMsg, aLength, &lIndex, &lParB, -1) &&
              GetInIntParamNoDef(aMsg, aLength, &lIndex, &lParC, -1) &&
              GetInIntParamNoDef(aMsg, aLength, &lIndex, &lParD, -1) ) {
            if ( SubstrComp("HW_", lSIID, 3) &&
                SubstrComp(APPASSW, &aMsg[lParPos], lParLen) && (lParB == 3) ) {
              aDescr->sESSubState++;
              PostLogText("Std AP Param!"S_CRLF);
            }
            else {
              if (lSIIDLen > 6)
                aDescr->sESSubMac = Hex2IntDef(&lSIID[lSIIDLen-6], 6, 0);
              aDescr->sESAPChan = lParA;
              PostLogHex("Non std AP Param: ", aDescr->sESSubMac,4, S_CRLF);
            }
          }
        }
        // if (CheckSAPParams(aDescr, aMsg, aLength));
        break;
      case ESC_FSR:
        // PostLogMsg("", aMsg, aLength, S_CRLF);
        if (GetInParam(aMsg, aLength, &lIndex, &lParPos, &lParLen))
          if (SubstrComp("STAIP", &aMsg[lParPos], lParLen))
            GetInIPParamNoDef(aMsg, aLength, &lIndex, &aDescr->sESSTAIP, 0);
          else
            if (SubstrComp("APIP", &aMsg[lParPos], lParLen))
              GetInIPParamNoDef(aMsg, aLength, &lIndex, &aDescr->sESAPIP, 0);
        break;
    }
}

void EspSetState(ESP_DESCR_TypeDef *aDescr,ESP_STATE_TypeDef aNewState)
{
  aDescr->sESState = aNewState;
  aDescr->sESSubState = 0;
}

void DoTransLink(ESP_DESCR_TypeDef *aDescr,uint32_t aState)
{
  MHandler_Call(&aDescr->sESOnTransLink, (void*)aState);
}

void DoAfterInit(ESP_DESCR_TypeDef *aDescr)
{
  MHandler_Call(&aDescr->sESOnAfterInit, (void*)NULL);
}

void EspHandleMain(ESP_DESCR_TypeDef *aDescr)
{
  if ((aDescr->sESCommand == ESC_NONE) && (aDescr->sESCmdAnswer == ESC_NONE))
    switch (aDescr->sESState) {
      case ESS_START:
        if TEST_NO(aDescr->sESFlags, ESF_MSGRDY)
          if (IsTimeOutEx(TICKS, &aDescr->sESCommandTicks, aDescr->sESStateDelay)) {
            aDescr->sESStateDelay = ESP_RESET_WAIT_TIME;
            CHANGE(aDescr->sESFlags, ESF_PWRRST);
            PIN_Set(aDescr->sESEnbPIN, !TEST_NO(aDescr->sESFlags, ESF_PWRRST));
            if TEST_OK(aDescr->sESFlags, ESF_TRANSMODE) EspSetState(aDescr, ESS_TRANS);
          } else;
        else
          EspSetState(aDescr, ESS_INIT);
        break;
      case ESS_INIT:
        if (aDescr->sESSubState < sizeof(ESP_INIT_CMDS))
          EspPostCommand(aDescr, ESP_INIT_CMDS[aDescr->sESSubState++]);
        else {
          //
          DoAfterInit(aDescr);
          //
          SetupDisconnTicks(aDescr);
          EspSetState(aDescr, ESS_WORK);
        }
        break;
      case ESS_TRANS:
        if (aDescr->sESSubState == 0) {
          //
          DoTransLink(aDescr, TRUE);
          aDescr->sESTransLink->sLRx->sCAccessTicks = TICKS;
          aDescr->sESStateDelay = aDescr->sESModeRetDelay; // ESP_START_PRG_TIME;
          aDescr->sESSubState++;
        }
        else
          if (IsTimeOut(TICKS, aDescr->sESTransLink->sLRx->sCAccessTicks, aDescr->sESStateDelay)) {
            DoTransLink(aDescr, FALSE);
            PostLogText("ESP restore"S_CRLF);
            EspReinit(aDescr, ESM_NORM,0);
          }
        break;
      case ESS_WORK:
        if (!EspCheckConnections(aDescr))
          EspCheckSendData(aDescr);
        break;
    }
}

void EspPostCommand(ESP_DESCR_TypeDef *aDescr,char aCmd)
{
  MY_CBUF_TypeDef *lTx;
  ESP_LINK_TypeDef *lLink;
  //
  char *lCmdStr, *lWSIID, *lWSPWD;
  int lWSIIDLen, lWSPWDLen;
  bool lQuery;
  //
  if (aDescr->sESCommand != ESC_NONE)
    PostLogText("ESP is Busy"S_CRLF);
  //
  lQuery = GET_F(aCmd, CQUERY); aCmd &= ~CQUERY;
  if (GetShortStr(aCmd, ESP_COMMANDS, &lCmdStr, NULL)) {
    //PostLogTxt("AT", lCmdStr, " ");
    lTx = aDescr->sESUART->sLTx;
    lLink = aDescr->sESSelLink;
    MyCBuf_WriteTxtEx(lTx, "AT", lCmdStr, "", FALSE, FALSE);
    if (lQuery) MyCBuf_WriteEx(lTx, "?", 1, FALSE);
    else {
      if ((aCmd != ESC_EOFF)&&(aCmd != ESC_FSR)) MyCBuf_WriteEx(lTx, "=", 1, FALSE);
      switch (aCmd) {
        case ESC_AUTOCONN: MyCBuf_WriteEx(lTx, "0", 1, FALSE); break;
        case ESC_MODE: MyCBuf_WriteEx(lTx, "3", 1, FALSE); break;
        case ESC_MUX: MyCBuf_WriteEx(lTx, "1", 1, FALSE); break;
        case ESC_SAP:
          MyCBuf_WriteHexEx(lTx, S_QUOTE"HW_", aDescr->sESSubMac, 3, S_QUOTE, FALSE);
          MyCBuf_WriteTxtEx(lTx, ",", APPASSW, "", TRUE, FALSE);
          MyCBuf_WriteIntEx(lTx, ",", aDescr->sESAPChan, ",3,4,0", FALSE);
          break;
        case ESC_SRV:
          MyCBuf_WriteIntEx(lTx, "1,", aDescr->sESSrvPort, "", FALSE);
          //
          break;
        case ESC_SEND:
        case ESC_SENDBUF:
          INCLUDE(lLink->sLFlags, ELF_WRIBSY);  
          aDescr->sESSendLink = lLink;
          MyCBuf_WriteIntEx(lTx, "", lLink->sELID, "", FALSE);
          MyCBuf_WriteIntEx(lTx, ",", aDescr->sESWrLen, "", FALSE);
          if (lLink->sELMode == ELM_UDP) {
            MyCBuf_WriteIPEx(lTx,","S_QUOTE, lLink->sELAddress, S_QUOTE, FALSE);
            MyCBuf_WriteIntEx(lTx, ",", lLink->sELPort, "", FALSE);
            /*
            PostLogInt( "", lLink->sELID, "");
            PostLogInt( ",", aDescr->sESWrLen, "");
            PostLogTextIP(","S_QUOTE, lLink->sELAddress, S_QUOTE);
            PostLogInt(",", lLink->sELPort, S_CRLF); */
          }
          break;
        case ESC_START:
          //PostLogInt("Conn Start: ", lLink->sELID, S_CRLF);
          if ((lLink->sELMode == ELM_UDP)||(lLink->sELMode == ELM_TCPC)) {
            MyCBuf_WriteIntEx(lTx, "", lLink->sELID, ","S_QUOTE, FALSE);
            if (lLink->sELMode == ELM_UDP) MyCBuf_WriteEx(lTx, "UDP", 3, FALSE);
            else MyCBuf_WriteEx(lTx, "TCP", 3, FALSE);
            MyCBuf_WriteIPEx(lTx,S_QUOTE","S_QUOTE, lLink->sELAddress, S_QUOTE, FALSE);
            MyCBuf_WriteIntEx(lTx, ",", lLink->sELPort, "", FALSE);
            if (lLink->sELMode == ELM_UDP)
              MyCBuf_WriteIntEx(lTx, ",", lLink->sELLocalPort, ",2", FALSE);
          }
          break;
        case ESC_CLOSE:
          //if TEST_OK(lLink->sLFlags, ELF_ACTIVE)
            MyCBuf_WriteIntEx(lTx, "", lLink->sELID, "", FALSE);
          break;
        case ESC_JAP:
          if (FindNamedData(WSIID, TOEND, &lWSIID, &lWSIIDLen)) {
            MyCBuf_WriteEx(lTx, S_QUOTE, 1, FALSE);
            MyCBuf_WriteEx(lTx, lWSIID, lWSIIDLen, FALSE);
            MyCBuf_WriteEx(lTx, S_QUOTE",", 2, FALSE);
          }
          else MyCBuf_WriteTxtEx(lTx, "", "lesson", ",", TRUE, FALSE);
          if (FindNamedData(WSPWD, TOEND, &lWSPWD, &lWSPWDLen)) {
            MyCBuf_WriteEx(lTx, S_QUOTE, 1, FALSE);
            MyCBuf_WriteEx(lTx, lWSPWD, lWSPWDLen, FALSE);
            MyCBuf_WriteEx(lTx, S_QUOTE, 1, FALSE);
          }
          else MyCBuf_WriteTxtEx(lTx, "", "armagedon1971", "", TRUE, FALSE);
          break;
      }
    }
    MyCBuf_WriteEx(lTx, S_CRLF, 2, TRUE);
    aDescr->sESCommand = (ESP_CMD_TypeDef)aCmd;
    aDescr->sESCommandTicks = TICKS;
    //PostLogText(S_CRLF);
  }
}

bool EspCheckConnections(ESP_DESCR_TypeDef *aDescr)
{
  ESP_LINK_TypeDef *lLink;
  int I;
  if TEST_OK(aDescr->sESFlags, ESF_ANYDCONNQRY)
    for (I=0;I<ESPLINKMAX;I++)
      if (EspLinkByIndex(aDescr, I, &lLink) && TEST_OK(lLink->sLFlags, ELF_DCONNQRY) &&
           TEST_OK(lLink->sLFlags, ELF_ACTIVE) ) {
        aDescr->sESSelLink = lLink;
        EspPostCommand(aDescr, ESC_CLOSE);
        SetDisconnFlags(lLink, FALSE);
        return TRUE;
      } else;
  if TEST_OK(aDescr->sESFlags, ESF_ANYCONNQRY)
    for (I=0;I<ESPLINKMAX;I++)
      if (EspLinkByIndex(aDescr, I, &lLink) && TEST_OK(lLink->sLFlags, ELF_CONNQRY) &&
           TEST_NO(lLink->sLFlags, ELF_ACTIVE) && (lLink->sELReconnDelay > 0) &&
           IsTimeOut(TICKS, lLink->sELDisconnTicks, lLink->sELReconnDelay)) {
        aDescr->sESSelLink = lLink;
        EspPostCommand(aDescr, ESC_START);
        SetConnFlags(lLink, FALSE);
        return TRUE;
      } else;
  return FALSE;
}

void EspCheckSendData(ESP_DESCR_TypeDef *aDescr)
{
  ESP_LINK_TypeDef *lLink;
  int16_t lBufSize, lCount = ESPLINKMAX;
  if TEST_OK(aDescr->sESFlags, ESF_ANYACTIVE)
    while (lCount-- > 0) {
      if (++aDescr->sESSendIdx == ESPLINKMAX) aDescr->sESSendIdx = 0;
      if (EspLinkByIndex(aDescr, aDescr->sESSendIdx, &lLink) && TEST_OK(lLink->sLFlags, ELF_ACTIVE)) {
        lBufSize = MyCBuf_ReadCount(lLink->sLTx);
        if (lBufSize > 0) {
          //PostLogInt("Send Buf: ", lLink->sELID, " - ");
          //PostLogInt("", lBufSize, S_CRLF);
          aDescr->sESSelLink= lLink;
          aDescr->sESWrLen = lBufSize;
          /*if (lLink->sELMode == ELM_UDP)*/ EspPostCommand(aDescr, ESC_SEND);
          /*else EspPostCommand(aDescr, ESC_SENDBUF);*/
          break;
        }
        if ((lLink->sELNoReadTime > 0) &&
            IsTimeOutEx(TICKS, &lLink->sLRx->sCAccessTicks, lLink->sELNoReadTime))
          SetDisconnFlags(lLink, TRUE);
      }
    }
}

void TCHEspCommand(ESP_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount > 0) {
    CharChange(aPar->sTCPPars[0], aPar->sTCPVals[0], APOS, QUOTE);
    MyCBuf_WriteW(aDescr->sESUART->sLTx, aPar->sTCPPars[0], aPar->sTCPVals[0]);
    MyCBuf_WriteText(aDescr->sESUART->sLTx, S_CRLF);
    //
    //PostLogMessage(aPar->sTCPPars[0], aPar->sTCPVals[0]);
    //PostLogText(S_CRLF);
    //
    aPar->sTCPResult = TRUE;
  }
}

void TCHEspReset(ESP_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  ESP_MODE_TypeDef lMode=ESM_NORM;
  //
  PostLogText("ESP Reset!!!"S_CRLF);
  if ((aPar->sTCPCount > 0)&&(aPar->sTCPVals[0] <= ESM_PROG)) {
    lMode = (ESP_MODE_TypeDef)aPar->sTCPVals[0];
    if ((aPar->sTCPCount > 1)&&(aPar->sTCPVals[1] > 0))
      aDescr->sESModeRetDelay = aPar->sTCPVals[1];
    else aDescr->sESModeRetDelay = ESP_START_PRG_TIME;
  }
  EspReinit(aDescr, lMode, 0);
  aPar->sTCPResult = TRUE;
}

void TCHWiFiMode(ESP_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  aPar->sTCPResult = (aPar->sTCPCount == 0) || (aPar->sTCPCount > 0) && (aPar->sTCPVals[0] <= 1);
  if (aPar->sTCPCount > 0) {
    WriteUIntVar(WMODE, aPar->sTCPVals[0], DEFWMODE);
  }
  else PostLogInt("", TEST_OK(aDescr->sESFlags, ESF_ENABLED), "");
}

void TCHWiFiNetAcc(ESP_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount == 2) {
    WriteNamedData(WSIID, TOEND, aPar->sTCPPars[0], aPar->sTCPVals[0]);
    WriteNamedData(WSPWD, TOEND, aPar->sTCPPars[1], aPar->sTCPVals[1]);
    //
    aPar->sTCPResult = TRUE;
  }
}

bool ESP_LinkParams(ESP_LINK_TypeDef* aLink,uint32_t aRmtAddr,uint16_t aRmtPort,uint16_t aLocalPort)
{
  if assigned(aLink) {
    aLink->sELAddress = aRmtAddr;
    aLink->sELPort = aRmtPort;
    aLink->sELLocalPort = aLocalPort;
    return TRUE;
  }
  return FALSE;
}

bool ESP_LinkConnect(ESP_LINK_TypeDef* aLink,uint32_t aReconnDelay,uint32_t aNoReadTime)
{
  if TEST_NO(aLink->sLFlags, ELF_ACTIVE) {
    aLink->sELReconnDelay = aReconnDelay;
    aLink->sELNoReadTime = aNoReadTime;
    SetConnFlags(aLink, TRUE);
    //
    return TRUE;
  }
  return FALSE;
}

bool ESP_LinkDisconnect(ESP_LINK_TypeDef* aLink,bool aReconnReset)
{
  if TEST_OK(aLink->sLFlags, ELF_ACTIVE) {
    if (aReconnReset) aLink->sELReconnDelay = 0;
    SetDisconnFlags(aLink, TRUE);
    //
    return TRUE;
  }
  return FALSE;
}

