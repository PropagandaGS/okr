// ����������� � �������� ������ ���������
// ������� SIMCOM
// � Olgvel, 2012-2016
//
#ifndef __MY_SIMCOM_H
#define __MY_SIMCOM_H

#include "my_stm32.h"
//
#define GIP            "GIP"
#define GPORT          "GPORT"
#define TNLIP          "TNLIP"
#define TNLPORT        "TNLPORT"
#define IPMODE         "IPMODE"
#define JAMMODE        "JAMMODE"
#define JAMOUT         "JAMOUT"
#define JAMTHR         "JAMTHR"

#define SIMBUFFERSIZE  768 //512
#define SIMSMSBUFSIZE  284 //160
#define IPCONNCOUNT    2
#define CTLCONNID      0
#define TNLCONNID      1
#define EMPTYCONNID    255

#define CTLBUFSIZE     512
#define TNLBUFSIZE     128

/* Exported types ------------------------------------------------------------*/

typedef enum {
  SCCMD_NONE, SCCMD_TEST, SCCMD_INIT, SCCMD_MODEL, SCCMD_SN, SCCMD_REGLEVEL,
  SCCMD_SIMOPER, SCCMD_SMSHDR, SCCMD_SMSBODY, SCCMD_IPOPEN, SCCMD_IPSTART,
  SCCMD_IPHDR, SCCMD_IPBODY, SCCMD_IPSTOP, SCCMD_IPCLOSE,
  SCCMD_PWRDOWN, SCCMD_HANGUP, SCCMD_BALANCE, SCCMD_JAMMING, 
  //
} SIMCOM_CMD_TypeDef;

typedef enum {
  SCANS_UNKNOWN, SCANS_OK, SCANS_ERROR, SCANS_BUSY, SCANS_NOTREADY, SCANS_PDPDEACT, SCANS_CREG,
  SCANS_POWERDOWN, SCANS_PROMPT, SCANS_SMSOK, SCANS_SMSCONFIRM, SCANS_SIGLEVEL, SCANS_VOLTAGE, SCANS_SIMOPER,
  SCANS_SMSRCV, SCANS_SMSLIST, SCANS_USSD, SCANS_RING, SCANS_INCALL, SCANS_NOCARRIER, SCANS_JAMINFO,
  SCANS_IPENB, SCANS_IPRCV, SCANS_IPSENDOK, SCANS_IPSENDFAIL, SCANS_IPCONNOK, SCANS_IPCONNFAIL,
  SCANS_IPALREADYCONN, SCANS_IPCLOSED, SCANS_IPCLOSEOK, SCANS_IPSHUT, SCANS_IPADDR,
  //
} SIMCOM_ANS_TypeDef;

typedef enum {
  SCPAR_SWSIMCARD, SCPAR_TNLCONNTIMEOUT,
  //
} SIMCOM_EXTPARAM_TypeDef;

typedef struct {
  char sCPISimName[12];
  char sCPIAPN[20];
} CPROV_ITEM_TypeDef;

typedef struct {
  uint32_t sCPLCount;
  CPROV_ITEM_TypeDef sCPLItem[];
} CPROV_LIST_TypeDef;

typedef struct {
  uint32_t sCDAddr;
  uint16_t sCDPort;
  uint16_t sCDOutBufSize;
  char*    sCDOutBuf;
  //uint32_t sCDEnabled;
  NOTIFY_CALLBACK  sSDOnChangeState;
  NOTIFY_CALLBACK  sSDOnSended;
  MESSAGE_CALLBACK sSDOnRead;
} IPCONNFX_DESCR_TypeDef;

typedef struct {
  uint32_t sCDStartTime;
  uint32_t sCDDisconnTime;
  uint16_t sCDOutBufCount;
  bool     sCDConnDisconn;
  bool     sCDConnected;
} IPCONN_DESCR_TypeDef;

typedef struct {
  MY_UART_TypeDef* sSCUART;
  //MY_USART_TypeDef* sSCLDRUSART;
  const PIN_HWCFG_TypeDef* sSCCardPIN;
  MY_OUT_TypeDef    sSCPower;
  MY_OUT_TypeDef    sSCReset;
  IPCONNFX_DESCR_TypeDef sSCIPCONNFX[IPCONNCOUNT];
  IPCONN_DESCR_TypeDef   sSCIPCONN[IPCONNCOUNT];
  char sSCOutSMSNumber[64];
  int16_t sSCProvIndex;
  int16_t sSCLastProvIndex;
  //
  uint32_t sSCQTicks;
  uint32_t sSCAnswerTicks;
  //uint32_t sSCRcvWaitTicks;
  //uint32_t sSCCRegTicks;
  uint32_t sSCLastCRegTicks;
  uint32_t sSCLongWaitCTicks;
  uint32_t sSCPowerSwitchCount;
  char     sSCInNumber[16];
  char     sSCOutSMS[SIMSMSBUFSIZE];
  //
  SIMCOM_CMD_TypeDef sSCWaitCommand;
  SIMCOM_CMD_TypeDef sSCShdlCommand;
  uint8_t  sSCMode;
  bool     sSCPowerSwitching;
  bool     sCSWasAnswer;
  bool     sSCReady;
  bool     sSCRegister;
  bool     sSCSMSSending;
  uint8_t  sSCOutSMSCount;
  uint8_t  sSCCurLevel;
  SIMCOM_ANS_TypeDef sSCLastResult;
  bool     sSCLdrMode;
  bool     sSCUCS2Mode;
  bool     sSCSecondSIM;
  bool     sSCNeedSIMChange;
  bool     sSCIPAvailable;
  bool     sSCIPOpenClose;
  uint8_t  sSCIPConnMask;
  uint8_t  sSCIPSelConn;
  uint8_t  sSCIPSending;
  uint8_t  sSCLastIPSending;
  uint8_t  sSCLastConnDisconn;
  bool     sSCInSMSRcv;
  bool     sSCInCallRcv;
  uint8_t  sSCJamMode;
  uint8_t  sSCJamThresh;
  uint8_t  sSCJamState;
  uint8_t  sSCOutSMSErrors;
  uint8_t  sSCOutIPErrors;
  uint8_t  sSCInIPConn;
//    uint8_t  sSCIPWaitState;
  uint16_t sSCCurVoltage;
  uint16_t sSCInIPLen;
  int16_t  sSCOutSMSID;
  uint16_t  sSCOutSMSFlags;
  uint32_t sSCSMSSendTicks;
  uint32_t sSCIPSendTicks;
  uint32_t sSCIPOpenCloseTicks;
  uint32_t sSCConnDisconnTicks;
  uint32_t sSCSelfIPAddr;
  NOTIFY_CALLBACK sSCOnChangeRegister;
  NOTIFY_CALLBACK sSCOnChangeOperator;
  NOTIFY_CALLBACK sSCOnSMSNotify;
  NOTIFY_CALLBACK sSCOnSMSConfirm;
  NOTIFY_CALLBACK sSCOnJamming;
  ADDRMESSAGE_CALLBACK sSCOnMsgReceive;
  ADDRSTATE_CALLBACK sSCOnInCall;
  //
} SIMCOM_DESCR_TypeDef;

#ifdef __cplusplus
extern "C" {
#endif

#define TCN_GMODE        "\x06+GMODE\0"
#define TCH_GMODE        {(TCHANDLER)TCHGsmMode,TCFINT,ACX_SIMCOM}
void TCHGsmMode(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_SWSIMCARD    "\x0A+SWSIMCARD\0"
#define TCH_SWSIMCARD    {(TCHANDLER)TCHSwitchSIMCard,TCFINT,ACX_SIMCOM}
void TCHSwitchSIMCard(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_SIMCARD      "\x08+SIMCARD\0"
#define TCH_SIMCARD      {(TCHANDLER)TCHSIMCard,TCFINT,ACX_SIMCOM}
void TCHSIMCard(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_ATC          "\x04+ATC\0"
#define TCH_ATC          {(TCHANDLER)TCHATCommand,TCFSTR,ACX_SIMCOM}
void TCHATCommand(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_SIMOFF       "\x07+SIMOFF\0"
#define TCH_SIMOFF       {(TCHANDLER)TCHSIMOff,0,ACX_SIMCOM}
void TCHSIMOff(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_SIMRESET     "\x09+SIMRESET\0"
#define TCH_SIMRESET     {(TCHANDLER)TCHSIMReset,0,ACX_SIMCOM}
void TCHSIMReset(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#define TCN_SMS          "\x04+SMS\0"
#define TCH_SMS          {(TCHANDLER)TCHSendSMS,(((TCFINT<<TCFBITS)|TCFSTR)<<TCFBITS)|TCFSTR,ACX_SIMCOM}
void TCHSendSMS(SIMCOM_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

#ifdef __cplusplus
}
#endif

/* Exported functions ------------------------------------------------------- */

SIMCOM_DESCR_TypeDef* SimCom_Init(SIMCOM_DESCR_TypeDef *aDescr,MY_UART_TypeDef *aUART,
  const PIN_HWCFG_TypeDef *aPowerPIN,const PIN_HWCFG_TypeDef *aCardPIN,const PIN_HWCFG_TypeDef *aResetPIN);
void SimCom_Process(SIMCOM_DESCR_TypeDef *aDescr);
//
void SimCom_SWSIMCard(SIMCOM_DESCR_TypeDef *aDescr,void *aParams,int aParLen);
//
bool SimCom_GetSMSBuffer(SIMCOM_DESCR_TypeDef *aDescr,char **aPBuffer,int *aBufSize);
bool SimCom_PostSMSBuffer(SIMCOM_DESCR_TypeDef *aDescr,char *aNumber,int aBufCount,uint16_t aFlags);
bool SimCom_SendSMS(SIMCOM_DESCR_TypeDef *aDescr,char *aNumber,char *aMessage,int aCount,uint16_t aFlags);
//
bool SimCom_GetIPConnBuffer(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelIPConn,
  char **aPBuffer,int *aBufSize,bool aEnbAdd);
bool SimCom_PostIPConnBuffer(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelIPConn,int aBufCount);
//
bool SimCom_IPConnWrite(SIMCOM_DESCR_TypeDef *aDescr,uint8_t aSelIPConn,
  void *aBuf,int aBufCount);
//
void SimCom_PostCommand(SIMCOM_DESCR_TypeDef *aDescr,SIMCOM_CMD_TypeDef aCommand);
//
void SimCom_PostSimLdrMode(SIMCOM_DESCR_TypeDef *aDescr);
void SimCom_Reset(SIMCOM_DESCR_TypeDef *aDescr);

#endif /* __MY_SIMCOM_H */

