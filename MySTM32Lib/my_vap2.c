// ���������� ��������� VAP2
// ����������� � �������� ��������� VAP2
// � Olgvel, 2011-2013
//
#include "my_vap2.h"
#include "my_crc32.h"

#define VAPMASTERPREAMB  0x5D24C36A
#define VAPSLAVEPREAMB   0x5A28C96D
#define CRCMASK          0xA001
#define CRCBASE          0xFFFF

#define CRC32SIZE        sizeof(uint32_t)
#define VFFLAGSMASK      0x07FF


#define CMD_UNKNOWN      1
#define CMD_DEVCONTROL   7
#define CMD_EVUPDATE     9
#define CMD_EVUPDATEANS  10
#define CMD_TUNNELDATA   11
//

/*
bool VapReadFrame(VAP_Descr_TypeDef *aVapDescr);
void VapHandleFrame(VAP_Descr_TypeDef *aVapDescr);
void VapFrameSetup(VAP_Descr_TypeDef *aVapDescr);
void VapBlockAdd(VAP_Descr_TypeDef *aVapDescr,uint16_t aCommand,
  void *aBlock,uint16_t aBlockSize);
void VapFramePost(VAP_Descr_TypeDef *aVapDescr,uint16_t aNumber);
*/
//==============================================================================
// http://www.radioelectronika.ru/?mod=cxemi&sub_mod=full_cxema&id=834   - �����
//
// 25A724782F9371005A5A5A5A24003B00174730353033353500000000
// 25A724782F9371005A5A5A5A9C99AF03D78A9429305D13D000000000
// 25A724782F9371005A5A5A5A9C99AF03D78A9429305D13D007703329
//
// 389C7D0F2F1371805A5A5A5A24003B001747303530333535 36EE44D7
// 389C7D0F2F1371805A5A5A5A24003B001747303530333535 B1192CD5
// 389C7D0F2F1371805A5A5A5A24003B001747303530333535 B1192CD5
// 389C7D0F2F1371805A5A5A5A24003B001747303530333535 535D3E35

void HashBlock(void *aBlock,uint32_t aBlockLen,uint32_t aHashMask)
{
  uint32_t *lItem;
  uint32_t lMod;
  //
  lMod = aBlockLen & 3; aBlockLen >>= 2; lItem = aBlock;
  while (aBlockLen-- > 0) {
    aHashMask = (aHashMask << 3) | (aHashMask >> 29);
    *lItem++ ^= aHashMask;
  }
  if (lMod > 0) {
    aHashMask = (aHashMask << 3) | (aHashMask >> 29);
    *lItem++ ^= (aHashMask & (0xFFFFFFFF >> ((4-lMod) << 3)));
  }
}

void VAP2_BuildFrame(VAP_SENDER_TypeDef aSender,uint32_t aDevID,uint16_t aFrameNum,
  uint16_t aFlags,void *aData,uint16_t aDataLen,void *aFrame,uint16_t *aFrameLen)
{
  VAP_Frame_TypeDef *lFrame;
  uint32_t lHashMask, lCRC32;
  uint16_t lFrameLen;
  //
//PostLogTextDly("vap_bf_1"S_CRLF, 5);
  lFrame = aFrame;
  lFrameLen = VAPFRAMESIZE + aDataLen;
  lFrame->sVFPreaMask = Random32();
  if (aSender == VSK_MASTER) lHashMask = lFrame->sVFPreaMask ^ VAPMASTERPREAMB;
  else lHashMask = lFrame->sVFPreaMask ^ VAPSLAVEPREAMB;
  lFrame->sVFPreamb = lHashMask ^ aDevID;
  lFrame->sVFLenFlags = lFrameLen & VFFLAGSMASK | aFlags & ~VFFLAGSMASK;
  lFrame->sVFNumFlags = aFrameNum & VFFLAGSMASK | aFlags & ~VFFLAGSMASK;
  *(uint32_t*)(&lFrame->sVFLenFlags) ^= lHashMask;
//PostLogTextDly("vap_bf_2"S_CRLF, 5);  
  if (((uint32_t)aFrame+VAPFRAMESIZE) != (uint32_t)aData)
    MemCopy(aData, &lFrame->sVFData, aDataLen);
//PostLogTextDly("vap_bf_3"S_CRLF, 5);    
  //*(uint32_t*)((uint32_t)(aFrame)+lFrameLen) = CRC32eth(aFrame, lFrameLen, 0);
  lCRC32 = CRC32eth(aFrame, lFrameLen, 0);
  MemCopy(&lCRC32, (void*)((uint32_t)aFrame+lFrameLen), sizeof(uint32_t));
//PostLogTextDly("vap_bf_4"S_CRLF, 5);      
  HashBlock(&lFrame->sVFData, aDataLen + CRC32SIZE, lHashMask); //align32
  *aFrameLen = lFrameLen + CRC32SIZE;
//PostLogTextDly("vap_bf_5"S_CRLF, 5);      
}

bool VAP2_ParseFrame(void *aFrame,uint16_t aFrameLen,VAP_SENDER_TypeDef aSenderFlt,
  uint32_t aDevID, VAP_SENDER_TypeDef *aSender,uint16_t *aFrameNum,uint16_t *aFlags,
  void *aData,uint16_t *aDataLen,int *aRetPos)
{
  VAP_Frame_TypeDef *lFrame;
  UINT32_U lUnion;
  uint32_t lHashMask, lCRC32;
  uint16_t lFrameLen, lFlags, lDataLen;
  //
  if (aFrameLen < VAPFRAMESIZE) { *aRetPos = aFrameLen; return FALSE; }
  lFrame = aFrame; *aRetPos = ERROR_RESULT;
  //
  lHashMask = lFrame->sVFPreamb ^ aDevID;
  if (aSenderFlt == VSK_AUTO) {
    if ((lHashMask ^ lFrame->sVFPreaMask) == VAPMASTERPREAMB) aSenderFlt = VSK_MASTER; else
    if ((lHashMask ^ lFrame->sVFPreaMask) == VAPSLAVEPREAMB) aSenderFlt = VSK_SLAVE; else
    return FALSE;
  } else
  if (aSenderFlt == VSK_MASTER)
    if ((lHashMask ^ lFrame->sVFPreaMask) != VAPMASTERPREAMB) return FALSE; else; else
  if (aSenderFlt == VSK_SLAVE)
    if ((lHashMask ^ lFrame->sVFPreaMask) != VAPSLAVEPREAMB) return FALSE; else; else
  return FALSE;
//PostLogTextDly("vap_pf_1"S_CRLF, 10);
  //
  lUnion.Long = *(uint32_t*)(&lFrame->sVFLenFlags) ^ lHashMask;
  lFrameLen = lUnion.Words[0] & VFFLAGSMASK;
  lFlags = lUnion.Words[0] & ~VFFLAGSMASK;
  if (lFlags != (lUnion.Words[1] & ~VFFLAGSMASK)) return FALSE;
  //
  lDataLen = lFrameLen-VAPFRAMESIZE;
  if (&lFrame->sVFData != aData)
    MemCopy(&lFrame->sVFData, aData, lDataLen + CRC32SIZE);
//PostLogTextDly("vap_pf_2"S_CRLF, 5);  
  HashBlock(aData, lDataLen + CRC32SIZE, lHashMask); //align32
//PostLogTextDly("vap_pf_3"S_CRLF, 5);    
  lCRC32 = CRC32eth(lFrame, VAPFRAMESIZE, 0);
//PostLogTextDly("vap_pf_4"S_CRLF, 5);    
  //
  if (lDataLen > 0) lCRC32 = CRC32eth(aData, lDataLen, lCRC32);
//PostLogTextDly("vap_pf_5"S_CRLF, 5);      
  //if (lCRC32 != *(uint32_t*)((uint32_t)(aData)+lDataLen)) { PostLogText("exit 4"S_CRLF); return FALSE; }
  if (!MemComp((char*)&lCRC32, (char*)((uint32_t)aData+lDataLen), sizeof(uint32_t))) { PostLogText("exit 4"S_CRLF); return FALSE; }
//PostLogTextDly("vap_pf_6"S_CRLF, 5);        
  //
  *aSender = aSenderFlt;
  *aFrameNum = lUnion.Words[1] & VFFLAGSMASK;
  *aFlags = lFlags;
  *aDataLen = lDataLen;
  *aRetPos = lFrameLen + CRC32SIZE;
//PostLogTextDly("vap_pf_7"S_CRLF, 5);      
  //
  return TRUE;
}

#define LONGDATAMASK     0x8000
char* GetNextCmdPtr(int aParLen,char *aParams,int *aLength)
{
  uint16_t lPos;
  lPos = *aLength;
  if (aParLen < 128) aParams[lPos++] = aParLen;
  else { *(uint16_t*)(&aParams[lPos]) = aParLen | LONGDATAMASK; lPos += 2; }
  *aLength = lPos + aParLen;
  return &aParams[lPos];
/*
  uint32_t lLenCode, lPos;
  uint8_t  lLenCount;
  //
  SetFieldLength(aParLen, &lLenCode, &lLenCount);
  lPos = *aLength; MemCopy(&lLenCode, &aParams[lPos], lLenCount); lPos += lLenCount;
  *aLength = lPos + aParLen;
  return &aParams[lPos];  */
}

bool FindNextCmdParam(char *aParams,int aLength,int *aParPos,int *aParLen)
{
  uint16_t lPos, lLen=0;
  lPos = *aParPos + *aParLen;
  if (aLength > 1) {
    if (aParams[lPos] < 128) { lLen = aParams[lPos]; lPos++; }
    else { lLen = *(uint16_t*)(&aParams[lPos]) & ~LONGDATAMASK; lPos += 2; }
    if ((lPos + lLen) <= aLength) {
      *aParPos = lPos; *aParLen = lLen;
      return TRUE;
    }
  }
  return FALSE;
/*
  uint32_t lPos
  uint16_t lLen;
  uint8_t  lOffs;
  bool     lError;
  lPos = *aParPos + *aParLen;
  if (GetFieldLength((uint32_t)&aParams[lPos], &lLen, &lOffs, &lError)) {
    *aParPos = lPos + lOffs;
    *aParLen = lLen;
    return TRUE;
  }
  return FALSE; */
}

void AddNextCmdParam(void *aPar,int aParLen,char *aParams,int *aLength)
{
  MemCopy(aPar, GetNextCmdPtr(aParLen, aParams, aLength), aParLen);
}

bool AddAnswerState(bool aState,char *aParams,int *aLength)
{
  AddNextCmdParam(&aState, 1, aParams, aLength);
  return TRUE;
}

bool FindNextCmdIntPar(char *aParams,int aLength,int *aParPos,int *aParLen,uint32_t *aPar)
{
  if (FindNextCmdParam(aParams, aLength, aParPos, aParLen) &&
     (*aParLen <= sizeof(uint32_t))) {
    *aPar = 0;
    MemCopy(&aParams[*aParPos], aPar, *aParLen);
    return TRUE;
  }
  return FALSE;
}

bool FindNextCmdIntParDef(char *aParams,int aLength,int *aParPos,int *aParLen,uint32_t *aPar,uint32_t aDef)
{
  if (FindNextCmdIntPar(aParams, aLength, aParPos, aParLen, aPar)) return TRUE;
  *aPar = aDef;
  return FALSE;
}


