// ����������� � �������� ����������� ������������ ������
// � Olgvel, 2015-16,18
//
#include "my_rtctl.h"
#include "r_crc32.h"

#ifdef __cplusplus
extern "C" {
#endif
  
#define TCN_MCUID        "\x06+MCUID\0"
#define TCH_MCUID        {TCHMCUID}
#define BCN_MCUID        "\x05MCUID\0"
#define BCH_MCUID        {BCHMCUID}
void TCHMCUID(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHMCUID(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_MCULOCK      "\x08+MCULOCK\0"
#define TCH_MCULOCK      {TCHMCULock}
#define BCN_MCULOCK      "\x07MCULOCK\0"
#define BCH_MCULOCK      {BCHMCULock}
void TCHMCULock(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHMCULock(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_MCURESET     "\x09+MCURESET\0"
#define TCH_MCURESET     {TCHMCUReset}
#define BCN_MCURESET     "\x08MCURESET\0"
#define BCH_MCURESET     {BCHMCUReset}
void TCHMCUReset(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHMCUReset(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_DID          "\x04+DID\0"
#define TCH_DID          {TCHDeviceID}
#define BCN_DID          "\x03""DID\0"
#define BCH_DID          {BCHDeviceID}
void TCHDeviceID(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHDeviceID(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_VER          "\x04+VER\0"
#define TCH_VER          {TCHVersion}
void TCHVersion(void *aContext,TXTCMD_PAR_TypeDef *aPar);

#define TCN_FCLEAR       "\x0B+FLASHCLEAR\0"
#define TCH_FCLEAR       {TCHFlashClear,(TCFINT<<TCFBITS)|TCFINT}
#define BCN_FCLEAR       "\x05""FLCLR\0"
#define BCH_FCLEAR       {BCHFlashClear}
void TCHFlashClear(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHFlashClear(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_FWRITE       "\x0B+FLASHWRITE\0"
#define TCH_FWRITE       {TCHFlashWrite,(TCFSTR<<TCFBITS)|TCFINT}
#define BCN_FWRITE       "\x05""FLWRI\0"
#define BCH_FWRITE       {BCHFlashWrite}
void TCHFlashWrite(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHFlashWrite(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_FREAD        "\x0A+FLASHREAD\0"
#define TCH_FREAD        {TCHFlashRead,(TCFINT<<TCFBITS)|TCFINT}
#define BCN_FREAD        "\x04""FLRD\0"
#define BCH_FREAD        {BCHFlashRead}
void TCHFlashRead(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHFlashRead(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_FCOPY        "\x0A+FLASHCOPY\0"//3                2                 1                
#define TCH_FCOPY        {TCHFlashCopy,(((TCFINT<<TCFBITS)|TCFINT)<<TCFBITS)|TCFINT}
#define BCN_FCOPY        "\x06""FLCOPY\0"
#define BCH_FCOPY        {BCHFlashCopy}
void TCHFlashCopy(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHFlashCopy(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_NDWRITE      "\x0B+WRITENDATA\0" //   3                2                 1
#define TCH_NDWRITE      {TCHWriteNamedData,(((TCFINT<<TCFBITS)|TCFSTR)<<TCFBITS)|TCFSTR}
#define BCN_NDWRITE      "\x05""NDWRI\0"
#define BCH_NDWRITE      {BCHWriteNamedData}
void TCHWriteNamedData(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHWriteNamedData(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_NDREAD       "\x0A+READNDATA\0"
#define TCH_NDREAD       {TCHReadNamedData,(TCFINT<<TCFBITS)|TCFSTR}
#define BCN_NDREAD       "\x04""NDRD\0"
#define BCH_NDREAD       {BCHReadNamedData}
void TCHReadNamedData(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHReadNamedData(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_NDDEL        "\x09+DELNDATA\0"
#define TCH_NDDEL        {TCHDeleteNamedData,(TCFINT<<TCFBITS)|TCFSTR}
#define BCN_NDDEL        "\x05""NDDEL\0"
#define BCH_NDDEL        {BCHDeleteNamedData}
void TCHDeleteNamedData(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHDeleteNamedData(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define BCN_NDINIT       "\x06""NDINIT\0"
#define BCH_NDINIT       {BCHInitNamedData}
void BCHInitNamedData(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define BCN_CRC          "\x03""CRC\0"
#define BCH_CRC          {BCHCRC}
void BCHCRC(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_APPCNG       "\x0A+APPCHANGE\0"
#define TCH_APPCNG       {TCHAppChange,TCFINT}
#define BCN_APPCNG       "\x06""APPCNG\0"
#define BCH_APPCNG       {BCHAppChange}
void TCHAppChange(void *aContext,TXTCMD_PAR_TypeDef *aPar);
void BCHAppChange(void *aContext,BINCMD_PAR_TypeDef *aBCP);

#define TCN_JAMPTO       "\x07+JAMPTO\0"
#define TCH_JAMPTO       {TCHJampTo,TCFINT}
void TCHJampTo(void *aContext,TXTCMD_PAR_TypeDef *aPar);

#ifdef __cplusplus
}
#endif
