// ���������� ���������� ������� ��� MCUs
// � Olgvel, 2012-2016
//

#include "my_lib.h"

//#define     MEMALLOCBUFSIZE    4096
#define       UCSMASK           0x00000003

extern char   gFixMemAllocBuffer[];  // [MEMALLOCBUFSIZE];

CMESSAGE_CALLBACK gPostLogMessageFunc;
uint32_t          gFixMemAllocPos;
bool              gWDGStart;

void* FixMemAlloc(uint32_t aAllocSize)
{
  uint32_t lPrevPos;
  lPrevPos = gFixMemAllocPos;
  gFixMemAllocPos = (gFixMemAllocPos + aAllocSize + 3) & (~3);
  return &gFixMemAllocBuffer[lPrevPos];
}

uint32_t GetFixMemAllocSize(void)
{
  return gFixMemAllocPos;
}

void SetWDGStart(bool aIsWDGStart)
{
  gWDGStart = aIsWDGStart;
}

bool WasWDGStart(void)
{
  return gWDGStart;
}

bool IsTimeOut(uint32_t aCurTicks,uint32_t aRemTicks,uint32_t aTimeOut)
{
  aCurTicks -= aRemTicks;
  if (aCurTicks >= aTimeOut)
    return TRUE;
  else return FALSE;
}

bool IsTimeOutEx(uint32_t aCurTicks,uint32_t *aRemTicks,uint32_t aTimeOut)
{
  if (IsTimeOut(aCurTicks, *aRemTicks, aTimeOut)) {
    *aRemTicks = aCurTicks;
    return TRUE;
  }
  else return FALSE;
}

void MemClear(void *aBuffer,int aCount)
{
  char *lBuffer;
  //
  lBuffer = aBuffer;
  while (aCount-- > 0) 
    *lBuffer++ = 0;
}

void MemFill(void *aBuffer,int aCount,uint8_t aByte)
{
  char *lBuffer;
  //
  lBuffer = aBuffer;
  while (aCount-- > 0)
    *lBuffer++ = aByte;
}

void MemCopy(const void *aSource,void *aDest,int aCount)
{
  const char *lSource;
  char *lDest;
  lSource = aSource; lDest = aDest;
  if ((lSource == lDest) || (aCount == 0)) return;
  if (lSource < lDest) {
    lSource += aCount; lDest += aCount;
    while (aCount-- > 0) *--lDest = *--lSource;
  }
  else
    while (aCount-- > 0) *lDest++ = *lSource++;
/*
  while (aCount-- > 0) {
    *(char*)aDest = *(char*)aSource;
    (*(int*)&aSource)++;
    (*(int*)&aDest)++;
  }
*/
}

bool MemComp(const char* aStrA,const char* aStrB,int aCount)
{
  while (aCount-- > 0) 
    if (*aStrA++ != *aStrB++) return FALSE;
  return TRUE;  
}

bool SubstrCompare(char *aStr,int aStrCount,char *aSource,int aCount,bool *aStrict)
{
  while ((aCount > 0) && ((aStrCount > 0) || (aStrCount < 0) && (*aStr != 0))) {
    aCount--; aStrCount--;
    if (*aStr++ != *aSource++) return FALSE;
  }
  if ((aStrCount == 0) || (aStrCount < 0) && (*aStr == 0)) {
    if (aStrict != NULL) *aStrict = (aCount == 0);
    return TRUE;
  }
  else return FALSE;
}

bool SubstrComp(char *aStr,char *aSource,int aCount)
{
  return SubstrCompare(aStr, TOEND, aSource, aCount, NULL);
}

bool SubtextComp(char *aStr,char *aSource,int aCount)
{
  char lChA, lChB;
  while ((aCount-- > 0) && (*aStr != 0)) {
    lChA = *aStr++; if ((lChA >= 'a') && (lChA <= 'z')) lChA -= 'a' - 'A';
    lChB = *aSource++; if ((lChB >= 'a') && (lChB <= 'z')) lChB -= 'a' - 'A';
    if (lChA != lChB) return FALSE;
  }
  if (*aStr == 0) return TRUE;
  else return FALSE;
}

bool StrComp(char *aStrA,char *aStrB)
{
  while ((*aStrA != 0) && (*aStrB != 0))
    if (*aStrA++ != *aStrB++) return FALSE;
  if ((*aStrA == 0) && (*aStrB == 0)) return TRUE;
  else return FALSE;
}

int Arr32NEqual(void *aArrA,void *aArrB,int aCount)
{
  uint32_t *lArrA, *lArrB;
  int lIndex = -1;
  lArrA = aArrA; lArrB = aArrB;
  while (++lIndex < aCount)
    if (*lArrA++ != *lArrB++) return lIndex;
  return NOT_FOUND;
}

int Arr32NFill(void *aArr,uint32_t aVal,int aCount)
{
  uint32_t *lArr;
  int lIndex = -1;
  lArr = aArr;
  while (++lIndex < aCount)
    if (*lArr++ != aVal) return lIndex;
  return NOT_FOUND;
}
/*
int ReduceUInt(uint32_t aValue)
{
  int I=3;
  while (I > 0) if (((char*)&aValue)[I] != 0) break; else I--;
  return I+1;
}
*/
void Int2Hex(unsigned int aInt,void *aBuffer,int aDigits)
{
  char lCodes[16] = "0123456789ABCDEF";
  (*(int*)&aBuffer) += aDigits;
  while (aDigits-- > 0) {
    (*(int*)&aBuffer)--;
    *(char*)aBuffer = lCodes[aInt & 15];
    aInt >>= 4;
  }
}

char* Int2HexZ(unsigned int aInt,void *aBuffer,int aDigits)
{
  char *lBuf;
  //
  lBuf = aBuffer;
  Int2Hex(aInt, aBuffer, aDigits);
  lBuf[aDigits] = 0;
  return aBuffer;
}

char* Bin2HexStrZ(const void *aBinBuf,void *aHexBuf,int aByteCount,bool aInsSpace)
{
  char *lHexBuf, *lBinBuf;
  lHexBuf = aHexBuf; lBinBuf = (void*)aBinBuf;
  while (aByteCount-- > 0) {
    Int2Hex(*lBinBuf++, lHexBuf, 2);
    lHexBuf += 2;
    if ((aInsSpace) && (aByteCount > 0)) *lHexBuf++ = 0x20;
  }
  *lHexBuf = 0x00;
  return aHexBuf;
}

bool HexStr2Bin(void *aStr,int aLength,void *aBinBuf,int *aBinCount)
{
  char *lStr, *lBin;
  int lCount = 0;
  char lChar;
  //
  lStr = aStr; lBin = aBinBuf;
  while (aLength-- > 0) {
    lChar = *lStr++;
    if ((lChar >= '0') && (lChar <= '9')) lChar -= '0';
    else
      if ((lChar >= 'A') && (lChar <= 'F')) lChar = lChar - 'A' + 10;
      else return FALSE;
    if (aLength & TRUE) *lBin = lChar << 4;
    else { *lBin++ |= lChar; lCount++; }
  }
  *aBinCount = lCount;
  return TRUE;
}

bool UCS2StrToWin1251(char *aSource,int aLength,char *aDest)
{
  char lChar;
  if (aLength <= TOEND) aLength = StrLength(aSource);
  if ((aLength & UCSMASK) != 0) return FALSE;
  for (int I=0;I<aLength;I++) {
    lChar = aSource[I];
    switch (I & UCSMASK) {
      case 1: if (lChar == '4') break;
      case 0: if (lChar != '0') return FALSE; break;
      case 2:
      case 3:
        if ((lChar >= '0')&&(lChar <= '9')||(lChar >= 'A')&&(lChar <= 'F')) break;
        else return FALSE;
    }
  }
  for (int I=0;I<aLength;I+=4) {
    lChar = Hex2IntDef(&aSource[I+2], 2, 0);
    if (aSource[I+1] != '0')
      if ((lChar>=0x10)&&(lChar<=0x4F)) lChar += 0xB0; else
      if (lChar == 0x01) lChar = 0xA8; else
      if (lChar == 0x51) lChar = 0xB8; else
      if (lChar == 0x07) lChar = 0xAF; else
      if (lChar == 0x57) lChar = 0xBF; else
      if (lChar == 0x04) lChar = 0xAA; else
      if (lChar == 0x54) lChar = 0xAB; else
      if (lChar == 0x0E) lChar = 0xA1; else
      if (lChar == 0x5E) lChar = 0xA2; else lChar = '.';
    *aDest++ = lChar;
  }
  *aDest = 0;
  return TRUE;
}

bool Win1251ToUCS2Str(char *aSource,int aLength,char *aDest)   //
{
  char lChar;
  if (aLength <= TOEND) aLength = StrLength(aSource);
  aLength--; aSource += aLength; aDest += aLength << 2;
  while (aLength-- >= 0) {
    lChar = *aSource--;
    if (lChar < 128)
      Int2Hex(lChar, aDest, 4);
    else {
      if (lChar>=0xC0) lChar -= 0xB0; else
      if (lChar == 0xA8) lChar = 0x01; else
      if (lChar == 0xB8) lChar = 0x51; else
      if (lChar == 0xAF) lChar = 0x07; else
      if (lChar == 0xBF) lChar = 0x57; else
      if (lChar == 0xAA) lChar = 0x04; else
      if (lChar == 0xAB) lChar = 0x54; else
      if (lChar == 0xA1) lChar = 0x0E; else
      if (lChar == 0xA2) lChar = 0x5E; else lChar = 0x87;
      Int2Hex(0x0400+lChar, aDest, 4);
    }
    aDest -= 4;
  }
  return FALSE;
}

bool IsExtAscii(char *aStr,int aLength)
{
  while ((aLength == TOEND) && (*aStr != 0x00) || (aLength-- > 0))
    if (*aStr++ >= 128) return TRUE;
  return FALSE;
}

int MaxInt(int aValue,int aDefInt)
{
  if (aDefInt < aValue) return aDefInt;
  else return aValue;
}

int MinInt(int aValue,int aDefInt)
{
  if (aDefInt > aValue) return aDefInt;
  else return aValue;
}

int Int2StrDig(int aInt,char *aBuffer,int aDig)
{
  char *lBuf;
  bool lSign=FALSE;
  if (aDig > 0) {
    lSign = aInt < 0;
    if (lSign) { if (--aDig == 0) return 0; *aBuffer++; aInt = -aInt; }
    lBuf = aBuffer; lBuf += aDig - 1;
    while (TRUE) {
      *lBuf = ('0' + (aInt % 10));
      aInt /= 10;
      if (lBuf == aBuffer) break;else lBuf--;
    }
  }
  if (lSign) { aDig++; *--aBuffer = '-'; }
  return aDig;
}

int Int2Str(int aInt,char *aBuffer,int aSize)
{
  char *lBuf;
  bool lSign=FALSE;
  if (aSize > 0) {
    lSign = aInt < 0;
    if (lSign) { if (--aSize == 0) return 0; *aBuffer++ = '-'; aInt = -aInt; }
    lBuf = aBuffer; lBuf += aSize - 1;
    while (TRUE) {
      *lBuf = ('0' + (aInt % 10));
      aInt /= 10;
      if (aInt == 0) {
        aSize -= (uint32_t)lBuf - (uint32_t)aBuffer;
        MemCopy(lBuf, aBuffer, aSize);
        break;
      }
      if (lBuf == aBuffer) return 0;else lBuf--;
    }
  }
  if (lSign) aSize++;
  return aSize;
}

int IntLL2Str(int64_t aInt,char *aBuffer,int aSize)
{
  char *lBuf;
  bool lSign=FALSE;
  if (aSize > 0) {
    lSign = aInt < 0;
    if (lSign) { if (--aSize == 0) return 0; *aBuffer++ = '-'; aInt = -aInt; }
    lBuf = aBuffer; lBuf += aSize - 1;
    while (TRUE) {
      *lBuf = ('0' + (aInt % 10));
      aInt /= 10;
      if (aInt == 0) {
        aSize -= (uint32_t)lBuf - (uint32_t)aBuffer;
        MemCopy(lBuf, aBuffer, aSize);
        break;
      }
      if (lBuf == aBuffer) return 0;else lBuf--;
    }
  }
  if (lSign) aSize++;
  return aSize;
}

char* Int2StrDigZ(int aInt,char *aBuffer,int aDigits)
{
  char *lBuf;
  int lLength;
  //
  lBuf = aBuffer;
  lLength = Int2StrDig(aInt, aBuffer, aDigits);
  lBuf[lLength] = 0;
  return aBuffer;
}

char* Int2StrZ(int aInt,char *aBuffer,int aSize)
{
  char *lBuf;
  int lLength;
  //
  lBuf = aBuffer;
  lLength = Int2Str(aInt, aBuffer, aSize);
  lBuf[lLength] = 0;
  return aBuffer;
}

int Str2IntDef(void *aStr,int aLength,int aDefault)
{
  char *lStr;
  int lResult = 0;
  //
  lStr = aStr;
  if ((aLength > 2) && (lStr[1] == 'x') && (lStr[0] == '0'))
    return Hex2IntDef(&lStr[2], aLength - 2, aDefault);
  else
    while (aLength > 0) {
      if ((*lStr < '0') || (*lStr > '9')) return aDefault;
      lResult = (lResult * 10) + ((*lStr++) - '0');
      aLength--;
    }
  return lResult;
}

int Hex2IntDef(void *aStr,int aLength,int aDefault)
{
  char *lStr;
  int lResult = 0;
  char lChar;
  lStr = aStr;
  //
  if (aLength == 0) return aDefault;
  while (aLength > 0) {
    lChar = *lStr++;
    if ((lChar >= '0') && (lChar <= '9')) lChar -= '0';
    else
      if ((lChar >= 'A') && (lChar <= 'F')) lChar = lChar - 'A' + 10;
      else return aDefault;
    lResult = (lResult << 4) + lChar;
    aLength--;
  }
  return lResult;
}

char* Int2TextIPZ(uint32_t aIntIP,char *aStr)
{
  int lIndex=0, I, lSize;
  //
  for (I=3;I>=0;I--) {
    lSize = Int2Str((aIntIP >> (I*8)) & 0xFF, &aStr[lIndex], 3);
    lIndex += lSize + 1; if (I > 0) aStr[lIndex-1] = '.'; else aStr[lIndex-1] = 0;
  }
  return aStr;
}

bool TextIP2Int(char *aStr,int aLength,uint32_t *aAddr)
{
  int32_t lIndex=0, I, lPos, lValue;
  uint32_t lResult=0;
  //
  if (aLength <= 0) aLength = StrLength(aStr);
  if ((aLength < 7) || !((aStr[0] >= '0') && (aStr[0] <= '9'))) return FALSE;
  //
  for (I=4;I>0;I--) {
    if (lIndex >= aLength) return FALSE;
    lPos = CharPos('.', &aStr[lIndex], aLength-lIndex);
    if (lPos < 0) lPos = aLength; else lPos += lIndex;
    lValue = Str2IntDef(&aStr[lIndex], lPos-lIndex, ERROR_RESULT);
    if ((lValue >= 0) && (lValue <= 255))
      lResult = (lResult << 8) + lValue;
    else return FALSE;
    lIndex = lPos + 1;
  }
  *aAddr = lResult;
  return TRUE;
}

uint32_t StrIP2Int(char *aStr)
{
  uint32_t lIntAddr;
  if (TextIP2Int(aStr, TOEND, &lIntAddr)) return lIntAddr;
  else return 0;
}

bool UnpackIPAndPort(char *aStr,int aLength,uint32_t *aIP,uint16_t *aPort)
{
  int  lIndex = 0, lParPos, lParLen;
  //
  if (GetInParamEx(':', aStr, aLength, &lIndex, &lParPos, &lParLen)) {
    TextIP2Int(aStr, lParLen, aIP);
    *aPort = Str2IntDef(&aStr[lParLen+1], aLength - lParLen-1, 0);
    return TRUE;
  }
  return FALSE;
}

int GetShortIndex(short aSource,short *aBuf,int aSize)
{
   int lIndex = 0;
   //
   while (aSize > 0) {
     if (*aBuf++ == aSource) return lIndex;
     aSize -= sizeof(short);
     lIndex++;
   }
   return ERROR_RESULT;
}
/*
bool FindShortStr(void *aStr,const void *aStrBuf,int *aIndex,int *aLength)
{
  int lIndex = 0, lPos = 0;
  char *lStrBuf;
  //
  lStrBuf = (char*)aStrBuf;
  while (lStrBuf[lPos] > 0)
    if (SubstrComp(&lStrBuf[lPos+1], aStr, lStrBuf[lPos])) {
      if (aIndex != NULL) *aIndex = lIndex;
      if (aLength != NULL) *aLength = lStrBuf[lPos];
      return TRUE;
    }
    else {
      lPos += (lStrBuf[lPos] + 2);
      lIndex++;
    }
  return FALSE;
}
*/
bool FindShortStr(char *aStr,int aStrLen,const char *aStrBuf,int *aIndex,int *aLength)
{
  int lIndex = 0, lPos = 0;
  //
  while (aStrBuf[lPos] > 0)
    if (((aStrLen == TOEND)||(aStrLen == aStrBuf[lPos]))&&
        SubstrComp((char*)&aStrBuf[lPos+1], aStr, aStrBuf[lPos])) {
      if (aIndex != NULL) *aIndex = lIndex;
      if (aLength != NULL) *aLength = aStrBuf[lPos];
      return TRUE;
    }
    else {
      lPos += (aStrBuf[lPos] + 2);
      lIndex++;
    }
  return FALSE;
}

bool GetShortStr(int aIndex,const char *aStrBuf,char **aPStr,int *aLength)
{
  int lPos = 0;
  while (aIndex >= 0) {
    if (aIndex == 0) {
      *aPStr = (char*)&aStrBuf[lPos+1];
      if assigned(aLength) *aLength = aStrBuf[lPos];
      return TRUE;
    }
    lPos += (aStrBuf[lPos] + 2);
    aIndex--;
  }
  return FALSE;
}

char* GetShortStrZ(int aIndex,const char *aStrBuf)
{
  char *lStr;
  int lLength;
  //
  if (GetShortStr(aIndex, aStrBuf, &lStr, &lLength)) return lStr;
  else return NULL;
}

int CharPosEx(char aChar,void *aBuf,int aTestCount,bool aInvCond)
{
  int lResult = 0;
  char *lBuf;
  lBuf = aBuf;
  if (aTestCount > 0)
    while (aTestCount-- > 0) {
      if ((aChar == *lBuf++) ^ aInvCond) return lResult;
      lResult++;
    }
  else
    while (*lBuf != 0) {
      if ((aChar == *lBuf++) ^ aInvCond) return lResult;
      lResult++;
    }
  return ERROR_RESULT;
}

int CharPos(char aChar,void *aBuf,int aTestCount)
{
  return CharPosEx(aChar, aBuf, aTestCount, FALSE);
}

bool GetCharPos(char aChar,void *aBuf,int aTestCount,int *aPos)
{
  int lPos;
  lPos = CharPos(aChar, aBuf, aTestCount);
  if (lPos >= 0) {
    *aPos = lPos;
    return TRUE;
  }
  else return FALSE;
}

void CharChange(char *aSource,int aCount,char aSChar,char aDChar)
{
  int lPos;
  while ((aCount > 0) && GetCharPos(aSChar, aSource, aCount, &lPos)) {
    aSource[lPos] = aDChar; lPos++;
    aSource = &aSource[lPos];
    aCount -= lPos;
  }
}

int StrLength(const char *aStr)
{
  int lLength = 0;
  while (*aStr++ != 0) lLength++;
  return lLength;
}

int StrLengthEx(char *aStr,char *aEndChars,int aMaxLen)
{
  int lLength = 0;
  char *lTC;
  bool lBreak;
  while ((*aStr != 0) && (lLength != aMaxLen)) {
    lTC = aEndChars; lBreak = FALSE;
    while (*lTC != 0) if (*aStr == *lTC) {lBreak = TRUE; break;} else lTC++;
    if (lBreak) break;
    else {
      lLength++;
      aStr++;
    }
  }
  return lLength;
}

char* AssignStr(const char *aSubstr,char *aBuf)
{
  MemCopy(aSubstr, aBuf, StrLength(aSubstr)+1);
  return aBuf;
}

char* MakeStr(char *aSource,int aCount,char *aBuf)
{
  if (aCount <= 0) aCount = StrLength(aSource);
  MemCopy(aSource, aBuf, aCount);
  aBuf[aCount] = 0;
  return aBuf;
}

int AddStr(char *aSubstr,char *aBuf,int aCount)
{
  if (aCount <= 0) aCount = StrLength(aBuf);
  aBuf += aCount;
  while (*aSubstr != 0) { *aBuf++ = *aSubstr++; aCount++; }
  *aBuf = 0;
  return aCount;
}

void TrimRight(char *aMessage,int *aLength)
{
  while ((*aLength > 0) && ((aMessage[*aLength-1] == SPACE) ||
      (aMessage[*aLength-1] == CR) || (aMessage[*aLength-1] == LF))) (*aLength)--;
}

void TrimLeft(char *aMessage,int *aIndex,int *aLength)
{
  while ((*aLength > 0) && ((aMessage[*aIndex] == SPACE) ||
      (aMessage[*aIndex] == CR) || (aMessage[*aIndex] == LF))) {
    (*aLength)--; (*aIndex)++;
  }
}

char LowerCase(char aChar)
{
  if ((aChar >= 'A') && (aChar <= 'Z')) return (aChar + 'a' - 'A');
  else return aChar;
}

char UpCase(char aChar)
{
  if ((aChar >= 'a') && (aChar <= 'z')) return (aChar + 'A' - 'a');
  else return aChar;  
}

uint32_t RevLong(uint32_t aValue)
{
  UINT32_U lUIntA, lUIntB;
  lUIntA.Long = aValue;
  lUIntB.Bytes[0] = lUIntA.Bytes[3];
  lUIntB.Bytes[1] = lUIntA.Bytes[2];
  lUIntB.Bytes[2] = lUIntA.Bytes[1];
  lUIntB.Bytes[3] = lUIntA.Bytes[0];
  return lUIntB.Long;
}

uint16_t RevWord(uint16_t aValue)
{
  UINT16_U lUIntA, lUIntB;
  lUIntA.Word = aValue;
  lUIntB.Bytes[0] = lUIntA.Bytes[1];
  lUIntB.Bytes[1] = lUIntA.Bytes[0];
  return lUIntB.Word;
}

void BufCopy(const void *aSource,void *aDest,int aCount,bool aRev)
{
  if ((aRev)&&(aCount == 2)) *(uint16_t*)aDest = RevWord(*(uint16_t*)aSource); else
  if ((aRev)&&(aCount == 4)) *(uint32_t*)aDest = RevLong(*(uint32_t*)aSource); else
  MemCopy(aSource, aDest, aCount);
}

/*
const char Base64ShTbl[4] = {2,0,3,1};
#define    BASE64MASK       0xED9BC6A5;

void EncodeBase64(void *aSource,int aCount,void *aDest,int *aDestCount)
{
  int I, K;
  char *lSource, *lDest;
  uint32_t lLong;
  uint8_t lCode;
  //
  lSource = aSource; lDest = aDest;
  aCount = (aCount + 2) / 3;
  *aDestCount = aCount << 2;
  while (aCount-- > 0) {
    //lLong = *(uint32_t*)lSource ^ BASE64MASK;
    MemCopy(lSource, &lLong, 3);
    lLong ^= BASE64MASK;
    for (I=0;I<4;I++) {
      lCode = lLong & 0x3F; K = Base64ShTbl[I];
      if (lCode < 27) lDest[K] = 0x40+lCode; else
      if (lCode < 33) lDest[K] = 0x30+lCode-27; else
      if (lCode < 59) lDest[K] = 0x61+lCode-33; else
      if (lCode < 63) lDest[K] = 0x36+lCode-59;
      else lDest[K] = 0x24;
      lLong >>= 6;
    }
    lSource += 3;
    lDest += 4;
    //aCount--;
  }
}

bool DecodeBase64(void *aSource,int aCount,void *aDest,int *aDestCount)
{
  int I;
  char *lSource, *lDest;
  UINT32_U lU;
  //uint32_t lLong=0;
  uint8_t lCode;
  //
  lSource = aSource; lDest = aDest; lU.Long = 0;
  aCount = (aCount + 3) / 4;
  *aDestCount = aCount * 3;
  while (aCount-- > 0) {
    for (I=3;I>=0;I--) {
      lCode = lSource[Base64ShTbl[I]];
      if ((lCode >= 0x40) && (lCode <= 0x5A)) lCode -= 0x40; else
      if ((lCode >= 0x61) && (lCode <= 0x7A)) lCode -= 0x40; else
      if ((lCode >= 0x30) && (lCode <= 0x35)) lCode -= 0x15; else
      if ((lCode >= 0x36) && (lCode <= 0x39)) lCode += 5; else
      if (lCode == 0x24) lCode = 63;
      else return FALSE;
      lU.Long = (lU.Long << 6) | lCode & 0x3F;
    }
    lU.Long ^= BASE64MASK;
    for (I=0;I<3;I++) *lDest++ = lU.Bytes[I];
    // *(uint32_t*)lDest = lLong;
    //lDest += 3;
    lSource += 4;
  }
  return TRUE;
}
*/

bool GetInParamEx(char aParser,char *aStr,int32_t aLen,int32_t *aSearchPos,
  int32_t *aParPos,int32_t *aParLen)
{
  int32_t lLen;
  bool lBegFix = FALSE;
  //char lEndChar = COMMA;
  //
  lLen = aLen - *aSearchPos;
  aStr += *aSearchPos;
  while (lLen-- > 0) {
    if (!lBegFix)
      if (*aStr != SPACE) {
        *aParPos = aLen - lLen - 1;
        lBegFix = TRUE;
        if (*aStr == QUOTE) { aParser = QUOTE; (*aParPos)++; aStr++; continue;}
      }
    if ((lBegFix) && ((*aStr == aParser) || (aParser != QUOTE) && (lLen == 0))) {
      *aParLen = aLen - lLen - *aParPos - 1;
      *aSearchPos = aLen - lLen;
      if (*aStr != aParser) (*aParLen)++;
      if (aParser == QUOTE) (*aSearchPos)++;
      //
      return TRUE;
    }
    aStr++;
  }
  return FALSE;
}

bool GetInParam(char *aStr,int32_t aLen,int32_t *aSearchPos,
  int32_t *aParPos,int32_t *aParLen)
{
  return GetInParamEx(COMMA, aStr, aLen, aSearchPos, aParPos, aParLen);
}

bool GetInParamPC(char *aStr,int32_t aLen,int32_t *aSearchPos,
  int32_t *aParPos,int32_t *aParLen)
{
  return GetInParamEx(PC, aStr, aLen, aSearchPos, aParPos, aParLen);
}

bool GetInParamEQ(char *aStr,int32_t aLen,int32_t *aSearchPos,
  int32_t *aParPos,int32_t *aParLen)
{
  return GetInParamEx(EQ, aStr, aLen, aSearchPos, aParPos, aParLen);
}

bool GetInIntParam(char *aStr,int aLen,int *aSearchPos,int *aIntVal,int aDefault)
{
  int lParPos, lParLen;
  if (GetInParam(aStr, aLen, aSearchPos, &lParPos, &lParLen)) {
    *aIntVal = Str2IntDef(&aStr[lParPos], lParLen, aDefault);
    return TRUE;
  }
  else {
    *aIntVal = aDefault;
    return FALSE;
  }
}

bool GetInIntParamNoDef(char *aStr,int aLen,int *aSearchPos,int *aIntVal,int aDefault)
{
  int lSearchPos = *aSearchPos;
  if (GetInIntParam(aStr, aLen, &lSearchPos, aIntVal, aDefault) && (*aIntVal != aDefault)) {
    *aSearchPos = lSearchPos;
    return TRUE;
  } else return FALSE;
}

bool GetInIPParamNoDef(char *aStr,int aLen,int *aSearchPos,uint32_t *aIPVal,uint32_t aDefault)
{
  int lParPos, lParLen;
  int lSearchPos = *aSearchPos;
  if (GetInParam(aStr, aLen, aSearchPos, &lParPos, &lParLen) &&
      TextIP2Int(&aStr[lParPos], lParLen, aIPVal) ) {
    *aSearchPos = lSearchPos;
    return *aIPVal != aDefault;
  } else return FALSE;
}

bool GetNamedParam(char *aStr,int aStrLen,const char *aStrBuf,int *aNameIdx,
  int *aParPos,int *aParLen)
{
  int lPos=0, lNPos, lNLen;
  if (GetInParamEx(EQ, aStr, aStrLen, &lPos, &lNPos, &lNLen)) {
    TrimLeft(aStr, &lNPos, &lNLen);
    if (FindShortStr(&aStr[lNPos], lNLen, aStrBuf, aNameIdx, NULL)) {
      *aParPos = lPos;
      *aParLen = aStrLen - lPos;
      return TRUE;
    }
    else return FALSE;
  }
  else return FALSE;
}

//------------------------------------------------------------------------------

void AssignPostLogMessage(CMESSAGE_CALLBACK aPostLogMessageFunc)
{
  gPostLogMessageFunc = aPostLogMessageFunc;
}

void PostLogMessage(const char *aMessage,int aLength)
{
  if (gPostLogMessageFunc != NULL)
    gPostLogMessageFunc(aMessage, aLength);
}

void PostLogText(const char *aText)
{
  PostLogMessage(aText, TOEND);
}

void PostLogMsg(const char *aBeforeText,const char *aStr,int aLength,const char *aAfterText)
{
  if (aBeforeText != NULL) PostLogText(aBeforeText);
  PostLogMessage(aStr, aLength);
  if (aAfterText != NULL) PostLogText(aAfterText);
}

void PostLogTxt(const char *aBeforeText,const char *aText,const char *aAfterText)
{
  PostLogMsg(aBeforeText, aText, TOEND, aAfterText);
}

void PostLogInt(const char *aBeforeText,int aValue,const char *aAfterText)
{
  char lStr[12];
  int lLen = Int2Str(aValue, lStr, sizeof(lStr));
  PostLogMsg(aBeforeText, lStr, lLen, aAfterText);
}

void PostLogInt64(const char *aBeforeText,int64_t aValue,const char *aAfterText)
{
  char lStr[24];
  int lLen = IntLL2Str(aValue, lStr, sizeof(lStr));
  PostLogMsg(aBeforeText, lStr, lLen, aAfterText);
}

void PostLogHex(char *aBeforeText,uint32_t aValue,int aByteCount,char *aAfterText)
{
  char lStr[10];
  PostLogTxt(aBeforeText, Int2HexZ(aValue, lStr, aByteCount << 1), aAfterText);
}

void PostLogTextIP(char *aBeforeText,uint32_t aValue,char *aAfterText)
{
  char lStr[16];
  PostLogTxt(aBeforeText, Int2TextIPZ(aValue, lStr), aAfterText);
}

void PostLogHexStr(char *aBeforeText,char *aBuf,int aByteCount,char *aAfterText)
{
  char lStr[128];
  if (aByteCount > 42) aByteCount = 42;
  if (aBeforeText != NULL) PostLogText(aBeforeText);
  PostLogText(Bin2HexStrZ(aBuf, lStr, aByteCount, TRUE));
  if (aAfterText != NULL) PostLogText(aAfterText);
}

void LogProcess(void)
{

}

//------------------------------------------------------------------------------
MY_PESTATE_TypeDef* MyPS_Init(MY_PESTATE_TypeDef *aMyPS,STATEHANDLER_CALLBACK aOnSetState,
  MYHANDLER_CALLBACK aOnStop,void *aData)
{
  if (aMyPS == NULL)
    aMyPS = FixMemAlloc( sizeof(MY_PESTATE_TypeDef) );
  //
  MemClear(aMyPS, sizeof(MY_PESTATE_TypeDef));
  aMyPS->sPSOnSetState = aOnSetState;
  aMyPS->sPSOnStop = aOnStop;
  aMyPS->sPSData = aData;
  return aMyPS;
}

void MyPS_Setup(void *aMyPS,int16_t aStartState,uint16_t aPeriod,uint16_t aPulseWidth,uint16_t aCycles)
{
  MY_PESTATE_TypeDef *lMyPS;
  //
  if (aMyPS == NULL) return;
  lMyPS = (MY_PESTATE_TypeDef*)aMyPS;
  lMyPS->sPSStartState = aStartState;
  lMyPS->sPSState = !aStartState;
  lMyPS->sPSPeriod = aPeriod;
  lMyPS->sPSPulseWidth = aPulseWidth;
  if (aPeriod <= aPulseWidth) {
    lMyPS->sPSCycles = 0;
    if (lMyPS->sPSOnSetState != NULL)
      lMyPS->sPSOnSetState(lMyPS, aStartState ^ lMyPS->sPSInverseSet);
  }
  else {
    if (aCycles == 0) lMyPS->sPSCycles = MAXUINT16;
    else lMyPS->sPSCycles = aCycles + 1;
    lMyPS->sPSTimeOut = aPulseWidth;
    lMyPS->sPSTicks = TICKS - aPeriod;
  }
}

void MyPS_Process(void *aMyPS)
{
  MY_PESTATE_TypeDef *lMyPS;
  lMyPS = (MY_PESTATE_TypeDef*)aMyPS;
  if ((lMyPS->sPSCycles > 0) && (IsTimeOutEx(TICKS, &lMyPS->sPSTicks, lMyPS->sPSTimeOut))) {
    lMyPS->sPSState = !lMyPS->sPSState;
    if (lMyPS->sPSState == lMyPS->sPSStartState) {
      lMyPS->sPSTimeOut = lMyPS->sPSPulseWidth;
      if (lMyPS->sPSCycles != MAXUINT16) {
        lMyPS->sPSCycles--;
        if ((lMyPS->sPSCycles == 0) && (lMyPS->sPSOnStop != NULL))
          lMyPS->sPSOnStop(lMyPS, lMyPS->sPSData);
      }
    }
    else lMyPS->sPSTimeOut = lMyPS->sPSPeriod - lMyPS->sPSPulseWidth;
    //
    if ((lMyPS->sPSCycles > 0) && (lMyPS->sPSOnSetState != NULL))
      lMyPS->sPSOnSetState(lMyPS, lMyPS->sPSState ^ lMyPS->sPSInverseSet);
  }
}

//=========================== MHandler =========================================

void MHandler_Setup(MHANDLER_TypeDef *aHandler,PHANDLER aFunc,void *aContext)
{
  aHandler->sFunc = aFunc;
  aHandler->sCtx = aContext;
}

bool MHandler_Call(MHANDLER_TypeDef *aHandler,void *aParam)
{
  if assigned(aHandler->sFunc) {
    aHandler->sFunc(aHandler->sCtx, aParam);
    return TRUE;
  }
  else return FALSE;
}

//=========================== UHandler =========================================

void UHandler_Assign(UHANDLER_TypeDef **aHandler,PHANDLER aFunc,void *aContext)
{
  UHANDLER_TypeDef *lHandler, *lPredHandler;
  lHandler = *aHandler; lPredHandler = NULL;
  while assigned(lHandler) {
    lPredHandler = lHandler;
    lHandler = lHandler->sNext;
  }
  lHandler = FixMemAlloc( sizeof(UHANDLER_TypeDef) );
  if assigned(lPredHandler) lPredHandler->sNext = lHandler;
  else *aHandler = lHandler;
  lHandler->sFunc = aFunc;
  lHandler->sCtx = aContext;
  lHandler->sNext = NULL;
}

void UHandler_Exec(UHANDLER_TypeDef *aHandler,void *aParam)
{
  while assigned(aHandler) {
    aHandler->sFunc(aHandler->sCtx, aParam);
    aHandler = aHandler->sNext;
  }  
}

//=========================== MyList ===========================================

MY_LIST_TypeDef* MyList_Init(MY_LIST_TypeDef *aList,uint16_t aCapacity,uint16_t aBlockSize)
{
  if (aList == NULL) aList = FixMemAlloc( MyList_Size(aCapacity, aBlockSize) );
  //
  aList->sLBuffer = (PCHAR_ARRAY)((uint32_t)(aList) + sizeof(MY_LIST_TypeDef));
  aList->sLBlockSize = aBlockSize;
  if (aBlockSize > sizeof(uint32_t))
    aList->sLIndexSize = sizeof(uint32_t);
  else aList->sLIndexSize = aBlockSize;
  aList->sLCapacity = aCapacity;
  aList->sLCount = 0;
  return aList;
}

uint16_t MyList_Size(uint16_t aCapacity,uint16_t aBlockSize)
{
  return aCapacity * aBlockSize + sizeof(MY_LIST_TypeDef);
}

void* MyList_Add(MY_LIST_TypeDef *aList)
{
  int32_t lCount;
  lCount = aList->sLCount;
  if (lCount < aList->sLCapacity) {
    aList->sLCount++;
    return &(*aList->sLBuffer)[lCount*aList->sLBlockSize];
  }
  else return NULL;
}

void* MyList_First(MY_LIST_TypeDef *aList)
{
  if (aList->sLCount != 0)
    return &(*aList->sLBuffer)[0];
  else return NULL;
}

void* MyList_Item(MY_LIST_TypeDef *aList,int32_t aIndex)
{
  int32_t lCount;
  lCount = aList->sLCount;
  if ((aIndex >= 0) && (aIndex < lCount))
    return &(*aList->sLBuffer)[aIndex*aList->sLBlockSize];
  else return NULL;
}

void* MyList_Last(MY_LIST_TypeDef *aList)
{
  return MyList_Item(aList, aList->sLCount-1);
}

/*
void* MyList_AddInt(MY_LIST_TypeDef *aList,uint32_t aValue)
{
  if ((aList->sLCount < aList->sLCapacity)) {
    if (aList->sLIndexSize == sizeof(uint32_t))
      *(uint32_t*)(&aList->sLBuffer[aList->sLCount*aList->sLBlockSize]) = aValue;
    else
      if (aList->sLIndexSize == sizeof(uint16_t))
        *(uint16_t*)(&aList->sLBuffer[aList->sLCount*aList->sLBlockSize]) = aValue;
      else
        if (aList->sLIndexSize == sizeof(uint8_t))
          *(uint8_t*)(&aList->sLBuffer[aList->sLCount*aList->sLBlockSize]) = aValue;
    aList->sLCount++;
  }
  return aList->sLCount - 1;
}
*/

void MyList_InsInt(MY_LIST_TypeDef *aList,int32_t aIndex,uint32_t aValue)
{
}

bool MyList_Delete(MY_LIST_TypeDef *aList,int32_t aIndex)
{
  int32_t lCount, lBlockSize;
  lCount = aList->sLCount;
  if ((aIndex < 0) || (aIndex >= lCount)) return FALSE;
  lBlockSize = aList->sLBlockSize;
  if ((aIndex+1) != lCount)
    MemCopy(&(*aList->sLBuffer)[(aIndex+1) * lBlockSize],
      &(*aList->sLBuffer)[aIndex * lBlockSize], (lCount-aIndex) * lBlockSize);
  aList->sLCount--;
  return TRUE;
}

int32_t MyList_IndexOf(MY_LIST_TypeDef *aList,uint32_t aValue)
{
  return ERROR_RESULT;
}

bool MyList_FindIndex(MY_LIST_TypeDef *aList,uint32_t aValue,void **aItem,int32_t *aIndex)
{
  UINT32_U *lUINT;
  for (int I=0;I<aList->sLCount;I++){
    lUINT = (void*)&(*aList->sLBuffer)[I*aList->sLBlockSize];
    if (lUINT->Long == aValue) {
      if (aItem != NULL) *aItem = lUINT;
      if (aIndex != NULL) *aIndex = I;
      return TRUE;
    }
  }  
  return FALSE;
}

//=========================== MyIndexList ======================================

void ClearItems(PCHAR_ARRAY aBuffer,int32_t aBlockSize,int32_t aFrom,int32_t aTo)
{
  while (aFrom++ < aTo) {
    *(uint32_t*)aBuffer = EMPTY32;
    aBuffer += aBlockSize;
  }
}

int32_t GetEmptyItem(PCHAR_ARRAY aBuffer,int32_t aBlockSize,int32_t aFrom,int32_t aTo)
{
  while (aFrom < aTo)
    if (*(uint32_t*)aBuffer == EMPTY32) return aFrom;
    else aFrom++;
  return NOT_FOUND;
}

//=========================== MyCirc4Buf =======================================

MY_CIRC4BUF_TypeDef* MyCirc4Buf_Init(MY_CIRC4BUF_TypeDef *aDescr,
  uint16_t aCapacity,uint16_t aMarker,uint16_t aQuoteSize)
{
  if (aDescr == NULL)
    aDescr = FixMemAlloc( sizeof(MY_CIRC4BUF_TypeDef) );
  MemClear(aDescr, sizeof(MY_CIRC4BUF_TypeDef));
  aDescr->sC4Buffer = FixMemAlloc( aCapacity );
  aDescr->sC4BufSize = aCapacity;
  aDescr->sC4Marker = aMarker;
  aDescr->sC4QuoteSize = aQuoteSize;
  return aDescr;
}

void incPos(MY_CIRC4BUF_TypeDef *aDescr,uint16_t *aPos,uint16_t aInc)
{
  *aPos += aInc + 4;
  if (((int16_t)aDescr->sC4BufSize - *aPos) <= 4) *aPos = 0;
}

uint16_t freeSize(MY_CIRC4BUF_TypeDef *aDescr)
{
  if (aDescr->sC4AddPos >= aDescr->sC4DelPos)
    return (int16_t)aDescr->sC4BufSize - aDescr->sC4AddPos - 4;
  else return (int16_t)aDescr->sC4DelPos - aDescr->sC4AddPos - 1 - 4;
}

bool MyCirc4Buf_InData(MY_CIRC4BUF_TypeDef *aDescr,char **aDestPtr,uint16_t aDestSize,uint16_t *aCount)
{
  PTR_U lBuf;
  int16_t lSize;
  //
  lSize = freeSize(aDescr);
  if (lSize > 0) {
    if (lSize < aDestSize) aDestSize = lSize;
    lBuf.uChar = &aDescr->sC4Buffer[aDescr->sC4AddPos];
    lBuf.uWord[0] = 0; lBuf.uWord[1] = aDestSize;
    incPos(aDescr, &aDescr->sC4AddPos, aDestSize);
    *aDestPtr = lBuf.uChar;
    *aCount = aDestSize;
    return TRUE;
  }
  *aCount = 0;
  return FALSE;
}

bool MyCirc4Buf_Read(MY_CIRC4BUF_TypeDef *aDescr,char *aDest,uint16_t aDestSize,uint16_t *aCount)
{
  PTR_U lBuf;
  uint16_t lCount;
  *aCount = 0;
  while (aDescr->sC4DelPos != aDescr->sC4AddPos) {
    lBuf.uChar = &aDescr->sC4Buffer[aDescr->sC4DelPos];
    if (lBuf.uWord[0] != aDescr->sC4Marker) break;
    lCount = lBuf.uWord[1];
    if (lCount > aDestSize) break;
    MemCopy(&lBuf.uWord[2], aDest, lCount);
    aDest += lCount; aDestSize -= lCount; *aCount += lCount;
    incPos(aDescr, &aDescr->sC4DelPos, lCount);
  }
  return (*aCount > 0);
}

bool MyCirc4Buf_OutData(MY_CIRC4BUF_TypeDef *aDescr,char **aSouPtr,uint16_t aMaxSouSize,uint16_t *aCount)
{
  PTR_U lBuf;
  uint16_t lCount;
  *aCount = 0;
  if (aDescr->sC4DelPos != aDescr->sC4AddPos) {
    lBuf.uChar = &aDescr->sC4Buffer[aDescr->sC4DelPos];
    if (lBuf.uWord[0] == aDescr->sC4Marker) return FALSE;
    lCount = lBuf.uWord[1];
    if (lCount > aMaxSouSize) return FALSE;
    if (aSouPtr != NULL) {
      incPos(aDescr, &aDescr->sC4DelPos, lCount);
      *aSouPtr = lBuf.uChar;
    }
    *aCount = lCount;
    return TRUE;
  }
  return FALSE;
}

bool MyCirc4Buf_Write(MY_CIRC4BUF_TypeDef *aDescr,char *aSource,uint16_t aCount)
{
  PTR_U lBuf;
  uint16_t lSizes[4], lOffs[4], lSize, lRemPos, lRemQuote, lIdx=0;
  int I;
  //
  lRemPos = aDescr->sC4AddPos; lRemQuote = aDescr->sC4QuoteCount;
  while (aCount > 0) {
    lSize = freeSize(aDescr);
    if (lSize <= 0) break;
    if (lSize > aCount) lSize = aCount;
    if (aDescr->sC4QuoteSize > 0) {
      if (aDescr->sC4QuoteCount == 0) aDescr->sC4QuoteCount = aDescr->sC4QuoteSize;
      if (lSize > aDescr->sC4QuoteCount) lSize = aDescr->sC4QuoteCount;
      aDescr->sC4QuoteCount -= lSize;
    }
    lSizes[lIdx] = lSize; lOffs[lIdx] = aDescr->sC4AddPos; lIdx++;
    incPos(aDescr, &aDescr->sC4AddPos, lSize);
    aCount -= lSize;
  }
  if (aCount > 0) {
    aDescr->sC4AddPos = lRemPos; aDescr->sC4QuoteCount = lRemQuote;
    return FALSE;
  }
  //PostLogInt(" QuoteCount::", aDescr->sC4QuoteCount, S_CRLF);
  for (I=0;I<lIdx;I++) {
    lBuf.uChar = &aDescr->sC4Buffer[lOffs[I]];
    lBuf.uWord[0] = 0; lBuf.uWord[1] = lSizes[I];
    MemCopy(aSource, &lBuf.uWord[2], lSizes[I]);
    //
    //PostLogMessage(aSource, lSizes[I]);
    //PostLogInt(" ::", lOffs[I], S_CRLF);
    //
    aSource += lSizes[I];
  }
  return TRUE;
}

void MyCirc4Buf_Clear(MY_CIRC4BUF_TypeDef *aDescr)
{
  aDescr->sC4AddPos = aDescr->sC4DelPos = 0;
}

//=========================== MyCBuf =======================================

MY_CBUF_TypeDef* MyCBuf_InitEx(void *aOwner,void *aBuffer,int aCapacity)
{
  MY_CBUF_TypeDef *lDescr;
  lDescr = FixMemAlloc( sizeof(MY_CBUF_TypeDef) );
  // lDescr->sCOwner = aOwner;
  lDescr->sCBuffer = aBuffer; // FixMemAlloc( aCapacity );
  lDescr->sCBufSize = aCapacity;
  return lDescr;
}

MY_CBUF_TypeDef* MyCBuf_Init(void *aOwner,int aCapacity)
{
  return MyCBuf_InitEx(aOwner, FixMemAlloc( aCapacity ), aCapacity);
}
                  
void MyCBuf_Clear(MY_CBUF_TypeDef *aDescr)
{
  aDescr->sCAddPos = aDescr->sCDelPos = aDescr->sCChkPos = 0;
}

int MyCBuf_ReadCount(MY_CBUF_TypeDef *aDescr)
{
  int lAddPos, lDelPos;
  //
  lAddPos = aDescr->sCAddPos;
  lDelPos = aDescr->sCDelPos;
  if (lAddPos >= lDelPos) return lAddPos - lDelPos;
  else return aDescr->sCBufSize - lDelPos + lAddPos;
}

int MyCBuf_WriteCount(MY_CBUF_TypeDef *aDescr)
{
  int lAddPos, lDelPos;
  //
  lAddPos = aDescr->sCAddPos;
  lDelPos = aDescr->sCDelPos;
  if (lDelPos > lAddPos) return lDelPos - lAddPos;
  else return aDescr->sCBufSize - lAddPos + lDelPos - 1;
}


int MyCBuf_CheckCount(MY_CBUF_TypeDef *aDescr)
{
  int lAddPos, lChkPos;
  lAddPos = aDescr->sCAddPos;
  lChkPos = aDescr->sCChkPos;
  if (lAddPos >= lChkPos) return lAddPos - lChkPos;
  else return aDescr->sCBufSize - lChkPos + lAddPos;
}

int MyCBuf_WriteEx(MY_CBUF_TypeDef *aDescr,const char *aSource,int aCount,bool aSending)
{
  int lCount;
  int lDelPos, lAddPos, lMaxCount, lBufSize;
  //
  if assigned(aSource) {
    if (aCount <= TOEND) aCount = StrLength(aSource);
    lBufSize = aDescr->sCBufSize;
    lAddPos = aDescr->sCAddPos;
    lDelPos = aDescr->sCDelPos;
    if ((lAddPos != 0) && (lAddPos == lDelPos)) {
      MyCBuf_Clear(aDescr); lAddPos = lDelPos = 0;
    }
    //
    if (lDelPos > lAddPos) lMaxCount = lDelPos - lAddPos;
    else lMaxCount = lBufSize - lAddPos + lDelPos - 1;
    //
    if (aCount > lMaxCount) aCount = lMaxCount;
    if (&aDescr->sCBuffer[lAddPos] != aSource) {
      lCount = aCount;
      while (lCount-- > 0) {
        aDescr->sCBuffer[lAddPos++] = *aSource++;
        if (lAddPos == lBufSize) lAddPos = 0;
      }
    }
    else {
      lAddPos += aCount;
      if (lAddPos >= lBufSize) lAddPos -= lBufSize;
    }
    aDescr->sCAddPos = lAddPos;
  }
  else aCount = 0;
  if (aSending) MHandler_Call(&aDescr->sCOnAfterWrite, aDescr);
  return aCount;
}

int MyCBuf_Write(MY_CBUF_TypeDef *aDescr,const char *aSource,int aCount)
{
  return MyCBuf_WriteEx(aDescr, aSource, aCount, TRUE);
}

int MyCBuf_WriteW(MY_CBUF_TypeDef *aDescr,const char *aSource,int aCount)
{
  return MyCBuf_WriteEx(aDescr, aSource, aCount, FALSE);
}

int MyCBuf_WriteText(MY_CBUF_TypeDef *aDescr,const char *aSource)
{
  return MyCBuf_WriteEx(aDescr, aSource, TOEND, TRUE);
}

int MyCBuf_WriteTextW(MY_CBUF_TypeDef *aDescr,char *aSource)
{
  return MyCBuf_WriteEx(aDescr, aSource, TOEND, FALSE);
}

void MyCBuf_WriteTxtEx(MY_CBUF_TypeDef *aDescr,char *aBeforeText,char *aText,
  char *aAfterText,bool aQuoted,bool aSending)
{
  if (aBeforeText != NULL) MyCBuf_WriteEx(aDescr, aBeforeText, TOEND, aSending);
  if (aQuoted) MyCBuf_WriteEx(aDescr, S_QUOTE, 1, aSending);
  MyCBuf_WriteEx(aDescr, aText, TOEND, aSending);
  if (aQuoted) MyCBuf_WriteEx(aDescr, S_QUOTE, 1, aSending);
  if (aAfterText != NULL) MyCBuf_WriteEx(aDescr, aAfterText, TOEND, aSending);
}

void MyCBuf_WriteIntEx(MY_CBUF_TypeDef *aDescr,void *aBeforeText,int aValue,void *aAfterText,bool aSending)
{
  char lStr[12];
  MyCBuf_WriteTxtEx(aDescr, aBeforeText, Int2StrZ(aValue, lStr, sizeof(lStr)-1),
    aAfterText, FALSE, aSending);
}

void MyCBuf_WriteHexEx(MY_CBUF_TypeDef *aDescr,void *aBeforeText,uint32_t aValue,
  int aByteCount,void *aAfterText,bool aSending)
{
  char lStr[10];
  MyCBuf_WriteTxtEx(aDescr, aBeforeText, Int2HexZ(aValue, lStr, aByteCount << 1),
    aAfterText, FALSE, aSending);
}

void MyCBuf_WriteIPEx(MY_CBUF_TypeDef *aDescr,void *aBeforeText,uint32_t aValue,
  void *aAfterText,bool aSending)
{
  char lStr[16];
  MyCBuf_WriteTxtEx(aDescr, aBeforeText, Int2TextIPZ(aValue, lStr), aAfterText, FALSE, aSending);
}

bool MyCBuf_Read(MY_CBUF_TypeDef *aDescr,char *aDest,int aDestSize,
  unsigned int aWaitTime,int *aCount)
{
  int lBufSize, lReadPos, lRcvPos, lCount;
  //
  MHandler_Call(&aDescr->sCOnBeforeRead, aDescr);
  lRcvPos = aDescr->sCAddPos;
  lReadPos = aDescr->sCDelPos;
  if (lRcvPos != lReadPos) {
    lBufSize = aDescr->sCBufSize;
    if (lRcvPos >= lReadPos) lCount = lRcvPos - lReadPos;
    else lCount = lBufSize - lReadPos + lRcvPos;
    if (lCount > aDestSize) lCount = aDestSize;
    if ((lCount > 0) && ((lCount == aDestSize) || (aWaitTime == 0) ||
       IsTimeOut(TICKS, aDescr->sCAccessTicks, aWaitTime))) {
      *aCount = lCount;
      if assigned(aDest)
        while (lCount-- > 0) {
          *aDest++ = aDescr->sCBuffer[lReadPos];
          if (++lReadPos == lBufSize) lReadPos = 0;
        }
      else {
        lReadPos += lCount;
        if (lReadPos >= lBufSize) lReadPos -= lBufSize;
      }
      aDescr->sCDelPos = lReadPos;
      aDescr->sCChkPos = lReadPos;
      return TRUE;
    }
  }
  return FALSE;
}

//const char ENDS_TBL[16] = {2, LF, CR, 1, CR, 1, LF, 1, DP, 2, ' ', '>',1, '>', 1, 0x14};
//                         0          3      5      7      9          12      14
int MyCBuf_ReadFmt(MY_CBUF_TypeDef *aDescr,char *aDest,int aDestSize,
  const char *aEndsTbl,uint8_t aFlags,unsigned int aWaitTime,int *aCount)
{
  int     lRes = RRF_CANCEL;
  char    *lCBuf;
  int     lBufSize, lCount, lRcvPos,
          lReadPos, lCheckPos, lCPos, lCheckLen;
  uint8_t lFlags, lEPos, lECou, lENxt, lDCou, lELen=0;
  //
  MHandler_Call(&aDescr->sCOnBeforeRead, aDescr);
  lRcvPos = aDescr->sCAddPos;
  lCheckPos = aDescr->sCChkPos;
  lReadPos = aDescr->sCDelPos;
  lBufSize = aDescr->sCBufSize;
  if (lCheckPos != lRcvPos) {
    if (lRcvPos >= lCheckPos) lCount = lRcvPos - lCheckPos;
    else lCount = lBufSize - lCheckPos + lRcvPos;
    if (lCheckPos >= lReadPos) lCheckLen = lCheckPos - lReadPos;
    else lCheckLen = lBufSize - lReadPos + lCheckPos;
    /*
    PostLogInt(S_CRLF"Add=", lRcvPos, ", ");
    PostLogInt("Del=", lReadPos, ", ");
    PostLogInt("Chk=", lCheckPos, ", ");
    PostLogInt("Chl=", lCheckLen, ", ");
    PostLogInt("Bsz=", lBufSize, ", ");
    PostLogInt("Cou=", lCount, ", ");
    PostLogHex("Flg=", aFlags, 1, S_CRLF);
    */
    lCBuf = aDescr->sCBuffer; aFlags >>= 1;
    while (lCount-- > 0) {
      lFlags = aFlags; lENxt = 0; lDCou = 1; lCheckLen++; lCPos = lCheckPos++;
      while (lFlags != 0) {
        lEPos = lENxt;
        lECou = lELen = aEndsTbl[lEPos++];
        lENxt = lEPos + lECou;
        if (lFlags & 1) {
          if (lECou <= lCheckLen) {
            while (TRUE) {
              if (lCBuf[lCPos] == aEndsTbl[lEPos])
                if (--lECou == 0) { lRes = lDCou; break; }
                else { lCPos--; if (lCPos < 0) lCPos += lBufSize; lEPos++; }
              else break;
            }
            if (lRes != RRF_CANCEL) break;
          }
        }
        lFlags >>= 1; lDCou++;
      }
      if (lCheckPos == lBufSize) lCheckPos = 0;
      if (lRes != RRF_CANCEL) break;
      if (lCheckLen == aDestSize) { lRes = RRF_INFULL; break;}
    }
    aDescr->sCChkPos = lCheckPos;
  } else lCheckLen = -1;
  //
  if (lReadPos != lCheckPos) {
    if (lRes == RRF_CANCEL) {
      if (lCheckLen < 0)
        if (lCheckPos >= lReadPos) lCheckLen = lCheckPos - lReadPos;
        else lCheckLen = lBufSize - lReadPos + lCheckPos;
      if (lCheckLen >= aDestSize) { lRes = RRF_INFULL; lCheckLen = aDestSize; }
      else if ((aWaitTime > 0) && IsTimeOut(TICKS, aDescr->sCAccessTicks, aWaitTime)) lRes = RRF_TIMER;
    }
    //
    if (lRes != RRF_CANCEL) {
      if (lRes < 0) lELen = 0;
      if assigned(aCount) *aCount = lCheckLen - lELen;
      if assigned(aDest) {
        while (lCheckLen-- > 0) {
          if (lCheckLen >= lELen)
            *aDest++ = aDescr->sCBuffer[lReadPos];
          if (++lReadPos == lBufSize) lReadPos = 0;
        }
        aDescr->sCDelPos = lReadPos;
      }
    }
  }
  return lRes;
}

bool MyCBuf_MoveTo(MY_CBUF_TypeDef *aSource,MY_CBUF_TypeDef *aDest,int aCount,bool aChkMove,int *aRetCount)
{
  char *lSource, *lDest;
  int  lRdPos, lRdSize, lWrPos, lWrCount, lWrSize, lCount;
  bool lRes = TRUE;
  //
  MHandler_Call(&aSource->sCOnBeforeRead, aSource);
  if (aChkMove) lCount = MyCBuf_CheckCount(aSource);
  else lCount = MyCBuf_ReadCount(aSource);
  if (lCount == 0) { if assigned(aRetCount) *aRetCount = 0; return FALSE; }
  //PostLogInt("moveto_count [", lCount, "] ");
  if (aCount == TOEND) aCount = lCount;
  if (lCount >= aCount) lCount = aCount; else lRes = FALSE;
  lWrCount = MyCBuf_WriteCount(aDest);
  if (lCount > lWrCount) { lCount = lWrCount; lRes = FALSE; }
  if assigned(aRetCount) *aRetCount = lCount;
  //PostLogInt("[", lCount, "]"S_CRLF);
  if (lCount > 0) {
    lSource = aSource->sCBuffer; lDest = aDest->sCBuffer;
    lRdPos = aSource->sCDelPos; lRdSize = aSource->sCBufSize;
    lWrPos = aDest->sCAddPos; lWrSize = aDest->sCBufSize;
    while (lCount-- > 0) {
      lDest[lWrPos++] = lSource[lRdPos++];
      if (lWrPos == lWrSize) lWrPos = 0;
      if (lRdPos == lRdSize) lRdPos = 0;
    }
    aDest->sCAddPos = lWrPos;
    aDest->sCAccessTicks = TICKS;
    aSource->sCDelPos = lRdPos;
    if (!aChkMove) aSource->sCChkPos = lRdPos;
    MHandler_Call(&aDest->sCOnAfterWrite, aDest);
  }
  return lRes;
}

//=========================== MyCircLBuf =======================================

void PostCircLBufDbg(MY_CIRCLBUF_TypeDef *aDescr)
{
  PostLogText(S_CRLF);
  PostLogInt("sCBufSize:", aDescr->sCLBufSize, ", ");
  PostLogInt("sCWrPos:", aDescr->sCLWrPos, ", ");
  PostLogInt("sCAddPos:", aDescr->sCLAddPos, ", ");
  PostLogInt("sCRdPos:", aDescr->sCLRdPos, S_CRLF);
}

MY_CIRCLBUF_TypeDef* MyCircLBuf_Init(MY_CIRCLBUF_TypeDef *aDescr,uint16_t aCapacity,MEMALLOC_CALLBACK aMemAlloc)
{
  if (aDescr == NULL)
    aDescr = aMemAlloc( sizeof(MY_CIRCLBUF_TypeDef) );
  //MemClear(aDescr, sizeof(MY_CIRCLBUF_TypeDef));
  aDescr->sCLReadNoDel = FALSE;
  aDescr->sCLBuffer = aMemAlloc( aCapacity );
  aDescr->sCLBufSize = aCapacity;
  aDescr->sCLAddPos = aDescr->sCLWrPos;
  return aDescr;
}

void MyCircLBuf_Clear(MY_CIRCLBUF_TypeDef *aDescr)
{
  aDescr->sCLRdPos = aDescr->sCLWrPos = aDescr->sCLAddPos = aDescr->sCLDelPos = 0;
}

uint16_t freeCSize(MY_CIRCLBUF_TypeDef *aDescr,bool aHasHdr)
{
  int16_t lSize;
  if (aDescr->sCLAddPos >= aDescr->sCLRdPos)
    lSize = (int16_t)aDescr->sCLBufSize - aDescr->sCLAddPos + aDescr->sCLRdPos - sizeof(uint16_t);
  else lSize = (int16_t)aDescr->sCLRdPos - aDescr->sCLAddPos - sizeof(uint16_t);
  if (aHasHdr) lSize -= sizeof(uint16_t);
  if (lSize > 0) return lSize; else return 0;
}

bool MyCircLBuf_Add(MY_CIRCLBUF_TypeDef *aDescr,char *aSource,int aCount)
{
  uint16_t *lCounter, lSubCount, lSize;
  bool lNeedHdr;
  //
  if (aCount <= TOEND) aCount = StrLength(aSource);
  if (aCount == 0) return TRUE;
  lNeedHdr = (aDescr->sCLWrPos == aDescr->sCLAddPos);
  lSize = freeCSize(aDescr, lNeedHdr);
  // *!* / PostLogInt("freeCSize:", lSize, S_CRLF);
  //if (aDescr->sCWrPos != aDescr->sCAddPos) lSize -= aDescr->sCWrPos;
  if (aCount > lSize) { aDescr->sCLAddPos = aDescr->sCLWrPos; return FALSE; }
  lCounter = (uint16_t*)&aDescr->sCLBuffer[aDescr->sCLWrPos];
  if (lNeedHdr) {
    aDescr->sCLAddPos += sizeof(uint16_t);
    if (aDescr->sCLAddPos >= aDescr->sCLBufSize) aDescr->sCLAddPos = 0;
    *lCounter = 0;
  }
  lSubCount = aDescr->sCLBufSize - aDescr->sCLAddPos;
  if (lSubCount < aCount) {
    MemCopy(aSource, &aDescr->sCLBuffer[aDescr->sCLAddPos], lSubCount);
    MemCopy(&aSource[lSubCount], aDescr->sCLBuffer, aCount - lSubCount);
  }
  else
    MemCopy(aSource, &aDescr->sCLBuffer[aDescr->sCLAddPos], aCount);
  *lCounter += aCount; aDescr->sCLAddPos += aCount;
  if (aDescr->sCLAddPos >= aDescr->sCLBufSize) aDescr->sCLAddPos -= aDescr->sCLBufSize;
  return TRUE;
}

bool MyCircLBuf_Write(MY_CIRCLBUF_TypeDef *aDescr,char *aSource,int aCount)
{
  bool lRes;
  //
  if (aSource != NULL)
    lRes = MyCircLBuf_Add(aDescr, aSource, aCount);
  else lRes = TRUE;
  if (lRes && (aDescr->sCLWrPos != aDescr->sCLAddPos)) {
    aDescr->sCLWrPos = aDescr->sCLAddPos;
    if ((aDescr->sCLBufSize - aDescr->sCLWrPos) < sizeof(uint16_t)) {
      aDescr->sCLWrPos = 0; aDescr->sCLAddPos = 0;
    }
    return TRUE;
  }
  return FALSE;
}

bool MyCircLBuf_Read(MY_CIRCLBUF_TypeDef *aDescr,char *aDest,uint16_t aDestSize,uint16_t *aCount)
{
  uint16_t lDelPos, lSize;
  //
  if (aDescr->sCLRdPos != aDescr->sCLWrPos) {
    *aCount = *(uint16_t*)&aDescr->sCLBuffer[aDescr->sCLRdPos];
    if (*aCount <= aDestSize) {
      if (aDest != NULL) {
        lDelPos = aDescr->sCLRdPos + sizeof(uint16_t);
        if (lDelPos >= aDescr->sCLBufSize) lDelPos = 0;
        if ((lDelPos + *aCount) > aDescr->sCLBufSize) {
          lSize = aDescr->sCLBufSize - lDelPos;
          MemCopy(&aDescr->sCLBuffer[lDelPos], aDest, lSize);
          MemCopy(aDescr->sCLBuffer, &aDest[lSize], *aCount - lSize);
        }
        else MemCopy(&aDescr->sCLBuffer[lDelPos], aDest, *aCount);
        if (!aDescr->sCLReadNoDel) MyCircLBuf_Delete(aDescr);
/*
        aDescr->sCRdPos += *aCount + sizeof(uint16_t);
        if (aDescr->sCRdPos >= aDescr->sCBufSize) aDescr->sCRdPos -= aDescr->sCBufSize;
        else
          if ((aDescr->sCBufSize - aDescr->sCRdPos) < sizeof(uint16_t)) aDescr->sCRdPos = 0;
*/
      }
      return TRUE;
    }
    else return FALSE;
  }
  else {
    *aCount = 0;
    return FALSE;
  }
}

bool MyCircLBuf_Delete(MY_CIRCLBUF_TypeDef *aDescr)
{
  uint16_t lSize;
  //
  if (aDescr->sCLRdPos != aDescr->sCLWrPos) {
    lSize = *(uint16_t*)&aDescr->sCLBuffer[aDescr->sCLRdPos];
    aDescr->sCLRdPos += lSize + sizeof(uint16_t);
    if (aDescr->sCLRdPos >= aDescr->sCLBufSize) aDescr->sCLRdPos -= aDescr->sCLBufSize;
    else
      if ((aDescr->sCLBufSize - aDescr->sCLRdPos) < sizeof(uint16_t)) aDescr->sCLRdPos = 0;
    return TRUE;
  }
  return FALSE;
}

bool MyCircLBuf_IsEmpty(MY_CIRCLBUF_TypeDef *aDescr)
{
  return (aDescr->sCLRdPos == aDescr->sCLWrPos);
}

/*
  ////
  if (aDescr->sC4AddPos >= aDescr->sC4DelPos) {
    lSizeA = (int16_t)aDescr->sC4BufSize - aDescr->sC4AddPos - 4;
    if (lSizeA < aCount)
      lSizeB = aDescr->sC4DelPos - 1 - 4;
  }
  else lSizeA = aDescr->sC4DelPos - aDescr->sC4AddPos - 1 - 4;
  if ((lSizeA + lSizeB) >= aCount) {
    lBuf.uChar = &aDescr->sC4Buffer[aDescr->sC4AddPos];
    lBuf.uWord[0] = 0; if (lSizeA > aCount) lSizeA = aCount; lBuf.uWord[1] = lSizeA;
    MemCopy(aSource, &lBuf.uWord[2], lSizeA);
    if (lSizeB == 0)
      incPos(aDescr, &aDescr->sC4AddPos, lSizeA);
    else {
      aSource += lSizeA; lSizeB = aCount - lSizeA;
      lBuf.uChar = aDescr->sC4Buffer;
      lBuf.uWord[0] = 0; lBuf.uWord[1] = lSizeB;
      MemCopy(aSource, &lBuf.uWord[2], lSizeB);
      aDescr->sC4AddPos = 0; incPos(aDescr, &aDescr->sC4AddPos, lSizeB);
    }
    return TRUE;
  }
  return FALSE;
}
*/
/*
MY_INDEXLIST_TypeDef* MyIndexList_Init(MY_INDEXLIST_TypeDef *aList,
  uint16_t aCapacity,uint16_t aBlockSize)
{
  Result := MyList_Init(@aList.sIL, aCapacity, sizeof(uint16_t));
  aList.sILBuffer := void(uint32_t(aList) + sizeof(TMyIndexList) -
    sizeof(TMyList) + Result);
  aList.sILBlockSize := aBlockSize;
  ClearItems(aList.sILBuffer, aBlockSize, 0, aCapacity);
  aList.sILIndexSize := sizeof(uint32_t);

  Result := MyIndexList_Size(aCapacity, aBlockSize)
}

uint16_t MyIndexList_Size(uint16_t aCapacity,uint16_t aBlockSize)
{
  return MyList_Size(aCapacity, sizeof(uint16_t)) + sizeof(MY_INDEXLIST_TypeDef) -
    sizeof(MY_LIST_TypeDef) + aCapacity * aBlockSize
}
*/

/*
#define CRCMASK    0xA001
#define CRCBASE    0xFFFF

uint16_t MyCompCRC16(void *aBuffer,int aCount)
{
  uint16_t lCRC = CRCBASE;
  bool lOdd;
  while (aCount-- > 0) {
    lCRC ^= *(char*)aBuffer;
    (*(int*)&aBuffer)++;
    for (register int I=7;0;I--) {
      lOdd = (lCRC & 1) == 1;
      lCRC >>= 1;
      if (lOdd) lCRC ^= CRCMASK;
    }
  }
  return lCRC;
}
*/