// ���������� ���������� � gps/glonass �������
// � Olgvel, 2018
//
#include "r_gnss.h"

#define GNSS_READ_WAIT_TIME  20
#define GNSS_BUFFER_SIZE     256

typedef enum {
    GNF_LOGENB
    //
} GNSS_FLAG_TypeDef;

//==============================================================================

bool getCmmParam(char* str,int size,int& searchPos,char*& parPtr,int& parSize)
{
    int lpos;
    bool lres = GetInParamEx(COMMA, str, size, &searchPos, &lpos, &parSize);
    if (lres) parPtr = &str[lpos];
    return lres;
}

void AddNmeaCSum(char *aStr)
{
    r_byte lSC=0;
    while (*aStr) 
        lSC ^= *aStr++;
    *aStr++ = '*';
    Int2HexZ(lSC, aStr, 2);
}  
//==============================================================================

void rGNSS_mng::init(MY_UART_TypeDef *myUART,const PIN_HWCFG_TypeDef *pinRST)
{
    fpUART = myUART;
    //
    PIN_Config(pinRST);
    PIN_Set(pinRST, true);
    //
    CSET(fFlags, GNF_LOGENB, false);
    //
    //fLong = 123456;
}

void rGNSS_mng::process()
{
    char lRxStr[GNSS_BUFFER_SIZE+4];
    //
    int lRes, lRxCount;
      lRes = MyCBuf_ReadFmt(fpUART->sLRx, lRxStr, GNSS_BUFFER_SIZE, "\2\n\r", 0x03, GNSS_READ_WAIT_TIME, &lRxCount);
      switch (lRes) {
        case RRF_TIMER:
          if TEST_OK(fFlags, GNF_LOGENB) {
            PostLogInt("GNSS TIMER [", lRxCount, "]  ");
            PostLogMessage(lRxStr, lRxCount); PostLogText(S_CRLF);
          }  
          break;
        case RRF_INFULL:
          if TEST_OK(fFlags, GNF_LOGENB)      
            PostLogInt("GNSS INFULL [", lRxCount, "]\r\n");
          break;
        case 1:
          rcvRMC(lRxStr, lRxCount);  
          if TEST_OK(fFlags, GNF_LOGENB) {     
            //rcvRMC(lRxStr, lRxCount);  
            PostLogMsg("GNSS >> ", lRxStr, lRxCount, S_CRLF);      
          }  
          break;
          //
      }
}

uint8_t str2bcd(char* str)
{    
    return ((str[0]-0x30)<<4)|(str[1]-0x30)&0x0F;
}

int strCoord2Int(char* str,int size)
{    
    char lstr[12];
    int ldp=0;
    if (size > 8) {
        if (str[4] == '.') ldp = 4; else if (str[5] == '.') ldp = 5;
        //
        if (ldp > 0) {
            MemCopy(str, lstr, ldp);
            MemCopy(&str[ldp+1], &lstr[ldp], size-ldp-1);
            int lres = Str2IntDef(lstr, size-1, 0);
            if ((size-ldp) == 5) lres *= 10;
            return lres;
        }    
    }    
    return 0;
}

void rGNSS_mng::rcvRMC(char* str,int size)
{
    UINT64_U lts, lcoords;
    char* lpar;
    int lsize, lsp =0, lidx =0;
    bool ldetect =false;
    
    while (getCmmParam(str, size, lsp, lpar, lsize)) {
        if ((lidx == 0) && MemComp("$G", lpar, 2) && MemComp("RMC", lpar+3, 3)/*MemComp("$GNRMC", lpar, lsize)*/) {
            ldetect = true;        
            MemClear(&lts, sizeof(lts));
            MemClear(&lcoords, sizeof(lcoords));
        }
        if (ldetect) 
            switch (lidx) {
                case 1:  
                    if (lsize >= 6) {
                        lts.Bytes[0] = str2bcd(lpar); lpar += 2;
                        lts.Bytes[1] = str2bcd(lpar); lpar += 2;
                        lts.Bytes[2] = str2bcd(lpar);
                    }    
                    break; //time
                case 3:  lcoords.Ints[0] = strCoord2Int(lpar, lsize); break; //latitude   
                case 4:  if ((lsize == 1) && (lpar[0] == 'S')) lcoords.Ints[0] *= -1; break; // N (S)   
                case 5:  lcoords.Ints[1] = strCoord2Int(lpar, lsize); break; //longitude   
                case 6:  if ((lsize == 1) && (lpar[0] == 'W')) lcoords.Ints[1] *= -1; break; // E (W)   
                case 9:  if (lsize >= 6) {
                        lts.Bytes[6] = str2bcd(lpar); lpar += 2;
                        lts.Bytes[5] = str2bcd(lpar); lpar += 2;
                        lts.Bytes[7] = str2bcd(lpar);
                    }    
                    break; //date 
            }    
        ++lidx;
    }
    if (ldetect) {
        if (lts.Bytes[7])  MHandler_Call(&onTimeStamp, &lts);    
        if (lcoords.LLong) MHandler_Call(&onCoords, &lcoords);    
    }    
}

//===============================================================================

void _tch_gnConfig(rGNSS_mng &mng,TXTCMD_PAR_TypeDef &par)
{
    par.sTCPResult = (par.sTCPCount == 0) || (par.sTCPCount > 0) && (par.sTCPVals[0] <= 48);
    if (par.sTCPResult) {
        char lstr[64];     
        switch (par.sTCPVals[0]) {
            case  0: CSET(mng.fFlags, GNF_LOGENB, false); break;
            case  1: CSET(mng.fFlags, GNF_LOGENB, true); break;
            case 10: AssignStr("$PSIMIPR,T", lstr); break;
            case 11: AssignStr("$PSIMIPR,W,115200", lstr); break;
        }
        if (par.sTCPVals[0] >= 10) {
            AddNmeaCSum(&lstr[1]);
            MyCBuf_WriteTextW(mng.fpUART->sLTx, lstr);
            MyCBuf_WriteText(mng.fpUART->sLTx, S_CRLF);
            PostLogText(lstr);
        }  
    }  
    //PostLogInt("\r\n_tch_gnConfig=", mng.fLong, "\r\n");
}
