// ����������� � �������� ��������� VAP[3]
// � Olgvel, 2016-2018
//
#ifndef __R_VAP3_H
#define __R_VAP3_H

#ifdef __ICCARM__
#include "my_lib.h"
#else
#include "r_nostm_supp.h"
#endif
//
#define BROADCASTDEVICE   0x0F0F0F0F

#define CPROCDATA         0x8000
#define PINGDATA          0x4000
#define FULLADDR          0x2000

#define VAPFRAMESIZE      sizeof(VAP_Frame_TypeDef)

/* Exported types ------------------------------------------------------------*/

typedef enum {
    BCS_AnsNo, BCS_AnsOk, BCS_Neutral, BCS_Query, BCS_NotFound, BCS_Notify,
    BCS_max
} BCS_TypeDef;

typedef struct {
  char *sBCPGet;
  int   sBCPGetPos;
  int   sBCPGetEnd;
  char *sBCPPut;
  int   sBCPPutPos;
  BCS_TypeDef sBCPState;
  bool  sBCPResult;
} BINCMD_PAR_TypeDef;

//
typedef struct {
    uint32_t sVFPreamb;
    union {
        struct {	  	
            uint16_t sVFLenFlags;
            uint16_t sVFNumFlags;
        };
        uint32_t sVFFlags;
    };
    uint32_t sVFPreaMask;
    uint8_t  sVFData[];
} VAP_Frame_TypeDef;

typedef enum {
  VSK_UNKNOWN, VSK_MASTER, VSK_SLAVE, VSK_AUTO
  //
} VAP_SENDER_TypeDef;

typedef void(* BCHANDLER)(void *aContext,BINCMD_PAR_TypeDef *aParam);

typedef struct {
  BCHANDLER sBCHandler;
  int16_t  sBCCtxIdx;
  uint16_t  sBCFlags;
  //
} BCHANDLER_TypeDef;

//==============================================================================
#ifdef __cplusplus
extern "C" {
#endif
bool VAPFrameHdrSize(uint32_t aDestID,uint32_t aSenderID,uint8_t *aSize);  
int VAPFrameSize(uint32_t aDestID,uint32_t aSenderID);
//
void VAPBuildFrame(VAP_SENDER_TypeDef aSender,uint32_t aDevID,
  uint32_t aDestID, uint32_t aSenderID, uint16_t aFrameNum, uint16_t aFlags,
  void *aData,uint16_t aDataLen,void *aFrame,uint16_t *aFrameLen);
//    
bool VAPParseFrame(void *aFrame,uint16_t aFrameLen,VAP_SENDER_TypeDef aSenderFlt,
  uint32_t aDevID, uint32_t *aDestID, uint32_t *aSenderID,
  VAP_SENDER_TypeDef *aSender,uint16_t *aFrameNum,uint16_t *aFlags,
  void *aData,uint16_t *aDataLen,int *aRetPos);
//
int EncodeBase64Size(int aSrcSize);
void EncodeBase64(void *aSource,int aCount,void *aDest,int *aDestCount);
bool DecodeBase64(void *aSource,int aCount,void *aDest,int *aDestCount);
//
bool GetFieldLength(void *aAddr,uint16_t *aLength,uint8_t *aOffs,bool *aError);
uint32_t SetFieldLength(uint32_t aLength,uint32_t *aLenCode,uint8_t *aCount);
uint32_t SetFieldLengthEx(uint32_t aLength,uint32_t *aLenCode,uint8_t *aCount,bool aFixLen);
int ReduceUInt(uint32_t aValue);
//
bool BCGetParPtr(BINCMD_PAR_TypeDef *aBCP,char **aParPtr,int *aParLen);
bool BCGetStrPar(BINCMD_PAR_TypeDef *aBCP,char *aStr,int *aParLen);
bool BCGetUIntPar(BINCMD_PAR_TypeDef *aBCP,uint32_t *aValue);
bool BCGetIntPar(BINCMD_PAR_TypeDef *aBCP,int *aValue);
bool BCPutParPtr(BINCMD_PAR_TypeDef *aBCP,char **aParPtr,int aParLen);
char* BCPutStrPar(BINCMD_PAR_TypeDef *aBCP,char *aStr,int aParLen);
bool BCPutUIntPar(BINCMD_PAR_TypeDef *aBCP,uint32_t aValue);
//
#ifdef __cplusplus
}
#endif
//
char* GetNextCmdPtr(int aParLen,char *aParams,int *aLength);
void AddNextCmdParam(void *aPar,int aParLen,char *aParams,int *aLength);
bool AddAnswerState(bool aState,char *aParams,int *aLength);
bool FindNextCmdParam(char *aParams,int aLength,int *aParPos,int *aParLen);
bool FindNextCmdIntPar(char *aParams,int aLength,int *aParPos,int *aParLen,uint32_t *aPar);
bool FindNextCmdIntParDef(char *aParams,int aLength,int *aParPos,int *aParLen,uint32_t *aPar,uint32_t aDef);

#endif /* __R_VAP_H */

