//---------------------------------------------------------------------------
// r_types.h - basic type definitions of rlib
// rlib - universal cross-platform realtime library
// � olgvel, 2018
//---------------------------------------------------------------------------

#ifndef __R_TYPES_H
#define __R_TYPES_H
//---------------------------------------------------------------------------

#ifdef __ICCARM__
#else
#endif

#ifdef __cplusplus
//extern "C" {

//#include <array>
//#include <bitset>
//#include <vector>
//#include <string>
//#include <complex>
//#include <functional>
//#include <iostream>

using r_uint8  = uint8_t;
using r_uint16 = uint16_t;
using r_uint32 = uint32_t;
using r_uint64 = uint64_t;
using r_int8  = int8_t;
using r_int16 = int16_t;
using r_int32 = int32_t;
using r_int64 = int64_t;
using r_byte = r_uint8;
using r_word = r_uint16;
using r_long = r_uint32;
using r_llong = r_uint64;
using r_sint = r_int8;
using r_short= r_int16;
using r_int  = r_int32;
using r_lint = r_int64;
//
typedef union {
  r_byte bytes[sizeof(r_long)];
  r_word words[sizeof(r_word)];
  r_long Long;
  r_int  Int;
} rLongU;
//
typedef union {
  r_byte bytes[sizeof(r_word)];
  r_word Word;
  r_short Short;
} rWordU;

#endif

/*
using rbytes = std::basic_string<r_byte>;
using rstring = std::string;
using rwstring = std::wstring;
using rsstream = std::stringstream;
//
using rmsgProc = void(*)(const rstring &);
//
using rbitset = std::bitset<32>;
using rflag_il = std::initializer_list<r_byte>;
//
class rflagset: public rbitset {
public:
    rflagset(): rbitset() {};
    rflagset(const rflag_il &l) { mset(l); }
    void mset(const rflag_il &l) { for (auto i : l) set(i); }
};*/
//
//template<typename T,int N>
//using rarray = std::array<T,N>;
/*
template<typename T>
using rvector = std::vector<T>;
//
template<typename T>
using revent = std::function<T>;
//
const auto rc_npos = std::string::npos;
*/
#endif
