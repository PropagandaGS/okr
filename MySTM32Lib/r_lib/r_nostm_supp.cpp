//---------------------------------------------------------------------------

#pragma hdrstop

#include "r_nostm_supp.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

using namespace std;

r_long random32(void)
{
    return (r_long)rand();
}

void PostLogText(const rstring& aText)
{

}

void int2hex(unsigned int aInt,void *aBuffer,int aDigits)
{
  const char lCodes[16] {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
  (*(int*)&aBuffer) += aDigits;
  while (aDigits-- > 0) {
    (*(int*)&aBuffer)--;
    *(char*)aBuffer = lCodes[aInt & 15];
    aInt >>= 4;
  }
}

rstring bin_hex(const rbytes& bytes,int byte_count,bool ins_spc)
{
  if((byte_count < 0)||(byte_count >  bytes.size())) byte_count = bytes.size();
  rstring lhex;
  if (ins_spc) lhex.resize(byte_count*3-1);else lhex.resize(byte_count*2);
  auto lbin_buf = bytes.data();
  auto lhex_buf = cdata(lhex);
  while (byte_count-- > 0) {
    int2hex(*lbin_buf++, lhex_buf, 2);
    lhex_buf += 2;
    if ((ins_spc) && (byte_count > 0)) *lhex_buf++ = 0x20;
  }
  return lhex;
}

r_long strIPv4_uint(const rstring &ip)
{
    r_long lres=0, lfrom = 0;
    //
    return lres;
}
/*
function StrIPv4ToUInt(const aIP: String): uint32_t;
var I, lFrom, lPos, lVal: Integer;
    lRes: uint32_t;
begin
  Result := 0; lRes := 0; lFrom := MinStrIdx;
  for I := 3 downto 0 do begin
    if I = 0 then lPos := Succ(Length(aIP))
    else lPos := Pos('.', aIP, lFrom);
    if lPos > 0 then begin
      lVal := StrToIntDef(Copy(aIP, lFrom, lPos-lFrom), -1);
      if (lVal >= 0) and (lVal <= $FF) then
        lRes := (lRes shl 8) or Byte(lVal)
      else Exit;
      lFrom := Succ(lPos)
    end
    else Exit
  end;
  Result := lRes
end;
*/

rstring uintIPv4_str(r_long ip)
{
    rstring lstr = "0.0.0.0";

    return lstr;
}
/*
function UIntIPv4ToStr(aBinIP: uint32_t): String;
var I: Integer;
begin
  for I := 3 downto 0 do begin
    Result := IntToStr(aBinIP and $FF) + Result;
    if I > 0 then Result := '.' + Result;
    aBinIP := aBinIP shr 8
  end
end;
*/

