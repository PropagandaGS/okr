//---------------------------------------------------------------------------

#ifndef r_nostm_suppH
#define r_nostm_suppH
//---------------------------------------------------------------------------
#include <rtypes.hpp>

const int ERROR_RESULT = -1;

typedef union {
  char *uChar;
  uint8_t *uByte;
  uint16_t *uWord;
  uint32_t *uLong;
  void *uPtr;
  uint32_t uAddr;
} PTR_U;

typedef union {
  uint8_t Bytes[sizeof(uint32_t)];
  uint16_t Words[sizeof(uint16_t)];
  uint32_t Long;
} UINT32_U;


typedef union {
  uint8_t Bytes[sizeof(uint16_t)];
  uint16_t Word;
} UINT16_U;

//---------------------------------------------------------------------------
uint32_t random32(void);
void PostLogText(const rstring &aText);
//
rstring bin_hex(const rbytes &bytes,int byte_count=-1,bool ins_spc=false);

r_long  strIPv4_uint(const rstring &ip);
rstring uintIPv4_str(r_long ip);

#endif
