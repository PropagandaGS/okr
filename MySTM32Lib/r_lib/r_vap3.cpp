// ���������� ��������� VAP[3]
// ����������� � �������� ��������� VAP[3]
// � Olgvel, 2016-2018
//
#include "r_vap3.h"
#include "r_crc32.h"
#include "string.h"
#include "my_stm32.h"

constexpr auto VAPMASTERPREAMB = 0x5D24C36A;
constexpr auto VAPSLAVEPREAMB  = 0x5A28C96D;

constexpr auto VFFLAGSMASK  = 0x07FF;
constexpr auto LONGDATAMASK = 0x8000;

constexpr auto crc32size = sizeof(uint32_t);

//constexpr uint32_t BROADCASTDEVICE = 0x0F0F0F0F;
//constexpr uint16_t CRCMASK = 0xA001;
//constexpr uint16_t CRCBASE = 0xFFFF;

/*
#define CMD_UNKNOWN      1
#define CMD_DEVCONTROL   7
#define CMD_EVUPDATE     9
#define CMD_EVUPDATEANS  10
#define CMD_TUNNELDATA   11
*/

void HashBlock(void *aBlock, uint32_t aBlockLen, uint32_t aHashMask)
{
    auto lMod = aBlockLen & 3; aBlockLen >>= 2; 
    auto lItem = (uint32_t*)aBlock;
    while (aBlockLen-- > 0) {
        aHashMask = (aHashMask << 3) | (aHashMask >> 29);
        *lItem++ ^= aHashMask;
    }
    if (lMod > 0) {
        aHashMask = (aHashMask << 3) | (aHashMask >> 29);
        *lItem++ ^= (aHashMask & (0xFFFFFFFF >> ((4-lMod) << 3)));
    }
}

bool VAPFrameHdrSize(uint32_t aDestID,uint32_t aSenderID,uint8_t *aSize)
{
    *aSize = VAPFRAMESIZE;
    if ((aDestID != 0)||(aSenderID != 0)) (*aSize) += 2*sizeof(uint32_t);
    return true;
}

int VAPFrameSize(uint32_t aDestID,uint32_t aSenderID)
{
    uint8_t lhdr;
    VAPFrameHdrSize(aDestID, aSenderID, &lhdr);
    return crc32size + lhdr;
}

void VAPBuildFrame(VAP_SENDER_TypeDef aSender, uint32_t aDevID,
    uint32_t aDestID, uint32_t aSenderID, uint16_t aFrameNum, uint16_t aFlags,
    void *aData,uint16_t aDataLen,void *aFrame,uint16_t *aFrameLen)
{
//PostLogTextDly("vap_bf_1"S_CRLF, 5);
    auto &lFrame = *(VAP_Frame_TypeDef*)aFrame;
    lFrame.sVFPreaMask = random32();
    uint32_t lHashMask;
    if (aSender == VSK_MASTER) lHashMask = lFrame.sVFPreaMask ^ VAPMASTERPREAMB;
    else lHashMask = lFrame.sVFPreaMask ^ VAPSLAVEPREAMB;
    lFrame.sVFPreamb = lHashMask ^ aDevID;
    uint8_t lDOffs = 0;
    if ((aDestID != 0)||(aSenderID != 0)) {
        auto lLongs = (uint32_t*)(&lFrame.sVFData);
        lLongs[0] = aDestID ^ lHashMask;
        lLongs[1] = aSenderID ^ lHashMask;
        lDOffs = 8; aFlags |= FULLADDR;
    }
    auto lFrameLen = (uint16_t)VAPFRAMESIZE + aDataLen + lDOffs;
    lFrame.sVFLenFlags = (lFrameLen & VFFLAGSMASK) | (aFlags & ~VFFLAGSMASK);
    lFrame.sVFNumFlags = (aFrameNum & VFFLAGSMASK) | (aFlags & ~VFFLAGSMASK);
    lFrame.sVFFlags ^= lHashMask;
//PostLogTextDly("vap_bf_2"S_CRLF, 5);
    if (&lFrame.sVFData[lDOffs] != aData)
        memcpy(&lFrame.sVFData[lDOffs], aData, aDataLen);
    //
  //PostLogTextDly("vap_bf_3"S_CRLF, 5);
    auto lCRC32 = crc32eth(aFrame, lFrameLen, 0);
    memcpy(&lFrame.sVFData[lDOffs + aDataLen], &lCRC32, crc32size);
//PostLogTextDly("vap_bf_4"S_CRLF, 5);
    HashBlock(&lFrame.sVFData[lDOffs], aDataLen + crc32size, lHashMask); //align32
    *aFrameLen = lFrameLen + crc32size;
//PostLogTextDly("vap_bf_5"S_CRLF, 5);
}

bool VAPParseFrame(void *aFrame,uint16_t aFrameLen,VAP_SENDER_TypeDef aSenderFlt,
    uint32_t aDevID, uint32_t *aDestID, uint32_t *aSenderID,
    VAP_SENDER_TypeDef *aSender,uint16_t *aFrameNum,uint16_t *aFlags,
    void *aData,uint16_t *aDataLen,int *aRetPos)
{
    if (aFrameLen < VAPFRAMESIZE) { *aRetPos = aFrameLen; return false; }
    auto &lFrame = *(VAP_Frame_TypeDef*)aFrame; *aRetPos = ERROR_RESULT;
    //
    auto lHashMask = lFrame.sVFPreamb ^ aDevID;
    if (aSenderFlt == VSK_AUTO) {
        if ((lHashMask ^ lFrame.sVFPreaMask) == VAPMASTERPREAMB) aSenderFlt = VSK_MASTER; else
        if ((lHashMask ^ lFrame.sVFPreaMask) == VAPSLAVEPREAMB) aSenderFlt = VSK_SLAVE; else
        return false;
    } else
    if (aSenderFlt == VSK_MASTER)
        if ((lHashMask ^ lFrame.sVFPreaMask) != VAPMASTERPREAMB) return false; else; else
    if (aSenderFlt == VSK_SLAVE)
        if ((lHashMask ^ lFrame.sVFPreaMask) != VAPSLAVEPREAMB) return false; else; else
    return false;
	//PostLogTextDly("vap_pf_1"S_CRLF, 10);
    UINT32_U lUnion;
    lUnion.Long = lFrame.sVFFlags ^ lHashMask;
    //
    auto lFrameLen = lUnion.Words[0] & VFFLAGSMASK;
    auto lFlags = lUnion.Words[0] & ~VFFLAGSMASK;
    if (lFlags != (lUnion.Words[1] & ~VFFLAGSMASK)) return false;
    uint8_t lDOffs = 0;
    if ((lFlags & FULLADDR) != 0) lDOffs = 8;
    //
    auto lDataLen = lFrameLen-VAPFRAMESIZE-lDOffs;
    if (&lFrame.sVFData[lDOffs] != aData)
    memcpy(aData, &lFrame.sVFData[lDOffs], lDataLen + crc32size);
//PostLogTextDly("vap_pf_2"S_CRLF, 5);
    HashBlock(aData, lDataLen + crc32size, lHashMask); //align32
//PostLogTextDly("vap_pf_3"S_CRLF, 5);
    auto lCRC32 = crc32eth(&lFrame, VAPFRAMESIZE+lDOffs, 0);
//PostLogTextDly("vap_pf_4"S_CRLF, 5);
    //
    if (lDataLen > 0) lCRC32 = crc32eth(aData, lDataLen, lCRC32);
//PostLogTextDly("vap_pf_5"S_CRLF, 5);
    if (memcmp(&lCRC32, &((uint8_t*)aData)[lDataLen], crc32size) != 0) {
    	PostLogText("exit 4\r\n"); return false;
    }
//PostLogTextDly("vap_pf_6"S_CRLF, 5);
  //
    *aSender = aSenderFlt;
    *aFrameNum = lUnion.Words[1] & VFFLAGSMASK;
    *aFlags = lFlags;
//PostLogTextDly("vap_pf_7"S_CRLF, 5);
    if ((lFlags & FULLADDR) != 0) {
        if /*assigned(*/(aDestID) *aDestID = *(uint32_t*)(&lFrame.sVFData[0]) ^ lHashMask;
        if /*assigned(*/(aSenderID) *aSenderID = *(uint32_t*)(&lFrame.sVFData[4]) ^ lHashMask;
    }
    else {
        if /*assigned*/(aDestID) *aDestID = 0;
        if /*assigned*/(aSenderID) *aSenderID = 0;
    }
//PostLogTextDly("vap_pf_8"S_CRLF, 5);
    *aDataLen = lDataLen;
    *aRetPos = lFrameLen + crc32size;
//PostLogTextDly("vap_pf_9"S_CRLF, 5);
  //
    return true;
}

char* GetNextCmdPtr(int aParLen,char *aParams,int *aLength)
{
    auto lPos = *aLength; 
    if (aParLen < 128) aParams[lPos++] = aParLen;
    else { *(uint16_t*)(&aParams[lPos]) = aParLen | LONGDATAMASK; lPos += 2; }
    *aLength = lPos + aParLen;
    return &aParams[lPos];
}

bool FindNextCmdParam(char *aParams,int aLength,int *aParPos,int *aParLen)
{
    uint16_t lPos = *aParPos + *aParLen;
    if (aLength > 1) {
        uint16_t lLen;
        if ((uint8_t)aParams[lPos] < 128) { lLen = aParams[lPos]; lPos++; }
        else { lLen = *(uint16_t*)(&aParams[lPos]) & ~LONGDATAMASK; lPos += 2; }
        if ((lPos + lLen) <= aLength) {
            *aParPos = lPos; *aParLen = lLen;
            return true;
        }
    }
    return false;
}

void AddNextCmdParam(void *aPar,int aParLen,char *aParams,int *aLength)
{
    memcpy(GetNextCmdPtr(aParLen, aParams, aLength), aPar, aParLen);
}

bool AddAnswerState(bool aState,char *aParams,int *aLength)
{
    AddNextCmdParam(&aState, 1, aParams, aLength);
    return true;
}

bool FindNextCmdIntPar(char *aParams,int aLength,int *aParPos,int *aParLen,uint32_t *aPar)
{
    if (FindNextCmdParam(aParams, aLength, aParPos, aParLen) &&
        (*aParLen <= sizeof(uint32_t))) {
        *aPar = 0;
        memcpy(aPar, &aParams[*aParPos], *aParLen);
        return true;
    }
    return false;
}

bool FindNextCmdIntParDef(char *aParams,int aLength,int *aParPos,int *aParLen,uint32_t *aPar,uint32_t aDef)
{
    if (FindNextCmdIntPar(aParams, aLength, aParPos, aParLen, aPar)) return true;
    *aPar = aDef;
    return false;
}

//==============================================================================

constexpr uint8_t base64ShTbl[4] = {2,0,3,1};
constexpr auto BASE64MASK = 0xED9BC6A5;

int EncodeBase64Size(int aSrcSize)
{
    return ((aSrcSize + 2) / 3) << 2;
}

void EncodeBase64(void *aSource,int aCount,void *aDest,int *aDestCount)
{
    auto lSource = (char*)aSource;
    auto lDest = (char*)aDest;
    aCount = (aCount + 2) / 3;
    *aDestCount = aCount << 2;
    while (aCount-- > 0) {
        uint32_t lLong;
        memcpy(&lLong, lSource, 3);
        lLong ^= BASE64MASK;
        for(int i=0;i<4;++i) {
            uint8_t lCode = lLong & 0x3F;
            auto k = base64ShTbl[i];
            if (lCode < 27) lDest[k] = 0x40+lCode; else
            if (lCode < 33) lDest[k] = 0x30+lCode-27; else
            if (lCode < 59) lDest[k] = 0x61+lCode-33; else
            if (lCode < 63) lDest[k] = 0x36+lCode-59;
            else lDest[k] = 0x24;
            lLong >>= 6;
        }
        lSource += 3;
        lDest += 4;
    }
}

bool DecodeBase64(void *aSource,int aCount,void *aDest,int *aDestCount)
{
    auto lSource = (uint8_t*)aSource;
    auto lDest = (uint8_t*)aDest;
    UINT32_U lU; lU.Long = 0;
    aCount = (aCount + 3) / 4;
    *aDestCount = aCount * 3;
    while (aCount-- > 0) {
    for(int i=3;i>=0;--i) {
        auto lCode = lSource[base64ShTbl[i]];
        if ((lCode >= 0x40) && (lCode <= 0x5A)) lCode -= 0x40; else
        if ((lCode >= 0x61) && (lCode <= 0x7A)) lCode -= 0x40; else
        if ((lCode >= 0x30) && (lCode <= 0x35)) lCode -= 0x15; else
        if ((lCode >= 0x36) && (lCode <= 0x39)) lCode += 5; else
        if (lCode == 0x24) lCode = 63;
        else return false;
        lU.Long = (lU.Long << 6) | (lCode & 0x3F);
    }
    lU.Long ^= BASE64MASK;
    for(int i=0;i<3;++i) *lDest++ = lU.Bytes[i];
    lSource += 4;
    }
    return true;
}

//==============================================================================

bool GetFieldLength(void *aAddr,uint16_t *aLength,uint8_t *aOffs,bool *aError)
{
    uint8_t lPreOffs=0;
    auto lAddr = (uint8_t*)aAddr;
    while ((lAddr[lPreOffs] == 0xFD) && (lPreOffs < 3)) ++lPreOffs;
    uint16_t lLength = lAddr[lPreOffs];
    if (lLength == 0) {
        *aError = true; return false;
    }
    else
        if (lLength == 0xFF) {
            *aError = false; return false;
        }
        else
            if (lLength <= 0x7F) {
                *aLength = lLength + lPreOffs; *aOffs = lPreOffs + 1; *aError = false; return true;
            }
            else {
                lLength = (lLength << 8) | *(++lAddr);
                if (lLength <= 0xFCFF) {
                    *aLength = (lLength & 0x7FFF) + lPreOffs; *aOffs = lPreOffs + 2; *aError = false; return true;
                }
            }
    *aError = true;
    return false;
}

uint32_t SetFieldLengthEx(uint32_t aLength,uint32_t *aLenCode,uint8_t *aCount,bool aFixLen)
{
    if (aLength > 0) {
        if ((aLength < 0x7F) && (!aFixLen)) {
            aLength++; *aLenCode = aLength; *aCount = 1; return aLength;
        }
        else
            if (aLength < 0xFCFD) {
                aLength += 2; *aLenCode = ((aLength << 8) & 0xFF00) | (aLength >> 8) | 0x0080;
                *aCount = 2; return aLength;
            }
    }
    return false;
}

uint32_t SetFieldLength(uint32_t aLength,uint32_t *aLenCode,uint8_t *aCount)
{
    return SetFieldLengthEx(aLength, aLenCode, aCount, false);
}

int ReduceUInt(uint32_t aValue)
{
    auto i=3;
    while(i > 0) if (((uint8_t*)&aValue)[i] != 0) break; else --i;
    return i+1;
}
//==============================================================================

bool BCGetParPtr(BINCMD_PAR_TypeDef *aBCP,char **aParPtr,int *aParLen)
{
    uint16_t lLength;
    uint8_t lOffs;
    bool lError;
    //
    if ((aBCP->sBCPGetPos < aBCP->sBCPGetEnd) &&
        GetFieldLength(&aBCP->sBCPGet[aBCP->sBCPGetPos], &lLength, &lOffs, &lError)) {
        *aParPtr = &aBCP->sBCPGet[aBCP->sBCPGetPos+lOffs];
        *aParLen = lLength-lOffs;
        aBCP->sBCPGetPos += lLength;
        return true;
    }
    return false;
}

bool BCGetStrPar(BINCMD_PAR_TypeDef *aBCP,char *aStr,int *aParLen)
{
    char *lBuf;
    int  lLength;
    if (BCGetParPtr(aBCP, &lBuf, &lLength) && (*aParLen >= lLength)) {
        memcpy(aStr, lBuf, lLength);
        *aParLen = lLength;
        return true;
    }
    return false;
}

bool BCGetUIntPar(BINCMD_PAR_TypeDef *aBCP,uint32_t *aValue)
{
    int lSize = sizeof(uint32_t);
    *aValue = 0;
    return BCGetStrPar(aBCP, (char*)aValue, &lSize);
}

bool BCGetIntPar(BINCMD_PAR_TypeDef *aBCP,int *aValue)
{
    return BCGetUIntPar(aBCP, (uint32_t*)aValue);
}

bool BCPutParPtr(BINCMD_PAR_TypeDef *aBCP,char **aParPtr,int aParLen)
{
    uint32_t lLenCode;
    uint8_t  lOffs;
    if (SetFieldLength(aParLen, &lLenCode, &lOffs) != 0) {
        auto lPut = aBCP->sBCPPut;
        memcpy(&lPut[aBCP->sBCPPutPos], &lLenCode, lOffs);
        aBCP->sBCPPutPos += lOffs;
        *aParPtr = &lPut[aBCP->sBCPPutPos];
        aBCP->sBCPPutPos += aParLen;
        return true;
    }
    return false;
}

char* BCPutStrPar(BINCMD_PAR_TypeDef *aBCP,char *aStr,int aParLen)
{
    char *lBuf;
    if(BCPutParPtr(aBCP, &lBuf, aParLen)) {
        memcpy(lBuf, aStr, aParLen);
        return lBuf;
    }     
    return NULL;
}

bool BCPutUIntPar(BINCMD_PAR_TypeDef *aBCP,uint32_t aValue)
{
    if(BCPutStrPar(aBCP, (char*)&aValue, ReduceUInt(aValue))) return true;
    else return false;  
    //return res;///*assigned*/(BCPutStrPar(aBCP, (char*)&aValue, ReduceUInt(aValue)));
}

