// ����������� � �������� ��������� CRC32 Ethernet
// � Olgvel, 2013-2019
//
#ifndef __R_CRC32_H
#define __R_CRC32_H

#ifdef __ICCARM__
#include "r_types.h"
#else
#include "r_nostm_supp.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif
uint32_t crc32eth(void* buffer,int size,uint32_t crc);
#ifdef __cplusplus
}
#endif

#endif