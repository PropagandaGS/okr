// ����������� � �������� ���������
// ������ RTC
// � Olgvel, 2018
//
#ifndef __R_RTC_H
#define __R_RTC_H

#include "r_types.h"
#include "my_stm32.h"

/* Exported types ------------------------------------------------------------*/

#ifdef __cplusplus

class rRTC_mng {
public:
    void init();
    void process();
    //
private:
    //MY_UART_TypeDef *fpUART;
    r_word           fFlags;
    //
    friend void _tch_rtcConfig(rRTC_mng &mng,TXTCMD_PAR_TypeDef &par);  
};

#define TCN_RTCCFG       "\x07+RTCCFG\0"
#define TCH_RTCCFG        {(TCHANDLER)_tch_rtcConfig,TCFINT,-ACX_RTC}   
void _tch_rtcConfig(rRTC_mng &mng,TXTCMD_PAR_TypeDef &par);  

#endif

#endif
