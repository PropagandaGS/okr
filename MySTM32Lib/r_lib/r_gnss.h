// ����������� � �������� ���������
// gps/glonass ������
// � Olgvel, 2018
//
#ifndef __R_GNSS_H
#define __R_GNSS_H

#include "r_types.h"
#include "my_stm32.h"

/* Exported types ------------------------------------------------------------*/

#ifdef __cplusplus

class rGNSS_mng {
public:
    void init(MY_UART_TypeDef *myUART,const PIN_HWCFG_TypeDef *pinGNRST);
    void process();
    //
    MHANDLER_TypeDef onTimeStamp;
    MHANDLER_TypeDef onCoords;
    //
private:
    MY_UART_TypeDef *fpUART;
    r_word           fFlags;
    //
    void rcvRMC(char* str,int size);
    //
    friend void _tch_gnConfig(rGNSS_mng& mng,TXTCMD_PAR_TypeDef& par);  
};

#define TCN_GNCFG       "\x06+GNCFG\0"
#define TCH_GNCFG        {(TCHANDLER)_tch_gnConfig,TCFINT,-ACX_GNSS}   
void _tch_gnConfig(rGNSS_mng& mng,TXTCMD_PAR_TypeDef& par);  

#endif

#endif
