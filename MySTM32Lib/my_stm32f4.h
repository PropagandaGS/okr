// �������� ���������� ������� ��� STM32F4XX
// � Olgvel, 2012-2018
//

#ifndef __MY_STM32F4_H
#define __MY_STM32F4_H

#include "my_lib.h"

#define UID_BASE        ((uint32_t)0x1FFF7A10)
#define UNIID           ((UNIQUE_TypeDef *)UID_BASE)
#define FLASHSIZE_BASE  ((uint32_t)0x1FFF7A22)
#define FLASHSIZE       (*(uint16_t *)FLASHSIZE_BASE)

#if defined(STM32F40_41xxx) 
#define  VTORLENGTH     0x0188 
#endif /* STM32F40_41xxx */
//
#if defined(STM32F427_437xx) 
#define  VTORLENGTH     0x01AC 
#endif /* STM32F427_437xx */
//
#define  VTORWORDS      (VTORLENGTH/sizeof(uint32_t))
#define  LO_APPADDR128K 0x08004000
#define  HI_APPADDR128K 0x08010000
#define  LO_APPADDR256K 0x08010000
#define  HI_APPADDR256K 0x08020000
#define  LO_APPADDR512K 0x08020000
#define  HI_APPADDR512K 0x08040000
#define  SHI_APPADDR     0x08020000
//#define  STDDATAADDR 0x0800C000
#define  LO_CODEDATASIZE  0x0000C000
#define  HI_CODEDATASIZE  0x00010000
#define  SHI_CODEDATASIZE 0x00020000

#define  OB_RDP_Level_2  0xCC

/* Exported types ------------------------------------------------------------*/
#ifdef __STM32F4xx_GPIO_H
typedef struct {
  GPIO_TypeDef *sPIN_PORT;
  uint32_t sPIN_CLK;
  GPIO_InitTypeDef sInitStruc;
/*
  uint16_t sPIN;
  GPIOMode_TypeDef sPIN_MODE;
  GPIOOType_TypeDef sPIN_OTYPE;
  GPIOPuPd_TypeDef sPIN_PUPD;
*/
  uint8_t sPIN_SOURCE, sPIN_AF;
} PIN_HWCFG_TypeDef;
#endif /* __STM32F4xx_GPIO_H */

#ifdef __STM32F4xx_USART_H
typedef struct {
  USART_TypeDef *sUSART;
  uint32_t sUSART_CLK;
  uint16_t sTXBufSize, sRXBufSize;
  USART_InitTypeDef sInitStruc;
  PIN_HWCFG_TypeDef sTX, sRX, sDIR;
  DMA_Stream_TypeDef *sDMA_STREAM_TX, *sDMA_STREAM_RX;
  uint32_t sDMA_CHAN_TX, sDMA_CHAN_RX;
  uint8_t sUSART_IRQn;
  uint8_t sDMA_TX_IRQn;
  uint32_t sDMA_IT_TC;
} USART_HWCFG_TypeDef, *USART_HWCFG_PtrDef;
#endif /* __STM32F4xx_USART_H */

#ifdef __STM32F4xx_SPI_H
typedef struct {
  SPI_TypeDef  *sSPI;
  uint32_t sSPI_CLK;
  uint16_t sBufSize;
  SPI_InitTypeDef sInitStruc;
  //uint16_t sSPI_Mode, sSPI_Dir;
  PIN_HWCFG_TypeDef sSCK, sMISO, sMOSI, sCHA, sCHB;
  DMA_Stream_TypeDef *sDMA_STREAM_TX, *sDMA_STREAM_RX;
  uint32_t sDMA_CHAN_TX, sDMA_CHAN_RX;
  uint8_t sSPI_IRQn;
  uint8_t sDMA_TX_IRQn;
  uint32_t sDMA_TX_IT_TC;
  uint8_t sDMA_RX_IRQn;
  uint32_t sDMA_RX_IT_TC;
  uint32_t sDMA_RX_IT_HT;
} SPI_HWCFG_TypeDef;
#endif /* __STM32F4xx_SPI_H */

#ifdef __STM32F4xx_TIM_H
typedef struct {
  TIM_TypeDef *sTIM;
  uint32_t sCLK;
  TIM_TimeBaseInitTypeDef sInitStruc;
} TIM_HWCFG_TypeDef;
#endif /* __STM32F2xx_TIM_H */

#ifdef __STM32F4xx_DAC_H
typedef struct {
  uint32_t sDACChan;
  uint32_t sCLK;
  uint32_t sDACAddr;
  DAC_InitTypeDef sInitStruc;
  PIN_HWCFG_TypeDef sPIN;
  TIM_HWCFG_TypeDef sTIM;
  DMA_Stream_TypeDef *sDMA_STREAM;
  uint32_t sDMA_CHAN;
} DAC_HWCFG_TypeDef;
#endif /* __STM32F4xx_DAC_H */

/* Exported functions ------------------------------------------------------- */

bool WasInitBackup(void);
uint32_t ReadBackupParam(int aIndex);
void WriteBackupParam(int aIndex, uint32_t aValue);

void* BkpMemAlloc(uint32_t aAllocSize);
void ADC_Config(void);
void ADC_GetBuffer(uint16_t **aBuffPtr);
void ADC_ShowLevels(void);
void ADC_ShowMeasures(void);

/*
void MySPI_Init(MY_SPI_TypeDef *aMySPI,const SPI_HWCFG_TypeDef *aConfig,
  void *aInBuffer,int32_t aInBufSize, void *aOutBuffer,int32_t aOutBufSize);
*/
/*
void MyFLASH_ProgInit(void);
void MyFLASH_EraseCodePlace(bool IsHighPlace);
void MyFLASH_ProgWord(uint32_t aAddress,uint32_t aData);
void MyFLASH_ProgDone(void);
*/


#endif /* __MY_STM32F4_H */

