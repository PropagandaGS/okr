// ����������� � �������� ���������
// gps/glonass ������
// � Olgvel, 2018
//
#include "my_stm32.h"
/*
#define MCR_CFGSENSOR_MAX  96
#define MCR_INFO_LEN       8
#define MCR_PING_LEN       3
#define MCR_MSG_LEN        (MCR_INFO_LEN + 2)
#define MCR_MSGPREF        0x3E
*/
/* Exported types ------------------------------------------------------------*/

typedef struct {
  MY_UART_TypeDef* sGNUART;
  //
  uint16_t sGNFlags;

  /*
  uint32_t sMDCfgSensors[MCR_CFGSENSOR_MAX];
  uint16_t sMDSenCodes[MCR_CFGSENSOR_MAX];
  uint32_t sMDLastPingTicks;
  char sMDLastInfo[MCR_INFO_LEN];
  uint32_t sMDLastInfoTicks;
  int8_t   sMDCfgSensorCount;
  int8_t   sMDSensorIndex;
  uint16_t sMDReserv;
  //
  MHANDLER_TypeDef sMDOnSensor;
//  MESSAGE_CALLBACK sMDOnMessage;*/
  //
} GNSS_DESCR_TypeDef;

#define TCN_GNCFG       "\x06+GNCFG\0"
#define TCH_GNCFG        {(TCHANDLER)_TCH_GNConfig,TCFINT,ACX_GNSS}
void _TCH_GNConfig(GNSS_DESCR_TypeDef *aDescr,TXTCMD_PAR_TypeDef *aPar);

/* Exported functions ------------------------------------------------------- */

GNSS_DESCR_TypeDef* GNSS_Init(GNSS_DESCR_TypeDef *aDescr,MY_UART_TypeDef *aMyUART,
  const PIN_HWCFG_TypeDef *aGNRST);
void GNSS_Process(GNSS_DESCR_TypeDef *aDescr);
//void GNSS_UpdateSensors(GNSS_DESCR_TypeDef *aDescr,uint32_t aIsBegin);
//void GNSS_NewSensor(GNSS_DESCR_TypeDef *aDescr,uint32_t aSensorID);

