// ���������� ����������� ������������ ������
// � Olgvel, 2015-16
//
#include "my_stdcmdprocs.h"

void TCHMCUID(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  char lBuf[28];
  TCOutText(aPar, Bin2HexStrZ((void*)UNIID, lBuf, 12, FALSE));
  aPar->sTCPResult = TRUE;
}

void BCHMCUID(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  //PostLogText("Enter MCUID"S_CRLF);
  aBCP->sBCPResult = assigned(BCPutStrPar(aBCP, (void*)UNIID, 12)) &&
    BCPutUIntPar(aBCP, VTORLength()) && BCPutUIntPar(aBCP, AppAddress()) && 
    BCPutUIntPar(aBCP, FLASHSIZE);
  //PostLogText("Leave MCUID"S_CRLF);
}

void TCHDeviceID(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  char lBuf[12];
  TCOutText(aPar, Int2HexZ(DeviceID(), lBuf, 8));
  aPar->sTCPResult = TRUE;
}

void BCHDeviceID(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  aBCP->sBCPResult = BCPutUIntPar(aBCP, DeviceID());
}

void TCHMCULock(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  while (TRUE);
}

void BCHMCULock(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  while (TRUE);
}

void TCHMCUReset(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  PostLogText(S_CRLF""S_CRLF);
  Delay(500);
  NVIC_SystemReset();
}

void BCHMCUReset(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  NVIC_SystemReset();
}

void TCHFlashClear(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  uint32_t lCount;
  if (aPar->sTCPCount > 0) {
    if (aPar->sTCPCount == 2) lCount = aPar->sTCPVals[1]; else lCount = 1;
    aPar->sTCPResult = FLASH_Clear(aPar->sTCPVals[0], lCount);
  }
}

void BCHFlashClear(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  uint32_t lPage, lCount;
  aBCP->sBCPResult = BCGetUIntPar(aBCP, &lPage) && BCGetUIntPar(aBCP, &lCount) &&
    FLASH_Clear(lPage, lCount);
}

void TCHFlashWrite(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  char lBuf[324];
  int  lCount;
  if ((aPar->sTCPCount == 2) && (aPar->sTCPVals[0] > 0) &&
      HexStr2Bin(aPar->sTCPPars[1], aPar->sTCPVals[1], lBuf, &lCount)) {
    aPar->sTCPResult = FLASH_Write(aPar->sTCPVals[0], lBuf, lCount);
    if (aPar->sTCPResult)
      aPar->sTCPResult = FLASH_Test(aPar->sTCPVals[0], lBuf, lCount);
  }
}

void BCHFlashWrite(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  uint32_t lAddr;
  int lCount;
  char *lBuf;
  aBCP->sBCPResult = BCGetUIntPar(aBCP, &lAddr) && BCGetParPtr(aBCP, &lBuf, &lCount) &&
    FLASH_Write(lAddr, lBuf, lCount) && FLASH_Test(lAddr, lBuf, lCount);
  if (aBCP->sBCPResult && BCGetUIntPar(aBCP, &lAddr)) BCPutParPtr(aBCP, &lBuf, lAddr);
}

void TCHFlashRead(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  char lBuf[260];
  if ((aPar->sTCPCount == 2) && (aPar->sTCPVals[0] > 0) && (aPar->sTCPVals[1] > 0)) {
    TCOutText(aPar, Bin2HexStrZ((void*)aPar->sTCPVals[0], lBuf, MaxInt(aPar->sTCPVals[1], sizeof(lBuf)/2), FALSE) );
    aPar->sTCPResult = TRUE;
  }
}

void BCHFlashRead(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  uint32_t lAddr, lCount;
 /* PostLogHexStr("FLRD: PARAM ", aBCP->sBCPParam, 20, S_CRLF);
  PostLogInt("FLRD: ParPos ", aBCP->sBCPParPos, ", ");
  PostLogInt("ParEnd ", aBCP->sBCPParEnd, S_CRLF);
  BCGetUIntPar(aBCP, &lAddr);
  PostLogInt("FLRD: lAddr ", lAddr, ", ");
  PostLogInt("ParPos ", aBCP->sBCPParPos, ", ");
  PostLogInt("ParEnd ", aBCP->sBCPParEnd, S_CRLF);
  BCGetUIntPar(aBCP, &lCount);
  PostLogInt("FLRD: lCount ", lCount, ", ");
  PostLogInt("ParPos ", aBCP->sBCPParPos, ", ");
  PostLogInt("ParEnd ", aBCP->sBCPParEnd, S_CRLF);*/
  //
  aBCP->sBCPResult = BCGetUIntPar(aBCP, &lAddr) && BCGetUIntPar(aBCP, &lCount) &&
    assigned(BCPutStrPar(aBCP, (char*)lAddr, lCount));
/*  aBCP->sBCPResult = BCGetUIntPar(aBCP, &lAddr) && BCGetUIntPar(aBCP, &lCount);
  if (aBCP->sBCPResult) {
    PostLogHex("BCHFlashRead Addr=", lAddr, 4, ", ");
    PostLogInt("Count=", lCount, S_CRLF);
    aBCP->sBCPResult = assigned(BCPutStrPar(aBCP, (char*)lAddr, lCount));
  }*/
  //
  //PostLogInt("lCount ", lCount, ", ");
  //PostLogInt("Result ", aBCP->sBCPResult, S_CRLF);
}

void TCHFlashCopy(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  if ((aPar->sTCPCount == 3) && (aPar->sTCPVals[0] > 0) &&
      (aPar->sTCPVals[1] > 0) && (aPar->sTCPVals[2] > 0)) {
    aPar->sTCPResult = FLASH_Write(aPar->sTCPVals[0], (void*)aPar->sTCPVals[1], aPar->sTCPVals[2]);
    if (aPar->sTCPResult)
      aPar->sTCPResult = FLASH_Test(aPar->sTCPVals[0], (char*)aPar->sTCPVals[1], aPar->sTCPVals[2]);
  }
}

void BCHFlashCopy(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  uint32_t lSAddr, lDAddr, lCount;
  aBCP->sBCPResult = BCGetUIntPar(aBCP, &lSAddr) && BCGetUIntPar(aBCP, &lDAddr) && BCGetUIntPar(aBCP, &lCount) &&
    FLASH_Write(lDAddr, (char*)lSAddr, lCount) && FLASH_Test(lDAddr, (char*)lSAddr, lCount);
}

void TCHWriteNamedData(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  char lBuf[324];
  int  lCount;
  if ((aPar->sTCPCount >= 2) && (aPar->sTCPVals[0] > 0) && (aPar->sTCPVals[1] > 0) &&
      HexStr2Bin(aPar->sTCPPars[1], aPar->sTCPVals[1], lBuf, &lCount)) {
    aPar->sTCPResult = WriteNamedIdxData(aPar->sTCPPars[0], aPar->sTCPVals[0],
                         (aPar->sTCPCount>2)?aPar->sTCPVals[2]:NOIDX, lBuf, lCount);
  }
}

void BCHWriteNamedData(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  char *lName, *lData;
  int lNameLen, lDataLen, lIdx;
  aBCP->sBCPResult = BCGetParPtr(aBCP, &lName, &lNameLen) && BCGetParPtr(aBCP, &lData, &lDataLen) &&
    WriteNamedIdxData(lName, lNameLen, BCGetIntPar(aBCP, &lIdx)?lIdx:NOIDX, lData, lDataLen);
}

void TCHReadNamedData(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  char lBuf[324], *lData;
  int  lCount;
  if ((aPar->sTCPCount >= 1) && (aPar->sTCPVals[0] > 0) && 
       FindNamedIdxData(aPar->sTCPPars[0], aPar->sTCPVals[0], 
         (aPar->sTCPCount > 1)?aPar->sTCPVals[1]:NOIDX, &lData, &lCount)) {
    if (lCount >= sizeof(lBuf)/2) lCount = sizeof(lBuf)/2 - 1;
    TCOutText(aPar, Bin2HexStrZ(lData, lBuf, lCount, FALSE) ); //PostLogText(S_CRLF);
    aPar->sTCPResult = TRUE;
  }
}

void BCHReadNamedData(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  char *lName, *lData;
  int lNameLen, lDataLen, lIdx;
  aBCP->sBCPResult = BCGetParPtr(aBCP, &lName, &lNameLen) &&
    FindNamedIdxData(lName, lNameLen, BCGetIntPar(aBCP, &lIdx)?lIdx:NOIDX, &lData, &lDataLen) &&
    assigned(BCPutStrPar(aBCP, lData, lDataLen));
}

void TCHDeleteNamedData(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  if ((aPar->sTCPCount >= 1) && (aPar->sTCPVals[0] > 0))
    aPar->sTCPResult = DeleteNamedIdxData(aPar->sTCPPars[0], aPar->sTCPVals[0],
     (aPar->sTCPCount > 1)?aPar->sTCPVals[1]:NOIDX);
}

void BCHDeleteNamedData(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  char *lName;
  int lNameLen, lIdx;
  aBCP->sBCPResult = BCGetParPtr(aBCP, &lName, &lNameLen) &&
    DeleteNamedIdxData(lName, lNameLen, BCGetIntPar(aBCP, &lIdx)?lIdx:NOIDX);
}

void BCHInitNamedData(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  uint32_t lStartAddr, lMaxSize;
  aBCP->sBCPResult = BCGetUIntPar(aBCP, &lStartAddr) && BCGetUIntPar(aBCP, &lMaxSize) &&
    InitNamedData(lStartAddr, lMaxSize);
}

void BCHCRC(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  uint32_t lAddr, lCount;
  aBCP->sBCPResult = BCGetUIntPar(aBCP, &lAddr) && BCGetUIntPar(aBCP, &lCount) &&
    BCPutUIntPar(aBCP, crc32eth((void*)lAddr, lCount, 0));
}

void TCHAppChange(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  uint32_t lAddr;
  if (aPar->sTCPCount > 0) lAddr = aPar->sTCPVals[0];else lAddr = 0;
  aPar->sTCPResult = App_Change(lAddr);
  //PostLogInt("AppChange: ", lAddr, S_CRLF);
}

void BCHAppChange(void *aContext,BINCMD_PAR_TypeDef *aBCP)
{
  uint32_t lAddr=0;
  BCGetUIntPar(aBCP, &lAddr);
  PostLogText(S_CRLF""S_CRLF);
  Delay(500);
  aBCP->sBCPResult = App_Change(lAddr);
}

void TCHJampTo(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
//  uint32_t lValue;
  //if ((aPar->sTCPCount > 0) && (aPar->sTCPVals[0] >= STARTPRGADDR))
  //  JampToApp(aPar->sTCPVals[0]);
  //lValue = VTORLength();
/*  lValue = GetStartAddr();
  PostLogHexStr("StartAddr: ", &lValue, 4, S_CRLF);
  lValue = AppAddress();
  PostLogHexStr("  AppAddr: ", &lValue, 4, S_CRLF);*/

  if (FLASH_SetReadProtection())
    PostLogText("Read Protection: OFF"S_CRLF);
  else PostLogText("Read Protection: ON"S_CRLF);
}
// 5202 ��  //

