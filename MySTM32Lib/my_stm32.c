// ���������� ���������� ������� ��� STM32
// � Olgvel, 2012-2018
//
#include "my_stm32.h"

uint32_t gAppAddress;
uint32_t gVTORLength;
//uint32_t gMaxCodeDataSize;
uint32_t gCfgAddress;
uint32_t gCfgLength;

void GetCfgParams(uint32_t *aAddress, uint32_t *aLength)
{
  *aAddress = gCfgAddress; *aLength = gCfgLength;
}

bool PIN_Get(const PIN_HWCFG_TypeDef *aConfig)
{
  if (aConfig == NULL) return FALSE; else
  return GPIO_ReadInputDataBit(aConfig->sPIN_PORT, aConfig->sInitStruc.GPIO_Pin);
}

void PIN_Set(const PIN_HWCFG_TypeDef *aConfig, unsigned int aState)
{
  if (aConfig != NULL)
    GPIO_WriteBit(aConfig->sPIN_PORT, aConfig->sInitStruc.GPIO_Pin, aState?Bit_SET:Bit_RESET);
}

void PIN_CondSet(const PIN_HWCFG_TypeDef *aConfig,bool aNewState,bool *aLastState)
{
  if (aNewState != *aLastState) {
    PIN_Set(aConfig, aNewState);
    *aLastState = aNewState;
  }
}

void OUTSet(MY_OUT_TypeDef *aMyOUT,int aState)
{
  PIN_Set(aMyOUT->sHWCFG, aState);
}

MY_OUT_TypeDef* MyOUT_Init(MY_OUT_TypeDef *aMyOUT,const PIN_HWCFG_TypeDef *aConfig)
{
  if (aMyOUT == NULL)
    aMyOUT = FixMemAlloc( sizeof(MY_OUT_TypeDef) );
  //
  MyPS_Init((MY_PESTATE_TypeDef*)aMyOUT, (STATEHANDLER_CALLBACK)OUTSet, NULL, NULL);
  PIN_Config(aConfig);
  aMyOUT->sHWCFG = aConfig;
  return aMyOUT;
}

//=============================================================================
/*
bool (uint32_t aAddr)
{
  if (aAddr >= BASE_APPADDR) {
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, aAddr - BASE_APPADDR);
    return TRUE;
  }
  else return FALSE;
  //__set_MSP(*(uint32_t *)0x08004000);
  //ISRPtr application_reset_handler=(ISRPtr)(*(uint32_t *)0x08002004);
  //application_reset_handler();
}
*/
void JampToApp(uint32_t aAddr)
{
  MYFUNC   lJumpFunc; //MYFUNC
  uint32_t lJumpAddr;
  //
  lJumpAddr = *(__IO uint32_t*) (aAddr + 4);
  lJumpFunc = (MYFUNC) lJumpAddr;
  //
  //__disable_interrupt();
  //VTOR_Setup(aAddr);
  //
  __set_MSP( *(__IO uint32_t*)aAddr );  // Initialise app's Stack Pointer
  lJumpFunc();
}
/*
void IRQ_Init(uint8_t aIRQChan)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  if (aIRQChan != 0) {
    NVIC_InitStructure.NVIC_IRQChannel = aIRQChan;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
  }
}
*/

/*------------------------------------------------------------------------------------------
MY_USART_TypeDef* MyUSART_Init(MY_USART_TypeDef *aMyUSART,const USART_HWCFG_TypeDef *aConfig)
{
  return MyUSART_InitEx(aMyUSART, aConfig, 0);
}

int MyUSART_WriteEx(MY_USART_TypeDef *aMyUSART,void *aBuffer,int aCount,bool aDataSend)
{
  int lCount, lMaxCount, lBufSize;
  uint16_t lSendingPos, lWritePos;
  char *lBuffer;
  //
  if (aBuffer != NULL) {
    lBuffer = aBuffer;
    if (aCount <= TOEND) aCount = StrLength(lBuffer);
    lBufSize = aMyUSART->sOutBufSize;
    lWritePos = aMyUSART->sWritePos;
    lSendingPos = aMyUSART->sSendingPos;
    if ((lSendingPos == lWritePos) && ((aMyUSART->sFlags & IN_ANY_TX) == 0))
      lWritePos = lSendingPos = aMyUSART->sSendingPos = aMyUSART->sSendPos = 0;
    //
    if (lSendingPos > lWritePos) lMaxCount = lSendingPos - lWritePos;
    else lMaxCount = lBufSize + lSendingPos - lWritePos;
    //
    if (aCount > lMaxCount) aCount = lMaxCount;
    if (&aMyUSART->sOutBuffer[lWritePos] != lBuffer) {
      lCount = aCount;
      while (lCount-- > 0) {
        aMyUSART->sOutBuffer[lWritePos++] = *lBuffer++;
        if (lWritePos == lBufSize) lWritePos = 0;
      }
    }
    else {
      lWritePos += aCount;
      if (lWritePos >= lBufSize) lWritePos -= lBufSize;
    }
    aMyUSART->sWritePos = lWritePos;
  }
  else aCount = 0;
  if (aDataSend)
    MyUSART_StartSend(aMyUSART);
  //
  return aCount;
}

int MyUSART_Write(MY_USART_TypeDef *aMyUSART,void *aBuffer,int aCount)
{
  return MyUSART_WriteEx(aMyUSART, (char*)aBuffer, aCount, TRUE);
}

int MyUSART_WriteW(MY_USART_TypeDef *aMyUSART,void *aBuffer,int aCount)
{
  return MyUSART_WriteEx(aMyUSART, (char*)aBuffer, aCount, FALSE);
}

int MyUSART_WriteText(MY_USART_TypeDef *aMyUSART,void *aBuffer)
{
  return MyUSART_WriteEx(aMyUSART, aBuffer, TOEND, TRUE);
}

int MyUSART_WriteTextW(MY_USART_TypeDef *aMyUSART,void *aBuffer)
{
  return MyUSART_WriteEx(aMyUSART, aBuffer, TOEND, FALSE);
}

bool MyUSART_Read(MY_USART_TypeDef *aMyUSART,void *aBuffer,int aBufSize,
  unsigned int aWaitTime, int *aCount)
{
  uint16_t lInBufSize, lReceivePos, lReadPos, lRcvCount;
  char *lBuffer;
  //
  MyUSART_UpdateReceivePos(aMyUSART);
  lReceivePos = aMyUSART->sReceivePos;
  lReadPos = aMyUSART->sReadPos;
  if (lReceivePos != lReadPos) {
    lInBufSize = aMyUSART->sInBufSize;
    if (lReceivePos > lReadPos) lRcvCount = lReceivePos - lReadPos;
    else lRcvCount = lInBufSize - lReadPos + lReceivePos;
    if (lRcvCount > aBufSize) lRcvCount = aBufSize;
    if ((lRcvCount > 0) && ((lRcvCount == aBufSize) || (aWaitTime == 0) ||
       IsTimeOut(TICKS, aMyUSART->sReceiveTicks, aWaitTime))) {
      lBuffer = aBuffer;
      *aCount = lRcvCount;
      while (lRcvCount-- > 0) {
        *lBuffer++ = aMyUSART->sInBuffer[lReadPos++];
        if (lReadPos == lInBufSize) lReadPos = 0;
      }
      aMyUSART->sReadPos = lReadPos;
      aMyUSART->sCheckPos = lReadPos;
      return TRUE;
    }
  }
  return FALSE;
}

READ_RF_T MyUSART_ReadFmt(MY_USART_TypeDef *aMyUSART,char *aBuffer,int aBufSize,uint8_t aFlags,
  unsigned int aWaitTime, int *aCount)
{
  uint16_t lInBufSize, lCount, lReceivePos,
           lReadPos, lCheckPos, lCheckLen;
  char lSCh = CR;
  READ_RF_T lRes = RRF_CANCEL; // RRF_DETECT, RRF_INFULL, RRF_TIMER,
  //
  MyUSART_UpdateReceivePos(aMyUSART);
  lReceivePos = aMyUSART->sReceivePos;
  lCheckPos = aMyUSART->sCheckPos;
  lReadPos = aMyUSART->sReadPos;
  lInBufSize = aMyUSART->sInBufSize;
  if (lCheckPos != lReceivePos) {
    if (lReceivePos > lCheckPos) lCount = lReceivePos - lCheckPos;
    else lCount = lInBufSize - lCheckPos + lReceivePos;
    if (lCheckPos > lReadPos) lCheckLen = lCheckPos - lReadPos;
    else lCheckLen = lInBufSize - lReadPos + lCheckPos;
    //
    //PostLogInt("Rv=", lReceivePos, ", ");
    //PostLogInt("Ch=", lCheckPos, ", ");
    //PostLogInt("CC=", lCount, S_CRLF);
    //
    while (lCount-- > 0) {
      if (aMyUSART->sInBuffer[lCheckPos++] == lSCh) lRes = RRF_DETECT;
      if (lCheckPos == lInBufSize) lCheckPos = 0;
      if (lRes != RRF_CANCEL) break;
      if (++lCheckLen == aBufSize) { lRes = RRF_INFULL; break;}
    }
    aMyUSART->sCheckPos = lCheckPos;
  }
  if (lReadPos != lCheckPos) {
    if (aMyUSART->sInBuffer[lReadPos] == LF) {
      if (++lReadPos == lInBufSize) lReadPos = 0;
      aMyUSART->sReadPos = lReadPos;
    }
    if ((lRes == RRF_CANCEL) &&
        IsTimeOut(TICKS, aMyUSART->sReceiveTicks, aWaitTime)) lRes = RRF_TIMER;
    if (lRes != RRF_CANCEL) {
      //PostLogInt("Rd=", lReadPos, S_CRLF);
      *aCount = 0;
      while (lReadPos != lCheckPos) {
        *aBuffer++ = aMyUSART->sInBuffer[lReadPos++];
        if (lReadPos == lInBufSize) lReadPos = 0;
        if (!((lReadPos == lCheckPos)&&(lRes == RRF_DETECT))) (*aCount)++;
      }
      aMyUSART->sReadPos = lReadPos;
    }
  }
  return lRes;
}

bool MyUSART_ReadText(MY_USART_TypeDef *aMyUSART,void *aBuffer,int aBufSize,
  unsigned int aWaitTime, int *aCount)
{
  if (MyUSART_Read(aMyUSART,
     aBuffer, (aBufSize > 0)?(aBufSize-1):0, aWaitTime, aCount)) {
    (*(char*)aBuffer)[aCount] = 0;
    return TRUE;
  }
  return FALSE;
}
*/
//----------------------------- MyUART -----------------------------------------

void MyUARTBeforeRead(MY_UART_TypeDef *aDescr, MY_CBUF_TypeDef *aRxBuf)
{
  MyUART_UpdateReceivePos(aDescr);
}

void MyUARTAfterWrite(MY_UART_TypeDef *aDescr, MY_CBUF_TypeDef *aTxBuf)
{
  MyUART_StartSend(aDescr);
}

MY_UART_TypeDef* MyUART_InitEx(MY_UART_TypeDef *aDescr,const USART_HWCFG_TypeDef *aConfig,uint32_t aBaudRate)
{
  if (aDescr == NULL)
    aDescr = FixMemAlloc( sizeof(MY_UART_TypeDef) );
  MemClear(aDescr, sizeof(MY_UART_TypeDef));
  aDescr->sHWCFG = aConfig;
  aDescr->sBaudRate = aBaudRate;
  aDescr->sLRx = MyCBuf_Init(aDescr, aConfig->sRXBufSize );
  MHandler_Setup(&aDescr->sLRx->sCOnBeforeRead, (PHANDLER)MyUARTBeforeRead, aDescr);
  aDescr->sLTx = MyCBuf_Init(aDescr, aConfig->sTXBufSize );
  MHandler_Setup(&aDescr->sLTx->sCOnAfterWrite, (PHANDLER)MyUARTAfterWrite, aDescr);
  MyUART_Initialize(aDescr);
  INCLUDE(aDescr->sLFlags, LF_ACTIVE);
  return aDescr;
}

MY_UART_TypeDef* MyUART_Init(MY_UART_TypeDef *aDescr,const USART_HWCFG_TypeDef *aConfig)
{
  return MyUART_InitEx(aDescr, aConfig, 0);
}

//==============================================================================

void Delay(uint32_t aDelay)
{
  uint32_t lFrom = TICKS;
  while (!IsTimeOut(TICKS, lFrom, aDelay));
}

void PostLogTextDly(void *aText,uint32_t aDelay)
{
  PostLogText(aText);
  Delay(aDelay);
}

//==============================================================================
/*
bool GetFieldLength(uint32_t aAddr,uint16_t *aLength,uint8_t *aOffs,bool *aError)
{
  uint16_t lLength;
  uint8_t lPreOffs=0;
  //
  while ((*(uint8_t*)(aAddr+lPreOffs) == 0xFD) && (lPreOffs < 3)) lPreOffs++;
  lLength = *(uint8_t*)(aAddr+lPreOffs);
  if (lLength == 0) {
    *aError = TRUE; return FALSE;
  }
  else
    if (lLength == 0xFF) {
      *aError = FALSE; return FALSE;
    }
    else
      if (lLength <= 0x7F) {
        *aLength = lLength + lPreOffs; *aOffs = lPreOffs + 1; *aError = FALSE; return TRUE;
      }
      else {
        lLength = (lLength << 8) | *(uint8_t*)(++aAddr);
        if (lLength <= 0xFCFF) {
          *aLength = (lLength & 0x7FFF) + lPreOffs; *aOffs = lPreOffs + 2; *aError = FALSE; return TRUE;
        }
      }
  *aError = TRUE;
  return FALSE;
}

uint32_t SetFieldLengthEx(uint32_t aLength,uint32_t *aLenCode,uint8_t *aCount,bool aFixLen)
{
  if (aLength > 0)
    if ((aLength < 0x7F) && (!aFixLen)) {
      aLength++; *aLenCode = aLength; *aCount = 1; return aLength;
    }
    else
      if (aLength < 0xFCFD) {
        aLength += 2; *aLenCode = (aLength << 8) & 0xFF00 | (aLength >> 8) | 0x0080;
        *aCount = 2; return aLength;
      }
  return FALSE;
}

uint32_t SetFieldLength(uint32_t aLength,uint32_t *aLenCode,uint8_t *aCount)
{
  return SetFieldLengthEx(aLength, aLenCode, aCount, FALSE);
}
*/
uint8_t AlignLenCode(uint8_t aCount,uint32_t aAddr,uint8_t aAlign)
{
  return (aAlign-(aAddr+aCount)&(aAlign-1))&(aAlign-1);
}

bool CheckFieldStatus(uint32_t aAddr,uint8_t *aOffs)
{
  uint8_t lFlags = *(uint8_t*)(aAddr + *aOffs);
  //PostLogText("---- CheckFieldStatus --- "); PostLogHex("Flags=", lFlags, 1, S_CRLF);
  if (lFlags == 0x00) return FALSE;
  else { if (lFlags == 0xFE) (*aOffs)++; return TRUE;}
}

bool PakNameIdx(char *aName,int aNameLen,int aNameIdx,char *aOutName,int *aOutLen)
{
  if (aNameIdx >= 0) {
    MemCopy(aName, aOutName, aNameLen);
    if (aNameIdx < 0x20) {
      aOutName[aNameLen] = aNameIdx;
      *aOutLen = aNameLen + 1;
    } else
    if (aNameIdx < 0x100) {
      aOutName[aNameLen] = aNameIdx;
      aOutName[aNameLen+1] = 0x20;
      *aOutLen = aNameLen + 2;
    } else
    if (aNameIdx < 0x10000) {
      MemCopy(&aNameIdx, &aOutName[aNameLen], 2);
      aOutName[aNameLen+2] = 0x60;
      *aOutLen = aNameLen + 3;
    } else {
      MemCopy(&aNameIdx, &aOutName[aNameLen], 4);
      aOutName[aNameLen+4] = 0x7F;
      *aOutLen = aNameLen + 5;
    }
    return TRUE;
  }
  return FALSE;
}

bool UnpakNameIdx(char *aName,int aNameLen,int *aNewLen,int *aNameIdx)
{
  int lIdx;
  char lCode = aName[aNameLen-1];
  if (lCode < 0x20) {
    *aNameIdx= lCode;
    *aNewLen = aNameLen - 1;
    return TRUE;
  }
  switch (lCode) {
    case 0x20:
      *aNameIdx= aName[aNameLen-2];
      *aNewLen = aNameLen - 2;
      break;
    case 0x60:
      lIdx=0;
      MemCopy(&aName[aNameLen-3], &lIdx, 2);
      *aNameIdx = lIdx;
      *aNewLen = aNameLen - 3;
      break;
    case 0x7F:
      MemCopy(&aName[aNameLen-5], aNameIdx, 4);
      *aNewLen = aNameLen - 5;
      break;
    default:
      *aNewLen = aNameLen;
      *aNameIdx = NOIDX;
      return FALSE;
  }
  return TRUE;
}

void SetNameData(uint32_t aAddr,void *aBuffer,int aCount)
{
  FLASH_Write2(aAddr, aBuffer, aCount);
}

void ClearFieldStatus(uint32_t aAddr)
{
  uint16_t lStatus=0;
  SetNameData(aAddr & ~1, &lStatus, sizeof(lStatus));
}

bool GetNamedField(uint32_t *aAddr,char *aName,int aNameLen,bool aStrictName,bool aSearch,
  uint32_t *aField,int *aLength,uint8_t *aDataOffs)
{
  uint32_t lNameAddr;
  uint16_t lLength; // ,lNameLen;
  uint8_t  lOffs;   // ,lNameOffs;
  bool     lError, lCoincided;
  //
  while (GetFieldLength((void*)*aAddr, &lLength, &lOffs, &lError)) {
    lNameAddr = *aAddr + lOffs;
    *aAddr += lLength;
    //PostLogText("---- level 1: "S_CRLF);
    //
    if (GetFieldLength((void*)lNameAddr, &lLength, &lOffs, &lError) && CheckFieldStatus(lNameAddr, &lOffs) &&
        SubstrCompare(aName, aNameLen, (char*)(lNameAddr+lOffs), lLength-lOffs, &lCoincided ) &&
        (!aStrictName || lCoincided)) {
      //PostLogText("---- level 2: "S_CRLF);
      *aField = lNameAddr;
      *aLength = lLength;
      *aDataOffs = lOffs;
      return TRUE;
    }
    else if (!aSearch) return FALSE;
  }
  return FALSE;
}

bool _DeleteNamedData(uint32_t aNDBase,char *aName,int aNameLen,int aNameIdx)
{
  char     lName[32];
  uint32_t lFieldAddr;
  int      lNameLen;
  uint8_t  lNOffs;
  //
  if (aNameLen == TOEND) aNameLen = StrLength(aName);
  if (PakNameIdx(aName, aNameLen, aNameIdx, lName, &aNameLen)) aName = lName;
  if ( GetNamedField(&aNDBase, aName, aNameLen, TRUE, TRUE, &lFieldAddr, &lNameLen, &lNOffs) ) {
    PostLogText("---- delete last value !!! "S_CRLF);
    ClearFieldStatus(lFieldAddr + lNOffs);
    return TRUE;
  }
  return FALSE;
}

bool DeleteNamedIdxData(char *aName,int aNameLen,int aNameIdx)
{
  if assigned(gCfgAddress)
    return _DeleteNamedData(gCfgAddress, //SYSBLOCK->hsbBaseAddr + SYSBLOCK->hsbFullSize,
      aName, aNameLen, aNameIdx);
  else return FALSE;
}

bool DeleteNamedData(char *aName,int aNameLen)
{
  return DeleteNamedIdxData(aName, aNameLen, NOIDX);
}

bool _WriteNamedData(uint32_t aNDBase,uint32_t aNDSize,
       char *aName,int aNameLen,int aNameIdx,char *aData,int aDataLen)
{
  char *lName, lPreamb[40];
  int  lFieldLen;
  uint32_t lAddr, lSAddr, lACode, lNCode, lDCode, lFiller, lFieldAddr=0;
  uint16_t lLength;
  uint8_t  lAOffs, lNOffs, lDOffs, lROffs, lAFill, lNFill, lDFill, lPrePos, lAlign, lDPos;
  bool     lError, lStrict;
  //
  PostLogText("---- WriteNamedData ----"S_CRLF);
  if (aNameLen == TOEND) aNameLen = StrLength(aName);
  PostLogMessage(aName, aNameLen);
  if (aNameIdx >= 0) PostLogInt(": ", aNameIdx, ""); PostLogText(S_CRLF);
  //
  lAddr = aNDBase;  lName = &lPreamb[sizeof(lPreamb)-5+1];
  if (PakNameIdx(aName, aNameLen, aNameIdx, lName, &aNameLen)) aName = lName;
  if ( GetNamedField(&lAddr, aName, aNameLen, TRUE, TRUE, &lFieldAddr, &lFieldLen, &lNOffs) ) {
    lFieldAddr += lFieldLen;
    if (GetFieldLength((void*)lFieldAddr, &lLength, &lDOffs, &lError) && //(lLength == aCount) &&
        SubstrCompare(aData, aDataLen, (char*)(lFieldAddr+lDOffs), lLength-lDOffs, &lStrict) && lStrict) {
      PostLogText("---- same value !!! "S_CRLF);
      return FALSE;
    }
    lFieldAddr -= lFieldLen; lFieldAddr += lNOffs;
  }
  //
  while (GetFieldLength((void*)lAddr, &lLength, &lAOffs, &lError)) lAddr += lLength;
  if (!lError) {
    lLength = SetFieldLength(aNameLen, &lNCode, &lNOffs);
    lLength += SetFieldLength(aDataLen, &lDCode, &lDOffs)+6;
    lAFill = ((lAddr & 1) != 0);
    SetFieldLength(lLength, &lACode, &lAOffs); lROffs = lAOffs; lSAddr = lAddr + lAFill + lAOffs;
    lNFill = AlignLenCode(lNOffs, lSAddr, 2); lSAddr += lNFill + lNOffs + aNameLen;
    if (aDataLen > 1) {
      if (aDataLen > 3) lAlign = 4; else lAlign = 2;
      lDFill = AlignLenCode(lDOffs, lSAddr, lAlign);
    }
    else lDFill = 0;
    lSAddr = (lSAddr + lDFill + lDOffs + aDataLen + 1) & ~1;
    SetFieldLengthEx(lSAddr-lAddr-lAFill-lAOffs, &lACode, &lAOffs, lROffs==2);
    //
    if (lSAddr >= (aNDBase + aNDSize)) {
      PostLogText("---- memory out !!!"S_CRLF);
      return FALSE;
    }
    lFiller = 0xFDFDFD; lPrePos = 0;
    if (lAFill > 0) { lPreamb[0] = 0xFD; lPrePos = lAFill;}
    MemCopy(&lACode, &lPreamb[lPrePos], lAOffs); lPrePos += lAOffs;
    if (lNFill > 0) { MemCopy(&lFiller, &lPreamb[lPrePos], lNFill); lPrePos += lNFill;}
    MemCopy(&lNCode, &lPreamb[lPrePos], lNOffs); lPrePos += lNOffs;
    MemCopy(aName, &lPreamb[lPrePos], aNameLen); lPrePos += aNameLen;
    if (lDFill > 0) { MemCopy(&lFiller, &lPreamb[lPrePos], lDFill); lPrePos += lDFill;}
    MemCopy(&lDCode, &lPreamb[lPrePos], lDOffs); lPrePos += lDOffs;
    if (((lPrePos-lAFill) & 1) != 0) { lPreamb[lPrePos++] = aData[0]; lDPos = 1;}
    else lDPos = 0;
    //
    if (lFieldAddr > 0) {
      PostLogText("---- erase last value !!! "S_CRLF);
      ClearFieldStatus(lFieldAddr);
    }
    PostLogText("---- start write data !!! "S_CRLF);
    SetNameData(lAddr, lPreamb, lPrePos); lAddr += lPrePos;
    aDataLen -= lDPos;
    if (((lAddr + aDataLen) & 1) != 0) lLength = aDataLen - 1;
    else lLength = aDataLen;
    if (lLength > 0) SetNameData(lAddr, &aData[lDPos], lLength);
    if (lLength < aDataLen) {
      lAddr += lLength;
      lLength = 0xFD00 | aData[lLength+lDPos];
      SetNameData(lAddr, &lLength, sizeof(lLength));
    }
    return TRUE;
  }
  return FALSE;
}

bool WriteNamedIdxData(char *aName,int aNameLen,int aNameIdx,void *aData,int aDataLen)
{
  //uint32_t lBase = SYSBLOCK->hsbBaseAddr + SYSBLOCK->hsbFullSize;
  if assigned(gCfgAddress)
    return _WriteNamedData(gCfgAddress, gCfgLength, //lBase, gMaxCodeDataSize - SYSBLOCK->hsbFullSize,
      aName, aNameLen, aNameIdx, aData, aDataLen);
  else return FALSE;
}

bool WriteNamedData(char *aName,int aNameLen,void *aData,int aDataLen)
{
  return WriteNamedIdxData(aName, aNameLen, NOIDX, aData, aDataLen);
}

bool _FindNamedData(uint32_t *aBase,char *aName,int aNameLen,int aNameIdx,char **aData,int *aCount)
{
  char     lName[32];
  uint32_t lFieldAddr;
  int      lFieldLen;
  uint16_t lLength;
  uint8_t  lOffs;
  bool     lError;
  //
  //PostLogText("---- FindNamedData ----"S_CRLF);
  if (aNameLen == TOEND) aNameLen = StrLength(aName);
  //PostLogMessage(aName, aNameLen); PostLogText(S_CRLF);
  if (PakNameIdx(aName, aNameLen, aNameIdx, lName, &aNameLen)) aName = lName;
  if ( GetNamedField(aBase, aName, aNameLen, TRUE, TRUE, &lFieldAddr, &lFieldLen, &lOffs) ) {
    lFieldAddr += lFieldLen;
    //PostLogText("---- finded Name ----"S_CRLF);
    if (GetFieldLength((void*)lFieldAddr, &lLength, &lOffs, &lError)) {
      *aData = (void*)(lFieldAddr+lOffs);
      *aCount = lLength - lOffs;
      return TRUE;
    }
  }
  return FALSE;
}

bool _FindNextNamedDataAndIdx(uint32_t *aAddr,char *aName,int aNameLen,char **aData,int *aCount,int *aNameIdx)
{
  uint32_t lNameAddr, lDataAddr;
  int      lNameLen;
  uint16_t lDataLen;
  uint8_t  lOffs;
  bool     lError;
  //
  if (aNameLen == TOEND) aNameLen = StrLength(aName);
  while ( GetNamedField(aAddr, aName, aNameLen, FALSE, TRUE, &lNameAddr, &lNameLen, &lOffs) ) {
    lDataAddr = lNameAddr + lNameLen;
    lNameAddr += lOffs; lNameLen -= lOffs;
    UnpakNameIdx((char*)lNameAddr, lNameLen, &lNameLen, aNameIdx);
    if ((aNameLen == lNameLen) && GetFieldLength((void*)lDataAddr, &lDataLen, &lOffs, &lError)) {
      *aData = (void*)(lDataAddr+lOffs);
      *aCount = lDataLen - lOffs;
      return TRUE;
    }
  }
  return FALSE;
}

bool FindNamedIdxData(char *aName,int aNameLen,int aNameIdx,char **aData,int *aCount)
{
  uint32_t lBase = gCfgAddress;//SYSBLOCK->hsbBaseAddr + SYSBLOCK->hsbFullSize;
  if assigned(lBase)
    return _FindNamedData(&lBase, aName, aNameLen, aNameIdx, aData, aCount);
  else return FALSE;
}

bool FindNamedData(char *aName,int aNameLen,char **aData,int *aCount)
{
  return FindNamedIdxData(aName, aNameLen, NOIDX, aData, aCount);
}

bool FindNextNamedDataAndIdx(uint32_t *aAddr,char *aName,int aNameLen,char **aData,int *aCount,int *aNameIdx)
{
  if (*aAddr == 0) *aAddr = gCfgAddress;//SYSBLOCK->hsbBaseAddr + SYSBLOCK->hsbFullSize;
  if (*aAddr == 0)
    return _FindNextNamedDataAndIdx(aAddr, aName, aNameLen, aData, aCount, aNameIdx);
  else return FALSE;
}

uint32_t ReadUIntVar(char *aName,uint32_t aDefValue)
{
  char *lValPtr;
  uint32_t lResult; 
  int lCount;
  //
  if (FindNamedData(aName, TOEND, &lValPtr, &lCount) &&
      (lCount <= sizeof(uint32_t))) {
    lResult = 0; MemCopy(lValPtr, &lResult, lCount);
    return lResult; //(*lIntPtr) & (MAXUINT32 >> (8*(sizeof(uint32_t) - lCount)));
  }
  else return aDefValue;
}

bool WriteUIntVar(char *aName,uint32_t aValue,uint32_t aDefValue)
{
  if (aValue == aDefValue) return DeleteNamedData(aName, TOEND);
  else return WriteNamedData(aName, TOEND, (char*)&aValue, ReduceUInt(aValue));
}

char* ReadNamedStr(char *aName,char *aStr,char *aDefStr)
{
  char *lSrc;
  int lCount;
  if (FindNamedData(aName, TOEND, (void*)&lSrc, &lCount))
    MakeStr(lSrc, lCount, aStr);
  else AssignStr(aDefStr, aStr);
  return aStr;
}

bool _PackCopyNamedData(uint32_t aSourceBase,uint32_t aDestBase,uint32_t aMaxDestSize)
{
  uint32_t lAddr, lNameAddr, lDataAddr;
  int      lNameLen, lNameIdx;
  uint16_t lDataLen;
  uint8_t  lOffs;
  bool     lError;
  //
  lAddr = aSourceBase;
  while ( GetNamedField(&lAddr, NULL, 0, FALSE, TRUE, &lNameAddr, &lNameLen, &lOffs) ) {
    lDataAddr = lNameAddr + lNameLen;
    lNameAddr += lOffs; lNameLen -= lOffs;
    if (GetFieldLength((void*)lDataAddr, &lDataLen, &lOffs, &lError)) {
      UnpakNameIdx((char*)lNameAddr, lNameLen, &lNameLen, &lNameIdx);
      if (!_WriteNamedData(aDestBase, aMaxDestSize,
        (char*)lNameAddr, lNameLen, lNameIdx, (void*)(lDataAddr+lOffs), lDataLen-lOffs)) return FALSE;
    }
  }
  return TRUE;
}

bool InitNamedData(uint32_t aStartAddr,uint32_t aMaxSize)
{
  //aMaxSize += GetMaxCodeDataSize(aStartAddr) - LO_CODEDATASIZE;
  //PostLogInt("MaxSize=", aMaxSize, S_CRLF));
  /*
  if assigned(gCfgAddress)
    return _PackCopyNamedData(gCfgAddress, //SYSBLOCK->hsbBaseAddr + SYSBLOCK->hsbFullSize,
      aStartAddr, aMaxSize);
  else*/ return FALSE;
}

uint32_t DeviceID(void)
{
  UINT32_U lDID;
  //
  lDID.Bytes[0] = ((char*)UNIID)[0];
  lDID.Bytes[1] = ((char*)UNIID)[2];
  lDID.Bytes[2] = ((char*)UNIID)[4];
  lDID.Bytes[3] = ((char*)UNIID)[11];
  return lDID.Long;
}

bool IsValidAppAddr(uint32_t aAddr)
{
  return assigned(aAddr) && (aAddr != EMPTY32) &&
    ((*(uint32_t*)aAddr >> 20) == 0x0200) &&
    ((*(uint32_t*)(aAddr + 4) >> 20) == 0x0080);
}

uint32_t GetVTORLength(uint32_t aAddr)
{
  uint32_t I, *lAddrPtr;
  if (IsValidAppAddr(aAddr)) {
    lAddrPtr = (uint32_t*)(aAddr + 8);
    for (I=0;I < 0x80;I++)
      if ((lAddrPtr[I] != 0) && ((lAddrPtr[I] >> 20) != 0x0080)) break;
    if (I < 0x80) return (I + 2) << 2;
  }
  return 0;
}

uint32_t AppAddress(void)
{
  return gAppAddress;
}

uint32_t VTORLength(void)
{
  return gVTORLength;
}

void PushAppAddress(uint32_t aAddr)
{
  *(uint32_t*)(BASE_RAMADDR+0) = APPADDRSIGN;
  *(uint32_t*)(BASE_RAMADDR+4) = aAddr;
}

uint32_t PopAppAddress(void)
{
  if (*(uint32_t*)(BASE_RAMADDR+0) == APPADDRSIGN)
    return *(uint32_t*)(BASE_RAMADDR+4);
  return 0;
}

void AssignAppAddress(void)
{
  gAppAddress = PopAppAddress();
}

void AssignVTORLength(uint32_t aDefVTORLength)
{
  if (aDefVTORLength == 0) gVTORLength = GetVTORLength(gAppAddress);
  else gVTORLength = aDefVTORLength;
}

void AssignAppVTOR(uint32_t aAddr)
{
  if (aAddr == NULL) gAppAddress = PopAppAddress(); else gAppAddress = aAddr;
  if (gAppAddress > 0) {
    gVTORLength = GetVTORLength(gAppAddress);
    VTOR_Setup(gAppAddress);

    //gMaxCodeDataSize = GetMaxCodeDataSize(gAppAddress);
  }
}

bool GetSysBlock(uint32_t aAddr,SYSBLOCK_TypeDef **aSysBlock)
{
  SYSBLOCK_TypeDef *lSysBlock;
  if (aAddr == 0) aAddr = gAppAddress;
  if (IsValidAppAddr(aAddr) && (gVTORLength > 0)) {
    lSysBlock = (SYSBLOCK_TypeDef*)(aAddr + gVTORLength);
    if (lSysBlock->hsbSignature == SYSBLOCKSIGN) {
      *aSysBlock = lSysBlock;
      return TRUE;
    }
  }
  return FALSE;
}

bool UnCodeAddr(uint16_t aCode,uint32_t *aAddress,uint32_t *aLength)
{
  if (aCode != 0) {
    *aAddress = BASE_APPADDR + ((uint32_t)(aCode & 0xFF80) << 4);
    *aLength = ((uint32_t)((aCode & 0x007F) + 1)) << 10;
    return TRUE;
  }
  return FALSE;
}

bool SetCfgBlock(uint32_t aAddr,uint32_t aLen)
{
  gCfgAddress = aAddr+sizeof(int); gCfgLength = aLen-sizeof(int); return TRUE;
}

bool InitCfgBlock(uint32_t aAddr,uint32_t aLen,uint8_t aBegPage,uint8_t aPageCount)
{
  uint32_t lData=CFGBLOCKSIGN;
  if (*(uint32_t*)aAddr != EMPTY32) FLASH_Clear(aBegPage, aPageCount);
  FLASH_Write(aAddr, &lData, sizeof(lData));
  SetCfgBlock(aAddr, aLen);
  return TRUE;
}

bool AssignCfgBlock(uint32_t aAddr)
{
  SYSBLOCK_TypeDef *lSysBlock;
  uint32_t lAddrA, lLenA, lAddrB, lLenB;
  //
  if (GetSysBlock(aAddr, &lSysBlock)) {
    lAddrA = lAddrB = 0;
    if (UnCodeAddr(lSysBlock->hsbCfgA.mBaseSegmCode, &lAddrA, &lLenA) &&
        (*(uint32_t*)lAddrA == CFGBLOCKSIGN)) return SetCfgBlock(lAddrA, lLenA);
    if (UnCodeAddr(lSysBlock->hsbCfgB.mBaseSegmCode, &lAddrB, &lLenB) &&
        (*(uint32_t*)lAddrB == CFGBLOCKSIGN)) return SetCfgBlock(lAddrB, lLenB);
    if assigned(lAddrA) return InitCfgBlock(lAddrA, lLenA, lSysBlock->hsbCfgA.mBegPage, lSysBlock->hsbCfgA.mPageCount);
    if assigned(lAddrB) return InitCfgBlock(lAddrB, lLenB, lSysBlock->hsbCfgB.mBegPage, lSysBlock->hsbCfgB.mPageCount);
  }
  return FALSE;
}

bool ReadCfgBlock(uint32_t aAddr)
{
  SYSBLOCK_TypeDef *lSysBlock;
  uint32_t lAddrA, lLenA, lAddrB, lLenB;
  //
  if (GetSysBlock(aAddr, &lSysBlock)) {
    lAddrA = lAddrB = 0;
    UnCodeAddr(lSysBlock->hsbCfgA.mBaseSegmCode, &lAddrA, &lLenA);
    UnCodeAddr(lSysBlock->hsbCfgB.mBaseSegmCode, &lAddrB, &lLenB);
    //
    PostLogHex("Code A: ", lSysBlock->hsbCfgA.mBaseSegmCode, 2, ","); PostLogHex(" Addr A: ", lAddrA, 4, ","); PostLogInt(" Len A: ", lLenA, ",");
      PostLogInt(" Page A: ", lSysBlock->hsbCfgA.mBegPage, ","); PostLogInt(" PCount A: ", lSysBlock->hsbCfgA.mPageCount, S_CRLF);
    PostLogHex("Code B: ", lSysBlock->hsbCfgB.mBaseSegmCode, 2, ","); PostLogHex(" Addr B: ", lAddrB, 4, ","); PostLogInt(" Len B: ", lLenB, ",");
      PostLogInt(" Page B: ", lSysBlock->hsbCfgB.mBegPage, ","); PostLogInt(" PCount B: ", lSysBlock->hsbCfgB.mPageCount, S_CRLF);
  }
  return FALSE;
}

uint32_t GetStartAddr(void)
{
  SYSBLOCK_TypeDef *lSysBlock, *lHiSysBlock;
  uint32_t lLoAppAddr, lHiAppAddr;
  uint16_t lAppIndex;
  lLoAppAddr = DefaultAppAddr(FALSE);
  if (GetSysBlock(lLoAppAddr, &lSysBlock) && (lSysBlock->hsbAppEnbSign == APPENBSIGN)) {
    lAppIndex = lSysBlock->hsbAppIndex;
    if (GetSysBlock(lSysBlock->hsbNextAddr, &lHiSysBlock) && (lHiSysBlock->hsbAppEnbSign == APPENBSIGN)) {
      lAppIndex -= lHiSysBlock->hsbAppIndex;
      if (lAppIndex > 0x7FFF) return lSysBlock->hsbNextAddr;
    }
  }
  else {
    lHiAppAddr = DefaultAppAddr(TRUE);
    if (GetSysBlock(lHiAppAddr, &lSysBlock) && (lSysBlock->hsbAppEnbSign == APPENBSIGN))
      return lHiAppAddr;
  }  
  return lLoAppAddr;
}

// +FLASHREAD=0x0800C000,48
// +READNDATA=$FIRST
// +READNDATA=$PAR1
// +WRITENDATA=$PAR1,43210FED
//                   +FLASHREAD:OK,0D 07 FE 24 50 41 52 31   05 43 21 0F ED   4646464646      FFFFFFFFFFFFFFFFFFFFFFFFFFFF
//                                 0  1  2  3  4  5  6  7    8  9  10 11 12
// +FLASHCLEAR=3,1
// +WRITENDATA=$PAR2,2233445566
/*
+FLASHREAD:OK,                     0D 07 FE 24 50 41 52 31 05 00 21 0F ED0E07FE24504152320622334455660D07FE24504152310511223344FFFFFFFFFFFFFFFF
// 22446688113355772244668811335577224466881133557722446688113355772244668811335577224466881133557722446688113355772244668811335577224466881133
//     55772244668811335577224466881133557722446688113355772244668811335577224466881133557722446688113355772244668811BB
+WRITENDATA=$PAR1,11223344
+WRITENDATA---- WriteNamedData ----
$PAR1
---- level 1:
---- CheckNameFlags ----
---- level 2:
---- erase last value !!!
----w1
---- start write data !!!
----w1
----w1
----w1
----w2
----w2
----w1
----w1
---- last write data
----w4
*/

