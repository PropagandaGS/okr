// �������� ���������� ������� ��� MCUs
// � Olgvel, 2012-2018
//

#ifndef __MY_LIB_H
#define __MY_LIB_H

#ifdef __cplusplus
extern "C" {
#endif

#define NULL            0
#define FALSE           0
#define TRUE            1
#define bool            char
#define byte            short
#define ERROR_RESULT    -1
#define CONFIRM_RESULT  -2
#define TOEND           -1
#define DEFAULT         0

#define CR              13
#define LF              10
#define TAB             0x09
#define EOF             0x1A
#define QUOTE           0x22    // "
#define APOS            0x27    // '
#define COMMA           0x2C    // ,
#define DP              0x3A    // :
#define PC              0x3B    // ;
#define EQ              0x3D    // =
#define SPACE           0x20

#define S_CR            "\x0D"
#define S_LF            "\x0A"
#define S_CRLF          "\x0D\x0A"
#define S_EOF           "\x1A"
#define S_TAB           "\x09"
#define S_QUOTE         "\x22"
#define S_APOS          "\x27"
#define S_COMMA         "\x2C"
#define S_DP            "\x3A"
#define S_PC            "\x3B"
#define S_EQ            "\x3D"
#define S_SPACE         "\x20"

#define MAXUINT8        0xFF
#define MAXUINT16       0xFFFF
#define MAXUINT32       0xFFFFFFFF
#define MININT32        -2147483648
#define MAXINT32        2147483647
#define MAXRAMSIZE      64*1024

#define MSECS           1000
#define SECSPERMIN      60
#define MINSPERHOUR     60
#define HOURSPERDAY     24

#define EMPTY16         0xFFFF
#define EMPTY32         0xFFFFFFFF
#define NOT_FOUND      -1
#define EMPTY          -1
#define TCFSTR          1
#define TCFINT          2
#define TCFANY          3
#define TCFBITS         2

#define RRF_TIMER      -1
#define RRF_INFULL     -2
#define RRF_CANCEL      0

#define BYTESZ          sizeof(uint8_t)
#define WORDSZ          sizeof(uint16_t)
#define DWORDSZ         sizeof(uint32_t)

#define assigned(a)     (a != NULL)
#define notassigned(a)  (a == NULL)
#define countof(a)      ((sizeof(a)/sizeof((a)[0])))
#define align32(a)      ((a + 3) & ~3)
#define member_offset )type,member:  ))int:)&))type*:0:->member::
//#define member_size(type, member) sizeof(((type *)0)->member)
#define member_size(type, member) sizeof(((type){0}).member)

#define CSET_F(pattern,flag,state)  if(state)pattern|=flag;else pattern&=~flag
#define GET_F(pattern,flag)   ((pattern & (flag)) != 0)
#define GET_FZ(pattern,flag)  ((pattern & (flag)) == 0)
#define GET_NF(pattern,flag)  ((pattern & ~(flag)) != 0)
#define GET_FA(pattern,flag)  ((pattern & (flag)) == (flag))
#define F(bit)                  (1 << bit)
#define INCLUDE(set,bit)        (set |= F(bit))
#define EXCLUDE(set,bit)        (set &= ~F(bit))
#define CHANGE(set,bit)         (set ^= F(bit))
#define CSET(pattern,bit,state) if(state)pattern|=F(bit);else pattern&=~F(bit)
#define SGET(pattern,bit)       ((pattern >> bit) & 1)
#define TEST_OK(pattern,bit)    ((pattern & F(bit)) != 0)
#define TEST_NO(pattern,bit)    ((pattern & F(bit)) == 0)
//#define TEST_EQ(pattern,bit,state)    ((pattern >> bit) & 1) == state)
//#define TEST_NQ(pattern,bit,state)    ((pattern >> bit) & 1) != state)

#define MAX_TXTCMDPARAMS   4
    
#define offsetof(s, memb)       ((int) ((char *) & ((s *) 0) -> memb - (char *) 0))
    

extern volatile unsigned int TICKS;

/* Exported types ------------------------------------------------------------*/

typedef void* pvoid;
typedef char* pchar;
typedef uint8_t  CHAR_ARRAY[MAXRAMSIZE];
typedef CHAR_ARRAY* PCHAR_ARRAY;

typedef uint32_t UINT32_ARRAY[MAXRAMSIZE/sizeof(uint32_t)];

typedef union {
  char *uChar;
  uint8_t *uByte;
  uint16_t *uWord;
  uint32_t *uLong;
  void *uPtr;
  uint32_t uAddr;
} PTR_U;

typedef union {
  uint8_t Bytes[sizeof(uint32_t)];
  uint16_t Words[sizeof(uint16_t)];
  uint32_t Long;
  int32_t  Int;
} UINT32_U;

typedef union {
  uint8_t Bytes[sizeof(uint64_t)];
  uint16_t Words[sizeof(uint32_t)];
  uint32_t Longs[sizeof(uint16_t)];
  int32_t  Ints[sizeof(uint16_t)];
  uint64_t LLong;
} UINT64_U;

typedef union {
  uint8_t Bytes[sizeof(uint16_t)];
  uint16_t Word;
} UINT16_U;

typedef struct {
  UINT32_U sParamA;
  UINT32_U sParamB;
  UINT32_U sParamC;
  UINT32_U sParamD;
} UINTPARS_T;

typedef struct {
  char*    sTCPPars[MAX_TXTCMDPARAMS];
  uint32_t sTCPVals[MAX_TXTCMDPARAMS];
  uint8_t  sTCPCmdIdx;
  uint8_t  sTCPCount;
  bool     sTCPResult;
  bool     sTCPCommonRes;
  //
} TXTCMD_PAR_TypeDef;

typedef struct {
  uint8_t sRLCount;
  uint8_t sRLCapacity;
  uint8_t sRLSize;
  uint8_t sRLRes;
  char sRLBuf[];
} RLIST_T;

typedef struct {
  uint32_t sEVHostID;
  uint32_t sEVSourceID;
  uint32_t sEVCode;
  uint32_t sEVTicks;
} EVENT_DESCR_TypeDef;

typedef void(* MYFUNC)(void);
typedef void(* MESSAGE_CALLBACK)(char *aMessage,int32_t aLength);
typedef void(* CMESSAGE_CALLBACK)(const char *aMessage,int32_t aLength);
typedef void(* DATA_CALLBACK)(void *aData);
typedef void(* ADDRMESSAGE_CALLBACK)(void *aAddress,char *aMessage,int32_t aLength);
typedef void(* ADDRSTATE_CALLBACK)(char *aAddress,int32_t aState);
typedef void(* NOTIFY_CALLBACK)(int32_t aState);
typedef void(* STATEHANDLER_CALLBACK)(void *aSender,int32_t aState);
typedef void(* MYHANDLER_CALLBACK)(void *aSender,void *aContext);
//typedef void(* MYHANDLER)(void *aContext,void *aParam);
typedef void(* PHANDLER)(void *aContext,void *aParam);
typedef void(* TCHANDLER)(void *aContext,TXTCMD_PAR_TypeDef *aParam);
//typedef void(* BCHANDLER)(void *aContext,BINCMD_PAR_TypeDef *aParam);
typedef void*(* MEMALLOC_CALLBACK)(uint32_t aAllocSize);

typedef struct {
  PHANDLER sFunc;
  void*    sCtx;
} MHANDLER_TypeDef;

typedef struct {
  PHANDLER sFunc;
  void*    sCtx;
  void*    sNext;
} UHANDLER_TypeDef;

typedef struct {
  TCHANDLER sTCHandler;
  uint16_t  sTCFlags;
  int16_t  sTCCtxIdx;
  //
} TCHANDLER_TypeDef;

typedef struct {
  uint32_t sPSTicks;
  void*    sPSData;
  int16_t  sPSStartState;
  int16_t  sPSInverseSet;
  int16_t  sPSState;
  uint16_t sPSPeriod;
  uint16_t sPSPulseWidth;
  uint16_t sPSCycles;
  uint16_t sPSTimeOut;
  STATEHANDLER_CALLBACK sPSOnSetState;
  MYHANDLER_CALLBACK sPSOnStop;
} MY_PESTATE_TypeDef, *MY_PESTATE_PtrDef;

/* Exported constants --------------------------------------------------------*/

typedef struct {
  PCHAR_ARRAY sLBuffer;
  uint16_t sLCapacity;
  uint16_t sLCount;
  uint16_t sLBlockSize;
  uint8_t  sLIndexSize;
  bool     sLIsIndexList;
} MY_LIST_TypeDef;

typedef struct {
  PCHAR_ARRAY sILBuffer;
  uint16_t sILBlockSize;
  uint8_t  sILIndexSize;
  bool     sILFlag;
  MHANDLER_TypeDef sIL_OnAfterIns;
  MHANDLER_TypeDef sIL_OnBeforeDel;
}  MY_INDEXLIST_TypeDef;

typedef struct {
  //void *sCOwner;
  char *sCBuffer;
  int  sCBufSize;
  volatile int sCAddPos, sCDelPos, sCChkPos;
  uint32_t sCAccessTicks;
  MHANDLER_TypeDef sCOnBeforeRead;
  MHANDLER_TypeDef sCOnAfterWrite;
}  MY_CBUF_TypeDef;

typedef enum {
  LF_ACTIVE,
  //
} LINK_FLAG_TypeDef;

#define LINK_HDR \
   uint16_t sLSign; \
   uint8_t sLFlags, sLR8_1; \
   MY_CBUF_TypeDef *sLRx, *sLTx;

typedef struct {
  LINK_HDR
}  MY_LINK_TypeDef;

typedef struct {
  char *sC4Buffer;
  uint16_t sC4BufSize, sC4Marker;
  uint16_t sC4QuoteSize, sC4QuoteCount;
  uint16_t sC4AddPos, sC4DelPos;
}  MY_CIRC4BUF_TypeDef;

typedef struct {
  char *sCLBuffer;
  uint16_t sCLBufSize;
  bool sCLReadNoDel, sCLRes;
  uint16_t sCLRdPos, sCLWrPos;
  uint16_t sCLAddPos, sCLDelPos;
}  MY_CIRCLBUF_TypeDef;
//
// 0503869425 - receiv
// 0504144356 - trans
//
/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

//#ifdef __cplusplus
//extern "C" {
//#endif
    
void SetWDGStart(bool aIsWDGStart);
bool WasWDGStart(void);

void* FixMemAlloc(uint32_t aAllocSize);
uint32_t GetFixMemAllocSize(void);

bool IsTimeOut(unsigned int aCurTicks,unsigned int aRemTicks,unsigned int aTimeOut);
bool IsTimeOutEx(unsigned int aCurTicks,unsigned int *aRemTicks,unsigned int aTimeOut);

void MemClear(void *aBuffer,int aCount);
void MemFill(void *aBuffer,int aCount,uint8_t aByte);
void MemCopy(const void *aSource,void *aDest,int aCount);
bool MemComp(const char* aStrA,const char* aStrB,int aCount);
//bool StrCompare(char *aTemplate,void *aSource,int aCount);
bool StrComp(char *aStrA,char *aStrB);
bool SubstrCompare(char *aStr,int aStrCount,char *aSource,int aCount,bool *aStrict);
//bool SubstrCompare(char *aStr,char *aSource,int aCount,bool *aStrict);
bool SubstrComp(char *aStr,char *aSource,int aCount);
bool SubtextComp(char *aStr,char *aSource,int aCount);
int Arr32NEqual(void *aArrA,void *aArrB,int aCount);
int Arr32NFill(void *aArr,uint32_t aVal,int aCount);
//int ReduceUInt(uint32_t aValue);
void Int2Hex(unsigned int aInt,void *aBuffer,int aDigits);
char* Int2HexZ(unsigned int aInt,void *aBuffer,int aDigits);
char* Bin2HexStrZ(const void *aBinBuf,void *aHexBuf,int aByteCount,bool aInsSpace);
bool HexStr2Bin(void *aStr,int aLength,void *aBinBuf,int *aBinCount);
int  Int2StrDig(int aInt,char *aBuffer,int aDigits);
char* Int2StrDigZ(int aInt,char *aBuffer,int aDigits);
int  Int2Str(int aInt,char *aBuffer,int aSize);
int  IntLL2Str(int64_t aInt,char *aBuffer,int aSize);
char* Int2StrZ(int aInt,char *aBuffer,int aSize);
int Str2IntDef(void *aStr,int aLength,int aDefault);
int Hex2IntDef(void *aStr,int aLength,int aDefault);
bool UCS2StrToWin1251(char *aSource,int aLength,char *aDest);
bool Win1251ToUCS2Str(char *aSource,int aLength,char *aDest);
bool IsExtAscii(char *aStr,int aLength);
int MinInt(int aValue,int aDefInt);
int MaxInt(int aValue,int aDefInt);
//
char* Int2TextIPZ(uint32_t aIntIP,char *aStr);
bool TextIP2Int(char *aStr,int aLength,uint32_t *aAddr);
uint32_t StrIP2Int(char *aStr);
bool UnpackIPAndPort(char *aStr,int aLength,uint32_t *aIP,uint16_t *aPort);
bool FindShortStr(char *aStr,int aStrLen,const char *aStrBuf,int *aIndex,int *aLength);
//bool FindShortStr(void *aStr,const void *aStrBuf,int *aIndex,int *aLength);
bool GetShortStr(int aIndex,const char *aStrBuf,char **aPStr,int *aLength);
char* GetShortStrZ(int aIndex,const char *aStrBuf);
//bool GetShortStrIndex(char *aStr,const void *aStrBuf,int *aIndex);
int GetShortIndex(short aSource,short *aBuf,int aSize);
int StrLength(const char *aStr);
int StrLengthEx(char *aStr,char *aEndChars,int aMaxLen);
int CharPos(char aChar,void *aBuf,int aTestCount);
bool GetCharPos(char aChar,void *aBuf,int aTestCount,int *aPos);
int CharPosEx(char aChar,void *aBuf,int aTestCount,bool aInvCond);
void CharChange(char *aSource,int aCount,char aSChar,char aDChar);
char* AssignStr(const char *aSubstr,char *aBuf);
char* MakeStr(char *aSource,int aCount,char *aBuf);
int AddStr(char *aSubstr,char *aBuf,int aCount);
void TrimLeft(char *aMessage,int *aIndex,int *aLength);
void TrimRight(char *aMessage,int *aLength);
char LowerCase(char aChar);
char UpCase(char aChar);
uint32_t RevLong(uint32_t aValue);
uint16_t RevWord(uint16_t aValue);
void BufCopy(const void *aSource,void *aDest,int aCount,bool aRev);
//void EncodeBase64(void *aSource,int aCount,void *aDest,int *aDestCount);
//bool DecodeBase64(void *aSource,int aCount,void *aDest,int *aDestCount);
bool GetInParam(char *aStr,int32_t aLen,int32_t *aSearchPos,
  int32_t *aParPos,int32_t *aParLen);
bool GetInParamPC(char *aStr,int32_t aLen,int32_t *aSearchPos,
  int32_t *aParPos,int32_t *aParLen);
bool GetInParamEx(char aParser,char *aStr,int32_t aLen,int32_t *aSearchPos,
  int32_t *aParPos,int32_t *aParLen);
bool GetNamedParam(char *aStr,int aStrLen,const char *aStrBuf,int *aNameIdx,
  int *aParPos,int *aParLen);
bool GetInIntParam(char *aStr,int aLen,int *aSearchPos,int *aIntVal,int aDefault);
bool GetInIntParamNoDef(char *aStr,int aLen,int *aSearchPos,int *aIntVal,int aDefault);
bool GetInIPParamNoDef(char *aStr,int aLen,int *aSearchPos,uint32_t *aIPVal,uint32_t aDefault);
//
void AssignPostLogMessage(CMESSAGE_CALLBACK aPostLogMessageFunc);
void PostLogMessage(const char *aMessage,int aLength);
void PostLogText(const char *aText);
void PostLogMsg(const char *aBeforeText,const char *aStr,int aLength,const char *aAfterText);
void PostLogTxt(const char *aBeforeText,const char *aText,const char *aAfterText);
void PostLogInt(const char *aBeforeText,int aValue,const char *aAfterText);
void PostLogInt64(const char *aBeforeText,int64_t aValue,const char *aAfterText);
void PostLogHex(char *aBeforeText,uint32_t aValue,int aByteCount,char *aAfterText);
void PostLogHexStr(char *aBeforeText,char *aBuf,int aByteCount,char *aAfterText);
void PostLogTextIP(char *aBeforeText,uint32_t aValue,char *aAfterText);
void LogProcess(void);
//
MY_PESTATE_TypeDef* MyPS_Init(MY_PESTATE_TypeDef *aMyPS,STATEHANDLER_CALLBACK aOnSetState,
  MYHANDLER_CALLBACK aOnStop,void *aData);
void MyPS_Setup(void *aMyPS,int16_t aStartState,uint16_t aPeriod,uint16_t aPulseWidth,uint16_t aCycles);
void MyPS_Process(void *aMyPS);

//=========================== MHandler =========================================
void MHandler_Setup(MHANDLER_TypeDef *aHandler,PHANDLER aFunc,void *aContext);
bool MHandler_Call(MHANDLER_TypeDef *aHandler,void *aParam);
//
//=========================== UHandler =========================================
void UHandler_Assign(UHANDLER_TypeDef **aHandler,PHANDLER aFunc,void *aContext);
void UHandler_Exec(UHANDLER_TypeDef *aHandler,void *aParam);

//=========================== MyList ===========================================
MY_LIST_TypeDef* MyList_Init(MY_LIST_TypeDef *aList,uint16_t aCapacity,uint16_t aBlockSize);
uint16_t MyList_Size(uint16_t aCapacity,uint16_t aBlockSize);
void* MyList_Add(MY_LIST_TypeDef *aList);
void MyList_InsInt(MY_LIST_TypeDef *aList,int32_t aIndex,uint32_t aValue);
bool MyList_Delete(MY_LIST_TypeDef *aList,int32_t aIndex);
void* MyList_First(MY_LIST_TypeDef *aList);
void* MyList_Last(MY_LIST_TypeDef *aList);
void* MyList_Item(MY_LIST_TypeDef *aList,int32_t aIndex);
int32_t MyList_IndexOf(MY_LIST_TypeDef *aList,uint32_t aValue);
bool MyList_FindIndex(MY_LIST_TypeDef *aList,uint32_t aValue,void **aItem,int32_t *aIndex);

//=========================== MyIndexList ======================================
MY_INDEXLIST_TypeDef* MyIndexList_Init(MY_INDEXLIST_TypeDef *aList,
  uint16_t aCapacity,uint16_t aBlockSize);
uint16_t MyIndexList_Size(uint16_t aCapacity,uint16_t aBlockSize);
void* MyIndexList_Add(MY_INDEXLIST_TypeDef *aList);

//=========================== MyCBuf =======================================
MY_CBUF_TypeDef* MyCBuf_Init(void *aOwner,int aCapacity);
MY_CBUF_TypeDef* MyCBuf_InitEx(void *aOwner,void *aBuffer,int aCapacity);
int MyCBuf_WriteEx(MY_CBUF_TypeDef *aDescr,const char *aSource,int aCount,bool aSending);
int MyCBuf_Write(MY_CBUF_TypeDef *aDescr,const char *aSource,int aCount);
int MyCBuf_WriteText(MY_CBUF_TypeDef *aDescr,const char *aSource);
int MyCBuf_WriteW(MY_CBUF_TypeDef *aDescr,const char *aSource,int aCount);
int MyCBuf_WriteTextW(MY_CBUF_TypeDef *aDescr,char *aSource);
void MyCBuf_WriteTxtEx(MY_CBUF_TypeDef *aDescr,char *aBeforeText,char *aText,
  char *aAfterText,bool aQuoted,bool aSending);
void MyCBuf_WriteIntEx(MY_CBUF_TypeDef *aDescr,void *aBeforeText,int aValue,
  void *aAfterText,bool aSending);
void MyCBuf_WriteHexEx(MY_CBUF_TypeDef *aDescr,void *aBeforeText,uint32_t aValue,
  int aByteCount,void *aAfterText,bool aSending);
void MyCBuf_WriteIPEx(MY_CBUF_TypeDef *aDescr,void *aBeforeText,uint32_t aValue,
  void *aAfterText,bool aSending);
//
bool MyCBuf_Read(MY_CBUF_TypeDef *aDescr,char *aDest,int aDestSize,
  unsigned int aWaitTime,int *aCount);
int MyCBuf_ReadFmt(MY_CBUF_TypeDef *aDescr,char *aDest,int aDestSize,
  const char *aEndsTbl,uint8_t aFlags,unsigned int aWaitTime,int *aCount);
bool MyCBuf_MoveTo(MY_CBUF_TypeDef *aSource,MY_CBUF_TypeDef *aDest,int aCount,bool aChkMove,int *aRetCount);
//
int MyCBuf_ReadCount(MY_CBUF_TypeDef *aDescr);
int MyCBuf_WriteCount(MY_CBUF_TypeDef *aDescr);
void MyCBuf_Clear(MY_CBUF_TypeDef *aDescr);

//=========================== MyCirc4Buf =======================================
MY_CIRC4BUF_TypeDef* MyCirc4Buf_Init(MY_CIRC4BUF_TypeDef *aDescr,uint16_t aCapacity,
  uint16_t aMarker,uint16_t aQuoteSize);
bool MyCirc4Buf_InData(MY_CIRC4BUF_TypeDef *aDescr,char **aDestPtr,uint16_t aDestSize,uint16_t *aCount);
bool MyCirc4Buf_Read(MY_CIRC4BUF_TypeDef *aDescr,char *aDest,uint16_t aDestSize,uint16_t *aCount);
bool MyCirc4Buf_OutData(MY_CIRC4BUF_TypeDef *aDescr,char **aSouPtr,uint16_t aMaxSouSize,uint16_t *aCount);
bool MyCirc4Buf_Write(MY_CIRC4BUF_TypeDef *aDescr,char *aSource,uint16_t aCount);
void MyCirc4Buf_Clear(MY_CIRC4BUF_TypeDef *aDescr);

//=========================== MyCircLBuf =======================================
MY_CIRCLBUF_TypeDef* MyCircLBuf_Init(MY_CIRCLBUF_TypeDef *aDescr,uint16_t aCapacity,MEMALLOC_CALLBACK aMemAlloc);
void MyCircLBuf_Clear(MY_CIRCLBUF_TypeDef *aDescr);
bool MyCircLBuf_Add(MY_CIRCLBUF_TypeDef *aDescr,char *aSource,int aCount);
bool MyCircLBuf_Write(MY_CIRCLBUF_TypeDef *aDescr,char *aSource,int aCount);
bool MyCircLBuf_Read(MY_CIRCLBUF_TypeDef *aDescr,char *aDest,uint16_t aDestSize,uint16_t *aCount);
bool MyCircLBuf_Delete(MY_CIRCLBUF_TypeDef *aDescr);
bool MyCircLBuf_IsEmpty(MY_CIRCLBUF_TypeDef *aDescr);
void PostCircLBufDbg(MY_CIRCLBUF_TypeDef *aDescr);
#ifdef __cplusplus
}
#endif

#endif /* __MY_LIB_H */
