// ���������� ��������� VAP
// ����������� � �������� ��������� VAP
// � Olgvel, 2011-2013
//
#include "my_vap.h"
#include "my_devctl.h"

#define VAPMASTERPREAMB  0x5D24C36A
#define VAPSLAVEPREAMB   0x5A28C96D
#define BROADCASTDEVICE  0x0F0F0F0F
#define CRCMASK          0xA001
#define CRCBASE          0xFFFF

#define CMD_UNKNOWN      1
#define CMD_DEVCONTROL   7
#define CMD_EVUPDATE     9
#define CMD_EVUPDATEANS  10
#define CMD_TUNNELDATA   11

//

uint16_t VAPCompCRC16(void *aBuffer,int aCount)
{
  uint16_t lCRC = CRCBASE;
  bool lOdd;
  while (aCount-- > 0) {
    lCRC ^= *(char*)aBuffer;
    (*(int*)&aBuffer)++;
    for (register int i=8;i>0;i--) {
      lOdd = lCRC & 1;
      lCRC >>= 1;
      if (lOdd) lCRC ^= CRCMASK;
    }
  }
  return lCRC;
}

void CompIterCRC16(char aChar,uint16_t *aCRC)
{
  uint16_t lCRC;
  bool lOdd;
  lCRC = *aCRC ^ aChar;
  for (register int i=8;i>0;i--) {
    lOdd = lCRC & 1;
    lCRC >>= 1;
    if (lOdd) lCRC ^= CRCMASK;
  }
  *aCRC = lCRC;
}

// MY_CBUF

void MyInitCBuf(MY_CBUF_TypeDef *aMyCBuf,void *aBuffer,int aSize)
{
  MemClear(aMyCBuf, sizeof(MY_CBUF_TypeDef));
  aMyCBuf->sCBSize = aSize;
  aMyCBuf->sCBBuffer = aBuffer;

}

void MyPutToCBuf(MY_CBUF_TypeDef *aMyCBuf,void *aBuffer,int aSize)
{
  MemClear(aMyCBuf, sizeof(MY_CBUF_TypeDef));
  aMyCBuf->sCBSize = aSize;
  aMyCBuf->sCBBuffer = aBuffer;
}

bool VapReadFrame(VAP_Descr_TypeDef *aVapDescr);
void VapHandleFrame(VAP_Descr_TypeDef *aVapDescr);
//bool VapPostFrame(VAP_Descr_TypeDef *aVapDescr,uint16_t aCommand,
//  void *aBlock,uint16_t aBlockSize);
void VapFrameSetup(VAP_Descr_TypeDef *aVapDescr);
void VapBlockAdd(VAP_Descr_TypeDef *aVapDescr,uint16_t aCommand,
  void *aBlock,uint16_t aBlockSize);
void VapFramePost(VAP_Descr_TypeDef *aVapDescr,uint16_t aNumber);

//==============================================================================

VAP_Descr_TypeDef* Vap_Init(VAP_Descr_TypeDef *aVapDescr,MY_USART_TypeDef *aMyUSART,bool aUseTunnel)
{
  if (aVapDescr == NULL)
    aVapDescr = FixMemAlloc( sizeof(VAP_Descr_TypeDef) );
  //
  MemClear(aVapDescr, sizeof(VAP_Descr_TypeDef));
  aVapDescr->sVDUSART = aMyUSART;
  aVapDescr->sVDSelfID = UNIID->b16_31;
  //
  aVapDescr->sVDTempBuffer = FixMemAlloc( VAPBUFSIZE );
  aVapDescr->sVDTempBufSize = VAPBUFSIZE;
  //
  if (aUseTunnel) {
    aVapDescr->sVDTunnelBuffer = FixMemAlloc( VAPTUNNELBUFSIZE );
    aVapDescr->sVDTunnelBufSize = VAPTUNNELBUFSIZE;
  }
  //
  aVapDescr->sVDIsMaster = FALSE;
  aVapDescr->sVDRcvPreamb.Long = VAPMASTERPREAMB;
  aVapDescr->sVDSndPreamb.Long = VAPSLAVEPREAMB;
  return aVapDescr;
}

void Vap_Process(VAP_Descr_TypeDef *aVapDescr)
{
//  MyUSART_ReturnDirByTC(aVapDescr->sVDUSART);
  /*if (aVapDescr->sVDTempBufCount > 0) {
    MyUSART_Write(aVapDescr->sVDUSART,
      (char*)aVapDescr->sVDTempBuffer, aVapDescr->sVDTempBufCount);
    aVapDescr->sVDTempBufCount = 0;
  }*/
  if (VapReadFrame(aVapDescr)) {
    VapHandleFrame(aVapDescr);
  }
//
//  if (MyUSART_Read_Fmt(MyUSART1, (char*)&lRxStr, sizeof(lRxStr),
//     0, AJAX_WAIT_TIME, &lReadCount))
//    MyUSART_Write(MyUSART2, (char*)&lRxStr, lReadCount);
//  Vap_Read_Frame(aVapDescr);
}

//------------------------------------------------------------------------------

bool VapReadFrame(VAP_Descr_TypeDef *aVapDescr)
  //MY_USART_TypeDef *aMyUSART,char *aBuffer,int aBufSize,
  //char aEndCode,unsigned int aWaitTime, int *aCount)
{
  MY_USART_TypeDef *lMyUSART;
  int lFrmIndex;
  char *lPChar;
  uint16_t lBufSize, lBufIndex, lBlockIndex, lCount;
  //uint16_t lTmpU16;
  char lCurChar;
  bool /*lTimeOut,*/ lResult = FALSE;
  //
  lMyUSART = aVapDescr->sVDUSART;
  lBufSize = lMyUSART->sInBufSize;
  lBufIndex = lBufSize - DMA_GetCurrDataCounter(lMyUSART->sHWCFG->sDMA_CHANNEL_RX);
  if (lBufIndex != lMyUSART->sReceivePos) {
    lMyUSART->sReceiveTicks = TICKS;
    lMyUSART->sReceivePos = lBufIndex;
  }
  //aVapDescr->sVDCurNumber = lMyUSART->sReceivePos;
/*
  lTimeOut = (aWaitTime != 0) && (lMyUSART->sReceivePos != lMyUSART->sReadPos) &&
      IsTimeOut(TICKS, lMyUSART->sReceiveTicks, aWaitTime);
  if (lTimeOut) {
    lMyUSART->sCheckPos--;
    if (lMyUSART->sCheckPos < 0) lMyUSART->sCheckPos = lInBufSize - 1;
  }
*/
  while ((!lResult) && (lMyUSART->sCheckPos != lMyUSART->sReceivePos)) {
    lBufIndex = lMyUSART->sCheckPos;
   //aVapDescr->sVDCurNumber++;
    //
    lFrmIndex = aVapDescr->sVDFIndex;
    lCurChar = lMyUSART->sInBuffer[lBufIndex];
    if (lFrmIndex < sizeof(VAP_Frame_TypeDef))
      aVapDescr->sVDRcvFBytes[lFrmIndex] = lCurChar;
    if (lFrmIndex < 4)
      if (lCurChar != aVapDescr->sVDRcvPreamb.Bytes[lFrmIndex]) {
        if (lFrmIndex > 0) lBufIndex = aVapDescr->sVDBegPos;
        lFrmIndex = -1;
      }
      else
        if (lFrmIndex == 0) {
          aVapDescr->sVDBegPos = lBufIndex;
          aVapDescr->sVDCurNumber = 0;
        }
        else;
    else
      if (lFrmIndex >= 6) {
        if (lFrmIndex == 6) aVapDescr->sVDRcvCRC = CRCBASE;
        CompIterCRC16(lCurChar, &aVapDescr->sVDRcvCRC);
/*        if (lFrmIndex == 11) { // ���� ������������ �����
          aVapDescr->sVDCurNumber = 0;
          lTmpU16 = aVapDescr->sVDRcvFrame.sVFLength ^ (~aVapDescr->sVDRcvFrame.sVFLengthInv);
          if ((aVapDescr->sVDRcvFrame.sVFLength <= lBufSize) && (lTmpU16 == 0));
          else {
            aVapDescr->sVDCurNumber = 0x7777;//((aVapDescr->sVDRcvFrame.sVFLength > lBufSize) || (lTmpU16 != 0));
            //lBufIndex = aVapDescr->sVDBegPos;
            //lFrmIndex = -1;
          }
        }

        if ((lFrmIndex == 11) &&
            ((aVapDescr->sVDRcvFrame.sVFLength > lBufSize) ||
             //(aVapDescr->sVDRcvFrame.sVFLength != ~aVapDescr->sVDRcvFrame.sVFLengthInv))) {
             ((aVapDescr->sVDRcvFrame.sVFLength ^ (~aVapDescr->sVDRcvFrame.sVFLengthInv)) ==0))){
          //   aVapDescr->sVDCurNumber = aVapDescr->sVDRcvFrame.sVFLength ^ (~aVapDescr->sVDRcvFrame.sVFLengthInv);
          aVapDescr->sVDCurNumber = 0x7777;
          //
          //lFrmIndex = -1;
        }
*/
      }
    lFrmIndex++;
    if ((lFrmIndex > 0) && (lFrmIndex == aVapDescr->sVDRcvFrame.sVFLength)) {
      if ((aVapDescr->sVDRcvFrame.sVFCRC == aVapDescr->sVDRcvCRC) &&
          ((aVapDescr->sVDRcvFrame.sVFDeviceID == aVapDescr->sVDSelfID) ||
           (aVapDescr->sVDRcvFrame.sVFDeviceID == BROADCASTDEVICE)) ) {
        // ����� ������� ������
        // ����������� �� ��������� �����
        lBlockIndex = aVapDescr->sVDBegPos + sizeof(VAP_Frame_TypeDef);
        lCount = aVapDescr->sVDRcvFrame.sVFLength - sizeof(VAP_Frame_TypeDef);
        aVapDescr->sVDTempBufCount = lCount;
        lPChar = aVapDescr->sVDTempBuffer;
        while (lCount-- > 0) {
          if (lBlockIndex >= lBufSize) lBlockIndex -= lBufSize;
          *lPChar++ = lMyUSART->sInBuffer[lBlockIndex++];
        }
        //aVapDescr->sVDCurNumber = aVapDescr->sVDRcvFrame.sVFNumber;
        //
        aVapDescr->sVDResult = 1;
        lResult = TRUE;
      }
      else { // �� ������ CRC ��� DeviceID
        lBufIndex = aVapDescr->sVDBegPos;
        //
        aVapDescr->sVDResult = 2;
        lResult = FALSE;
      }
      lFrmIndex = 0;
    }
    aVapDescr->sVDFIndex = lFrmIndex;
    if (++lBufIndex >= lBufSize) lBufIndex = 0;
    lMyUSART->sCheckPos = lBufIndex;
  }
  if (lResult) lMyUSART->sReadPos = lBufIndex; 
  return lResult;
}

void VapHandleFrame(VAP_Descr_TypeDef *aVapDescr)
{
  VAP_Block_TypeDef *lBlock;
  char *lTunnelData;
  int32_t lCount, lTunnelDataLen;
  //
  lCount = aVapDescr->sVDRcvFrame.sVFLength - sizeof(VAP_Frame_TypeDef);
  VapFrameSetup(aVapDescr);
  //
  lBlock = (VAP_Block_TypeDef*)aVapDescr->sVDTempBuffer;
  //PostLogMessage(lBlock, lCount);
  //
  while (lCount > 0) {
    switch (lBlock->sVBCommand) {
      case CMD_EVUPDATE:
        if (lBlock->sVBLength == (sizeof(VAP_Block_TypeDef) + sizeof(UINT32_U))) {
          aVapDescr->sVDViewState = *(UINT32_U*)&aVapDescr->sVDTempBuffer[sizeof(VAP_Block_TypeDef)];
          aVapDescr->sVDNewState = TRUE;
        }
        if ((aVapDescr->sVDExtData != NULL) && (*(uint32_t*)aVapDescr->sVDExtData != 0))
          VapBlockAdd(aVapDescr, CMD_EVUPDATEANS, aVapDescr->sVDExtData, aVapDescr->sVDExtDataSize);
        else
          VapBlockAdd(aVapDescr, CMD_EVUPDATEANS, NULL, 0);
        break;
        //
      case CMD_TUNNELDATA:
        lTunnelDataLen = lBlock->sVBLength - sizeof(VAP_Block_TypeDef);
        if (lTunnelDataLen > 0) {
          lTunnelData = (char*)((uint32_t)lBlock + sizeof(VAP_Block_TypeDef));
          //
          if (aVapDescr->sVDOnTunnelRead != NULL)
            aVapDescr->sVDOnTunnelRead(lTunnelData, lTunnelDataLen);
        }
        //PostLogMessage(lTunnelData, lTunnelDataLen);
        //
        //for (I=0; I < lTunnelDataLen; I++) lTunnelData[I]++;
        //
        //VapBlockAdd(aVapDescr, CMD_TUNNELDATA, lTunnelData, lTunnelDataLen);
        break;
        //
      default: VapBlockAdd(aVapDescr, CMD_UNKNOWN, NULL, 0);
    }
    lCount -= lBlock->sVBLength;
    lBlock = (void*)((uint32_t)lBlock + lBlock->sVBLength);
  }
  //
  if (aVapDescr->sVDTunnelBufCount > 0) {
    VapBlockAdd(aVapDescr, CMD_TUNNELDATA, aVapDescr->sVDTunnelBuffer, aVapDescr->sVDTunnelBufCount);
    aVapDescr->sVDTunnelBufCount = 0;
  }
  //
  VapFramePost(aVapDescr, aVapDescr->sVDRcvFrame.sVFNumber);
}

bool Vap_GetTunnelBuffer(VAP_Descr_TypeDef *aVapDescr,pvoid *aPBuffer,int *aBufSize)
{
  if (aVapDescr->sVDTunnelBuffer != NULL) {
    *aPBuffer = aVapDescr->sVDTunnelBuffer;
    *aBufSize = aVapDescr->sVDTunnelBufSize;
    return TRUE;
  }
  else return FALSE;
}

void Vap_SetTunnelBufferCount(VAP_Descr_TypeDef *aVapDescr,int aCount)
{
  if (aVapDescr->sVDTunnelBuffer != NULL)
    aVapDescr->sVDTunnelBufCount = aCount;
}

void Vap_PostTunnelData(VAP_Descr_TypeDef *aVapDescr,void *aBuffer,int aCount)
{
  if (aVapDescr->sVDTunnelBuffer != NULL) {
    if (aCount > aVapDescr->sVDTunnelBufSize) aCount = aVapDescr->sVDTunnelBufSize;
    MemCopy(aBuffer, aVapDescr->sVDTunnelBuffer, aCount);
    aVapDescr->sVDTunnelBufCount = aCount;
  }
}

//==============================================================================

void VapFrameSetup(VAP_Descr_TypeDef *aVapDescr)
{
  VAP_Frame_TypeDef *lFrame;
  //
  lFrame = (VAP_Frame_TypeDef*)aVapDescr->sVDUSART->sOutBuffer;
  lFrame->sVFPreamb = aVapDescr->sVDSndPreamb.Long;
  lFrame->sVFLength = sizeof(VAP_Frame_TypeDef);
}

void VapBlockAdd(VAP_Descr_TypeDef *aVapDescr,uint16_t aCommand,
  void *aBlock,uint16_t aBlockSize)
{
  VAP_Frame_TypeDef *lFrame;
  VAP_Block_TypeDef *lBlock;
  //
  lFrame = (VAP_Frame_TypeDef*)aVapDescr->sVDUSART->sOutBuffer;
  lBlock = (VAP_Block_TypeDef*)&aVapDescr->sVDUSART->sOutBuffer[lFrame->sVFLength];
  lBlock->sVBLength = sizeof(VAP_Block_TypeDef) + aBlockSize;
  lBlock->sVBCommand = aCommand;
  lBlock->sVBTimeStamp = TICKS;
  lBlock->sVBDeviceID = aVapDescr->sVDSelfID;
  if (aBlockSize > 0)
    MemCopy(aBlock, &aVapDescr->sVDUSART->sOutBuffer[lFrame->sVFLength+sizeof(VAP_Block_TypeDef)], aBlockSize);
  lFrame->sVFLength += lBlock->sVBLength;
}

void VapFramePost(VAP_Descr_TypeDef *aVapDescr,uint16_t aNumber)
{
  VAP_Frame_TypeDef *lFrame;
  //
  lFrame = (VAP_Frame_TypeDef*)aVapDescr->sVDUSART->sOutBuffer;
  lFrame->sVFLengthInv = ~lFrame->sVFLength;
  lFrame->sVFNumber = aNumber;
  lFrame->sVFDeviceID = aVapDescr->sVDSelfID;
  lFrame->sVFCRC = VAPCompCRC16(&lFrame->sVFLength, lFrame->sVFLength-6);
  //
  MyUSART_Write(aVapDescr->sVDUSART, lFrame, lFrame->sVFLength);
}
/*
bool VapPostFrame(VAP_Descr_TypeDef *aVapDescr,uint16_t aCommand,
  void *aBlock,uint16_t aBlockSize)
{
  char *lOutBuf;
  VAP_Frame_TypeDef *lFramePtr;
  VAP_Block_TypeDef *lBlockPtr;
  uint16_t lHeaderLen;
  //
  lOutBuf = aVapDescr->sVDUSART->sOutBuffer; //(char*)&lTmpBuf;
  lHeaderLen = sizeof(VAP_Frame_TypeDef) + sizeof(VAP_Block_TypeDef);
  lFramePtr = (VAP_Frame_TypeDef*)lOutBuf;
  lFramePtr->sVFPreamb = aVapDescr->sVDSndPreamb.Long;
  lFramePtr->sVFLength = lHeaderLen + aBlockSize;
  lFramePtr->sVFLengthInv = ~lFramePtr->sVFLength;
  lFramePtr->sVFNumber = aVapDescr->sVDRcvFrame.sVFNumber;
  lFramePtr->sVFDeviceID = aVapDescr->sVDSelfID;
  //
  lBlockPtr = (VAP_Block_TypeDef*)&lOutBuf[sizeof(VAP_Frame_TypeDef)];
  lBlockPtr->sVBLength = sizeof(VAP_Block_TypeDef) + aBlockSize;
  lBlockPtr->sVBCommand = aCommand; //aVapDescr->sVDResult;
  lBlockPtr->sVBTimeStamp = TICKS;
  lBlockPtr->sVBDeviceID = aVapDescr->sVDSelfID;
  if (aBlockSize > 0)
    MemCopy(aBlock, &lOutBuf[lHeaderLen], aBlockSize);
  //
  lFramePtr->sVFCRC = VAPCompCRC16(&lFramePtr->sVFLength, lFramePtr->sVFLength-6);
  //
  MyUSART_Write(aVapDescr->sVDUSART, lOutBuf, lFramePtr->sVFLength); //NULL, lFramePtr->sVFLength);
  //
  return FALSE;
}
*/
