// ���������� ���������� ������� ��� STM32F4XX
// � Olgvel, 2012-2018
//
#include "my_stm32.h"

//#define RTC_BKP_DR_NUMBER     0x14
#define RTC_MAGICDATA           0x32F2
#define STARTCOUNTER_BKP_INDEX  0x13
#define BACKUP_MEM_ADDRESS      0x40024000
#define BACKUP_MEM_SIZE         0x00001000

RTC_InitTypeDef   RTC_InitStructure;
RTC_TimeTypeDef   RTC_TimeStructure;
RTC_DateTypeDef   RTC_DateStructure;

uint32_t NamedDataBase;
uint32_t NamedDataSize;
uint32_t gBkpMemAllocPos = BACKUP_MEM_ADDRESS;

//__IO uint32_t AsynchPrediv = 0, SynchPrediv = 0;

bool     InitBackup;
  //__IO uint32_t TimeDisplay = 0;
  //uint32_t errorindex = 0, i = 0;
void Backup_Config(void);
void RTC_Config(void);
void RTC_TimeRegulate(void); 
void RNG_Config(void);
bool AssignOldCfgBlock(uint32_t aAddr);

void MySys_Init(void)
{
  RCC_ClocksTypeDef RCC_Clocks;
  uint32_t lAppAddr;
  //
  AssignVTORLength(VTORLENGTH);
  lAppAddr = DefaultAppAddr(TRUE);
  if (Arr32NEqual((void*)BASE_APPADDR, (void*)lAppAddr, VTORWORDS) < 0)
    AssignAppVTOR(lAppAddr);     //VTOR_Setup(HI_APPADDR);
  else {
    lAppAddr = DefaultAppAddr(FALSE);
    if (Arr32NEqual((void*)BASE_APPADDR, (void*)lAppAddr, VTORWORDS) < 0)
      AssignAppVTOR(lAppAddr);   //VTOR_Setup(LO_APPADDR);
    else 
      if (Arr32NFill((void*)lAppAddr, EMPTY32, VTORWORDS) < 0) {
        FLASH_Write(lAppAddr, (void*)BASE_APPADDR, VTORLENGTH);
        AssignAppVTOR(lAppAddr); //VTOR_Setup(LO_APPADDR);
      }
  }
  if (!AssignCfgBlock(NULL)) AssignOldCfgBlock(NULL);
  //
  FLASH_SetReadProtection();
  // ������������� ���������� �������
  RCC_GetClocksFreq(&RCC_Clocks);
  //
  SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);
  // Configure the Priority Group to 2 bits
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  IRQ_Init(SysTick_IRQn);
  //
  //RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);
  RNG_Config();
  //
  if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET) {
    // IWDGRST flag set
    // Clear reset flags
    RCC_ClearFlag();
    SetWDGStart(TRUE);
  }
  else SetWDGStart(FALSE);
  //
  RTC_Initialize();
  //
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
  // IWDG counter clock: 32KHz(LSI) / 32 = 1KHz
  IWDG_SetPrescaler(IWDG_Prescaler_32);
  // Set counter reload value to 2000 //349
  IWDG_SetReload(10000); //4000
  // Reload IWDG counter
  IWDG_ReloadCounter();
  // Enable IWDG (the LSI oscillator will be enabled by hardware)
  IWDG_Enable();
}

void RTC_Initialize(void)
{
  // Enable the PWR APB1 Clock Interface
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
  // Allow access to BKP Domain
  PWR_BackupAccessCmd(ENABLE);
  //
  InitBackup = (RTC_ReadBackupRegister(RTC_BKP_DR0) != RTC_MAGICDATA);

  //RTC_Config();
  Backup_Config();

  if (InitBackup) {
    // RTC Configuration
    //*!*/ PostLogText("RTC_Config\r\n");
    //RTC_Config();
    // Configure the time&date register 
    //*!*/ PostLogText("RTC_TimeRegulate\r\n");
    //RTC_TimeRegulate();
    //
    // Write to the first RTC Backup Data Register
    RTC_WriteBackupRegister(RTC_BKP_DR0, RTC_MAGICDATA);
    WriteBackupParam(7, 0);
  }
  else  {
    //*!*/ PostLogText("RTC was configured\r\n");
    // Wait for RTC APB registers synchronisation
//    RTC_WaitForSynchro();
    //RTC_ClearITPendingBit(RTC_IT_WUT);
    //EXTI_ClearITPendingBit(EXTI_Line22);
    // Backup SRAM ************************************************************
    // Enable BKPSRAM Clock
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_BKPSRAM, ENABLE);
  }
  //
  // IWDG timeout equal to 350ms (the timeout may varies due to LSI frequency
  // dispersion) -------------------------------------------------------------
  // Enable write access to IWDG_PR and IWDG_RLR registers    
}  

void Backup_Config(void)
{
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_BKPSRAM, ENABLE);
  //
  // Enable the Backup SRAM low power Regulator to retain it's content in VBAT mode
  PWR_BackupRegulatorCmd(ENABLE);
  //
  // Wait until the Backup SRAM low power Regulator is ready
  while(PWR_GetFlagStatus(PWR_FLAG_BRR) == RESET);    
}

void RTC_Config(void)
{
  // Enable the LSE OSC
  //*!*/ PostLogText(" 1");
  //RCC_LSEConfig(RCC_LSE_ON);
  // Wait till LSE is ready
  //*!*/ PostLogText(" 2");
  //while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET);   // !!!!!!!!!!!!!!!!!!
  // Select the RTC Clock Source
  //*!*/ PostLogText(" 3");
  //RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
  //
  //*!*/ PostLogText(" 1");
  RCC_LSICmd(ENABLE);
  //*!*/ PostLogText(" 2");
  // Wait till LSI is ready   
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);   //  
  //*!*/ PostLogText(" 3");
  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
  //
  //
  //*!*/ PostLogText(" 4");
  // Calendar Configuration
  RTC_InitStructure.RTC_AsynchPrediv = 0x7F; //AsynchPrediv;
  RTC_InitStructure.RTC_SynchPrediv =  0xFF; //SynchPrediv;
  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
  RTC_Init(&RTC_InitStructure);
  //
  //*!*/ PostLogText(" 5");
  // Enable the RTC Clock
  RCC_RTCCLKCmd(ENABLE);
  //
  //*!*/ PostLogText(" 6");
  // Wait for RTC APB registers synchronisation
  RTC_WaitForSynchro();
  //
  //*!*/ PostLogText(" 7");  
  // Enable The TimeStamp 
  RTC_TimeStampCmd(RTC_TimeStampEdge_Falling, ENABLE);
  //
  //  Backup SRAM **************************************************************
  //*!*/ PostLogText(" 8");  
  // Enable BKPRAM Clock
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_BKPSRAM, ENABLE);
  //
  //*!*/ PostLogText(" 9");  
  // Enable the Backup SRAM low power Regulator to retain it's content in VBAT mode
  PWR_BackupRegulatorCmd(ENABLE);
  //
  //*!*/ PostLogText(" 10");  
  // Wait until the Backup SRAM low power Regulator is ready
  while(PWR_GetFlagStatus(PWR_FLAG_BRR) == RESET);
  // RTC Backup Data Registers *************************************************
  // Write to RTC Backup Data Registers
  //WriteToBackupReg(FIRST_DATA);
  //*!*/ PostLogText("\r\n");  
}

void RTC_TimeRegulate(void)
{
  //*!*/ PostLogText(" 1");
  // Set the Time
  RTC_TimeStructure.RTC_Hours   = 0x08;
  RTC_TimeStructure.RTC_Minutes = 0x00;
  RTC_TimeStructure.RTC_Seconds = 0x00;
  RTC_SetTime(RTC_Format_BCD, &RTC_TimeStructure);  
  
  //*!*/ PostLogText(" 2");
  // Set the Date
  RTC_DateStructure.RTC_Month = RTC_Month_November;
  RTC_DateStructure.RTC_Date = 0x16;
  RTC_DateStructure.RTC_Year = 0x18;
  RTC_DateStructure.RTC_WeekDay = RTC_Weekday_Friday;
  RTC_SetDate(RTC_Format_BCD, &RTC_DateStructure);
  //
  //*!*/ PostLogText("\r\n");  
}

void GetTimeStamp(UINT64_U *aTimeStamp)
{
    RTC_GetTime(RTC_Format_BCD, (RTC_TimeTypeDef*)&aTimeStamp->Longs[0]);
    RTC_GetDate(RTC_Format_BCD, (RTC_DateTypeDef*)&aTimeStamp->Longs[1]);   
    //RTC_GetTimeStamp(RTC_Format_BCD, (RTC_TimeTypeDef*)&aTimeStamp->Longs[0], (RTC_DateTypeDef*)&aTimeStamp->Longs[1]);
}

void RNG_Config(void)
{
  // Enable RNG clock source
  RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);
  // RNG Peripheral enable
  RNG_Cmd(ENABLE);
}

void PIN_Config(const PIN_HWCFG_TypeDef *aConfig)
{
  //if ((aConfig->sPIN_MODE == GPIO_Mode_OUT) || (aConfig->sPIN_MODE == GPIO_Mode_IN))
  if (aConfig == NULL) return;
  if (aConfig->sPIN_PORT == NULL) return;
  // Enable the GPIO Clock
  RCC_AHB1PeriphClockCmd(aConfig->sPIN_CLK, ENABLE);
  //
  if (aConfig->sInitStruc.GPIO_Mode == GPIO_Mode_AF)
    // Connect PXx to ...
    GPIO_PinAFConfig(aConfig->sPIN_PORT, aConfig->sPIN_SOURCE, aConfig->sPIN_AF);
  // Configure the GPIO
  GPIO_Init(aConfig->sPIN_PORT, (GPIO_InitTypeDef*)&aConfig->sInitStruc);
}

void TIM_TRG_Config(const TIM_HWCFG_TypeDef *aConfig)
{
  RCC_APB1PeriphClockCmd(aConfig->sCLK, ENABLE);
  //
  TIM_TimeBaseInit(aConfig->sTIM, (TIM_TimeBaseInitTypeDef*)&aConfig->sInitStruc);
  //
  TIM_SelectOutputTrigger(aConfig->sTIM, TIM_TRGOSource_Update);
  //
  TIM_Cmd(aConfig->sTIM, ENABLE);
}

//DAC_InitTypeDef  DAC_InitStructure;
#if defined(STM32F40_41xxx) 
void DAC_Config(const DAC_HWCFG_TypeDef *aConfig)
{
  RCC_APB1PeriphClockCmd(aConfig->sCLK, ENABLE);
  //
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
  //
  PIN_Config(&aConfig->sPIN);
  //
  TIM_TRG_Config(&aConfig->sTIM);
  //
  //DAC_DeInit();
  //
  DAC_Init(aConfig->sDACChan, (DAC_InitTypeDef*)&aConfig->sInitStruc);

/*
  DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;

  DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_Triangle;
  DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_TriangleAmplitude_4095;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
  //
  DAC_Init(aConfig->sDACChan, &DAC_InitStructure);
*/
}

void DAC_TriangleStart(const DAC_HWCFG_TypeDef *aConfig)
{
  // Enable DAC Channel
  DAC_Cmd(aConfig->sDACChan, ENABLE);
  // Set DAC channel2 DHR12RD register
  DAC_SetChannel2Data(DAC_Align_12b_R, 0x2000); // 0x100);
}

void DAC_Start(const DAC_HWCFG_TypeDef *aConfig,void *aBuffer,uint32_t aSize)
{
/*
  DMA_InitTypeDef DMA_InitStructure;
  //
  // DMA1_StreamX channelY configuration
  DMA_DeInit(aConfig->sDMA_STREAM);
  DMA_InitStructure.DMA_Channel = aConfig->sDMA_CHAN;
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)aConfig->sDACAddr;//DAC_DHR12R2_ADDRESS;
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)aBuffer;
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
  DMA_InitStructure.DMA_BufferSize = aSize;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(aConfig->sDMA_STREAM, &DMA_InitStructure);
  // Enable DMA1_StreamX
  DMA_Cmd(aConfig->sDMA_STREAM, ENABLE);
  // Enable DAC ChannelY
  DAC_Cmd(aConfig->sDACChan, ENABLE);
  // Enable DMA for DAC Channel
  DAC_DMACmd(aConfig->sDACChan, ENABLE);
*/
  // Enable DAC Channel2
//  DAC_DMACmd(aConfig->sDACChan, ENABLE);
  DAC_Cmd(aConfig->sDACChan, ENABLE);
//  DAC_Cmd(DAC_Channel_2, ENABLE);
  // Set DAC channel2 DHR12RD register
  DAC_SetChannel2Data(DAC_Align_12b_R, 0x2000); // 0x100);
}

void DAC_TriangleStop(const DAC_HWCFG_TypeDef *aConfig)
{
  DAC_Cmd(aConfig->sDACChan, DISABLE);
}

void DAC_Stop(const DAC_HWCFG_TypeDef *aConfig)
{
  DAC_DMACmd(aConfig->sDACChan, DISABLE);
  //
  DAC_Cmd(aConfig->sDACChan, DISABLE);
  //
  DMA_Cmd(aConfig->sDMA_STREAM, DISABLE);
}
#endif /* STM32F40_41xxx */

//------------------------------------------------------------------------------
volatile uint16_t ADCConvertedValue[13];  // <- the results are here

#define ADC_SampleTime_Cycles ADC_SampleTime_480Cycles
void ADC_Config(void)
{
  ADC_InitTypeDef ADC_InitStruct;
  ADC_CommonInitTypeDef ADC_CommonInitStruct;
  DMA_InitTypeDef DMA_InitStructure;
  //
  // ports are already configured
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);          // enable clocking of ADC1
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);          // enable clocking of DMA2
  //== Configure DMA2 - Channel0 Stream 4 ==
  DMA_InitStructure.DMA_Channel = DMA_Channel_0;
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&ADCConvertedValue;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
  DMA_InitStructure.DMA_BufferSize = 13;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA2_Stream4, &DMA_InitStructure);
  DMA_Cmd( DMA2_Stream4, ENABLE);
  //
  // ADC:
  ADC_DeInit();     // turn ADC off
  //
  ADC_CommonInitStruct.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStruct.ADC_Prescaler = ADC_Prescaler_Div4;
  ADC_CommonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
  ADC_CommonInit(&ADC_CommonInitStruct);
  //
  ADC_StructInit(&ADC_InitStruct);
  ADC_InitStruct.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStruct.ADC_ScanConvMode = ENABLE;
  ADC_InitStruct.ADC_ContinuousConvMode = ENABLE;
  ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;//Left;
  ADC_InitStruct.ADC_NbrOfConversion = 13;
  ADC_Init(ADC1, &ADC_InitStruct);
  //----channels_order
  ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1, ADC_SampleTime_Cycles);
  ADC_RegularChannelConfig(ADC1, ADC_Channel_Vbat, 2, ADC_SampleTime_Cycles); //ADC_Channel_Vrefint
  ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 3, ADC_SampleTime_Cycles); // PC0
  ADC_RegularChannelConfig(ADC1, ADC_Channel_6 , 4, ADC_SampleTime_Cycles); // PA6
  ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 5, ADC_SampleTime_Cycles); // PC2
  ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 6, ADC_SampleTime_Cycles); // PC3
  ADC_RegularChannelConfig(ADC1, ADC_Channel_1,  7, ADC_SampleTime_Cycles); // PA1
  ADC_RegularChannelConfig(ADC1, ADC_Channel_0,  8, ADC_SampleTime_Cycles); // PA0
  ADC_RegularChannelConfig(ADC1, ADC_Channel_7,  9, ADC_SampleTime_Cycles); // PA7
  ADC_RegularChannelConfig(ADC1, ADC_Channel_14,10, ADC_SampleTime_Cycles); // PC4
  ADC_RegularChannelConfig(ADC1, ADC_Channel_15,11, ADC_SampleTime_Cycles); // PC5
  ADC_RegularChannelConfig(ADC1, ADC_Channel_8 ,12, ADC_SampleTime_Cycles); // PB0
  ADC_RegularChannelConfig(ADC1, ADC_Channel_11,13, ADC_SampleTime_Cycles); // PC1
  //
  ADC_TempSensorVrefintCmd(ENABLE);
  ADC_VBATCmd(ENABLE);
  //
  // enable ADC and DMA:
  ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);
  //
  ADC_DMACmd(ADC1, ENABLE);    // Enable ADC1 DMA
  ADC_Cmd(ADC1, ENABLE);       // Enable ADC1
  //
  ADC_SoftwareStartConv(ADC1); // Start ADC conversions
}

void ADC_ShowLevels(void)
{
  char lText[8];
  uint16_t *lValue;
  PostLogText(" ADC_Levels: ");
  lValue = (uint16_t*)&ADCConvertedValue[4];
  for (int I=4;I<13;I++) {
    PostLogText(Int2StrZ(*lValue++ >> 4, lText, 6));
    PostLogText("  ");
  }
  PostLogText(S_CRLF);
}

void ADC_ShowMeasures(void)
{
  char lText[8];
  uint16_t *lValue;
  PostLogText(" ADC_Measures: ");
  lValue = (uint16_t*)&ADCConvertedValue;
  for (int I=0;I<4;I++) {
    PostLogText(Int2StrZ(*lValue++ >> 4, lText, 6));
    PostLogText("  ");
  }
  PostLogText(S_CRLF);
}

void ADC_GetBuffer(uint16_t **aBuffPtr)
{
  if assigned(aBuffPtr) *aBuffPtr = (uint16_t*)&ADCConvertedValue;
}

/* ------------------------------------------------------------------------------
MY_USART_TypeDef* MyUSART_InitEx(MY_USART_TypeDef *aMyUSART,const USART_HWCFG_TypeDef *aConfig,uint32_t aBaudRate)
{
  USART_InitTypeDef USART_InitStructure;
  DMA_InitTypeDef DMA_InitStructure;
  //
  if (aMyUSART == NULL)
    aMyUSART = FixMemAlloc( sizeof(MY_USART_TypeDef) );
  //
  MemClear(aMyUSART, sizeof(MY_USART_TypeDef));
  aMyUSART->sHWCFG = aConfig;
  aMyUSART->sInBuffer = FixMemAlloc( aConfig->sRXBufSize );
  aMyUSART->sInBufSize = aConfig->sRXBufSize;
  aMyUSART->sOutBuffer = FixMemAlloc( aConfig->sTXBufSize );
  aMyUSART->sOutBufSize = aConfig->sTXBufSize;
  //
  if (aConfig->sDIR.sPIN_PORT != NULL) {
    PIN_Config(&aConfig->sDIR);
    PIN_Set(&aConfig->sDIR, FALSE);
  }
  //
  // Enable USART clock and release reset.
  if ((aConfig->sUSART == USART1) || (aConfig->sUSART == USART6)) {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
    RCC_APB2PeriphClockCmd(aConfig->sUSART_CLK, ENABLE);
    RCC_APB2PeriphResetCmd(aConfig->sUSART_CLK, DISABLE);
  }
  else {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
    RCC_APB1PeriphClockCmd(aConfig->sUSART_CLK, ENABLE);
    RCC_APB1PeriphResetCmd(aConfig->sUSART_CLK, DISABLE);
  }
  PIN_Config(&aConfig->sTX);
  PIN_Config(&aConfig->sRX);
  // Configure the USART
  USART_DeInit(aConfig->sUSART);
  //
  //USART_InitStructure.USART_BaudRate = aBaudRate;
  //USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  //USART_InitStructure.USART_StopBits = USART_StopBits_1;
  //USART_InitStructure.USART_Parity = USART_Parity_No;
  //USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  //USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  //
  USART_InitStructure = aConfig->sInitStruc;
  if IS_USART_BAUDRATE(aBaudRate)
    USART_InitStructure.USART_BaudRate = aBaudRate;
  USART_Init(aConfig->sUSART, &USART_InitStructure);
  //
  USART_ClearFlag(aConfig->sUSART, USART_FLAG_CTS | USART_FLAG_LBD  |
                        USART_FLAG_TC  | USART_FLAG_RXNE);
  //
  if (aConfig->sDMA_STREAM_RX != 0) {
    // Enable USART DMA RX request
    USART_DMACmd(aConfig->sUSART,  USART_DMAReq_Rx, ENABLE);
    //
    DMA_DeInit(aConfig->sDMA_STREAM_RX);
    MemClear(&DMA_InitStructure, sizeof(DMA_InitTypeDef));
    DMA_InitStructure.DMA_Channel = aConfig->sDMA_CHAN_RX;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&aConfig->sUSART->DR;
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)aMyUSART->sInBuffer;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_BufferSize = aMyUSART->sInBufSize;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    //DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    //DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
    //DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    //DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    //
    DMA_Init(aConfig->sDMA_STREAM_RX, &DMA_InitStructure);
    DMA_Cmd(aConfig->sDMA_STREAM_RX, ENABLE);
  }
  if (aConfig->sDMA_STREAM_TX != 0) {
    aMyUSART->sFlags |= USE_DMA_TX;
    // Enable USART DMA TX request
    USART_DMACmd(aConfig->sUSART,  USART_DMAReq_Tx, ENABLE);
    IRQ_Init(aConfig->sDMA_TX_IRQn);
  }
  //
  IRQ_Init(aConfig->sUSART_IRQn);
  //
  USART_Cmd(aConfig->sUSART, ENABLE);
  return aMyUSART;
}

bool MyUSART_StartSend(MY_USART_TypeDef *aMyUSART)
{
  DMA_InitTypeDef DMA_InitStructure;
  const USART_HWCFG_TypeDef *lConfig;
  uint16_t lWritePos, lSendPos;
  int lCount = 0;
  //
  lWritePos = aMyUSART->sWritePos;
  lSendPos = aMyUSART->sSendPos;
  if ((lWritePos == lSendPos) || ((aMyUSART->sFlags & IN_DMA_TX) != 0)) return FALSE;
  //
  lConfig = aMyUSART->sHWCFG;
  if (lConfig->sDIR.sPIN_PORT != NULL) {
    PIN_Set(&lConfig->sDIR, TRUE);
    aMyUSART->sFlags |= TX_PIN_SET;
    //
    USART_ClearFlag(lConfig->sUSART, USART_FLAG_TC);
    if (lConfig->sUSART_IRQn != 0)
      USART_ITConfig(lConfig->sUSART, USART_IT_TC, ENABLE);
  }
  //
  if (lConfig->sDMA_STREAM_TX != 0) {
    if (lWritePos > lSendPos) {
      lCount = lWritePos - lSendPos;
      aMyUSART->sSendPos += lCount;
    }
    else {
      lCount = aMyUSART->sOutBufSize - lSendPos;
      aMyUSART->sSendPos = 0;
    }
    DMA_DeInit(lConfig->sDMA_STREAM_TX);
    MemClear(&DMA_InitStructure, sizeof(DMA_InitTypeDef));
    DMA_InitStructure.DMA_Channel = lConfig->sDMA_CHAN_TX;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&lConfig->sUSART->DR;
    //
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)aMyUSART->sOutBuffer +
      aMyUSART->sSendingPos;
    //
    DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
    DMA_InitStructure.DMA_BufferSize = lCount;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    //
    DMA_Init(lConfig->sDMA_STREAM_TX, &DMA_InitStructure);
    aMyUSART->sFlags |= IN_DMA_TX;
    //
    if (lConfig->sDMA_TX_IRQn != 0)
      DMA_ITConfig(lConfig->sDMA_STREAM_TX, DMA_IT_TC, ENABLE);
    DMA_Cmd(lConfig->sDMA_STREAM_TX, ENABLE);
  }
  else
    if (lConfig->sUSART_IRQn != 0) {
      aMyUSART->sFlags |= IN_IRQ_TX;
      USART_ITConfig(lConfig->sUSART, USART_IT_TXE, ENABLE);
    }
  return TRUE;
}

void MyUSART_DMA_TX_IRQHandler(MY_USART_TypeDef *aMyUSART)
{
  // Test on DMA Stream Transfer Complete interrupt
  if (DMA_GetITStatus(aMyUSART->sHWCFG->sDMA_STREAM_TX,
     aMyUSART->sHWCFG->sDMA_IT_TC)) {
    // Clear DMA Stream Transfer Complete interrupt pending bit
    DMA_ClearITPendingBit(aMyUSART->sHWCFG->sDMA_STREAM_TX,
      aMyUSART->sHWCFG->sDMA_IT_TC);
    aMyUSART->sFlags &= ~IN_DMA_TX;
    aMyUSART->sRsv2++;
    aMyUSART->sSendingPos = aMyUSART->sSendPos;
    MyUSART_StartSend(aMyUSART);
  }
}

void MyUSART_IRQHandler(MY_USART_TypeDef *aMyUSART)
{
  // USART in mode Transmitter -------------------------------------------------
  if (USART_GetITStatus(aMyUSART->sHWCFG->sUSART, USART_IT_TXE) == SET) {
    aMyUSART->sRsv1++;
    USART_SendData(aMyUSART->sHWCFG->sUSART, aMyUSART->sOutBuffer[aMyUSART->sSendingPos++]);
    if (aMyUSART->sSendingPos == aMyUSART->sOutBufSize) aMyUSART->sSendingPos = 0;
    aMyUSART->sSendPos = aMyUSART->sSendingPos;
    if (aMyUSART->sSendingPos == aMyUSART->sWritePos) {
      // Disable the USARTx transmit data register empty interrupt
      USART_ITConfig(aMyUSART->sHWCFG->sUSART, USART_IT_TXE, DISABLE);
      aMyUSART->sFlags &= ~IN_IRQ_TX;
    }
  }
  // USART in mode Receiver ----------------------------------------------------
  if (USART_GetITStatus(aMyUSART->sHWCFG->sUSART, USART_IT_RXNE) == SET) {
    //aMyUSART->sRsv2++;
    aMyUSART->sInBuffer[aMyUSART->sReceivePos++] = USART_ReceiveData(aMyUSART->sHWCFG->sUSART);
    if (aMyUSART->sReceivePos >= aMyUSART->sInBufSize) aMyUSART->sReceivePos = 0;
    aMyUSART->sReceiveTicks = TICKS;
  }
  if (USART_GetITStatus(aMyUSART->sHWCFG->sUSART, USART_IT_TC) == SET) {
    aMyUSART->sRsv3++;
    PIN_Set(&aMyUSART->sHWCFG->sDIR, FALSE);
    aMyUSART->sFlags &= ~TX_PIN_SET;
    aMyUSART->sSendTicks = TICKS;
    USART_ITConfig(aMyUSART->sHWCFG->sUSART, USART_IT_TC, DISABLE);
  }
}

void MyUSART_ReturnDirByTC(MY_USART_TypeDef *aMyUSART)
{
  if ((aMyUSART->sHWCFG->sDIR.sPIN_PORT != NULL) && ((aMyUSART->sFlags & TX_PIN_SET) != 0) &&
    (USART_GetFlagStatus(aMyUSART->sHWCFG->sUSART, USART_FLAG_TC) != RESET)) {
    //USART_ClearFlag(aMyUSART->sUSART, USART_FLAG_TC);
    PIN_Set(&aMyUSART->sHWCFG->sDIR, FALSE);
    aMyUSART->sFlags &= ~TX_PIN_SET;
  }
  // Wait of DMA completion
  //while(DMA_GetFlagStatus(DMA1_FLAG_TC4) == RESET);
}

void MyUSART_UpdateReceivePos(MY_USART_TypeDef *aMyUSART)
{
  int lIndex;
  lIndex = aMyUSART->sInBufSize - DMA_GetCurrDataCounter(aMyUSART->sHWCFG->sDMA_STREAM_RX);
  if (lIndex != aMyUSART->sReceivePos) {
    aMyUSART->sReceiveTicks = TICKS;
    aMyUSART->sReceivePos = lIndex;
  }
}
*/
//==============================================================================
void MyUART_Initialize(MY_UART_TypeDef *aDescr)
{
  const USART_HWCFG_TypeDef *lConfig;
  USART_InitTypeDef USART_InitStruc;
  DMA_InitTypeDef DMA_InitStruc;
  //
  lConfig = aDescr->sHWCFG;
  if (lConfig->sDIR.sPIN_PORT != NULL) {
    PIN_Config(&lConfig->sDIR);
    PIN_Set(&lConfig->sDIR, FALSE);
  }
  // Enable USART clock and release reset.
  if ((lConfig->sUSART == USART1) || (lConfig->sUSART == USART6)) {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
    RCC_APB2PeriphClockCmd(lConfig->sUSART_CLK, ENABLE);
    RCC_APB2PeriphResetCmd(lConfig->sUSART_CLK, DISABLE);
  }
  else {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
    RCC_APB1PeriphClockCmd(lConfig->sUSART_CLK, ENABLE);
    RCC_APB1PeriphResetCmd(lConfig->sUSART_CLK, DISABLE);
  }
  PIN_Config(&lConfig->sTX);
  if (lConfig->sRX.sPIN_PORT != NULL) PIN_Config(&lConfig->sRX);
  // Configure the USART
  USART_DeInit(lConfig->sUSART);
  //
  USART_InitStruc = lConfig->sInitStruc;
  if IS_USART_BAUDRATE(aDescr->sBaudRate)
    USART_InitStruc.USART_BaudRate = aDescr->sBaudRate;
  USART_Init(lConfig->sUSART, &USART_InitStruc);
  //
  USART_ClearFlag(lConfig->sUSART, USART_FLAG_CTS | USART_FLAG_LBD  |
                        USART_FLAG_TC  | USART_FLAG_RXNE);
  //
  if (lConfig->sDMA_STREAM_RX != 0) {
    // Enable USART DMA RX request
    USART_DMACmd(lConfig->sUSART,  USART_DMAReq_Rx, ENABLE);
    //
    DMA_DeInit(lConfig->sDMA_STREAM_RX);
    MemClear(&DMA_InitStruc, sizeof(DMA_InitTypeDef));
    DMA_InitStruc.DMA_Channel = lConfig->sDMA_CHAN_RX;
    DMA_InitStruc.DMA_PeripheralBaseAddr = (uint32_t)&lConfig->sUSART->DR;
    DMA_InitStruc.DMA_Memory0BaseAddr = (uint32_t)aDescr->sLRx->sCBuffer;
    DMA_InitStruc.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStruc.DMA_BufferSize = aDescr->sLRx->sCBufSize;
    DMA_InitStruc.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStruc.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStruc.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStruc.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStruc.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStruc.DMA_Priority = DMA_Priority_VeryHigh;
    //
    DMA_Init(lConfig->sDMA_STREAM_RX, &DMA_InitStruc);
    DMA_Cmd(lConfig->sDMA_STREAM_RX, ENABLE);
  }
  if (lConfig->sDMA_STREAM_TX != 0) {
    aDescr->sLFlags |= USE_DMA_TX;
    // Enable USART DMA TX request
    USART_DMACmd(lConfig->sUSART,  USART_DMAReq_Tx, ENABLE);
    IRQ_Init(lConfig->sDMA_TX_IRQn);
  }
  //
  IRQ_Init(lConfig->sUSART_IRQn);
  //
  if (lConfig->sRX.sPIN_PORT == NULL)
    USART_HalfDuplexCmd(lConfig->sUSART, ENABLE);
  //
  USART_Cmd(lConfig->sUSART, ENABLE);
}

bool MyUART_StartSend(MY_UART_TypeDef *aDescr)
{
  const USART_HWCFG_TypeDef *lConfig;
  MY_CBUF_TypeDef *lTx;
  DMA_InitTypeDef DMA_InitStruc;
  uint16_t lAddPos, lDelPos;
  int lCount = 0;
  //
  lTx = aDescr->sLTx;
  lAddPos = lTx->sCAddPos;
  lDelPos = lTx->sCDelPos;
  //if ((lWritePos == lSendPos) || ((aDescr->sFlags & IN_DMA_TX) != 0)) return FALSE;
  if ((lAddPos == lDelPos) || ((aDescr->sLFlags & IN_DMA_TX) != 0)) return FALSE;
  //
  lConfig = aDescr->sHWCFG;
  if (lConfig->sDIR.sPIN_PORT != NULL) {
    PIN_Set(&lConfig->sDIR, TRUE);
    aDescr->sLFlags |= TX_PIN_SET;
    //
    USART_ClearFlag(lConfig->sUSART, USART_FLAG_TC);
    if (lConfig->sUSART_IRQn != 0)
      USART_ITConfig(lConfig->sUSART, USART_IT_TC, ENABLE);
  }
  //
  if (lConfig->sDMA_STREAM_TX != 0) {
    if (lAddPos > lDelPos) {
      lCount = lAddPos - lDelPos;
      lTx->sCChkPos = lAddPos;
    }
    else {
      lCount = lTx->sCBufSize - lDelPos;
      lTx->sCChkPos = 0;
    }
    DMA_DeInit(lConfig->sDMA_STREAM_TX);
    MemClear(&DMA_InitStruc, sizeof(DMA_InitTypeDef));
    DMA_InitStruc.DMA_Channel = lConfig->sDMA_CHAN_TX;
    DMA_InitStruc.DMA_PeripheralBaseAddr = (uint32_t)&lConfig->sUSART->DR;
    //
    DMA_InitStruc.DMA_Memory0BaseAddr = (uint32_t)lTx->sCBuffer + lDelPos;
    //
    DMA_InitStruc.DMA_DIR = DMA_DIR_MemoryToPeripheral;
    DMA_InitStruc.DMA_BufferSize = lCount;
    DMA_InitStruc.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStruc.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStruc.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStruc.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStruc.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStruc.DMA_Priority = DMA_Priority_VeryHigh;
    //
    DMA_Init(lConfig->sDMA_STREAM_TX, &DMA_InitStruc);
    aDescr->sLFlags |= IN_DMA_TX;
    //
    if (lConfig->sDMA_TX_IRQn != 0)
      DMA_ITConfig(lConfig->sDMA_STREAM_TX, DMA_IT_TC, ENABLE);
    DMA_Cmd(lConfig->sDMA_STREAM_TX, ENABLE);
  }
  else
    if (lConfig->sUSART_IRQn != 0) {
      aDescr->sLFlags |= IN_IRQ_TX;
      USART_ITConfig(lConfig->sUSART, USART_IT_TXE, ENABLE);
    }
  return TRUE;
}

void MyUART_DMA_TX_IRQHandler(MY_UART_TypeDef *aDescr)
{
  // Test on DMA Stream Transfer Complete interrupt
  if (DMA_GetITStatus(aDescr->sHWCFG->sDMA_STREAM_TX,
     aDescr->sHWCFG->sDMA_IT_TC)) {
    // Clear DMA Stream Transfer Complete interrupt pending bit
    DMA_ClearITPendingBit(aDescr->sHWCFG->sDMA_STREAM_TX,
      aDescr->sHWCFG->sDMA_IT_TC);
    aDescr->sRsv2++;
    aDescr->sLTx->sCDelPos = aDescr->sLTx->sCChkPos;
    aDescr->sLFlags &= ~IN_DMA_TX;
    MyUART_StartSend(aDescr);
  }
}

void MyUART_IRQHandler(MY_UART_TypeDef *aDescr)
{
  MY_CBUF_TypeDef *lCb;
  int16_t lAddPos;
  // USART in mode Transmitter -------------------------------------------------
  if (USART_GetITStatus(aDescr->sHWCFG->sUSART, USART_IT_TXE) == SET) {
    lCb = aDescr->sLTx; aDescr->sRsv1++;
    USART_SendData(aDescr->sHWCFG->sUSART, lCb->sCBuffer[lCb->sCDelPos++]);
    if (lCb->sCDelPos == lCb->sCBufSize) lCb->sCDelPos = 0;
    lAddPos = lCb->sCAddPos;
    if (lCb->sCDelPos == lAddPos) {
      // Disable the USARTx transmit data register empty interrupt
      USART_ITConfig(aDescr->sHWCFG->sUSART, USART_IT_TXE, DISABLE);
      aDescr->sLFlags &= ~IN_IRQ_TX;
    }
  }
  // USART in mode Receiver ----------------------------------------------------
  if (USART_GetITStatus(aDescr->sHWCFG->sUSART, USART_IT_RXNE) == SET) {
    lCb = aDescr->sLRx;//aMyUSART->sRsv2++;
    lCb->sCBuffer[lCb->sCAddPos++] = USART_ReceiveData(aDescr->sHWCFG->sUSART);
    if (lCb->sCAddPos == lCb->sCBufSize) lCb->sCAddPos = 0;
    lCb->sCAccessTicks = TICKS;
  }
  if (USART_GetITStatus(aDescr->sHWCFG->sUSART, USART_IT_TC) == SET) {
    aDescr->sRsv3++;
    PIN_Set(&aDescr->sHWCFG->sDIR, FALSE);
    aDescr->sLFlags &= ~TX_PIN_SET;
    aDescr->sLTx->sCAccessTicks = TICKS;
    USART_ITConfig(aDescr->sHWCFG->sUSART, USART_IT_TC, DISABLE);
  }
}

void MyUART_ReturnDirByTC(MY_UART_TypeDef *aDescr)
{
  if ((aDescr->sHWCFG->sDIR.sPIN_PORT != NULL) && ((aDescr->sLFlags & TX_PIN_SET) != 0) &&
    (USART_GetFlagStatus(aDescr->sHWCFG->sUSART, USART_FLAG_TC) != RESET)) {
    //USART_ClearFlag(aDescr->sUSART, USART_FLAG_TC);
    PIN_Set(&aDescr->sHWCFG->sDIR, FALSE);
    aDescr->sLFlags &= ~TX_PIN_SET;
  }
  // Wait of DMA completion
  //while(DMA_GetFlagStatus(DMA1_FLAG_TC4) == RESET);
}

void MyUART_UpdateReceivePos(MY_UART_TypeDef *aDescr)
{
  int lIndex;
  lIndex = aDescr->sLRx->sCBufSize - DMA_GetCurrDataCounter(aDescr->sHWCFG->sDMA_STREAM_RX);
  if (lIndex != aDescr->sLRx->sCAddPos) {
    aDescr->sLRx->sCAccessTicks = TICKS;
    aDescr->sLRx->sCAddPos = lIndex;
  }
}
//------------------------------------------------------------------------------

bool WasInitBackup(void)
{
  return InitBackup;
}

uint32_t ReadBackupParam(int aIndex)
{
  aIndex += RTC_BKP_DR0;
  if IS_RTC_BKP(aIndex)
    return RTC_ReadBackupRegister(aIndex);
  else return 0;
}

void WriteBackupParam(int aIndex, uint32_t aValue)
{
  aIndex += RTC_BKP_DR0;
  if IS_RTC_BKP(aIndex)
    RTC_WriteBackupRegister(aIndex, aValue);
}

void IncStartCounter(bool aIsLowPart)
{
  uint32_t lCounter;
  lCounter  = RTC_ReadBackupRegister(STARTCOUNTER_BKP_INDEX);
  if (aIsLowPart) lCounter = lCounter & 0xFFFF0000 | (lCounter + 1) & MAXUINT16;
  else lCounter += 0x10000;
  RTC_WriteBackupRegister(STARTCOUNTER_BKP_INDEX, lCounter);
}

int GetStartCounter(bool aIsLowPart)
{
  uint32_t lCounter;
  lCounter  = RTC_ReadBackupRegister(STARTCOUNTER_BKP_INDEX);
  if (aIsLowPart) return lCounter & MAXUINT16;
  else return lCounter >> 16;
}

//------------------------------------------------------------------------------

MY_SPI_TypeDef* MySPI_Init(MY_SPI_TypeDef *aSPI,const SPI_HWCFG_TypeDef *aConfig)
{
  SPI_InitTypeDef SPI_InitStructure;
  uint16_t SPI_DMAReq =0;
  //
  if (aSPI == NULL)
    aSPI = FixMemAlloc( sizeof(MY_SPI_TypeDef) );
  //
  MemClear(aSPI, sizeof(MY_SPI_TypeDef));
  aSPI->sHWCFG = aConfig;
  aSPI->sBuffer = FixMemAlloc( aConfig->sBufSize );
  aSPI->sBufSize = aConfig->sBufSize;
  //
  if (aConfig->sCHA.sPIN_PORT != NULL) {
    PIN_Config(&aConfig->sCHA);
    PIN_Set(&aConfig->sCHA, TRUE);
  }
  // Enable SPI clock and release reset
  if (aConfig->sSPI == SPI1) {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
    RCC_APB2PeriphClockCmd(aConfig->sSPI_CLK, ENABLE);
    RCC_APB2PeriphResetCmd(aConfig->sSPI_CLK, DISABLE);
  }
  else {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
    RCC_APB1PeriphClockCmd(aConfig->sSPI_CLK, ENABLE);
    RCC_APB1PeriphResetCmd(aConfig->sSPI_CLK, DISABLE);
  }
  PIN_Config(&aConfig->sSCK);
  //
  if (aConfig->sMISO.sPIN_PORT != NULL) {
    SPI_DMAReq |= SPI_I2S_DMAReq_Rx;
    PIN_Config(&aConfig->sMISO);
  }  
  //
  if (aConfig->sMOSI.sPIN_PORT != NULL) {
    SPI_DMAReq |= SPI_I2S_DMAReq_Tx;  
    PIN_Config(&aConfig->sMOSI);
  }  
  // SPI configuration
  SPI_I2S_DeInit(aConfig->sSPI);
  SPI_InitStructure = aConfig->sInitStruc;
  SPI_Init(aConfig->sSPI, &SPI_InitStructure);
  // Enable the SPI
//  SPI_Cmd(aConfig->sSPI, ENABLE);
  
//  SPI_NSSInternalSoftwareConfig(aConfig->sSPI, SPI_NSSInternalSoft_Set);
  // Enable dma tx & rx request
//  SPI_I2S_DMACmd(aConfig->sSPI, SPI_DMAReq/*SPI_I2S_DMAReq_Tx | SPI_I2S_DMAReq_Rx*/, ENABLE);
  //
//  IRQ_Init(aConfig->sDMA_RX_IRQn);
  //
  return aSPI;
}

uint16_t MySPI_DataOI(MY_SPI_TypeDef *aSPI,uint16_t aData)
{
  while (SPI_I2S_GetFlagStatus(aSPI->sHWCFG->sSPI, SPI_I2S_FLAG_TXE) == RESET);
  SPI_I2S_SendData(aSPI->sHWCFG->sSPI, aData);
  while (SPI_I2S_GetFlagStatus(aSPI->sHWCFG->sSPI, SPI_I2S_FLAG_RXNE) == RESET);
  return SPI_I2S_ReceiveData(aSPI->sHWCFG->sSPI);
}

uint16_t MySPI_DataIn(MY_SPI_TypeDef *aSPI)
{
  while (SPI_I2S_GetFlagStatus(aSPI->sHWCFG->sSPI, SPI_I2S_FLAG_RXNE) == RESET);
  return SPI_I2S_ReceiveData(aSPI->sHWCFG->sSPI);
}

//------------------------------------------------------------------------------
bool MySPI_Prepare(MY_SPI_TypeDef *aSPI,uint16_t aBuffOffs)
{
  PTR_U lFrame;
  uint16_t lFlags, lIdx, lBufCur;
  //
  if (aSPI->sInTrans) return FALSE;
  lBufCur = aSPI->sBufOffs;
  while (lBufCur < aSPI->sBufCur) {
    lFrame.uPtr = &aSPI->sBuffer[lBufCur];
    lFlags = lFrame.uWord[0];
    if ((lFlags & (EXTBUF_FLAG | WRITE_FLAG)) == 0) {
      if ((lFlags & SF_FLAG) != 0) lIdx = 3;else lIdx = 2;
      aSPI->sOnCopyAfterRead(aSPI->sOwnerDescr, (char*)lFrame.uLong[1],
        (char*)lFrame.uLong[lIdx], lFrame.uWord[1] | (lFlags & REV_FLAG));
    }
    if ((lFlags & EXTBUF_FLAG) != 0) 
      aSPI->sOnAfterExtTrans(aSPI->sOwnerDescr, (char*)lFrame.uLong[1],
        lFrame.uWord[1]);
    lBufCur += lFlags & BUFSIZE_MASK;
  }
  aSPI->sBufOffs = aBuffOffs;
  aSPI->sBufPos = aBuffOffs;
  aSPI->sBufCur = aBuffOffs;
  aSPI->sBufSubCur = 0;
  return TRUE;
}

bool MySPI_AddFrame(MY_SPI_TypeDef *aSPI,void *aBufPtr,uint16_t aCount,
  uint16_t aFlags,uint32_t aSFData,char **aFramePtr)
{
  PTR_U lFrame;
  uint16_t lIdx;
  if (aSPI->sInTrans) return FALSE;
  lFrame.uPtr = &aSPI->sBuffer[aSPI->sBufPos];
  lFrame.uWord[1] = aCount;
  //
  if ((aFlags & SF_FLAG) != 0) {
    lFrame.uLong[2] = aSFData;
    lIdx = 3;
  }
  else lIdx = 2;
  //
  if ((aFlags & EXTBUF_FLAG) != 0) {
    *aFramePtr = aBufPtr;
    aCount = 0;
  }
  else {
    aCount &= BUFSIZE_MASK;
    if ((aFlags & WRITE_FLAG) == 0) {
      lFrame.uLong[lIdx] = (uint32_t)aBufPtr;
      *aFramePtr = (char*)&lFrame.uLong[lIdx+1];
      aCount += sizeof(uint32_t);
    }
    else *aFramePtr = (char*)&lFrame.uLong[lIdx];
  }
  aCount += sizeof(uint16_t)*2 + sizeof(uint32_t);
  if ((aFlags & SF_FLAG) != 0) aCount += sizeof(uint32_t);
  lFrame.uLong[1] = (uint32_t)*aFramePtr;
  lFrame.uWord[0] = aCount | aFlags;
  aSPI->sBufPos += aCount;
  return TRUE;
}

bool MySPI_Start(MY_SPI_TypeDef *aSPI)
{
  DMA_InitTypeDef DMA_InitStructure;
  const SPI_HWCFG_TypeDef *lConfig;
  PTR_U lFrame, lIBuf;
  //
  if (aSPI->sBufCur >= aSPI->sBufPos) return FALSE;
  lConfig = aSPI->sHWCFG;
  PIN_Set(&aSPI->sHWCFG->sCHA, FALSE);
  //
  lFrame.uPtr = &aSPI->sBuffer[aSPI->sBufCur];
  lIBuf.uAddr = lFrame.uLong[1];
  //
  if ((lConfig->sDMA_STREAM_TX != 0)&&(lConfig->sDMA_STREAM_RX != 0)) {
    DMA_DeInit(lConfig->sDMA_STREAM_TX);
    DMA_StructInit(&DMA_InitStructure);
    DMA_InitStructure.DMA_Channel = lConfig->sDMA_CHAN_TX;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&lConfig->sSPI->DR;
    DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
    if ((lFrame.uWord[0] & SF_FLAG) == 0) {
      DMA_InitStructure.DMA_Memory0BaseAddr = lIBuf.uAddr;
      DMA_InitStructure.DMA_BufferSize = lFrame.uWord[1];
    }
    else {
      if (aSPI->sOnPrepSubFrame != NULL)
        aSPI->sOnPrepSubFrame(lIBuf.uChar, aSPI->sBuffer, &lFrame.uLong[2]);
      DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)aSPI->sBuffer;
      DMA_InitStructure.DMA_BufferSize = aSPI->sBufOffs;
    }
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_Init(lConfig->sDMA_STREAM_TX, &DMA_InitStructure);
    //
    DMA_DeInit(lConfig->sDMA_STREAM_RX);
    DMA_InitStructure.DMA_Channel = lConfig->sDMA_CHAN_RX;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_Init(lConfig->sDMA_STREAM_RX, &DMA_InitStructure);
  }
  //
  DMA_ITConfig(lConfig->sDMA_STREAM_RX, DMA_IT_TC, ENABLE);
  //
  if (!aSPI->sInTrans) {
    aSPI->sStartTicks = TICKS;
    aSPI->sInTrans = TRUE;
  }
  DMA_Cmd(lConfig->sDMA_STREAM_RX, ENABLE);
  DMA_Cmd(lConfig->sDMA_STREAM_TX, ENABLE);
  return TRUE;
}

void MySPI_DMA_RX_IRQHandler(MY_SPI_TypeDef *aSPI)
{
  PTR_U lFrame;
  uint16_t lFlags;
  // Test on DMA Stream Receive Complete interrupt
  if (DMA_GetITStatus(aSPI->sHWCFG->sDMA_STREAM_RX,
     aSPI->sHWCFG->sDMA_RX_IT_TC)) {
    // Clear DMA Stream Receive Complete interrupt pending bit
    DMA_ClearITPendingBit(aSPI->sHWCFG->sDMA_STREAM_RX,
      aSPI->sHWCFG->sDMA_RX_IT_TC);
    //
    PIN_Set(&aSPI->sHWCFG->sCHA, TRUE);
    aSPI->sInRx++;
    //
    lFrame.uPtr = &aSPI->sBuffer[aSPI->sBufCur];
    lFlags = lFrame.uWord[0];
    if ((lFlags & SF_FLAG) != 0) {
      if ((lFlags & WRITE_FLAG) == 0)
        aSPI->sOnReadSubFrame(aSPI->sBuffer, (char*)lFrame.uLong[1], &lFrame.uLong[2]);
      aSPI->sBufSubCur++;
      if (aSPI->sBufSubCur >= lFrame.uWord[1]) {
        aSPI->sBufCur += lFlags & BUFSIZE_MASK;
        aSPI->sBufSubCur = 0;
      }
    }
    else
      aSPI->sBufCur += lFlags & BUFSIZE_MASK;
    if (aSPI->sBufCur < aSPI->sBufPos) MySPI_Start(aSPI);
    else {
      aSPI->sInTrans = FALSE;
      aSPI->sStopTicks = TICKS;
    }
  }
}

//------------------------------------------------------------------------------
void TICKER_Config(void)
{
  TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
  // TIM6 Periph clock enable
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
  //
  // Time base configuration
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Period = 0x0F;
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
  //
  // TIM6 TRGO selection
  TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update);
  //
  // TIM6 enable counter
  TIM_Cmd(TIM6, ENABLE);
}

bool FLASH_SetReadProtection(void)
{
  if ((*(__IO uint8_t*)(OPTCR_BYTE1_ADDRESS) != (uint8_t)OB_RDP_Level_1)) {
    FLASH_OB_Unlock();
    FLASH_OB_RDPConfig(OB_RDP_Level_1);
    FLASH_OB_Launch();
    FLASH_OB_Lock();
    NVIC_SystemReset();
    return TRUE;
  }
  return FALSE;
/*  if (FLASH_OB_GetRDP() == OB_RDP_Level_0) {
    FLASH_OB_Unlock();
    //
    FLASH_OB_RDPConfig(OB_RDP_Level_1);
    //
    if (FLASH_OB_Launch() != FLASH_COMPLETE);
    //
    FLASH_OB_Lock();
  }*/
}

bool FlashLockRes(bool aResult)
{
  FLASH_Lock();
  return aResult;
}

bool FLASH_Clear(uint32_t aIndex,uint32_t aCount)
{
  FLASH_Unlock();
  //
  aIndex <<= 3;
  // Clear pending flags (if any)
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
                  FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
  //
  while (aCount-- > 0) {
    if (FLASH_EraseSector(aIndex, VoltageRange_3) != FLASH_COMPLETE) {
      // Error occurred while sector erase.
      //   User can add here some code to deal with this error
      FLASH_Lock();
      return FALSE;
    }
    aIndex += 8;
  }
  //
  return FlashLockRes(TRUE);
}

bool FLASH_Write(uint32_t aAddr,void *aBuffer,int aCount)
{
  char *lBuffer;
  //
  FLASH_Unlock();
  //
  // Clear pending flags (if any)
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
                  FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
  //
  lBuffer = aBuffer;
  aCount = (aCount + 3) >> 2;
  while (aCount > 0) {
    if (FLASH_ProgramWord(aAddr, *(uint32_t*)lBuffer) == FLASH_COMPLETE) {
      lBuffer += 4;
      aAddr += 4;
      aCount--;
    }
      // Error occurred while writing data in Flash memory.
      // User can add here some code to deal with this error
    else
      return FlashLockRes(FALSE);
  }
  return FlashLockRes(TRUE);
}

bool FLASH_Write2(uint32_t aAddr,void *aBuffer,int aCount)
{
  char *lBuffer;
  //
  FLASH_Unlock();
  //
  // Clear pending flags (if any)
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
                  FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
  //
  lBuffer = aBuffer;
  for (int I=4;I>0;I--) {
    if (((aAddr & 1) != 0) || (aCount == 1))
      if (FLASH_ProgramByte(aAddr, *(uint8_t*)lBuffer) == FLASH_COMPLETE) {
        lBuffer++; aAddr++; aCount--;
        //PostLogText("----w1"S_CRLF);
        if (aCount == 0) return FlashLockRes(TRUE);
      } else return FlashLockRes(FALSE);
    if ((aCount > 1) && (((aAddr & 2) != 0) || (aCount < 4)))
      if (FLASH_ProgramHalfWord(aAddr, *(uint16_t*)lBuffer) == FLASH_COMPLETE) {
        lBuffer += 2; aAddr += 2; aCount -= 2;
        //PostLogText("----w2"S_CRLF);
        if (aCount == 0) return FlashLockRes(TRUE);
      } else return FlashLockRes(FALSE);
/*    if ((aCount > 3) && (((aAddr & 4) != 0) || (aCount < 8)))
      if (FLASH_ProgramWord(aAddr, *(uint32_t*)lBuffer) == FLASH_COMPLETE) {
        lBuffer += 4; aAddr += 4; aCount -= 4;
        PostLogText("----w4"S_CRLF);
        if (aCount == 0) return FlashLockRes(TRUE);
      } else return FlashLockRes(FALSE);
    while (aCount > 7)
      if (FLASH_ProgramDoubleWord(aAddr, *(uint64_t*)lBuffer) == FLASH_COMPLETE) {
        lBuffer += 8; aAddr += 8; aCount -= 8;
        PostLogText("----w8"S_CRLF);
        if (aCount == 0) return FlashLockRes(TRUE);
      } else return FlashLockRes(FALSE);*/
    while (aCount > 3)
      if (FLASH_ProgramWord(aAddr, *(uint32_t*)lBuffer) == FLASH_COMPLETE) {
        lBuffer += 4; aAddr += 4; aCount -= 4;
        //PostLogText("----w4"S_CRLF);
        if (aCount == 0) return FlashLockRes(TRUE);
      } else return FlashLockRes(FALSE);
  }
  return FlashLockRes(FALSE);
}

bool FLASH_Verify(uint32_t aAddr,void *aBuffer,int aCount)
{
  return FALSE;
}

uint32_t random32(void)
{
  RNG_GetFlagStatus(RNG_FLAG_DRDY);
  return RNG_GetRandomNumber();
}

uint32_t DefaultAppAddr(bool aIsHiAddr)
{
  if (aIsHiAddr)
    if (FLASHSIZE <= FLASH128K) return HI_APPADDR128K; else
    if (FLASHSIZE == FLASH256K) return HI_APPADDR256K; else
    if (FLASHSIZE >= FLASH512K) return HI_APPADDR512K; else;
  else
    if (FLASHSIZE <= FLASH128K) return LO_APPADDR128K; else
    if (FLASHSIZE == FLASH256K) return LO_APPADDR256K; else
    if (FLASHSIZE >= FLASH512K) return LO_APPADDR512K;
  return LO_APPADDR128K;  
}

void* BkpMemAlloc(uint32_t aAllocSize)
{
  uint32_t lPrevAddr;
  lPrevAddr = gBkpMemAllocPos;
  gBkpMemAllocPos = (gBkpMemAllocPos + aAllocSize + 3) & (~3);
  return (void*)lPrevAddr;
}

void BkpMemClear(void)
{
  MemClear((void*)BACKUP_MEM_ADDRESS, BACKUP_MEM_SIZE);
}

bool App_Change(uint32_t aAddr)
{
  int lWriteLen = VTORLENGTH;
  uint32_t lHiAppAddr;
  SYSBLOCK_TypeDef *lSysBlock;
  bool lResult, lTest;
  //
  lTest = (aAddr == 1); if (lTest) aAddr = 0;
  if (aAddr == 0) {
    lHiAppAddr = DefaultAppAddr(TRUE);
    if (SYSBLOCK->hsbSignature == SYSBLOCKSIGN)
      if (SYSBLOCK->hsbNextAddr != EMPTY32) aAddr = SYSBLOCK->hsbNextAddr;
      else
        if (SYSBLOCK->hsbBaseAddr == lHiAppAddr) aAddr = DefaultAppAddr(FALSE);
        else aAddr = lHiAppAddr;
    else aAddr = lHiAppAddr;
  }  
  lResult = (aAddr > BASE_APPADDR);
  if (lResult) {
    lResult = FLASH_Clear(0, 1);
    Delay(10);
    if (lResult) {
      lSysBlock = (SYSBLOCK_TypeDef*)(aAddr+VTORLENGTH);
      if (lSysBlock->hsbSignature == SYSBLOCKSIGN)
        lWriteLen += lSysBlock->hsbSysBlockSize;
      if (lTest) PostLogHex("AppChangeAddr=", aAddr, 4, S_CRLF);
      else
        lResult = FLASH_Write(BASE_APPADDR, (void*)aAddr, lWriteLen);
      Delay(10);
      if (lResult && (!lTest)) {
        NVIC_SystemReset();
        return TRUE;
      }
    }
  }
  return FALSE;
}

bool VTOR_Setup(uint32_t aAddr)
{
  if (aAddr >= BASE_APPADDR) {
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, aAddr - BASE_APPADDR);
    return TRUE;
  }
  else return FALSE;
  //__set_MSP(*(uint32_t *)0x08004000);
  //ISRPtr application_reset_handler=(ISRPtr)(*(uint32_t *)0x08002004);
  //application_reset_handler();
}

bool AssignOldCfgBlock(uint32_t aAddr)
{
  return FALSE;
}

void IRQ_InitEx(uint8_t aIRQChan, uint8_t aPrePri, uint8_t aSubPri)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  if (aIRQChan != 0) {
    NVIC_InitStructure.NVIC_IRQChannel = aIRQChan;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = aPrePri;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = aSubPri;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
  }
}

void IRQ_Init(uint8_t aIRQChan)
{
  IRQ_InitEx(aIRQChan, 0,0);
}

uint32_t GetMaxCodeDataSize(uint32_t aAddr)
{
  if (aAddr >= SHI_APPADDR) return SHI_CODEDATASIZE; else
  if (aAddr >= HI_APPADDR128K) return HI_CODEDATASIZE; else
  if (aAddr >= LO_APPADDR128K) return LO_CODEDATASIZE; else return 0;
}

bool FLASH_Test(uint32_t aAddr,char *aBuf,int aCount)
{
  return Arr32NEqual((void*)aAddr, aBuf, aCount >> 2) == NOT_FOUND;
}
