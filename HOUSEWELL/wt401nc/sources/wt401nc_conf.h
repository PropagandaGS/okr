// �������� ������������ ������ WT-401nc
// � Olgvel, 2019
//

#ifndef __WT401NC_CONF_H
#define __WT401NC_CONF_H

#include "my_stm32.h"
#include "my_rtctl.h"
#include "my_stdcmdprocs.h"
#include "my_simcom.h"
#include "my_esp.h"
//#include "my_isadc.h"
//#include "my_sdio.h"
//
//#include "r_isadc.h"
#include "r_rtc.h"
#include "r_gnss.h"
#include "r_vap3.h"

/* Exported variables --------------------------------------------------------*/

extern volatile uint32_t TICKS;

/* Exported constants --------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
void WT401nc_Init(void);
void WT401nc_Process(void);
void SetTimeStamp(void* aContext,UINT64_U* aTimeStamp);
void SetCoords(void* aContext,UINT64_U* aCoords);
#ifdef __cplusplus
}
#endif

//#define LOW_BLNK_TIME     9000
#define LED_ST2_OFF_TIME   150
#define LED_ST1_OFF_TIME   500
#define LED_ST0_OFF_TIME   1000
#define LED_LIGHT_TIME     70

#define READ_WAIT_TIME     5

#define BUSBUFSIZE         1024  // 2048
//#define LOGBUFSIZE         1024

//******************************************************************************

#define TEMPBUFFERSIZE     128
#define INBUFFERSIZE       256
#define OUTBUFFERSIZE      256
#define WIZBUFFERSIZE      512
#define INBUFSIZE          1024
#define OUTBUFSIZE         3*1024

#define MAXOUTS            8
#define MAXOUTFUNCS        16

extern RTCTL_DESCR_TypeDef *gRTCTL;
extern MY_UART_TypeDef  *gUART_SIM, *gUART_WL, *gUART_BUS, *gUART_GNSS, *gUART_ESP;
//extern MY_SPI_TypeDef   *gSPI_ISADC;
//extern ISADC_DESCR_TypeDef *gISADC;
//extern SDC_DESCR_TypeDef *gSDIO;
//
//extern SIMCOM_DESCR_TypeDef *gSIMCOM;
extern ESP_DESCR_TypeDef *gESP;
//

#define MEMALLOCBUFSIZE    64*1024  //32768  //24576   //16384    //12288 //8192
extern char    gFixMemAllocBuffer[MEMALLOCBUFSIZE];

//******************************************************************************
SYSTICK_HANDLER

const PIN_HWCFG_TypeDef LD_1      = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_0,  GPIO_Mode_OUT, GPIO_Speed_50MHz}};
const PIN_HWCFG_TypeDef LD_2      = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_7,  GPIO_Mode_OUT, GPIO_Speed_50MHz}};
const PIN_HWCFG_TypeDef LD_3      = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_14, GPIO_Mode_OUT, GPIO_Speed_50MHz}};

// BUS1
const USART_HWCFG_TypeDef UART_BUS = {
  USART3, RCC_APB1Periph_USART3, OUTBUFSIZE, INBUFSIZE,
  {115200, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Rx | USART_Mode_Tx},
  {GPIOC, RCC_AHB1Periph_GPIOC, {GPIO_Pin_10, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP}, GPIO_PinSource10, GPIO_AF_USART3},
  {GPIOC, RCC_AHB1Periph_GPIOC, {GPIO_Pin_11, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP}, GPIO_PinSource11, GPIO_AF_USART3},
  {NULL},
  DMA1_Stream3,DMA1_Stream1,
  DMA_Channel_4,DMA_Channel_4,
  0, DMA1_Stream3_IRQn, DMA_IT_TCIF3
};
UART_DMA_TX_IRQ_HANDLER(DMA1_Stream3_IRQHandler, gUART_BUS)

//#define UART_ESP_IRQHandler USART3_IRQHandler
//#define DMA_TX_ESP_IRQHandler DMA1_Stream3_IRQHandler
const USART_HWCFG_TypeDef UART_ESP = {
  UART5, RCC_APB1Periph_UART5, BUSBUFSIZE, BUSBUFSIZE, //OUTBUFFERSIZE, INBUFFERSIZE,
  {9600, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Rx | USART_Mode_Tx},
  {GPIOC, RCC_AHB1Periph_GPIOC, {GPIO_Pin_12, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP}, GPIO_PinSource12, GPIO_AF_UART5},
  {GPIOD, RCC_AHB1Periph_GPIOD, {GPIO_Pin_2,  GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP}, GPIO_PinSource2, GPIO_AF_UART5},
  {NULL},
  DMA1_Stream7,DMA1_Stream0,
  DMA_Channel_4,DMA_Channel_4,
  0,// UART5_IRQn,
    DMA1_Stream7_IRQn, DMA_IT_TCIF7
};
const PIN_HWCFG_TypeDef ESP_ENB   = {GPIOG, RCC_AHB1Periph_GPIOG, {GPIO_Pin_2, GPIO_Mode_OUT, GPIO_Speed_50MHz, GPIO_OType_OD, GPIO_PuPd_UP}};
const PIN_HWCFG_TypeDef ESP_PRG   = {GPIOG, RCC_AHB1Periph_GPIOG, {GPIO_Pin_3, GPIO_Mode_OUT, GPIO_Speed_50MHz, GPIO_OType_OD, GPIO_PuPd_UP}};
UART_DMA_TX_IRQ_HANDLER(DMA1_Stream7_IRQHandler, gUART_ESP)
//
// *****************************************************************************
/*
// GNSS: UART, PPS *************************************************************
const USART_HWCFG_TypeDef UART_GNSS = {
  USART6, RCC_APB2Periph_USART6, OUTBUFFERSIZE, INBUFFERSIZE,
  {115200, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Rx | USART_Mode_Tx},
  {GPIOC, RCC_AHB1Periph_GPIOC, {GPIO_Pin_6, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP}, GPIO_PinSource6, GPIO_AF_USART6},
  {GPIOC, RCC_AHB1Periph_GPIOC, {GPIO_Pin_7, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP}, GPIO_PinSource7, GPIO_AF_USART6},
  {NULL},
  DMA2_Stream6,DMA2_Stream1,
  DMA_Channel_5,DMA_Channel_5,
  0,//USART6_IRQn, 
    DMA2_Stream6_IRQn, DMA_IT_TCIF6
};
//
const PIN_HWCFG_TypeDef GNSS_RST = {GPIOA, RCC_AHB1Periph_GPIOA, {GPIO_Pin_13, GPIO_Mode_OUT, GPIO_Speed_100MHz}};
const PIN_HWCFG_TypeDef GNSS_PPS = {GPIOA, RCC_AHB1Periph_GPIOA, {GPIO_Pin_8, GPIO_Mode_IN, GPIO_Speed_100MHz}};
UART_IRQ_HANDLER(USART6_IRQHandler, gUART_GNSS)
UART_DMA_TX_IRQ_HANDLER(DMA2_Stream6_IRQHandler, gUART_GNSS)
*/
// *****************************************************************************

// ISADC: SPI, MCLK, DRDY ******************************************************
/*
const SPI_HWCFG_TypeDef SPI_ISADC = {
  SPI3, RCC_APB1Periph_SPI3, WIZBUFFERSIZE,
  {SPI_Direction_1Line_Rx, SPI_Mode_Slave, SPI_DataSize_8b, SPI_CPOL_Low,      // High
      SPI_CPHA_2Edge, SPI_NSS_Soft, SPI_BaudRatePrescaler_4, SPI_FirstBit_MSB}, // 1Edge
  {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_3, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_DOWN}, GPIO_PinSource3, GPIO_AF_SPI3},  // SCK
  {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_4, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_DOWN}, GPIO_PinSource4, GPIO_AF_SPI3},  // MISO !!! 
  {NULL}, // {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_5, GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_DOWN}, GPIO_PinSource5, GPIO_AF_SPI3},  //MOSI
  {GPIOC, RCC_AHB1Periph_GPIOC, {GPIO_Pin_0, GPIO_Mode_OUT, GPIO_Speed_50MHz}}, {NULL},
  DMA1_Stream7, DMA1_Stream0,
  DMA_Channel_0, DMA_Channel_0,
  SPI3_IRQn,
  DMA1_Stream7_IRQn, DMA_IT_TCIF7,
  DMA1_Stream0_IRQn, DMA_IT_TCIF0, DMA_IT_HTIF0
};*/
/*
const PIN_HWCFG_TypeDef SDCARD_PWR = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_13, GPIO_Mode_OUT, GPIO_Speed_50MHz}};
const PIN_HWCFG_TypeDef SDCARD_DET = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_14, GPIO_Mode_IN, GPIO_Speed_100MHz}};
//
const PIN_HWCFG_TypeDef ISADC_MCLK =
  {GPIOA, RCC_AHB1Periph_GPIOA, {GPIO_Pin_5, GPIO_Mode_AF, GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_UP}, GPIO_PinSource5, GPIO_AF_TIM2};
const PIN_HWCFG_TypeDef ISADC_SCLK =
  {GPIOE, RCC_AHB1Periph_GPIOE, {GPIO_Pin_9, GPIO_Mode_AF, GPIO_Speed_100MHz, GPIO_OType_PP, GPIO_PuPd_UP}, GPIO_PinSource9, GPIO_AF_TIM1};
//
const PIN_HWCFG_TypeDef C4V_PWR    = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_15, GPIO_Mode_OUT, GPIO_Speed_100MHz}};
const PIN_HWCFG_TypeDef ISADC_PWR  = {GPIOD, RCC_AHB1Periph_GPIOD, {GPIO_Pin_8,  GPIO_Mode_OUT, GPIO_Speed_100MHz}}; //, GPIO_OType_OD, GPIO_PuPd_NOPULL}};
const PIN_HWCFG_TypeDef ISADC_EN   = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_7,  GPIO_Mode_OUT, GPIO_Speed_100MHz}}; //, GPIO_OType_OD, GPIO_PuPd_NOPULL}};
const PIN_HWCFG_TypeDef ISADC_DRDY = {GPIOA, RCC_AHB1Periph_GPIOA, {GPIO_Pin_4,  GPIO_Mode_IN, GPIO_Speed_100MHz}};
//
const PIN_HWCFG_TypeDef BTN_A      = {GPIOD, RCC_AHB1Periph_GPIOD, {GPIO_Pin_7,  GPIO_Mode_IN, GPIO_Speed_100MHz}};
const PIN_HWCFG_TypeDef BTN_B      = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_6,  GPIO_Mode_IN, GPIO_Speed_100MHz}};
const PIN_HWCFG_TypeDef BTN_C      = {GPIOE, RCC_AHB1Periph_GPIOE, {GPIO_Pin_1,  GPIO_Mode_IN, GPIO_Speed_100MHz}};
const PIN_HWCFG_TypeDef BTN_D      = {GPIOE, RCC_AHB1Periph_GPIOE, {GPIO_Pin_0,  GPIO_Mode_IN, GPIO_Speed_100MHz}};
//
const PIN_HWCFG_TypeDef GNSS_PWR  = {GPIOD, RCC_AHB1Periph_GPIOD, {GPIO_Pin_14, GPIO_Mode_OUT, GPIO_Speed_100MHz, GPIO_OType_OD, GPIO_PuPd_NOPULL}};
const PIN_HWCFG_TypeDef RS485_PWR = {GPIOD, RCC_AHB1Periph_GPIOD, {GPIO_Pin_3,  GPIO_Mode_OUT, GPIO_Speed_100MHz, GPIO_OType_OD, GPIO_PuPd_NOPULL}};
const PIN_HWCFG_TypeDef SIM_PWR   = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_2,  GPIO_Mode_OUT, GPIO_Speed_100MHz, GPIO_OType_OD, GPIO_PuPd_NOPULL}};
const PIN_HWCFG_TypeDef CAN_PWR   = {GPIOA, RCC_AHB1Periph_GPIOA, {GPIO_Pin_0,  GPIO_Mode_OUT, GPIO_Speed_100MHz, GPIO_OType_OD, GPIO_PuPd_NOPULL}};
const PIN_HWCFG_TypeDef DISP_PWR  = {GPIOB, RCC_AHB1Periph_GPIOB, {GPIO_Pin_9,  GPIO_Mode_OUT, GPIO_Speed_100MHz, GPIO_OType_OD, GPIO_PuPd_NOPULL}};
*/
/*
IRQ_HANDLER(DMA1_Stream0_IRQHandler, ISADC_DMA_RX_IRQHandler, gISADC)
IRQ_HANDLER(TIM1_UP_TIM10_IRQHandler, ISADC_SCLK_IRQHandler, gISADC)
//
IRQ_HANDLER(EXTI4_IRQHandler, ISADC_DRDY_IRQHandler, gISADC)
IRQ_HANDLER(EXTI9_5_IRQHandler, ISADC_PPS_BTNs_IRQHandler, gISADC)
IRQ_HANDLER(EXTI15_10_IRQHandler, SDC_DET_IRQHandler, gSDIO)
*/
// *****************************************************************************

// Command Handlers ************************************************************

#ifdef __cplusplus
extern "C" {
#endif
//  
#define TCN_MEMCLR       "\x07+MEMCLR\0"
#define TCH_MEMCLR       {TCHMemClear}
void TCHMemClear(void *aContext,TXTCMD_PAR_TypeDef *aPar);
//
#define TCN_WLTEST       "\x07+WLTEST\0"
#define TCH_WLTEST       {TCHWlTest,(TCFSTR<<TCFBITS)|TCFINT} //,(((TCFINT<<TCFBITS)|TCFINT)<<TCFBITS)|TCFSTR}
void TCHWlTest(void *aContext,TXTCMD_PAR_TypeDef *aPar);
//
#define TCN_BUSTEST      "\x08+BUSTEST\0"
#define TCH_BUSTEST      {TCHBusTest,(TCFSTR<<TCFBITS)|TCFINT}
void TCHBusTest(void *aContext,TXTCMD_PAR_TypeDef *aPar);
//
#define TCN_WNETBR       "\x07+WNETBR\0"
#define TCH_WNETBR       {TCHWiFiBaudRate,(TCFINT<<TCFBITS)|TCFINT}
void TCHWiFiBaudRate(void *aContext,TXTCMD_PAR_TypeDef *aPar);
//
#define TCN_TIME         "\x05+TIME\0"
#define TCH_TIME         {TCHTimeStamp,(TCFINT<<TCFBITS)|TCFINT}
void TCHTimeStamp(void *aContext,TXTCMD_PAR_TypeDef *aPar);
//
#ifdef __cplusplus
}
#endif

// Command Registration ********************************************************
typedef enum {
  ACX_EMPTY, ACX_RTRTL, ACX_RTC, ACX_SIMCOM, ACX_ESP, ACX_ISADC, ACX_GNSS, ACX_SDIO, 
  //
  ACX_HI
} APPCTXENUM_TypeDef;

extern pvoid APPCTX_TABLE[ACX_HI];

typedef enum {
  TCE_MCUID, TCE_DID, TCE_MCULOCK, TCE_MCURESET,
  TCE_FCLEAR, TCE_FWRITE, TCE_FREAD, TCE_FCOPY, TCE_NDWRITE, TCE_NDREAD, TCE_NDDEL,
  TCE_APPCNG, TCE_JAMPTO, //TCE_OUTCTL, TCE_OUTCFG, TCE_OUTFUNC, TCE_OUTPUT,
  TCE_GMODE, TCE_SWSIMCARD, TCE_SIMCARD, TCE_ATC, TCE_SIMOFF, TCE_SMS,
  //TCE_EMODE, TCE_EIPCFG, TCE_NETRESET,
  TCE_SIMRESET, TCE_MEMCLR, TCE_ESPC, TCE_ESPRST, TCE_WMODE, TCE_WNETACC,
  TCE_WLTEST, TCE_BUSTEST, TCE_TIME, TCE_WNETBR,
  //TCE_IASTATE, TCE_IASS, TCE_IACFG, TCE_GNCFG,  TCE_SDCFG, TCE_SDREAD, TCE_RTCCFG,
  //
  TCE_HI
} TXTCMDENUM_TypeDef;

extern const char TXTCMD_NAMES[];
extern const TCHANDLER_TypeDef TXTCMD_HANDLERS[TCE_HI];

typedef enum {
  BCE_MCUID, BCE_DID, BCE_MCULOCK, BCE_MCURESET,
  BCE_FCLEAR, BCE_FWRITE, BCE_FREAD, BCE_FCOPY,
  BCE_NDWRITE, BCE_NDREAD, BCE_NDDEL, BCE_NDINIT,
  BCE_CRC, BCE_APPCNG, //BCE_IASMP, BCE_SDREAD,
  //
  BCE_HI
} BINCMDENUM_TypeDef;

extern const char BINCMD_NAMES[];
extern const BCHANDLER_TypeDef BINCMD_HANDLERS[BCE_HI];

#endif /* __WT401NC_CONF_H */
