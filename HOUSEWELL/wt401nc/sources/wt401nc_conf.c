// CPU main, 2019
//

#include "wt401nc_conf.h"

#define LOW_VER             "20"

//#define INSRCCMDBUFSIZE     768 
//#define INBINCMDBUFSIZE     576

#define START_LOG_TIMEOUT   500
#define MSGMNG_TIMEOUT      4000
#define LOGRCV_TIMEOUT      180000
#define MSGRCV_TIMEOUT      60000
#define MSGIDL_TIMEOUT	    25000
#define INCALL_TIMEOUT      1600

#define MSGNTFCHAR          0x06
#define ENDCHAR             0x0D
#define S_ICQ               "\x7F"

#define SMSIT               "SMSIT"
#define DEFSMSIT            0

#define WNETBR              "WBR"
#define DEF_WNETBR          115200

typedef struct {
  uint32_t sStartTicks;
  uint32_t sAutoTestTicks;
  uint8_t sSirenOut;
  bool sWasLogStart;
  uint32_t sCRegTicks;
  uint32_t sLastLogRecvTicks;
  uint32_t sLastMsgSendTicks;
  uint32_t sLastMsgRecvTicks;
  uint32_t sLastInSMSTicks;
  uint32_t sLastGNSSTimeTicks;
  uint32_t sLastGNSSCoordsTicks;
  int      sGNSSLedState;
  uint32_t sInSMSTimeOut;
  uint32_t sLastInCallTicks;
  int      sExSMSID;
  bool     sMsgNotified;
  bool     sOutStates[2];
  //
} HWMAIN_TypeDef;

//==============================================================================
//
MY_OUT_TypeDef /**gLED1,*/ *gLED2, *gOUT1, *gOUT2;
//WIZ_SOCKET_TypeDef *gLOGSCKT;
//WIZ_SOCKET_TypeDef *gMSGSCKT;
uint32_t *gMsgCounter;
MY_CIRCLBUF_TypeDef *gCBuffer;
HWMAIN_TypeDef gHWMain;
ESP_LINK_TypeDef *gWLinkA, *gWLinkB, *gWLinkC, *gWLinkD, *gWLinkE;
MY_LINK_TypeDef *gLogLinkA, *gLogLinkB;

//==============================================================================

/*void _PostLogMessage(char *aMessage,int aLength,void *aSelDest)
{
  if (assigned(gHWMain.sCtlLink) && (notassigned(aSelDest)||(aSelDest==gHWMain.sCtlLink)))
    MyCBuf_Write(gHWMain.sCtlLink->sLTx, aMessage, aLength);
  //
  if (assigned(gLOGSCKT) && (notassigned(aSelDest)||(aSelDest==gLOGSCKT)))
    WizNet_Write(gLOGSCKT, aMessage, aLength);
  //
  if (TEST_OK(gWLinkA->sLFlags, ELF_ACTIVE) && (notassigned(aSelDest)||(aSelDest==gWLinkA)))
    MyCBuf_Write(gWLinkA->sLTx, aMessage, aLength);
}
*/
void _PostLogMessage(char *aMessage,int aLength,MY_LINK_TypeDef *aSelLink)
{
  if assigned(aSelLink) {
     if TEST_OK(aSelLink->sLFlags, LF_ACTIVE)
       MyCBuf_Write(aSelLink->sLTx, aMessage, aLength);
  }
  else {
    if (assigned(gLogLinkA) && TEST_OK(gLogLinkA->sLFlags, LF_ACTIVE))
      MyCBuf_Write(gLogLinkA->sLTx, aMessage, aLength);
    if (assigned(gLogLinkB) && TEST_OK(gLogLinkB->sLFlags, LF_ACTIVE))
      MyCBuf_Write(gLogLinkB->sLTx, aMessage, aLength);
  }
}

void StdPostLogMessage(const char *aMessage,int aLength)
{
  _PostLogMessage((char*)aMessage, aLength, NULL);
}

void PostBCTextAnswer(void *aSource, char *aMessage,int aLength)
{
  _PostLogMessage(aMessage, aLength, aSource);
}

void LedGNSSIndicator(int aState)
{
    switch (aState) {
        case 0: MyPS_Setup(gLED2, TRUE, LED_ST0_OFF_TIME, LED_LIGHT_TIME, 0); break;
        case 1: MyPS_Setup(gLED2, TRUE, LED_ST1_OFF_TIME, LED_LIGHT_TIME, 0); break;
        case 2: MyPS_Setup(gLED2, TRUE, LED_ST2_OFF_TIME, LED_LIGHT_TIME, 0); break;
    }    
    gHWMain.sGNSSLedState = aState;
}

void Msg_Send(char *aSource,int aCount,bool aClrNtf)
{ /*
  if (aClrNtf) gHWMain.sMsgNotified = FALSE;
  gHWMain.sLastMsgSendTicks = TICKS;
  if assigned(gMSGSCKT) WizNet_Write(gMSGSCKT, aSource, aCount);*/
}

void MsgSendAns(bool aIsOkRes,int aID,char *aParam)
{
  char lStr[12], lPref;
  if (aIsOkRes) lPref = '*'; else lPref = '#';
  Msg_Send(&lPref, 1, FALSE);
  if (aID >= 0) Msg_Send(Int2StrZ(aID, lStr, 10), TOEND, FALSE);
  else if (aID == ERROR_RESULT) Msg_Send("?", 1, FALSE); else Msg_Send("CDS", 3, FALSE);
  if (aParam != NULL) {
    Msg_Send(S_COMMA, 1, FALSE);
    Msg_Send(aParam, TOEND, FALSE);
  }
  Msg_Send(S_CR, 1, FALSE);
}

void SIMOff(void)
{
  gHWMain.sLastInSMSTicks = TICKS;
//  SimCom_PostCommand(gSIMCOM, SCCMD_PWRDOWN);
}

bool AddNewMessage(char *aNumber,char *aMessage,int32_t aLength)
{
  char lStr[6];
  //
  if (++(*gMsgCounter) == 10000) *gMsgCounter = 0;
  return ( MyCircLBuf_Add(gCBuffer, Int2StrDigZ(*gMsgCounter, lStr, 4), 4) &&
    MyCircLBuf_Add(gCBuffer, S_TAB, 1) &&
    MyCircLBuf_Add(gCBuffer, aNumber, TOEND) &&
    MyCircLBuf_Add(gCBuffer, S_TAB, 1) &&
    MyCircLBuf_Add(gCBuffer, aMessage, aLength) &&
    MyCircLBuf_Write(gCBuffer, S_CR, 1) );
}
/*
void TestOutControl(int aOutIdx,int aOutVal)
{
  switch (aOutIdx) {
    case 0: PIN_Set(&WIZ_INT, aOutVal); break;
    case 1: PIN_Set(&WIZ_CS, aOutVal); break;
    case 2: PIN_Set(&WL_RST, aOutVal); break;
    case 3: PIN_Set(&ATSU_CK, aOutVal); break;
    case 4: PIN_Set(&PWR_CTL, aOutVal); break; // power off ctl
    case 5: PIN_Set(&LED_R, aOutVal); break;   // R - led
    case 6: PIN_Set(&LED_G, aOutVal); break;   // G - led
    case 7: PIN_Set(&LED_B, aOutVal); break;   // B - led
  }
}
*/
void ReceiveMsg(char *aMessage,int aLength)
{
  PostLogInt("Rcv: ", aLength, "  ");
  PostLogMsg("", aMessage, aLength, S_CRLF);
  if (aLength==2)
    switch (aMessage[1]) {
      case '1':
        gHWMain.sOutStates[0] ^= TRUE;
        //TestOutControl(0, gHWMain.sOutStates[0]);
        break;
      case '2':
        gHWMain.sOutStates[1] ^= TRUE;
        //TestOutControl(1, gHWMain.sOutStates[1]);
        break;
    }
}

//==============================================================================

void TCHOutControl(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount > 0)
    if (aPar->sTCPVals[0] != gHWMain.sSirenOut) {
      switch (aPar->sTCPVals[0]) {
        case 1: MyPS_Setup(gOUT1, TRUE, 1000, 900, 1); break;
        case 2: MyPS_Setup(gOUT2, TRUE, 1000, 900, 1); break;
        default:
          if (aPar->sTCPVals[0] >= 11)
            ;//TestOutControl(aPar->sTCPVals[0]-11, aPar->sTCPVals[1]); 
      }
      aPar->sTCPResult = TRUE;
    }
}

void TCHMemClear(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount == 0) {
    *gMsgCounter = 0;
    MyCircLBuf_Clear(gCBuffer);
    aPar->sTCPResult = TRUE;
  }
}

//------------------------------------------------------------------------------
const char ENDS_TBL[16] = {2,LF,CR, 1,DP, 2,' ','>', 1,CR, 1,LF, 1,'>', 1,0x14};

void TCHWlTest(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  //int lRes, lRxSize, lRxCount;
  //char lRxBuf[34], lRxFlags=0x07;
  //MY_CBUF_TypeDef *lSrc;
  ESP_LINK_TypeDef *lLink;
  //
  if (aPar->sTCPCount > 1) {
    switch (aPar->sTCPVals[0]) {
      case 0: lLink = gWLinkA; break;
      case 1: lLink = gWLinkB; break;
      case 2: lLink = gWLinkC; break;
      case 3: lLink = gWLinkD; break;
      case 4: lLink = gWLinkE; break;
      default: lLink = NULL;
    }
    if (assigned(lLink) && TEST_OK(lLink->sLFlags, ELF_ACTIVE)) {
      MyCBuf_Write(lLink->sLTx, aPar->sTCPPars[1], aPar->sTCPVals[1]);
      //PostLogMsg("send> ", aPar->sTCPPars[1], aPar->sTCPVals[1], S_CRLF);
    }
    else PostLogText("Link no Active !!!"S_CRLF);
  }
  else if (aPar->sTCPCount == 1) {
    if (aPar->sTCPVals[0] == 1) INCLUDE(gESP->sESFlags, ESF_DEBUG);
    else EXCLUDE(gESP->sESFlags, ESF_DEBUG);
  }
  /*
  lSrc = gUART_ESP->sRx;
  PostLogInt("Add=", lSrc->sCAddPos, ", ");
  PostLogInt("Del=", lSrc->sCDelPos, ", ");
  PostLogInt("Chk=", lSrc->sCChkPos, ", ");
  PostLogInt("Bsz=", lSrc->sCBufSize, S_CRLF); */
  /*
  if (aPar->sTCPCount > 0) {
      PostLogInt("Add=", gCBuf->sCAddPos, ", ");
      PostLogInt("Del=", gCBuf->sCDelPos, ", "); sCChkPos
      PostLogInt("Bsz=", gCBuf->sCBufSize, S_CRLF);
    //
    if (aPar->sTCPVals[0] > 0) {
      CharChange(aPar->sTCPPars[0], aPar->sTCPVals[0], '\\', CR);
      CharChange(aPar->sTCPPars[0], aPar->sTCPVals[0], '|', LF);
      lRes = MyCBuf_Write(gCBuf, aPar->sTCPPars[0], aPar->sTCPVals[0]);
      PostLogInt("    try write:", aPar->sTCPVals[0], ",  ");
      PostLogInt("...was written:", lRes, S_CRLF);
    }
    else {
      lRxSize = sizeof(lRxBuf);
      if ((aPar->sTCPCount > 1) && (lRxSize > aPar->sTCPVals[1])) lRxSize = aPar->sTCPVals[1];
      if (aPar->sTCPCount > 2) lRxFlags = aPar->sTCPVals[2] & 0x07;
      lRes = MyCBuf_ReadFmt(gCBuf, lRxBuf, lRxSize, ENDS_TBL, lRxFlags, 0, &lRxCount);
      PostLogInt("    read fmt:", lRes, "");
      if (lRes != 0) {
        PostLogInt(",  cou:", lRxCount, ",  ");
        PostLogMessage(lRxBuf, lRxCount);
      }
      PostLogText(S_CRLF);
    }
    //MyCBuf_Write(gUART_WL->sTx, aPar->sTCPPars[0], aPar->sTCPVals[0]);
	//MyCBuf_WriteText(gUART_WL->sTx, S_CRLF);
    //
    //PostLogMessage(aPar->sTCPPars[0], aPar->sTCPVals[0]);
    //PostLogText(S_CRLF);
    //
    aPar->sTCPResult = TRUE;
  }*/
}

//------------------------------------------------------------------------------

void ReceiveSMSHandler(void *aNumber,char *aMessage,int32_t aLength)
{ /* 
  if (FindShortStr(aNumber, TOEND, CTL_PHONES, NULL, NULL)) {
    PostLogText("Control Number!"S_CRLF);
    TrimRight(aMessage, &aLength);
    if (aLength == 0)
      PostLogText("Empty message"S_CRLF);
//    else
//      ExecTextCommands(aMessage, aLength);
  }
  //else
  //  PostLogText("Unknown Number!"S_CRLF);
  //
  gHWMain.sLastInSMSTicks = TICKS;
  AddNewMessage(aNumber, aMessage, aLength); */
}

void InCallHandler(char *aNumber,int32_t aState)
{ /*
  if (FindShortStr(aNumber, TOEND, CTL_PHONES, NULL, NULL)) {
    if (aState) PostLogText("Control Number!"S_CRLF);
  }
  if (aState) {
    gHWMain.sLastInCallTicks = TICKS;
    AddNewMessage(aNumber, S_ICQ"CALL"S_ICQ, TOEND);
  }*/
}

void _SMSHandler(int32_t aID,int32_t aState)
{
  char lStr[12];
  if (aState >= 0) Int2StrZ(aState, lStr, 10);
  else AssignStr("ERROR", lStr);
  MsgSendAns((aState >= 0), aID, lStr);
}

void SMSNotifyHandler(int32_t aState)
{
  _SMSHandler(gHWMain.sExSMSID, aState);
}

void SMSConfirmHandler(int32_t aState)
{
  _SMSHandler(CONFIRM_RESULT, aState);
}

void HWMain_Init(void)
{
  MemClear(&gHWMain, sizeof(HWMAIN_TypeDef));
  //
  gMsgCounter = BkpMemAlloc(sizeof(uint32_t));
  gCBuffer = MyCircLBuf_Init(NULL, 3080, BkpMemAlloc); //FixMemAlloc
  gCBuffer->sCLReadNoDel = TRUE;
  //
  gHWMain.sInSMSTimeOut = MSECS * ReadUIntVar(SMSIT, DEFSMSIT);
//  ReadNamedStr(PHONE, gHWMain.sSrvPhone, STDALARMSMSNUM);
  //
  IncStartCounter(FALSE);
}

void ChangeRegisterHandler(int32_t aSendState)
{
  if (aSendState) {
    gHWMain.sCRegTicks = TICKS;
    gHWMain.sLastInSMSTicks = TICKS;
  }
  else gHWMain.sCRegTicks = 0;
}

void LogHelloText(void)
{
  SYSBLOCK_TypeDef *lSysBlock;
  uint32_t lAddress, lLength;
  char lStr[8];
  uint8_t tlbHelloCmds[] = {TCE_MCUID, TCE_DID, TCE_GMODE, TCE_WMODE};
  if (WasWDGStart()) PostLogText("WDG"); else PostLogText("Normal");
  PostLogText(" start for wt401nc version ");
  if (GetSysBlock(0, &lSysBlock)) {
    Int2Hex(lSysBlock->hsbVersion, lStr, 8);
    PostLogMessage(&lStr[1], 1); PostLogMsg(".", &lStr[2], 2, "");
    PostLogMsg(" build:", &lStr[4], 3, "");
  }
  else PostLogText("0."LOW_VER);
  PostLogInt(" AllocMem: ", GetFixMemAllocSize(), ", ");
  PostLogInt("Restarts: ", GetStartCounter(FALSE), S_CRLF);
  //PostLogInt("GSM Regs: ", GetStartCounter(TRUE), S_CRLF);
  //PostLogText(Int2StrZ(GetFixMemAllocSize(), lStr, sizeof(lStr))); PostLogText(S_CRLF);
  //PostLogText("Restarts: "); PostLogText(Int2StrZ(GetStartCounter(FALSE), lStr, 5));
  //PostLogText(",  GSM Regs: "); PostLogText(Int2StrZ(GetStartCounter(TRUE), lStr, 5)); PostLogText(S_CRLF);
  if (WasInitBackup()) PostLogText("Backup memory initialized."S_CRLF);
  //
  RTCtl_OutTextCommands(gRTCTL, tlbHelloCmds, sizeof(tlbHelloCmds));
  //
  ReadCfgBlock(NULL);
  GetCfgParams(&lAddress, &lLength);
  PostLogHex("CfgAddress=", lAddress, 4, S_CRLF);
  PostLogInt("CfgLength=", lLength, S_CRLF);
  /*
  uint32_t lValue = ReadUIntVar(SAMPFREQ, DEF_SAMPFREQ);
  PostLogInt("IASampFreq=", lValue, S_CRLF);
  lValue = ReadUIntVar(IACHNUM, DEF_IACHNUM);
  PostLogInt("IAChNum=", lValue, S_CRLF);
  */
}
/*
void HWMain_ExtCommands_Process()
{
  char lRxBuf[INSRCCMDBUFSIZE+4];    // = ">>>>> xx xx xx xxxx";
  int lRxCount;
  //
  if (MyUSART_Read(gUSART_BUS1, &lRxBuf, INSRCCMDBUFSIZE, READ_WAIT_TIME, &lRxCount))
    HandleCmdBuf(lRxBuf, lRxCount);
  //
  if (WizNet_Read(gLOGSCKT, &lRxBuf, INSRCCMDBUFSIZE, READ_WAIT_TIME, &lRxCount)) {
    gHWMain.sLastLogRecvTicks = TICKS;
    HandleCmdBuf(lRxBuf, lRxCount);
  }
  if (IsTimeOutEx(TICKS, &gHWMain.sLastLogRecvTicks, LOGRCV_TIMEOUT)) {
    PostLogInt("LogDisconn: ", gLOGSCKT->sWSn_SR, S_CRLF);
    WizNet_Disconnect(gLOGSCKT);
  }
}

void HWMain_ExtCommandsProcess()
{
  char lRxBuf[INSRCCMDBUFSIZE+4];
  int lRxCount;
  //
  if (assigned(gHWMain.sCtlLink) && MyCBuf_Read(gHWMain.sCtlLink->sLRx, lRxBuf, INSRCCMDBUFSIZE, READ_WAIT_TIME, &lRxCount))
    RTCtl_ExecCommands(gRTCTL, lRxBuf, lRxCount, gHWMain.sCtlLink);
}
*/
bool ReadLink(MY_LINK_TypeDef *aLink,MY_LINK_TypeDef *aNoLinkA,MY_LINK_TypeDef *aNoLinkB)
{
  char lRxBuf[3*INBINCMDBUFSIZE+4];//[INSRCCMDBUFSIZE+4];
  int lRxCount;
  if (assigned(aLink) && (aLink != aNoLinkA) && (aLink != aNoLinkA) &&
      TEST_OK(aLink->sLFlags, LF_ACTIVE) &&
      MyCBuf_Read(aLink->sLRx, lRxBuf, INSRCCMDBUFSIZE, READ_WAIT_TIME, &lRxCount)) {
    //PostLogInt("Link A [", lRxCount, "]: ");
    //PostLogMsg("", lRxBuf, lRxCount, S_CRLF);
    RTCtl_ExecCommands(gRTCTL, lRxBuf, lRxCount, aLink);
    return TRUE;
  }
  return FALSE;
}

void HWMain_ExtCommandsProcess(void)
{
  ReadLink((MY_LINK_TypeDef*)gWLinkA, gLogLinkA, gLogLinkB);
  ReadLink((MY_LINK_TypeDef*)gWLinkB, gLogLinkA, gLogLinkB);
  ReadLink((MY_LINK_TypeDef*)gWLinkC, gLogLinkA, gLogLinkB);
  ReadLink((MY_LINK_TypeDef*)gWLinkD, gLogLinkA, gLogLinkB);
  ReadLink((MY_LINK_TypeDef*)gWLinkE, gLogLinkA, gLogLinkB);
  ReadLink(gLogLinkA, NULL, NULL);
  ReadLink(gLogLinkB, NULL, NULL);
}

void HWMain_Process(void)
{
  if ((!gHWMain.sWasLogStart) && IsTimeOut(TICKS, gHWMain.sStartTicks, START_LOG_TIMEOUT)) {
    LogHelloText();
    gHWMain.sWasLogStart = TRUE;
  }
  if ((gHWMain.sGNSSLedState == 2) && IsTimeOut(TICKS, gHWMain.sLastGNSSCoordsTicks, 4000)) LedGNSSIndicator(1);
  if ((gHWMain.sGNSSLedState == 1) && IsTimeOut(TICKS, gHWMain.sLastGNSSTimeTicks, 8000)) LedGNSSIndicator(0);
}
/*
void LogSocketConnected(HWMAIN_TypeDef *aHWMAIN,WIZ_SOCKET_TypeDef *aSocket)
{
  PostLogText("LogSocket: Connected!"S_CRLF);
  gHWMain.sLastLogRecvTicks = TICKS;
}

void LogSocketDisconnected(HWMAIN_TypeDef *aHWMAIN,WIZ_SOCKET_TypeDef *aSocket)
{
  PostLogText("LogSocket: Disconnected"S_CRLF);
}

void MsgSocketConnected(HWMAIN_TypeDef *aHWMAIN,WIZ_SOCKET_TypeDef *aSocket)
{
  PostLogText("MsgSocket: Connected!"S_CRLF);
  gHWMain.sLastMsgRecvTicks = TICKS;
}

void MsgSocketDisconnected(HWMAIN_TypeDef *aHWMAIN,WIZ_SOCKET_TypeDef *aSocket)
{
  PostLogText("MsgSocket: Disconnected"S_CRLF);
}
*/
void StdLinkConnected(HWMAIN_TypeDef *aHWMAIN,ESP_LINK_TypeDef *aLink)
{
  PostLogInt("Link [", aLink->sELID, "]: Connected!"S_CRLF);
  //gHWMain.sLastMsgRecvTicks = TICKS;
}

void StdLinkDisconnected(HWMAIN_TypeDef *aHWMAIN,ESP_LINK_TypeDef *aLink)
{
  PostLogInt("Link [", aLink->sELID, "]: Disconnected!"S_CRLF);
}

void LogLinkDisconnect(void *aHWMAIN,uint32_t aDiscon)
{
  if (aDiscon) gLogLinkA = NULL;
  else gLogLinkA = (MY_LINK_TypeDef*)gUART_BUS;
}

bool HandleInMessage(char *aSource,int aSCount,char **aDest,int *aDCount,bool *aHasNotify)
{
  int lMsgPos, lCRPos;
  lMsgPos = CharPosEx(MSGNTFCHAR, aSource, aSCount, TRUE);
  *aHasNotify = (lMsgPos != 0);
  if (lMsgPos >= 0) {
    lCRPos = CharPos(CR, &aSource[lMsgPos], aSCount-lMsgPos);
    if (lCRPos > 0) {
      *aDest = &aSource[lMsgPos]; lCRPos += lMsgPos+1;
      *aDCount = lCRPos-1;
      if (!*aHasNotify)
        *aHasNotify = (CharPosEx(MSGNTFCHAR, &aSource[lCRPos], aSCount-lCRPos, TRUE) != 0);
      return TRUE; 
    }
  }
  return FALSE;
}

bool GetCmmParam(char *aStr,int32_t aLen,int32_t *aSearchPos,
  char **aParPtr,int32_t *aParLen)
{
  int32_t lPos;
  bool lResult;
  lResult = GetInParamEx(COMMA, aStr, aLen, aSearchPos, &lPos, aParLen);
  *aParPtr = &aStr[lPos];
  return lResult;
}
/*
void HandleSMSSendQuery(char *aMsg,int aCount)
{
  int lID, lFlg, lLen, lTextLen, lTextBufSize, lFrom=0;
  char *lTextBuf, *lNumber, *lText;
  //
  if (GetCmmParam(aMsg, aCount, &lFrom, &lText, &lLen)) {
    lID = Str2IntDef(lText, lLen, ERROR_RESULT);
    if ((lID >= 0) && GetCmmParam(aMsg, aCount, &lFrom, &lNumber, &lLen) &&
        GetCmmParam(aMsg, aCount, &lFrom, &lText, &lTextLen)) {
      lNumber[lLen] = 0;
      if (GetCmmParam(aMsg, aCount, &lFrom, &lTextBuf, &lLen))
        lFlg = Str2IntDef(lTextBuf, lLen, 0);
      else lFlg = 0;
      if (SimCom_GetSMSBuffer(gSIMCOM, &lTextBuf, &lTextBufSize)) {
        if (lTextLen > lTextBufSize) lTextLen = lTextBufSize;
        MemCopy(lText, lTextBuf, lTextLen);
        if (SimCom_PostSMSBuffer(gSIMCOM, lNumber, lTextLen, lFlg)) {
          MsgSendAns(TRUE, lID, NULL);
          gHWMain.sExSMSID = lID;
          return;
        }
      }
    }
    MsgSendAns(FALSE, lID, NULL);
  }
}
*/
/*
void MsgManager_Process(void)
{
  char lRxStr[200], *lMsgBody; //+NETOUT=@09FGFFHFHGFHGFFGHF87654321@*
  int lReadCount, lMsgCount;
  uint16_t lRxCount;
  bool lHasNotify;
  //
  if (gSIMCOM->sSCRegister && (gHWMain.sInSMSTimeOut > 0) &&
      IsTimeOut(TICKS, gHWMain.sLastInSMSTicks, gHWMain.sInSMSTimeOut)) {
    PostLogText("Soft SIMPowerOff"S_CRLF);
    SIMOff();
  }
  //
  if (assigned(gMSGSCKT) && IsTimeOutEx(TICKS, &gHWMain.sLastMsgRecvTicks, MSGRCV_TIMEOUT)) {
    PostLogInt("MsgDisconn: ", gMSGSCKT->sWSn_SR, S_CRLF);
    WizNet_Disconnect(gMSGSCKT);
  }
  else
    if (!MyCircLBuf_IsEmpty(gCBuffer) && (gHWMain.sMsgNotified ||
        IsTimeOut(TICKS, gHWMain.sLastMsgSendTicks, MSGMNG_TIMEOUT)) &&
        MyCircLBuf_Read(gCBuffer, lRxStr, sizeof(lRxStr), &lRxCount) )
      Msg_Send(lRxStr, lRxCount, TRUE);
    else
      if (IsTimeOut(TICKS, gHWMain.sLastMsgSendTicks, MSGIDL_TIMEOUT))
        Msg_Send(S_CR, 1, TRUE);
  //
  if (assigned(gMSGSCKT) && WizNet_Read(gMSGSCKT, lRxStr, sizeof(lRxStr), READ_WAIT_TIME, &lReadCount)) {
    gHWMain.sLastMsgRecvTicks = TICKS;
    if (HandleInMessage(lRxStr, lReadCount, &lMsgBody, &lMsgCount, &lHasNotify)) {
      HandleSMSSendQuery(lMsgBody, lMsgCount);
    }
    if (lHasNotify) { //(lRxStr[0] == MSGNTFCHAR) {
      gHWMain.sMsgNotified = TRUE;
      MyCircLBuf_Delete(gCBuffer);
    }
  }
  if ((gHWMain.sLastInCallTicks != 0) && IsTimeOut(TICKS, gHWMain.sLastInCallTicks, INCALL_TIMEOUT)) {
    gHWMain.sLastInCallTicks = 0;
    SimCom_PostCommand(gSIMCOM, SCCMD_HANGUP);
  }
}
*/
void BUSTest_Process(void)
{
  char lRxStr[260];
  int lReadCount;
  //
  if (MyCBuf_Read(gUART_GNSS->sLRx, lRxStr, 256, 10, &lReadCount)) {
    PostLogMsg("BUS test: ", lRxStr, lReadCount, S_CRLF);
  }
}

void TCHBusTest(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount >= 1) {
    // _PostLogMessage(aPar->sTCPPars[1], aPar->sTCPVals[1], (MY_LINK_TypeDef*)gWLinkB);
    MyCBuf_Write(gUART_GNSS->sLTx, aPar->sTCPPars[1], aPar->sTCPVals[1]);
    aPar->sTCPResult = TRUE;
  }
}

void TCHWiFiBaudRate(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  if (aPar->sTCPCount == 1) {
    PostLogInt("Baud rate setup: ", aPar->sTCPVals[0], S_CRLF);  
    WriteUIntVar(WNETBR, aPar->sTCPVals[0], DEF_WNETBR);  
    //
    aPar->sTCPResult = TRUE;
  }
  else if (aPar->sTCPCount == 2) {
      PostLogInt("Power setup: ", aPar->sTCPVals[0], ",");
      PostLogInt("", aPar->sTCPVals[1], S_CRLF);
    /*  
    switch (aPar->sTCPVals[0]) {
        case 4: PIN_Set(&C4V_PWR, aPar->sTCPVals[1]);
          break;
        case 7: PIN_Set(&ISADC_PWR, aPar->sTCPVals[1]); 
          break;
          //
        case 11: PIN_Set(&GNSS_PWR, !aPar->sTCPVals[1]); 
          break;
        case 12: PIN_Set(&RS485_PWR, !aPar->sTCPVals[1]); 
          break;
        case 13: PIN_Set(&SIM_PWR, !aPar->sTCPVals[1]); 
          break;
        case 14: PIN_Set(&CAN_PWR, !aPar->sTCPVals[1]); 
          break;
        case 15: PIN_Set(&DISP_PWR, !aPar->sTCPVals[1]); 
          break;
    } */
    aPar->sTCPResult = TRUE;
  }    
}

void TCHTimeStamp(void *aContext,TXTCMD_PAR_TypeDef *aPar)
{
  UINT64_U lTStamp;
  if (aPar->sTCPCount == 0) {
    GetTimeStamp(&lTStamp);
    PostLogHexStr("Time Stamp: ", (char*)lTStamp.Bytes, sizeof(lTStamp), "\r\n");
    aPar->sTCPResult = TRUE;
  } else
  if ((aPar->sTCPCount == 1) && (aPar->sTCPVals[0] == 0)) {
      PostLogText("RTC_Initialize\r\n");
      //RTC_Initialize();
  } else
  if ((aPar->sTCPCount == 2) && (aPar->sTCPVals[0] == 1)) {  
      PostLogHexStr("Set Time: ", (char*)&aPar->sTCPVals[1], 4, "\r\n");
      RTC_SetTime(RTC_Format_BCD, (RTC_TimeTypeDef*)&aPar->sTCPVals[1]);  
  } else
  if ((aPar->sTCPCount == 2) && (aPar->sTCPVals[0] == 2)) {  
      PostLogHexStr("Set Date: ", (char*)&aPar->sTCPVals[1], 4, "\r\n");
      RTC_SetDate(RTC_Format_BCD, (RTC_DateTypeDef*)&aPar->sTCPVals[1]);  
  }  
    
}

void SetTimeStamp(void* aContext,UINT64_U* aTimeStamp)
{
    //PostLogHexStr("SetTimeStamp=", (char*)&aTimeStamp->LLong, 8, S_CRLF);
//    ISADC_SetTimeStamp(gISADC, aTimeStamp);
    gHWMain.sLastGNSSTimeTicks = TICKS;
    if (gHWMain.sGNSSLedState < 1) LedGNSSIndicator(1);
}

void SetCoords(void* aContext,UINT64_U* aCoords)
{
    //PostLogInt("SetCoords=", aCoords->Ints[0], ",");
    //PostLogInt("", aCoords->Ints[1], "\r\n");
//    ISADC_SetCoords(gISADC, aCoords);
    gHWMain.sLastGNSSCoordsTicks = TICKS;
    if (gHWMain.sGNSSLedState < 2) LedGNSSIndicator(2);
}

void WT401nc_Init(void)
{
  MySys_Init();
  //
  gRTCTL = RTCtl_Init(NULL);
  gRTCTL->sRCAppCtxTable = APPCTX_TABLE;
  gRTCTL->sRCTextCmdNames = TXTCMD_NAMES;
  gRTCTL->sRCTextCmdHandlers = TXTCMD_HANDLERS;
  gRTCTL->sRCBinCmdNames = BINCMD_NAMES;
  gRTCTL->sRCBinCmdHandlers = BINCMD_HANDLERS;
  gRTCTL->sRCOnBCTextAnswer = PostBCTextAnswer;
  //
  HWMain_Init();
  //
  // ���������� ����������� Log-���������
  AssignPostLogMessage(StdPostLogMessage);
  //
  // ������������� ���������� ������������
  gLED2 = MyOUT_Init(NULL, &LD_2); // right
  LedGNSSIndicator(0);
  //
  // ������������� ���� 1
  gUART_BUS = MyUART_InitEx(NULL, &UART_BUS, 921600);
  //
  // ������������� ���� 2
//  gUART_GNSS = MyUART_InitEx(NULL, &UART_GNSS, 115200); //9600); 
  //
  // ������������� ������ ESP32
  //
  gUART_ESP = MyUART_InitEx(NULL, &UART_ESP, ReadUIntVar(WNETBR, DEF_WNETBR)); //115200);
  //
  gESP = ESP_Init(NULL, gUART_ESP, &ESP_ENB, TRUE, &ESP_PRG);
  ESP_AssignTransLink(gESP, (MY_LINK_TypeDef*)gUART_BUS);
  MHandler_Setup(&gESP->sESOnTransLink, (PHANDLER)LogLinkDisconnect, &gHWMain);
  gESP->sESDbgCBuf = gUART_BUS->sLTx;
  //
  gWLinkA = ESP_AddLink(gESP, 512, 512, ELM_TCPS);
  MHandler_Setup(&gWLinkA->sELOnConnected, (PHANDLER)StdLinkConnected, &gHWMain);
  MHandler_Setup(&gWLinkA->sELOnDisconnected, (PHANDLER)StdLinkDisconnected, &gHWMain);
  //
  gLogLinkA = (MY_LINK_TypeDef*)gUART_BUS;
  gLogLinkB = (MY_LINK_TypeDef*)gWLinkA; //gLogLinkB = NULL;
  /*
  PIN_Config(&C4V_PWR);
  PIN_Set(&C4V_PWR, TRUE);    
  //
  PIN_Config(&GNSS_PWR);
  PIN_Set(&GNSS_PWR, FALSE); 
  //
  PIN_Config(&RS485_PWR);
  PIN_Set(&RS485_PWR, TRUE); 
  //
  PIN_Config(&SIM_PWR);
  PIN_Set(&SIM_PWR, TRUE); 
  //
  PIN_Config(&CAN_PWR);
  PIN_Set(&CAN_PWR, TRUE); 
  //
  PIN_Config(&DISP_PWR);
  PIN_Set(&DISP_PWR, TRUE); 
  //
  PIN_Config(&ISADC_PWR);
  PIN_Set(&ISADC_PWR, FALSE);  
  //
  gISADC = ISADC_Init(NULL, &SPI_ISADC, 
      &ISADC_PWR, &ISADC_EN, &ISADC_MCLK, &ISADC_DRDY, &GNSS_PPS, &BTN_B, &BTN_A,
      &ISADC_SCLK, &LED_A);
  //  
  gSDIO = SDC_Init(NULL, &SDCARD_PWR, &SDCARD_DET, &LED_C);
  //
  MHandler_Setup(&gISADC->sIAOnSigSave, (PHANDLER)SDC_SaveBlocks, gSDIO);
  */
}

void WT401nc_Process(void)
{
    HWMain_Process();
    //
    // ������� �����������
    MyPS_Process(gLED2);
    
//    ISADC_Process(gISADC);
    //
//    SDC_Process(gSDIO);
    //
    ESP_Process(gESP);
    //
    HWMain_ExtCommandsProcess();
    //
    IWDG_ReloadCounter();  
}
