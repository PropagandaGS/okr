// CPU main, 2019
//

#include "wt401nc_main.h"

void main(void)
{
    WT401nc_Init();
    gRTC.init();
    //gGNSS.init(gUART_GNSS, &GNSS_RST);
    //MHandler_Setup(&gGNSS.onTimeStamp, (PHANDLER)SetTimeStamp, NULL);
    //MHandler_Setup(&gGNSS.onCoords, (PHANDLER)SetCoords, NULL);
    //
    while(true) {
        WT401nc_Process();
        //
        gRTC.process();
        //gGNSS.process();
    }
}
